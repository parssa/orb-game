﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VariableJoystick : Joystick
{
    public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }

    [SerializeField] private float moveThreshold = 1;
    [SerializeField] private JoystickType joystickType = JoystickType.Fixed;

    private Vector2 fixedPosition = Vector2.zero;

    public bool isHeldDown = false;
    public float lastReleasedTime = 0.0f;

    public void SetMode(JoystickType joystickType)
    {
        this.joystickType = joystickType;
        if(joystickType == JoystickType.Fixed)
        {
            background.anchoredPosition = fixedPosition;
            //background.gameObject.SetActive(true);
        }
        // else
            //background.gameObject.SetActive(false);
        //background.gameObject.SetActive(true);
    }

    Vector2 originalPos;
    private Vector2 posBeforeTouch;



    protected override void Start()
    {
        base.Start();
        fixedPosition = background.anchoredPosition;
        SetMode(joystickType);
        originalPos = background.anchoredPosition;
        posBeforeTouch = originalPos;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if(joystickType == JoystickType.Dynamic)
        {
            background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
            //background.gameObject.SetActive(true);
        }

        if (PauseMenu.AdjustingControls)
        {
            background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
            posBeforeTouch = background.anchoredPosition;
        }

        startingHoldingAtTime = Time.time;
        releasedAtTime = 0f;
        
        base.OnPointerDown(eventData);
        isHeldDown = true;
    }

    public Vector2 releasedDirection;

    public override void OnPointerUp(PointerEventData eventData)
    {
       // if(joystickType == JoystickType.Floating)
          //  background.gameObject.SetActive(false);
        
        isHeldDown = false;
        background.anchoredPosition = posBeforeTouch;
        lastReleasedTime = Time.time;
        releasedDirection = Direction;
       // Debug.Log(releasedDirection);
        base.OnPointerUp(eventData);
        
        releasedAtTime = Time.time;
       
    }

    protected override void HandleInput(float magnitude, Vector2 normalised, Vector2 radius, Camera cam)
    {
        if (joystickType == JoystickType.Dynamic && magnitude > moveThreshold)
        {
            Vector2 difference = normalised * (magnitude - moveThreshold) * radius;
            background.anchoredPosition += difference;
        }


        
        
        
        
        
        
        base.HandleInput(magnitude, normalised, radius, cam);
    }

    // private bool CanVibrateAgain = true;
    private float lastPlayedVibration;
    private float vibrationCooldownTime = 1f;
    private float startingHoldingAtTime;
    private float releasedAtTime;
    public float DurationHeld()
    {

        return Time.time - startingHoldingAtTime;
    }

    public enum CardinalDir
    {
        MID,
        UP,
        LEFT,
        RIGHT,
        DOWN,
    }

    public CardinalDir lastCardinalDir;    
    

    public bool CanPlayVibration(float x, float y)
    {

      
        bool atCardinalDir = Mathf.Abs(x) >= 0.9f || Mathf.Abs(y) >= 0.9f;
        // bool atCardinalDir = false;
        CardinalDir thisCardinalDir = lastCardinalDir;
        if (atCardinalDir)
        {
            
            if (x >= 0.9f)
            {
                thisCardinalDir = CardinalDir.RIGHT;
            } else if (x <= -0.9f)
            {
                thisCardinalDir = CardinalDir.LEFT;
            } else if (y >= 0.9f)
            {
                thisCardinalDir = CardinalDir.UP;
            } else if (y <= -0.9f)
            {
                thisCardinalDir = CardinalDir.DOWN;
            }
                            
            
            // Debug.Log($"Hit cardinal! {thisCardinalDir}");
                            
        }


        atCardinalDir = false;
        
        float mag = new Vector2(x, y).normalized.magnitude;
        
        
        
        
        if (mag <= DeadZone || atCardinalDir)
        {
            
            if (isHeldDown && DurationHeld() > 0.2f && Time.time - lastPlayedVibration >= vibrationCooldownTime) 
            {
                if (atCardinalDir)
                {
                    if (lastCardinalDir == thisCardinalDir)
                    {

                        return false;
                    }
                    else
                    {
                        lastCardinalDir = thisCardinalDir;
                    }
                }
                        
                lastPlayedVibration = Time.time;
                return true;
                
            }
        }
        return false;
    }

    public void SwitchThreshold(float amount)
    {
        moveThreshold = amount;
    }

    public void ResetPosition()
    {
        background.anchoredPosition = originalPos;
        posBeforeTouch = background.anchoredPosition;
    }

    // public void AdjustmentMode()
    // {
       
    // }
}

public enum JoystickType { Fixed, Floating, Dynamic }