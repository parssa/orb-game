﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPlayerCursor : MonoBehaviour
{

    private CircleCollider2D circleCollider;

    private Camera cam;
    
    
    // Start is called before the first frame update
    void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.isMobilePlatform)
        {
            if (Input.touchCount > 0)
            {
                Touch thisTouch = Input.GetTouch(0);
                transform.position = thisTouch.position;
                circleCollider.enabled = true;
            }
            else circleCollider.enabled = false;
        }
        else
        {
            Vector2 mousePosition = new Vector2(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y);
            transform.position = mousePosition;
        }
    }
    
    
}
