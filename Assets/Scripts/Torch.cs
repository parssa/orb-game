﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Torch : MonoBehaviour
{
    public bool activated = false;
    private bool onActivateRan = false;
    public Gradient notActiveGradient;
    public Gradient activatedGradient1;
    public Gradient activatedGradient2;

    public Color activeLightColor;
    public Color notActiveLightColor;

    private Gradient thisGradient;

    private ParticleSystem pSystem;
    private Light2D thisLight;


    // Start is called before the first frame update
    void Start()
    {
        pSystem = transform.GetChild(0).GetComponent<ParticleSystem>();
        thisLight = transform.GetChild(1).GetComponent<Light2D>();

        var main = pSystem.main;
        main.startColor = new ParticleSystem.MinMaxGradient(notActiveGradient);
        thisGradient = notActiveGradient;

        thisLight.color = notActiveLightColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated && !onActivateRan)
        {
            OnActivate();
        }
    }

    void OnActivate()
    {
        activated = true;
        
        // Debug.Log("Activated!");
         var main = pSystem.main;
         main.startColor = new ParticleSystem.MinMaxGradient(activatedGradient1, activatedGradient2);
         thisLight.color = activeLightColor;

        onActivateRan = true;
        
    }
}
