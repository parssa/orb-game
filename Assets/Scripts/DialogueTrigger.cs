﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;
	private DialogueManager dm;

	private void Start()
	{
		dm = DialogueManager.instance;
	}

	public void TriggerDialogue () {
		
		if (dialogue.isPartOfWeaponChoice)
		{
			if (CharacterMenu.instance.showCover)
			{
				// FindObjectOfType<DialogueManager> ().StartDialogue (dialogue);
				dm.StartDialogue(dialogue);
			}
		}
		else
		{
			// FindObjectOfType<DialogueManager> ().StartDialogue (dialogue);
			dm.StartDialogue(dialogue);
		}
		
		//FindObjectOfType<DialogueManager> ().StartDialogue (dialogue);

		

	}
}
