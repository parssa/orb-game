﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    /*
    Detect when collided with player's bow during a dash, react to being rammed into
        - Particle Effectt
        - Sound Effect
        - Movement
    */



    // -- COLLISION LOGISTICS --

    // Conditions to allow detection
    private Player player;
    private Camera cam;
    private bool canDetectCollision = false;
    private bool collisionDetected = false;
    private float lastDetectTime = 0.0f;
    private const float INVTERVAL_DETECT = 0.2f;

    // Colliders
    private BoxCollider2D hitbox;
    private Collider2D[] hits = new Collider2D[5];
    public ContactFilter2D filter;

    // -- EFFECTS -- 

    // Camera Shake
    private CameraMotor cameraShake;

    // Audio
    private AudioSource audioSource;
    private AudioClip impact;

    // Particle System
#pragma warning disable 0649
    [SerializeField] private ParticleSystem foliageParticles;  
    [SerializeField] private ParticleSystemRenderer foliageParticleRenderer;
    [SerializeField] private SpriteRenderer foliage;
#pragma warning restore 0649

    // Trees animation
    private Animator treeAnim;



    // Start is called before the first frame update
    void Start()
    {
        player = Player.instance;
        cam = Camera.main;
        
        hitbox = GetComponent<BoxCollider2D>();
        cameraShake = cam.GetComponent<CameraMotor>();

        // TODO CHANGE THIS TO TREE HIT SOUND
        audioSource = GetComponent<AudioSource>();
        audioSource.Pause();
        impact = SoundManager.instance.GetSounds("ArrowHit").clip;


        treeAnim = transform.parent.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        canDetectCollision = player.isDashing;
        hitbox.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {

            if (hits[i] == null)
                continue;
            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;

        }

        if (collisionDetected) 
        {
            StartCoroutine(cameraShake.Shake(0.1f, 7f));
            if (Random.Range(0, 11) > 5)
            {
                SoundManager.instance.Play("DashImpactOne", 0);
            }
            else
            {
                SoundManager.instance.Play("DashImpactTwo", 0);
            }

            collisionDetected = false;
        }
        foliageParticleRenderer.sortingLayerName = foliage.sortingLayerName;
            foliageParticleRenderer.sortingOrder = foliage.sortingOrder - 1;

    }

    protected virtual void OnCollide(Collider2D coll)
    {
        if (canDetectCollision)
        {
            if (coll.name == "weapon")
            {
                OneShotDetect();
            }
        }
    }

    void OneShotDetect()
    {
        if (Time.time - lastDetectTime >= INVTERVAL_DETECT)
        {
            lastDetectTime = Time.time;
           // player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 100;
           // Debug.Log((player.transform.position - gameObject.transform.GetChild(0).position).normalized / 100);
            collisionDetected = true;

            treeAnim.SetTrigger("gotHit");
           
            
            foliageParticles.Play();
            

        }
    }


}
