﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamagePopup : MonoBehaviour
{
    public const string DAMAGE = "typeDamage";
    public const string SOULS = "typeSouls";
    public const string POINTS = "typePoints";
    public const string STAMINA = "typeStamina";
    public const string HEALTH = "typeHealth";
    public const string ORBPICK = "typeOrb";
    public const string SPECIALT = "typeSpecial";
    public const string SCOREGATE = "typeGateEntered";
    public const string DEVMODE = "typeDev";
    public const string BOSSDAMAGE = "typeDamageBoss";
    public const string DUALITY = "typeDuality";

    private TextMeshPro textMesh;
    private float disappearTimer;
    private Color textColor;

    // == FONT SIZES ==
    private float normalFontSize;
    private float critFontSize;
    private float soulsFontSize;
    private float scoreFontSize;
    private float powerupFontSize;
    private float scoreGateFontSize;

    // == DISSAPEAR TIMES ==
    private float shorterDisappearTime;
    private float regularDisappearTime;
    private float longerDisappearTime;
    

    // == COLORS == 
    // Damage
    private Color damageColor;
    private Color critColor;

    // Misc
    private Color plusSoulsColor;
    private Color minusSoulsColor;
    private Color pointColor;

    // Powerups
    private Color staminaColor;
    private Color healthColor;
    private Color orbPickupColor;
    private Color dualityColor;

    public static DamagePopup Create(Vector2 position, int damageAmount, bool isCrit, string type)
    {
        Vector2 adjustedPosition = new Vector2(position.x, position.y);

        if ((type == STAMINA) || (type == HEALTH) || (type == ORBPICK) || (type == DEVMODE) || (type == DUALITY))
        {
            adjustedPosition = new Vector2(position.x, position.y + 1f);
        } else if (type == SCOREGATE)
        {
            adjustedPosition = new Vector2(position.x, position.y + 0.1f);
        }
        else
        {
            float xRand = Random.Range(0f, 1f);
            float yRand = Random.Range(0f, 1f);

            adjustedPosition = new Vector2(position.x + xRand, position.y + yRand);
        }
        

        Transform damagePopupTransform = Instantiate(GameManager.instance.damagePopupText, adjustedPosition, Quaternion.identity);
        DamagePopup damagePopup = damagePopupTransform.GetComponent<DamagePopup>();
        damagePopup.Setup(type, isCrit, damageAmount);
        
        return damagePopup;
    }


    private void Awake()
    {
        // References
        textMesh = transform.GetComponent<TextMeshPro>();
        textColor = textMesh.color;

        // Font Sizes
        normalFontSize = 4f;
        critFontSize = 4.5f;
        soulsFontSize = 2f;
        scoreFontSize = 2f;
        powerupFontSize = 5f;
        scoreGateFontSize = 6f;

        // Times
        shorterDisappearTime = 0.2f;
        regularDisappearTime = 0.3f;
        longerDisappearTime = 0.4f;

        // Colors
        damageColor = new Color32(200, 118, 0, 255);
        critColor = Color.red;

        plusSoulsColor = new Color32(0, 120, 200, 255);
        minusSoulsColor = new Color32(30, 90, 200, 255);
        pointColor = Color.white;

        staminaColor = new Color32(255, 255, 153, 255);
        healthColor = new Color32(255, 153, 153, 255);
        orbPickupColor = new Color32(204, 153, 255, 255);
        dualityColor = new Color32(255, 128, 255, 255);

    }


    public void Setup(string type, bool critHit, int damageAmount)
    {
        if (type == DAMAGE)
        {
            textMesh.SetText(damageAmount.ToString());
            if (!critHit)
            {
                textMesh.fontSize = normalFontSize;
                textColor = damageColor;
            }
            else
            {
                textMesh.fontSize = critFontSize;
                textColor = critColor;
            }
        }

        if (type == BOSSDAMAGE)
        {
            textMesh.SetText(damageAmount.ToString());
            if (!critHit)
            {
                textMesh.fontSize = powerupFontSize;
                textColor = Color.red;
            }
            else
            {
                textMesh.fontSize = powerupFontSize + 0.4f;
                textColor = Color.red;
            }
        }

        if (type == SOULS)
        {
            textMesh.fontSize = soulsFontSize;
            disappearTimer = shorterDisappearTime;
            if (critHit)
            {
                textMesh.SetText("+ " + damageAmount.ToString());
                textColor = plusSoulsColor;
            } else
            {
                textMesh.SetText("- " + damageAmount.ToString());
                textColor = minusSoulsColor;
            }
            
        }

        if (type == POINTS)
        {
            textMesh.fontSize = scoreFontSize;
            disappearTimer = shorterDisappearTime;
            textMesh.SetText(damageAmount.ToString() + "      pts");
            textColor = pointColor;
        }

        if (type == STAMINA)
        {
            disappearTimer = 0.2f;
            textMesh.fontSize = powerupFontSize;
            textMesh.SetText("STAMINA      UP");
            textColor = staminaColor;
        }

        if (type == HEALTH)
        {
            disappearTimer = 0.2f;
            textMesh.fontSize = powerupFontSize;
            textMesh.SetText("HEALTH      UP");
            textColor = healthColor;
        }

        if (type == DUALITY)
        {
            disappearTimer = 0.2f;
            textMesh.fontSize = powerupFontSize;
            textMesh.SetText("DUALITY      UP");
            textColor = dualityColor;
        }

        if (type == ORBPICK)
        {
            disappearTimer = 0.2f;
            textMesh.fontSize = powerupFontSize;
            textMesh.SetText("ORB      UP");
            textColor = orbPickupColor;
        }

        if (type == SPECIALT)
        {
            disappearTimer = longerDisappearTime;
            textMesh.fontSize = soulsFontSize;
            textMesh.SetText("+1");
            textColor = Color.red;
        }

        if (type == SCOREGATE)
        {
            disappearTimer = regularDisappearTime;
            textMesh.fontSize = scoreGateFontSize;
            textMesh.SetText(ComboManager.instance.currGate.gateName);
            textColor = holoColors[Random.Range(0, holoColors.Length)];
        }

        if (type == DEVMODE)
        {
            disappearTimer = regularDisappearTime;
            textMesh.fontSize = scoreGateFontSize;
            textMesh.SetText("Developer");
            textColor = Color.red;
        }
        

        textMesh.color = textColor;
        
    }

    

    private void Update()
    {
        float upSpeed = 3f;
        transform.position += new Vector3(0, upSpeed)* Time.deltaTime;

        disappearTimer -= Time.deltaTime;
        if (disappearTimer < 0)
        {

            float disappearSpeed = 5f;
            if (textMesh.fontSize == scoreGateFontSize)
            {
                Debug.Log("Poweruptex");
                float alphaPreConv = textColor.a;
                textColor = holoColors[Random.Range(0, holoColors.Length)];
                textColor.a = alphaPreConv;
                disappearSpeed = 2f;
            }
            
            textColor.a -= disappearSpeed * Time.deltaTime;
            textMesh.color = textColor;
            if (textColor.a < 0)
            {
                Destroy(gameObject);
            }
            // TOD

        }


    }

    private Color holoColor = Color.white;

    private Color[] holoColors =
    {
        new Color32(191, 192, 238, 255), // dominant
        new Color32(191, 237, 240, 255), // light blue
        new Color32(238, 187, 243, 255), // pink
        new Color32(148, 227, 243, 255), // bright blue
        
    };

}
