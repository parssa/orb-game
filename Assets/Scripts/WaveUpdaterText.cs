﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WaveUpdaterText : MonoBehaviour
{

    public static WaveUpdaterText instance;
    void Awake()
    {
        instance = this;
    }

    private WaveSpawner waveSpawner;

    private TextMeshProUGUI waveScoreText;
    private int waveNum;
    public bool hitZeroScale = false;



    // Start is called before the first frame update
    void Start()
    {

        waveScoreText = GetComponent<TextMeshProUGUI>();
        waveSpawner = WaveSpawner.instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (WaveSpawner.instance.GetCurrState() == "COUNTING")
        {
            hitZeroScale = false;
            float timeLeft = Mathf.Floor(WaveSpawner.instance.WaveCountdown * 10) / 10;
            if (timeLeft == 5)
            {
                waveScoreText.text = "TIME LEFT: " + "5.0";
            }
            else if (timeLeft == 4)
            {
                waveScoreText.text = "TIME LEFT: " + "4.0";
            }
            else if (timeLeft == 3)
            {
                waveScoreText.text = "TIME LEFT: " + "3.0";
            }
            else if (timeLeft == 2)
            {
                waveScoreText.text = "TIME LEFT: " + "2.0";
            }
            else if (timeLeft == 1)
            {
                waveScoreText.text = "TIME LEFT: " + "1.0";
            }
            else if (timeLeft <= 0)
            {
                waveScoreText.text = "TIME LEFT: " + "0.0";
            }
            else
            {
                waveScoreText.text = "TIME LEFT: " + timeLeft.ToString();
            }


        }
        else
        {
            waveNum = waveSpawner.waveNum + 1;
            if (waveSpawner.curr_num_enemies > 0 && waveSpawner.num_enemies_spawned_curr_wave >= waveSpawner.num_enemies_wave && waveSpawner.curr_num_enemies <= 5 && waveSpawner.waveNum > 7)
            {
                waveScoreText.text = "ENEMIES LEFT:  " + WaveSpawner.instance.curr_num_enemies.ToString();
            }
            else waveScoreText.text = "WAVE: " + waveNum.ToString();

        }



    }
}
