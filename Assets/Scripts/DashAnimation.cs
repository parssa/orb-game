﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAnimation : MonoBehaviour, IPooledObject
{
    // Start is called before the first frame update
    private float timeAlive = 0.0f;
    public float lifespan = 0.3f;
   
    public void OnObjectSpawn()
    {
        timeAlive = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeAlive > lifespan) gameObject.SetActive(false);
    }
}
