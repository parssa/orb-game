﻿using System;
using System.Collections;
using System.Collections.Generic;
using CloudOnce;
using CloudOnce.Internal;
using CloudOnce.QuickStart;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.Impl;
using Random = UnityEngine.Random;


public class MainMenuManager : MonoBehaviour
{
    public Color regularBackgroundPanelColor;
    public Color pinkBackgroundPanelColor;
    public Color optionsBackgroundPanelColor;
    public Color creditsBackgroundPanelColor;
    public Image backgroundSceneGradient;
    [Space] public AudioMixer audioMixer;
    public Animator canvasAnimator;
    public Animator backgroundEyeAnimator;
    public Animator fadeOutAnimatior;
    public Camera cam;
    [Space] public Animator playButtonAC;
    public Animator settingsAC;
    public Animator creditsAC;
    public Animator quitAC;
    public Animator allButtonsAC;
    [Space] public TextMeshProUGUI panelTitleText;
    public Image contentPanelBackground;
    public Color creditsPanelColor;
    public Color optionsPanelColor;
    public CanvasGroup creditsCanvasGroup;
    public CanvasGroup optionsCanvasGroup;
    public GameObject worldRankObj;
    public GameObject achievementsObj;
    [Space] public Slider volumeSlider;
    public AudioSource musicPlayer;
    private bool PlayMusic = true;
    public Toggle musicToggle;
    [Space] public Toggle tutorialToggle;
    public Toggle tipsToggle;
    [Space] public TextMeshProUGUI highScoreTex;
    public TextMeshProUGUI bestWaveTex;
    [Space] public TextMeshProUGUI playText;

    public GameObject quitButtonObj;
    [Space] public TextMeshProUGUI worldRankTex;
    public TextMeshProUGUI achievementsProgressTex;

    private bool formattedTextValues = false;
    // public TextMeshProUGUI debugPlayerIDTex;

    private float sceneLoadedTime = 0f;

    public enum ButtonsPos
    {
        CENTRE,
        LEFT,
        RIGHT,
    }

    public ButtonsPos buttonsCurrPos;

    public enum PanelContents
    {
        NOTHING,
        OPTIONS,
        CREDITS,
    }

    public PanelContents panelCurrContents;
    [Space] public AudioClip[] clickClips;
    public AudioClip playModePressedClip;

    private bool _succesfullyLoggedIn = false;

    private void Awake()
    {
        Debug.Log("Awake Ran");
        if (PlayerPrefs.GetFloat("sliderVolume", 10f) > 1f)
        {
            PlayerPrefs.SetFloat("sliderVolume", 1f);
        }

        QuitGamePressed = false;
    }


    void PlayRandomClickSound()
    {
        QuitGamePressed = false;
        AudioSource.PlayClipAtPoint(clickClips[Random.Range(0, clickClips.Length)], Vector3.one);
    }


    void Start()
    {
        Cloud.OnInitializeComplete += CloudOnceInitializeComplete;
        Cloud.Initialize(false, true);
        // Leaderboards.MyGame

        Vibration.Init();


        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Authentication Successful");
                _succesfullyLoggedIn = true;
                
            }
            else
            {
                _succesfullyLoggedIn = false;
                Debug.Log("Authentication Failed");
                worldRankTex.color = Color.red;
            }
        });
        
        if (PlayerPrefs.GetInt("HighScore") >= 1000)
        {
            highScoreTex.text = "00" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 100)
        {
            highScoreTex.text = "000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 10)
        {
            highScoreTex.text = "0000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else
        {
            highScoreTex.text = "00000" + PlayerPrefs.GetInt("HighScore").ToString();
        }


        bestWaveTex.SetText(PlayerPrefs.GetInt("bestWave", 1).ToString());
        
        cam = Camera.main;
        quitButtonObj.SetActive(false);
        // volumeSlider.value = PlayerPrefs.GetFloat("sliderVolume", 1f) != 1f ? PlayerPrefs.GetFloat("sliderVolume, 1") : 1f;
        ToggleMusic(PlayerPrefs.GetInt("playMusic", 1) == 1);


        StartCoroutine(PingPongBackground());


        // if (!Application.isMobilePlatform)
        if (false)
        {
            achievementsObj.SetActive(false);
            worldRankObj.SetActive(false);
            return;
        }

        
        // Social.LoadAchievements(achievements =>
        // {
        //     if (achievements.Length > 0)
        //     {
        //         Debug.Log($"Found {achievements.Length} achievements");
        //         foreach (Achievement achievement in achievements)
        //         {
        //             
        //             // if (achievement.completed)
        //             // {
        //             //     numAchievementsCompleted += 1;
        //             // }
        //             
        //             if (achievement.completed)
        //             {
        //                 numAchievementsCompleted += 1;
        //             }
        //         }
        //     }
        // });


        
    }


    public void CloudOnceInitializeComplete()
    {
        Cloud.OnInitializeComplete -= CloudOnceInitializeComplete;
        Debug.LogWarning("Initialized");
    }

    public int rankingInWorld = -1;
    // public int rankingInWorld2 = -1;
    private int numPlayersFound = 0;
    private int numTimesSearched = 0;
    private bool hitThisPlayer = false;
    // private bool hitThisPlayer2 = false;
    
    // public void TestRankID(string user, Action<int> rank)
    // {
    //     Debug.Log("testing rank other id name...");
    //     Cloud.Leaderboards.LoadScores(leaderboardID: "com.Kazakan.leaderboard",  callback: scores =>
    //     {
    //         
    //         // debugPlayerIDTex.SetText(user);
    //         Debug.Log("accessed other");
    //         if (scores.Length > 0)
    //         {
    //             Debug.Log($"Retrieved {scores.Length} scores in other");
    //
    //             // Filter the score with the username
    //             for (int i = 0; i < scores.Length; i++)
    //             {
    //                 Debug.Log("Comparing with player on board");
    //               
    //                 if (user == scores[i].userID)
    //                 {
    //                    
    //                     Debug.Log("Hit this player on other");
    //
    //                     hitThisPlayer2 = true;
    //                     rankingInWorld2 = scores[i].rank;
    //                     
    //                     rank(scores[i].rank);
    //                     break;
    //                 }
    //
    //                 
    //             }
    //             Debug.Log("Couldnt find player, returning length of leaderboard...");
    //             rank(scores.Length);
    //             
    //         }
    //         else
    //         {
    //             Debug.Log("Failed to Retrieve score on other");
    //             // bestWaveTex.color = Color.red;
    //             rank(-1);
    //         }
    //     });
    //     
    //     
    //
    //     // var leaderboard = Social.CreateLeaderboard();
    //     // leaderboard.id = "com.Kazakan.leaderboard";
    //     // leaderboard.LoadScores(success =>
    //     // {
    //     //
    //     //     var scores = leaderboard.scores;
    //     //     Debug.Log("success");
    //     //     if (scores.Length > 0)
    //     //     {
    //     //         Debug.Log($"Found {scores.Length} players");
    //     //         bestWaveTex.color = Color.green;
    //     //         for (int i = 0; i < scores.Length; i++)
    //     //         {
    //     //             if (user == scores[i].userID)
    //     //             {
    //     //                 bestWaveTex.color = Color.white;
    //     //                 Debug.Log("Hit this player");
    //     //                 rank(scores[i].rank);
    //     //                 break;
    //     //             }
    //     //             bestWaveTex.color = Color.white;
    //     //         }
    //     //         // foreach (Achievement achievement in achievements)
    //     //         // {
    //     //         //     if (achievement.completed)
    //     //         //     {
    //     //         //         numAchievementsCompleted += 1;
    //     //         //     }
    //     //         // }
    //     //     }
    //     // });
    //
    //     // Social.LoadScores(, scores =>
    //     // {
    //     //     if (scores.Length > 0)
    //     //     {
    //     //         Debug.Log($"Found {scores.Length} achievements");
    //     //         // foreach (Achievement achievement in achievements)
    //     //         // {
    //     //         //     if (achievement.completed)
    //     //         //     {
    //     //         //         numAchievementsCompleted += 1;
    //     //         //     }
    //     //         // }
    //     //     }
    //     // });
    // }
    public void GetUserRank(string user, Action<int> rank)
    {
        
        Debug.Log("Attempting Unity Version now...ww");
        // Social.LoadScores("com.Kazakan.leaderboard", scores =>
        // {
        //     if (scores.Length > 0)
        //     {
        //         Debug.Log($"Got {scores.Length} scores");
        // string myScores = "Leaderboard:\n";
        // foreach (IScore score in scores)
        // {
        //     myScores += "\t userID:" + score.userID + " leaderboardID" + score.leaderboardID + "userRank: " +
        //                 score.rank;
        // }
        //
        // Debug.Log(myScores);
        //     }
        //     else
        //     {
        //         Debug.Log("No scores Loaded");
        //     }
        // });

        var leaderboardID = "com.Kazakan.leaderboard";
        var leaderboard = Social.CreateLeaderboard();
        leaderboard.id = leaderboardID;
        leaderboard.timeScope = TimeScope.AllTime;
        leaderboard.LoadScores(success =>
        {
            var scores = leaderboard.scores;
            if (scores.Length > 0)
            {
                Debug.Log($"There are {scores.Length} scores");
                foreach (var score in scores)
                {
                    Debug.Log($"User {score.userID} has a score of {score.value} and a rank of {score.rank}");
                    if (user == score.userID)
                    {
                       
                        Debug.Log("Hit this player");
                        
                        hitThisPlayer = true;
                        // rankingInWorld = scores[i].rank;
                        
                        rankingInWorld = score.rank; 
                        rank(score.rank);
                        break; //TRY return
                    }
                }

                if (!hitThisPlayer)
                {
                    rankingInWorld = scores.Length;
                }
                
                
            } else Debug.Log("no scores found.");
        });

        


    }


    public void LoadScoresFromHighScore()
    {

        numTimesSearched += 1;
        int thisPlayerRank = -1;
        GetUserRank(Social.localUser.id, (status) => { thisPlayerRank = status; });
        
        

        Debug.Log($"{Social.localUser.userName} with ID: {Social.localUser.id} has a rank of {rankingInWorld}");
        if (hitThisPlayer)
        {
            Debug.Log("Ranking Was Found!!!!!! ------");
            Debug.Log(rankingInWorld);
        }
 
        
        worldRankTex.SetText(rankingInWorld == -1 ? "N/A" : $"{rankingInWorld}{DetermineEndPrefix(rankingInWorld)}");
    }

    // void LoadScoresFromOther() // test func
    // {
    //     int thisPlayerRank = -1;
    //     TestRankID(Social.localUser.id, (status) => { thisPlayerRank = status; });
    //     
    //     
    //     Debug.Log($"{Social.localUser.userName} 2with I2D: {Social.localUser.id} ha2s a 2rank of {thisPlayerRank}");
    //     if (hitThisPlayer2)
    //     {
    //         Debug.Log("OTHER:  Ranking Was Found!!!!!!!!!!!!!!!!! ----2--");
    //         Debug.Log(rankingInWorld2);
    //         
    //     }
    //
    //
    //    // worldRankTex.SetText(rankingInWorld == -1 ? "N/A" : $"{thisPlayerRank}{DetermineEndPrefix(thisPlayerRank)}");
    // }

    // public void LoadScoresFromHighScore()
    // {
    //     Cloud.Leaderboards.LoadScores("KazakanHighScore", delegate(IScore[] scores)
    //     {
    //
    //         int thisPlayerRank = -1;
    //         
    //         foreach (IScore _s in scores)
    //         {
    //             Debug.Log($"score is {_s.userID}'s");
    //             // if (_s.)
    //             
    //             // _s.rank    
    //         }
    //
    //         if (PlayerPrefs.GetInt("HighScore") > 0)
    //         {
    //             
    //             worldRankTex.SetText("N/A");
    //         }
    //         else
    //         {
    //             worldRankTex.SetText("N/A");
    //         }
    //         
    //         
    //     });
    //     
    // }


    private void OnEnable()
    {
        Debug.Log("Scene Loaded");
        canvasAnimator.SetTrigger("loaded");
        sceneLoadedTime = Time.time;


        audioMixer.SetFloat("masterVol", PlayerPrefs.GetFloat("volumePref", 0));
        volumeSlider.value = PlayerPrefs.GetFloat("sliderVolume");


        musicToggle.isOn = PlayerPrefs.GetInt("playMusic", 1) == 1;
        tutorialToggle.isOn = PlayerPrefs.GetInt("finishedTutorial") == 0;
        tipsToggle.isOn = PlayerPrefs.GetInt("finishedTips") == 0;


        if (PlayerPrefs.GetInt("HighScore") >= 1000)
        {
            highScoreTex.text = "00" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 100)
        {
            highScoreTex.text = "000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 10)
        {
            highScoreTex.text = "0000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else
        {
            highScoreTex.text = "00000" + PlayerPrefs.GetInt("HighScore").ToString();
        }


        bestWaveTex.SetText(PlayerPrefs.GetInt("bestWave", 1).ToString());

        // volumeSlider.value = 0.4f;
    }


    public void OnPlayPressed()
    {
        musicPlayer.volume = Mathf.Lerp(musicPlayer.volume, 0.01f, Time.deltaTime);
        AudioSource.PlayClipAtPoint(playModePressedClip, Vector3.one);
        
        StartCoroutine(ShimmerPlay());
        StartCoroutine(DelayUntilPlay());
    }

    private bool QuitGamePressed = false;
    public TextMeshProUGUI quitText;

    public void OnQuitPressed()
    {
        if (!QuitGamePressed)
        {
            QuitGamePressed = true;
            quitText.SetText("CONFIRM?");
        }
        else
        {
            Application.Quit();
        }
    }

    IEnumerator DelayUntilPlay()
    {
        backgroundEyeAnimator.SetTrigger("playPressed");
        canvasAnimator.SetTrigger("playPressed");
        fadeOutAnimatior.SetTrigger("playPressed");
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("beginning");
    }

    public void HoverOverPlayButton()
    {
        playButtonAC.SetBool("hover", true);
    }

    public void ExitPlayButton()
    {
        playButtonAC.SetBool("hover", false);
    }

    public void HoverOverSettingsButton()
    {
        settingsAC.SetBool("hover", true);
    }

    public void OnSettingsButtonPressed()
    {
        PlayRandomClickSound();

        if (buttonsCurrPos == ButtonsPos.LEFT && panelCurrContents == PanelContents.OPTIONS)
        {
            backgroundSceneGradient.color = regularBackgroundPanelColor;
            BackToCenter();
        }
        else
        {
            FormatPanel(PanelContents.OPTIONS);
            backgroundSceneGradient.color = optionsBackgroundPanelColor;
            SlideLeft();
        }
    }

    public void OnCreditsButtonPressed()
    {
        PlayRandomClickSound();

        if (buttonsCurrPos == ButtonsPos.LEFT && panelCurrContents == PanelContents.CREDITS)
        {
            StartCoroutine(PingPongBackground());
            backgroundSceneGradient.color = regularBackgroundPanelColor;
            BackToCenter();
        }
        else
        {
            FormatPanel(PanelContents.CREDITS);
            backgroundSceneGradient.color = creditsBackgroundPanelColor;
            SlideLeft();
        }
    }

    void SlideLeft()
    {
        if (buttonsCurrPos != ButtonsPos.CENTRE) return;
        allButtonsAC.SetBool("slideLeft", true);
        buttonsCurrPos = ButtonsPos.LEFT;
    }

    void FormatPanel(PanelContents newContents)
    {
        if (panelCurrContents == newContents) return; // If we already formatted it, then we don't need to do anything

        panelCurrContents = newContents; // 

        switch (newContents)
        {
            case PanelContents.CREDITS:
                FormatToCredits();
                break;

            case PanelContents.OPTIONS:
                FormatToOptions();
                break;
        }
    }

    void Update()
    {
        if (_succesfullyLoggedIn && !formattedTextValues)
        {
            
                
                if (PlayerPrefs.GetInt("HighScore") > 0)
                {
                    Leaderboards.KazakanHighScore.SubmitScore(PlayerPrefs.GetInt("HighScore"), result =>
                    {
                        if (!result.Result)
                        {
                            Debug.LogWarning("Couldn't submit score");
                            // highScoreTex.color = Color.red;
                        }
                    });
                }

                LoadScoresFromHighScore();
                // LoadScoresFromOther();

                if (PlayerPrefs.GetInt("BestWave") > 0)
                {
                    Leaderboards.KazakanBestWave.SubmitScore(PlayerPrefs.GetInt("BestWave"), result =>
                    {
                        if (!result.Result)
                        {
                            Debug.LogWarning("Couldn't submit bestWave");
                            // highScoreTex.color = Color.red;
                        }
                    });
                } else bestWaveTex.color = Color.red;

                int numAchievements = Achievements.All.Length;
                int numAchievementsCompleted = 0;
                // foreach (UnifiedAchievement _unif in Achievements.All)
                // {
                //     if (_unif.IsUnlocked)
                //     {
                //         numAchievementsCompleted += 1;
                //     }
                // }
                
                Social.LoadAchievements(achievements =>
                {
                    if (achievements.Length > 0)
                    {
                        Debug.Log($"Found {achievements.Length} achievements");
                        foreach (Achievement achievement in achievements)
                        {
                    
                            // if (achievement.completed)
                            // {
                            //     numAchievementsCompleted += 1;
                            // }
                    
                            if (achievement.completed)
                            {
                                numAchievementsCompleted += 1;
                            }
                        }
                        achievementsProgressTex.SetText($"{numAchievementsCompleted}/{numAchievements}");
                    }
                });


                
                formattedTextValues = true;
        }
        worldRankTex.SetText(rankingInWorld == -1 ? "N/A" : $"{rankingInWorld}{DetermineEndPrefix(rankingInWorld)}");

        
        
    }

    void FormatToCredits()
    {
        panelTitleText.SetText("CREDITS");
        contentPanelBackground.color = creditsPanelColor;
        optionsCanvasGroup.alpha = 0f;
        creditsCanvasGroup.alpha = 1f;
        optionsCanvasGroup.interactable = false;
        optionsCanvasGroup.blocksRaycasts = false;

        StopCoroutine(PingPongBackground());
        StopCoroutine(MakeBackgroundCertainColorQuick(optionsBackgroundPanelColor));
        StartCoroutine(MakeBackgroundCertainColorQuick(creditsBackgroundPanelColor));
    }

    IEnumerator MakeBackgroundCertainColorQuick(Color _toChange)
    {
        while (backgroundSceneGradient.color != _toChange)
        {
            if (panelCurrContents == PanelContents.NOTHING)
            {
                Debug.Log("PLayer pressed somethinf");
                yield break;
            }

            backgroundSceneGradient.color = Color.Lerp(backgroundSceneGradient.color, _toChange, 0.1f);
            if (backgroundSceneGradient.color == _toChange)
            {
                yield break;
            }

            yield return null;
        }
    }

    IEnumerator ShimmerPlay()
    {
        float timeElapsed = 0f;
        while (timeElapsed <= 4f)
        {
            playText.color = Color.Lerp(Color.white, Color.clear, Mathf.PingPong(Time.time * 10, 1f));
            // if (playText.color == Color.clear)
            // {
            //     playText.color = Color.Lerp(Color.clear, Color.white, Time.deltaTime);
            // }
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    public void SetVolume(float volume)
    {
        Debug.Log("Ran");

        // if (Time.time - sceneLoadedTime < 0.1f)
        // {
        //     Debug.Log("Do this Instead");
        //     if (PlayerPrefs.GetFloat("sliderVolume", 1f) != 1f)
        //     {
        //         Debug.Log($"{PlayerPrefs.GetFloat("sliderVolume")}");
        //         volumeSlider.value = PlayerPrefs.GetFloat("sliderVolume");
        //     }
        //     else
        //     {
        //         PlayerPrefs.SetFloat("sliderVolume", 1f);
        //     }
        //     return;
        // }


        PlayerPrefs.SetFloat("sliderVolume", volume);
        float actualVolume = Mathf.Log10(volume) * 20;
        audioMixer.SetFloat("masterVol", actualVolume);
        PlayerPrefs.SetFloat("volumePref", actualVolume);
        // gameManager.mastVol = actualVolume;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            if (buttonsCurrPos == ButtonsPos.CENTRE)
            {
                backgroundSceneGradient.color = regularBackgroundPanelColor;
            }
        }
    }


    IEnumerator PingPongBackground()
    {
        while (true)
        {
            if (panelCurrContents != PanelContents.NOTHING)
            {
                Debug.Log("PLayer pressed somethinf");
                yield break;
            }

            backgroundSceneGradient.color =
                Color.Lerp(regularBackgroundPanelColor, pinkBackgroundPanelColor, Mathf.PingPong(Time.time, 1f));

            yield return null;
        }
    }

    void FormatToOptions()
    {
        panelTitleText.SetText("OPTIONS");
        contentPanelBackground.color = optionsPanelColor;
        optionsCanvasGroup.alpha = 1f;
        creditsCanvasGroup.alpha = 0f;
        optionsCanvasGroup.interactable = true;
        optionsCanvasGroup.blocksRaycasts = true;
        StopCoroutine(PingPongBackground());
        StopCoroutine(MakeBackgroundCertainColorQuick(creditsBackgroundPanelColor));
        StartCoroutine(MakeBackgroundCertainColorQuick(optionsBackgroundPanelColor));
    }


    public void ToggleMusic(bool toPlay)
    {
        PlayMusic = toPlay;

        /*PlayRandomClickSound();*/

        if (PlayMusic)
        {
            PlayerPrefs.SetInt("playMusic", 1);
            musicPlayer.volume = 0.1f;
        }
        else
        {
            musicPlayer.volume = 0f;
            PlayerPrefs.SetInt("playMusic", 0);
        }
    }


    public void PlayTutorial(bool toPlay)
    {
        // PlayRandomClickSound();
        if (toPlay)
        {
            PlayerPrefs.SetInt("finishedTutorial", 0);
            PlayerPrefs.SetInt("NEW_BUILD__", 0);
        }
        else
        {
            PlayerPrefs.SetInt("NEW_BUILD__", 1);
            PlayerPrefs.SetInt("finishedTutorial", 1);
        }
    }

    public void PlayTips(bool toPlay)
    {
        // PlayRandomClickSound();
        if (toPlay)
        {
            PlayerPrefs.SetInt("NEW_BUILD__", 0);
            PlayerPrefs.SetInt("finishedTips", 0);
        }
        else
        {
            PlayerPrefs.SetInt("NEW_BUILD__", 1);
            PlayerPrefs.SetInt("finishedTips", 1);
        }
    }


    public void ExitSettingsButton()
    {
        QuitGamePressed = false;
        settingsAC.SetBool("hover", false);
    }

    public void HoverOverCreditsButton()
    {
        QuitGamePressed = false;
        creditsAC.SetBool("hover", true);
    }

    public void ExitCreditsButton()
    {
        QuitGamePressed = false;
        creditsAC.SetBool("hover", false);
    }

    public void BackToCenter()
    {
        QuitGamePressed = false;
        if (buttonsCurrPos == ButtonsPos.CENTRE) return;
        allButtonsAC.SetBool("slideLeft", false);
        buttonsCurrPos = ButtonsPos.CENTRE;

        StopCoroutine(MakeBackgroundCertainColorQuick(optionsBackgroundPanelColor));
        // StopCoroutine(MakeBackgroundCertainColorQuick(creditsBackgroundPanelColor));
        StartCoroutine(PingPongBackground());
    }

    public void HoverOverQuitButton()
    {
        quitAC.SetBool("hover", true);
    }

    public void ExitQuitButton()
    {
        quitAC.SetBool("hover", false);
    }


    string DetermineEndPrefix(int numberInQuestion)
    {
        string numberAsString = numberInQuestion.ToString();
        string[] allCharacters = new string[numberAsString.Length];

        for (int i = 0; i < numberAsString.Length; i++)
        {
            allCharacters[i] = System.Convert.ToString(numberAsString[i]);
        }

        string lastNum = allCharacters[numberAsString.Length - 1];

        if (lastNum == "1")
        {
            return "st";
        }
        else if (lastNum == "2")
        {
            return "nd";
        }
        else if (lastNum == "3")
        {
            return "rd";
        }
        else
        {
            return "th";
        }
    }


    public void PlayCinematicClip()
    {
        PlayerPrefs.SetInt("CinematicState", 1);
        SceneManager.LoadScene("CinematicAnimation");
    }


    public void HapticFeedbackTestButton()
    {
        // if (Application.isMobilePlatform)
        // {
        //     Handheld.Vibrate();
        // }
    }

    public void HapticPluginPop()
    {
        // if (Application.isMobilePlatform)
        // {
        //     Vibration.VibratePop();
        // }
    }

    public void HapticPluginPeek()
    {
        if (Application.isMobilePlatform)
        {
            Vibration.VibratePeek();
        }
    }

    public void HapticPluginNope()
    {
        if (Application.isMobilePlatform)
        {
            Vibration.VibrateNope();
        }
    }

    public void HapticPatternTest()
    {
        if (Application.isMobilePlatform)
        {
            long[] pattern = {0, 1000, 1000, 1000};
            Vibration.Vibrate(pattern, -1);
        }
    }
}