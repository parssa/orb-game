﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TurretProjectile : MonoBehaviour, IPooledObject
{

    //  public Transform targetTransform = null;
    //public Vector3 positionToShoot = new Vector3(0, 0, 0);
    //private float playerSpeed;
    private ObjectPooler objectPooler;
    public Turret thisTurret;
    private CameraMotor cameraShake;


    public AudioMixer mainMixx;
    public Vector2 targetPosition;
    public Vector2 projectilePosition;


    private float projectileSpeed = 5f;


    private float timeMade = 0.0f;
    private float timeTillBreak = 1f;

    private CircleCollider2D hitbox;
    private Collider2D[] hits = new Collider2D[10];

    public ContactFilter2D filter;

    private GameObject explosion;

    private int projDamage = 1;



    // Start is called before the first frame update
    void Start()
    {
        cameraShake = CameraMotor.instance;
        hitbox = GetComponent<CircleCollider2D>();
        objectPooler = ObjectPooler.instance;
        // if (targetTransform)  targetPosition = targetTransform.position;
        // else targetPosition = Vector3.up;

        // targetPosition = Vector3.up;

        projectilePosition = transform.position;


        // explosion = Resources.Load("OnRegChargeUpBreak") as GameObject;
        //playerSpeed = 20f;

        //  timeMade = Time.time;
    }

    public void OnObjectSpawn()
    {
        timeMade = Time.time;
        //  targetPosition = targetTransform.position;
        //  projectilePosition = transform.position;

        projDamage = GameManager.instance.turretDamage;

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }



        //

    }



    // Update is called once per frame
    private void Update()
    {


        if (thisTurret.thisTarget == null)
        {
            Debug.Log("No Target, i'll die");
            BreakProj();
        }
        else targetPosition = thisTurret.thisTarget.position;

        transform.position = Vector2.MoveTowards(transform.position, targetPosition, 0.1f * projectileSpeed);


        if (isActiveAndEnabled)
        {
            hitbox.OverlapCollider(filter, hits);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i] == null)
                    continue;


                OnCollide(hits[i]);
                //Cleans array
                hits[i] = null;

            }
        }



        if (Time.time - timeMade > timeTillBreak && timeMade != 0)
        {
            BreakProj();
        }


        // Destroy(gameObject, timeTillBreak);
    }

    protected virtual void OnCollide(Collider2D coll)
    {
        if (coll.CompareTag("Fighter"))
        {
            
            Damage dmg = new Damage
            {
                damageAmount = projDamage,
                origin = transform.position,
                pushForce = 2f,
                originType = Damage.damageType.Turret,
            };

            if (coll.gameObject != null) coll.SendMessage("RecieveDamage", dmg);
            else Debug.Log("NULL GAMEOBJECT TPROJ");
            
            
            
            
            
            BreakProj();
        }

    }

    void PlayProjectileBreakSound()
    {

        if (GameManager.instance.mastVol <= -80f) return;
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
        AudioClip shootClip = SoundManager.instance.GetSounds("turretProjToEnemy").clip;
        var shootSound = PlayClipAt(shootClip, soundPos);
        shootSound.pitch = Random.Range(0.8f, 1.2f);
        shootSound.volume = 0.05f;
        shootSound.reverbZoneMix = 0.078819f;
        shootSound.outputAudioMixerGroup = mainMixx.outputAudioMixerGroup;
    }

    void BreakProj()
    {
        gameObject.SetActive(false);
        if (thisTurret.projectilesExplode)
        {

            GameObject explosion = objectPooler.SpawnFromPool("explosionPurple", transform.position, transform.rotation);
            explosion.GetComponent<ExplosionCollison>().isTurretShot = true;
            explosion.GetComponent<ExplosionCollison>().damage = projDamage;

        }

    }
    
    AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        
        
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.spatialBlend = 0.5f;
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }
}
