﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Fighter : MonoBehaviour
{


    //public static Fighter instance;
   

    //Public Fields
    [Header("Combat Values (Fighter)")]
    public int hitpoint = 10;
    public int maxHitpoint = 10;
    public float pushRecoverySpeed = 0.2f;
    public float lastHitTime = 0.0f;

    // Private field


    //Immunity
    public float immuneTime = 0.1f;
    protected float lastImmune;

    //Push
    public Vector2 pushDirection;

    // Private
    private SpriteRenderer fighterSprite;

    public bool wasTargettedByTurret = false;
    // All fighters can ReceieveDamage / Die
    private enum HitBy
    {
        None,
        Player,
        Dash,
        Orb,
        Turret
    }
    private HitBy hitBy;
    public Vector2 lastPushDirection;

    private void Awake()
    {
        fighterSprite = GetComponent<SpriteRenderer>();
        hitBy = HitBy.None;
        lastPushDirection = Vector2.zero;
    }

   
    protected virtual void RecieveDamage(Damage dmg)
    {
        if (!transform.gameObject.activeSelf) 
        {
            Debug.Log("disabled and dead, breaking out of recieve damage");
            return;
        }

        if (!transform.gameObject.activeInHierarchy)
        {
            Debug.Log("disabled in hierarch. breaking out of recieve damage");
            return;
        }
        
        if ((Time.time - lastImmune > immuneTime) && !PauseMenu.GameIsPaused)
        {


        
            lastImmune = Time.time;

            if (CompareTag("Player"))
            {
                GameManager.instance.ShortVibrate();
            }
            
            if (CompareTag("Player") && (Player.instance.strengthPerk))
            {
                int damageActuallyDone = Random.Range(1, dmg.damageAmount + 1);
                hitpoint -= dmg.damageAmount;
            }
            else hitpoint -= dmg.damageAmount;
            //pushDirection = (transform.position - dmg.origin).normalized * dmg.pushForce;
            pushDirection = new Vector2(transform.position.x - dmg.origin.x, transform.position.y - dmg.origin.y).normalized * (1 / dmg.pushForce);


            //sif (CompareTag("Player")) Debug.Log("fighter pushDirection " + pushDirection);
            
            Vector2 newPositionAfterPush = new Vector2(transform.position.x + pushDirection.x, transform.position.y + pushDirection.y);
            if (CompareTag("Fighter"))
            {
                if ((dmg.originType == Damage.damageType.Arrow) ||
                    (dmg.originType == Damage.damageType.ChargeAttack))
                {
                    hitBy = HitBy.Player;
                }

                if (dmg.originType == Damage.damageType.Dash) hitBy = HitBy.Dash;

                if (hitBy == HitBy.Dash || hitBy == HitBy.Player)
                {
                    float maxStep = 0.1f;

                    if (Player.instance.strengthPerk) maxStep = 0.4f;
                    //FIX THIS
                    transform.position = Vector2.MoveTowards(transform.position, newPositionAfterPush, maxStep);
                }
            }


            // Debug.Log(pushDirection);
            lastPushDirection = pushDirection;
            if (CompareTag("Fighter") || CompareTag("Player") || CompareTag("Shrine"))
            {
                
                DamagePopup.Create(dmg.origin, dmg.damageAmount, dmg.isCrit, DamagePopup.DAMAGE);
            }

            if (CompareTag("Guardian"))
            {
                DamagePopup.Create(dmg.origin, dmg.damageAmount, dmg.isCrit, DamagePopup.BOSSDAMAGE);
            }

            if (CompareTag("Player"))
            {
                PlayRandomHitSound();
                GameManager.instance.IntroduceDamage();
                Player.instance.lastHit = 0f;
                Player.instance.gotHit = true;
                lastHitTime = Time.time;
            }





            if (hitpoint <= 0)
            {

                hitpoint = 0;
                Death();

            }
        }
    }

    void PlayRandomHitSound()
    {


        int randNum = Random.Range(0, 91);
        if (randNum >= 60)
        {
            SoundManager.instance.Play("enemy_p1", 0);
        }
        else if (randNum >= 30)
        {
            SoundManager.instance.Play("enemy_p2", 0);
        }
        else
        {
            SoundManager.instance.Play("enemy_p3", 0);
        }
    }


    protected virtual void Death()
    {
        if (hitBy == HitBy.Player)
        {
            ComboManager.instance.newKill = true;
            ComboManager.instance.recentKillTime = Time.time;
            ComboManager.instance.recentKillLocation = transform.position;
        }
        if (hitBy == HitBy.Dash)
        {
            ComboManager.instance.newKill = true;
            ComboManager.instance.recentKillTime = Time.time;
            ComboManager.instance.recentDashKillTime = Time.time;
            ComboManager.instance.recentKillLocation = transform.position;


        }

    }
}
