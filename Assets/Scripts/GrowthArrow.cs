﻿using UnityEngine;

public class GrowthArrow : MonoBehaviour, IPooledObject
{
    private ObjectPooler objectPooler;

    public float arrowSpeed;
    public int damage;
    public int critMult;
    private float breakTime = 0.4f;
    public float pushFor;


    private AudioClip impact;
    private float timeAlive;

    public bool isChargeUp = false;
    private CameraMotor cameraShake;

    private BoxCollider2D hitbox;
    private SpriteRenderer thisRenderer;

    private Animator arrowAnimator;

    // public bool childrenOfMain = false;
    public Sprite sporeSprite;
    public Sprite arrowSprite;

    private AudioSource audioSource;

    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;

    private float lastExplosionCreateTime = 0.0f;
    private const float EXPLOSION_COOLDOWN = 0.03f;

    public bool isPiercingArrow = false;

    private float timeMade = 0.0f;
    private int numEnemiesHit = 0;

    private Transform firstEnemyHit;

    // Start is called before the first frame update
    // private void Awake()
    // {
    //     childrenOfMain = false;
    // }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        impact = SoundManager.instance.GetSounds("ArrowHit").clip;
        if (!isChargeUp)
        {
            arrowAnimator = GetComponent<Animator>();
        }

        objectPooler = ObjectPooler.instance;

        cameraShake = CameraMotor.instance;
        hitbox = GetComponent<BoxCollider2D>();
        thisRenderer = GetComponent<SpriteRenderer>();

        // if (!childrenOfMain)
        // {
        //     arrowSpeed = 12f;
        //     breakTime = 1.4f;
        //     if (!isChargeUp) arrowAnimator.SetBool("isChild", false);
        // }
        // else
        // {
        //     damage = Weapon.instance.damagePoint;
        //     critMult = Weapon.instance.critMultiplier;
        //     breakTime = 0.2f;
        //     arrowSpeed = 8f;
        //     transform.localScale = new Vector3(2, 2, 1);
        //     thisRenderer.sprite = sporeSprite;
        //     if (!isChargeUp) arrowAnimator.SetBool("isChild", true);
        //
        //     pushFor = 1000f;
        // }

        if (isChargeUp)
        {
            breakTime = 0.3f;
            arrowSpeed = 12f;
            transform.localScale = new Vector3(4f, 4f, 1);
        }
        else
        {
            
            arrowSpeed = 12f;
            breakTime = 1.4f;
            if (!isChargeUp) arrowAnimator.SetBool("isChild", false);
        }
    }

    public void OnObjectSpawn()
    {
        //  Debug.Log("This Ran");  
        timeAlive = 0.0f;
        timeMade = Time.time;
        numEnemiesHit = 0;

        firstEnemyHit = null;
        if (isChargeUp)
        {
            breakTime = 0.3f;
            arrowSpeed = 12f;
            transform.localScale = new Vector3(4f, 4f, 1);
        }
        else
        {
            
            arrowSpeed = 12f;
            breakTime = 1.4f;
            // if (!isChargeUp) arrowAnimator.SetBool("isChild", false);
        }
        // if (!childrenOfMain)
        // {
        //     arrowSpeed = 12f;
        //     breakTime = 1.4f;
        //     GetComponent<SpriteRenderer>().sprite = arrowSprite;
        //     transform.localScale = new Vector3(3, 3, 1);
        //     
        //     if (!isChargeUp)
        //     {
        //         arrowAnimator = GetComponent<Animator>();
        //         arrowAnimator.SetBool("isChild", false);
        //     }
        //
        //
        //
        // }
        // else
        // {
        //     damage = Weapon.instance.damagePoint;
        //     critMult = Weapon.instance.critMultiplier;
        //     breakTime = 0.2f;
        //     arrowSpeed = 8f;
        //     transform.localScale = new Vector3(2, 2, 1);
        //     thisRenderer.sprite = sporeSprite;
        //     if (!isChargeUp) arrowAnimator.SetBool("isChild", true);
        //
        //     pushFor = 1000f;
        // }
        //
        // if (isChargeUp)
        // {
        //     breakTime = 0.3f;
        //     arrowSpeed = 12f;
        //
        //     transform.localScale = new Vector3(4f, 4f, 1);
        // }
    }



    // Update is called once per frame
    void Update()
    {


        transform.Translate(Vector3.right * (Time.deltaTime * arrowSpeed));

        hitbox.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            if (transform.gameObject.activeSelf) OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;

        }
        timeAlive += Time.deltaTime;
        // if (!isChargeUp)
        // {
        //     if (timeAlive > breakTime) BreakArrow();
        //     damage = Weapon.instance.damagePoint;
        //     critMult = Weapon.instance.critMultiplier;
        //     if (!childrenOfMain)
        //     {
        //         transform.localScale = new Vector3(3, 3, 1);
        //         arrowSpeed = 17f;
        //         breakTime = 1.4f;
        //         thisRenderer.sprite = arrowSprite;
        //          arrowAnimator.SetBool("isChild", false);
        //     } else
        //     {
        //         breakTime = 0.2f;
        //         arrowSpeed = 8f;
        //         transform.localScale = new Vector3(2, 2, 1);
        //         thisRenderer.sprite = sporeSprite;
        //         arrowAnimator.SetBool("isChild", true);
        //     }
        // }
        // else if (isChargeUp)
        // {
        //
        //     if (timeAlive > breakTime)
        //     {
        //         CreateChargeUpExplosion(-10);
        //         CreateChargeUpExplosion(0);
        //
        //         CreateChargeUpExplosion(10);
        //         BreakArrow();
        //     }
        //
        // }
        if (isChargeUp)
        {

            if (timeAlive > breakTime)
            {
                CreateChargeUpExplosion(-10);
                CreateChargeUpExplosion(0);

                CreateChargeUpExplosion(10);
                BreakArrow();
            }

        }
        else
        {
            if (timeAlive > breakTime) BreakArrow();
            damage = Weapon.instance.damagePoint;
            critMult = Weapon.instance.critMultiplier;
            transform.localScale = new Vector3(3, 3, 1);
            arrowSpeed = 17f;
            breakTime = 1.4f;
            thisRenderer.sprite = arrowSprite;
            arrowAnimator.SetBool("isChild", false);
        }
        
        
        



    }

    protected virtual void OnCollide(Collider2D coll)
    {

        if (Player.instance.strengthPerk) pushFor *= 3f;
        if (!transform.gameObject.activeInHierarchy) return;

        if (coll.CompareTag("Fighter") || coll.CompareTag("Shrine") || coll.CompareTag("Guardian") || coll.CompareTag("Boss"))
        {
            if ((coll.tag == "Shrine") && ((coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN) ||
            (coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.CLOSED)))
            {
                return;
            }

            if ((coll.tag == "Guardian") && !coll.transform.GetComponent<Guardian>().activated) return;
            else if ((coll.tag == "Boss") && !coll.transform.GetComponent<Boss>().activated) return;
            int damageToDeal = 0;

            int randNum = Random.Range(0, 100);
            if (randNum > 70)
            {
                damageToDeal = Random.Range(damage, (damage * 2) + critMult);
            }
            else if (randNum > 40)
            {
                damageToDeal = Random.Range(damage, damage + critMult);
            }
            else
            {
                damageToDeal = damage;
            }

            StartCoroutine(cameraShake.Shake(0.1f, 10f));

            Damage.damageType arrowType = Damage.damageType.Arrow;
            if (isChargeUp) arrowType = Damage.damageType.ChargeAttack;

            Damage dmg = new Damage
            {
                damageAmount = damageToDeal,
                origin = transform.position,
                pushForce = 100f,
                isCrit = damageToDeal >= (damage * 2),
                originType = arrowType

            };

            if (coll.gameObject != null)
            {
                coll.SendMessage("RecieveDamage", dmg);
                numEnemiesHit += 1;
            }
            else Debug.Log("NULL GAMEOBJECT");


            if (isChargeUp) CreateChargeUpExplosion(0);
            else
            {
                if (isPiercingArrow)
                {
                    if (firstEnemyHit == null)
                    {
                        CreateSpores();
                        PlayHitSound();
                    }
                    else
                    {
                        if (coll.transform != firstEnemyHit)
                        {
                            CreateSpores();
                            PlayHitSound();
                        }
                    }
                }
                else
                {
                    CreateSpores();
                    PlayHitSound();
                }
            }
            // else if (!isChargeUp && !childrenOfMain)
            // {

            //     CreateSpore(Random.Range(-180, 180));
            //     CreateSpore(Random.Range(-180, 180));
            //     CreateSpore(Random.Range(-180, 180));


            //     BreakArrow();
            // AudioSource.PlayClipAtPoint(audioSource.clip, transform.position, 1 / Mathf.Sqrt(GameManager.instance.mastVol * GameManager.instance.mastVol));
            // }
            // else
            // {

            
            BreakArrow();
           // }



        }
    }


    void BreakArrow()
    {
        //childrenOfMain = false;
        //  childrenOfMain = false;
        if ((isPiercingArrow) && (numEnemiesHit == 1) && (Time.time - timeMade < breakTime))
        {
            damage = 2;
            critMult = 1;
            Debug.Log("havent done enough killing");
            return;
        }
        gameObject.SetActive(false);
        //if (!isChargeUp) arrowAnimator.SetBool("isChild", false);
    }

    // private void CreateSpore(int angle)
    // {
    //     Quaternion randomRotation = Quaternion.Euler(0, 0, angle);
    //     //GameObject spawnOfMama = Instantiate(Weapon.instance.arrow, transform.position, randomRotation);
    //     GameObject spawnOfMama = objectPooler.SpawnFromPool("arrow_spore", transform.position, randomRotation);
    //
    //     // spawnOfMama.GetComponent<GrowthArrow>().childrenOfMain = true;
    //     spawnOfMama.transform.localScale = new Vector3(Random.Range(2, 3), Random.Range(2, 3), 1);
    //     spawnOfMama.GetComponent<SpriteRenderer>().sprite = sporeSprite;
    //     arrowAnimator.SetBool("isChild", true);
    // }

    void CreateSpores()
    {
        objectPooler.SpawnFromPool("sporeBreak", transform.position, transform.rotation);
    }

    private void CreateChargeUpExplosion(int adjustment)
    {
       // CreateSpore(0 + adjustment);
     //   CreateSpore(45 + adjustment);
        //CreateSpore(90 + adjustment);
       // CreateSpore(135 + adjustment);
       // CreateSpore(180 + adjustment);
        //CreateSpore(-45 + adjustment);
      // CreateSpore(-90 + adjustment);
        //CreateSpore(-135 + adjustment);
     //   CreateSpore(-180 + adjustment);


        if (Time.time - lastExplosionCreateTime >= EXPLOSION_COOLDOWN)
        {
            GameObject explosion = objectPooler.SpawnFromPool("sporeExplosion", transform.position, transform.rotation);
            explosion.GetComponent<ExplosionCollison>().damage = Weapon.instance.chargeUpDamage;
            
            lastExplosionCreateTime = Time.time;
        }


    }

    void PlayHitSound()
    {
        if (GameManager.instance.mastVol <= -78f) return;
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 2 - GameManager.instance.mastVol * 10);
        AudioSource.PlayClipAtPoint(impact, soundPos);
    }




}
