﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Weapon : Collidable
{

    [HideInInspector]
    public Player player;

    private WaveSpawner waveSpawner;
    private Camera cam;
    
    private ObjectPooler objectPooler;
    private ControlsHandler controlsHandler;
    private TutorialManager tutorialManager;
    private SoundManager soundManager;
    private HeaderPopup headerPopup;
    private CharacterMenu characterMenu;
    
    public static Weapon instance;

    private int playerCurrStamina;

    public BowManager bm;
    public bool hoveringOver = false;
    private int sDecrease;
    private int chargeShotCost = 5;

    public LayerMask notToHit;
    public Transform BulletTrailPrefab;

    public CameraMotor cameraShake;

    public bool burstPerkEquipped = false;

    //private float timeSinceLastDashCalled = 0.0f;
    public bool insaneModeActive = false;
    //Damage struct

    public int damagePoint = 1;
    public int dashDamage = 6;
    public int chargeUpDamage = 4;
    public int critMultiplier = 3;
    public float pushForce = 5f;
    public Sprite lightcookie;
    public GameObject arrow;
    public GameObject chargeUpEffect;
    public Color dashColor;

    //Upgrade
    public int weaponLevel = 1;

    private SpriteRenderer spriteRenderer;

    // Shoot
    public float cooldown = 0.2f;

    private float lastSwing;
    public bool canReleaseCharge = true;
    //private float lastShotTime;
    private float lastOverDriveTime;

    public bool isCharging = false;

    public bool justUpgradedWeapon = false;
    public bool overDriveMode = false;

    private float lastChargeTime;
    private float lastClawChargeBurstTime = 0.0f;

    public bool lastShot;
    public bool lastChargeShot;
    private float CHARGE_COOLDOWN_LIMIT = 6f; // 7, 5, 3
    public bool canShootAgain = true;



    public Transform firePoint;
    public Light2D weaponLight;
    public Light2D eyesLight;
    public Light2D chargeUpIndicLights;
    private const float normalEyesIntensity = 14.36f;
    private const float chargeUpReadyIntensity = 20f;

    private const float normalEyesIntensityLeech = 11.36f;
    private const float chargeUpReadyIntensityLeech = 15f;

    private const float normalLightIntensity = 4.22f;
    public Animator chargeUpStatusOrbs;
    private bool playerChargeUpReadySound = false;

#pragma warning disable 0649
    [SerializeField] private Animator staminaIconAnim;
#pragma warning restore 0649

    private Quaternion nullQuatern;

    private Animator animator;

    public bool piercingPerkEnabled = false;

    //private float timeHeldDown;


    void Awake()
    {
        hoveringOver = false;
        instance = this;
        //timeHeldDown = 0f;
    }

    protected override void Start()
    {
        base.Start();
        weaponLevel = 0;
        controlsHandler = ControlsHandler.instance;
        waveSpawner = WaveSpawner.instance;
        
        objectPooler = ObjectPooler.instance;
        tutorialManager = TutorialManager.instance;
        soundManager = SoundManager.instance;
        headerPopup = HeaderPopup.instance;
        characterMenu = CharacterMenu.instance;
        animator = GetComponent<Animator>();

        if (gameObject.name == "weapon")
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            // spriteRenderer.sprite = GameManager.instance.weaponSprites[GameManager.instance.weapon.weaponLevel];
            spriteRenderer.sprite = bm.currBow.bowImage;
        }

        nullQuatern = Quaternion.LookRotation(Vector3.forward, new Vector2(0, 0));

        cooldown = 0.3f;

        player = Player.instance;
        cam = Camera.main;

        //arrow = GameManager.instance.arrows[weaponLevel];
        sDecrease = 1;
        damagePoint = bm.currBow.arrowDamage;
        dashDamage = bm.currBow.dashDamage;
        critMultiplier = bm.currBow.critMultiplier;
        cooldown = bm.currBow.cooldown;
        arrow = bm.currBow.arrowProj;
        chargeUpEffect = bm.currBow.chargeUpProj;
        lightcookie = bm.currBow.bowLightCookie;
        pushForce = bm.currBow.pushForce;
        chargeUpDamage = bm.currBow.chargeUpDamage;
        dashColor = bm.currBow.dashColor;
        lastOverDriveTime = 10f;
        lastChargeTime = 0f;
        overDriveMode = false;
        chargeUpIndicLights.color = bm.currBow.dashColor;
    }

    protected override void Update()
    {



            

        if (overDriveMode) Debug.Log("Overdriving!!");
        
        base.Update();
        // Recover to regular light
        if (!isCharging &&
            !player.isDashing &&
            !player.godModeActive
            && !overDriveMode)
        {
            if (weaponLight.intensity > normalLightIntensity)
            {
                float intensityVal = weaponLight.intensity - 0.3f;
                weaponLight.intensity = intensityVal;
            }

            if (weaponLight.intensity < normalLightIntensity)
            {
                float intensityVal = weaponLight.intensity + 0.01f;
                weaponLight.intensity = intensityVal;

            }
        }

        if (lastOverDriveTime < 5f) lastOverDriveTime += Time.deltaTime;


        if (player.godModeActive) weaponLight.intensity = 8.3f;
        if (player.isDashing) weaponLight.intensity = 6f;
        if (overDriveMode) weaponLight.intensity += 0.01f;
        if (isCharging) weaponLight.intensity = 4f + Mathf.PingPong(20 * Time.time, 5f);



        playerCurrStamina = player.currStamina;
        // Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // mousePos.z = 0;

        if (!PauseMenu.GameIsPaused)
        {
            //transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
            if (controlsHandler.GetBowLookRotation() != nullQuatern) transform.rotation = controlsHandler.GetBowLookRotation();

        }



        if (player.godModeActive && !PauseMenu.GameIsPaused)
        {
            if (canShootAgain)
            {
                Shoot();
                StartCoroutine(ShootingDelay());
            }
        }
        canReleaseCharge = (Time.time - lastChargeTime > CHARGE_COOLDOWN_LIMIT);

        if (!tutorialManager.hasCompletedChargeUpTutorial)
        {
            if (canReleaseCharge && 
                playerCurrStamina > chargeShotCost && 
                !lastChargeShot && waveSpawner.num_enemies_wave > 5
                && !tutorialManager.hasCompletedChargeUpTutorial)
            {
                headerPopup.gameObject.SetActive(true);
                headerPopup.PlayChargePrompt();
            }
    
        }
        
        


        // Charge Up Orbs
        #region
        if (Time.time - lastChargeTime > (CHARGE_COOLDOWN_LIMIT - 1))
        {
            chargeUpStatusOrbs.SetBool("startLoad", true);
            chargeUpStatusOrbs.SetBool("hide", false);
        }
        else chargeUpStatusOrbs.SetBool("startLoad", false);


        if (Time.time - lastChargeTime > (CHARGE_COOLDOWN_LIMIT))
        {
            chargeUpStatusOrbs.SetTrigger("readyToRelease");
            if (!playerChargeUpReadySound)
            {

                Debug.Log("charge up ready sound");
                soundManager.Play("chargeUpReady", 0f);
                playerChargeUpReadySound = true;
            }
            
            chargeUpStatusOrbs.SetBool("hide", true);
            if (bm.currBow.bowName == "The Leech" || (bm.currBow.bowName == "The Seeker")) eyesLight.intensity = chargeUpReadyIntensityLeech;
            else eyesLight.intensity = chargeUpReadyIntensity;
            // player.bandanaLight.intensity = 0.5f + Mathf.PingPong(2 * Time.time, 0.5f);
        }
        else
        {
            if (bm.currBow.bowName == "The Leech" || (bm.currBow.bowName == "The Seeker")) eyesLight.intensity = normalEyesIntensityLeech;
            else eyesLight.intensity = normalEyesIntensity;
            // player.bandanaLight.intensity = 0.5f;
            chargeUpStatusOrbs.SetBool("hide", false);
        }
        #endregion

        if (controlsHandler.PlayerChargeShotInput()
            && !PauseMenu.GameIsPaused
            && !characterMenu.shopIsActive
            && !characterMenu.hoveringOverButton
            && !hoveringOver
            && !player.isDashing
            && !overDriveMode)
        {
            if (playerCurrStamina - chargeShotCost >= 0 && canReleaseCharge)
            {
                chargeUpStatusOrbs.SetBool("startLoad", false);
                lastChargeShot = true;
                canReleaseCharge = false;
                lastChargeTime = Time.time;
                ChargeUp();
            }
            else if (canReleaseCharge)
            {
                //  soundManager.Play("outofstam2", 0);
            }
        }

        // if (Input.GetKey(KeyCode.Mouse0)
        //     && !PauseMenu.GameIsPaused
        //     && !characterMenu.shopIsActive
        //     && !characterMenu.hoveringOverButton
        //     && !hoveringOver
        //     && !isCharging)
        if (controlsHandler.PlayerShootingInput()
            && !PauseMenu.GameIsPaused
            && !characterMenu.shopIsActive
            && !characterMenu.hoveringOverButton
            && !hoveringOver
            && !isCharging)
        {

            if (canShootAgain && (playerCurrStamina - sDecrease >= 0))
            {

                lastShot = true;
                Shoot();
                StartCoroutine(ShootingDelay());
            }
            else if (controlsHandler.PlayerShootingInput() && canShootAgain && (playerCurrStamina - sDecrease <= 0))
            {
                staminaIconAnim.SetTrigger("cantShoot");
                // if (Random.Range(0, 10) > 5) soundManager.Play("outofstam2", 0);
                // else soundManager.Play("outofstam3", 0);
            }

        }

    }
    protected override void OnCollide(Collider2D coll)
    {

        if (((coll.CompareTag("Fighter")) || (coll.CompareTag("Shrine")) || coll.CompareTag("Guardian") || coll.CompareTag("Boss")) && (player.isDashing || Time.time - player.lastShotTime <= 0.1f ) && !PauseMenu.GameIsPaused)
        {
            if (coll.gameObject == null) 
            {
                Debug.Log("NULL GAMEOBJECT RETURN");
                return;
            }



            if (coll.name == "Player")
                return;

            if ((coll.CompareTag("Shrine")) && ((coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN) ||
                                                (coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.CLOSED)))
            {
                return;
            }

            if ((coll.CompareTag("Guardian")) && !coll.transform.GetComponent<Guardian>().activated) return;
            else if ((coll.CompareTag("Boss")) && !coll.transform.GetComponent<Boss>().activated) return;
            // Create a new damage object, then we'll send it to the fighter we've hit
            //int damageToDeal = Random.Range(dashDamage, dashDamage * critMultiplier);
            int randNum = Random.Range(0, 100);
            int damageToDeal = dashDamage;
            if (randNum > 70)
            {
                damageToDeal = Random.Range(dashDamage + 1, (dashDamage * 2) + critMultiplier);
            }
            else if (randNum > 20)
            {
                damageToDeal = Random.Range(dashDamage, dashDamage + critMultiplier);
            }
            else
            {
                damageToDeal = dashDamage;
            }
            //StartCoroutine(cameraShake.Shake(0.14f, 10f));
            if (player.strengthPerk) pushForce = 10f;
            Damage dmg = new Damage
            {
                damageAmount = damageToDeal,
                origin = transform.position,
                pushForce = pushForce,
                isCrit = damageToDeal >= (dashDamage * 2),
                originType = Damage.damageType.Dash,

            };

            if (dmg.isCrit && damageToDeal > 1)
            {
                soundManager.Play("CriticalHit", 0);
            }

            if (bm.currBow.bowName == "The Claw")
            {
                if (Time.time - ComboManager.instance.recentDashKillTime <= 0.1f)
                {
                    if (Time.time - lastClawChargeBurstTime > 0.1f)
                    {
                        CreateClawShot(Random.Range(65, 116), true);
                        lastClawChargeBurstTime = Time.time;
                    }
                    
                   // CreateClawShot(Random.Range(65, 116), true);
                }
            }


            if (Time.time - player.lastShotTime > 0.1f)
            {
                StartCoroutine(cameraShake.Shake(0.2f, 10f));
                if (Random.Range(0, 11) > 5)
                {
                    soundManager.Play("DashImpactOne", 0);
                }
                else
                {
                    soundManager.Play("DashImpactTwo", 0);
                }
            }

            if (coll.gameObject != null) coll.SendMessage("RecieveDamage", dmg);
            

        }
    }

    void Shoot()
    {

        Vector2 mousePosition = new Vector2(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y);
        if (firePoint == null)
        {
            Debug.LogError("No Firepoint found!!!!");
        }
        else
        {
            Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
            RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 50, notToHit);
            Effect();
            if (hit.collider != null)
            {
                OnCollide(hit.collider);
            }
        }

    }

    void Effect()
    {
        float adj = 0;
        if (bm.currBow.bowName == "The Seeker") adj = 1f;
        else if (bm.currBow.bowName == "The Spore") adj = -0.2f;
        else if (bm.currBow.bowName == "The Claw") adj = -0.5f;
        else if (bm.currBow.bowName == "The Leech") adj = 0.3f;
        RandomShootingNoise(adj);
        StartCoroutine(cameraShake.Shake(0.02f, 5f));
        animator.SetTrigger("shotArrow");

        if ((bm.currBow.bowName == "The Seeker"))
        {
            if (overDriveMode)
            {
                int choice = Random.Range(0, 3);
                if (choice == 0) SeekerSprayShot(0f, 0f);
                else if (choice == 1) SeekerSprayShot(0.5f, 0);
                else if (choice == 2) SeekerSprayShot(-0.5f, -0);
                else SeekerSprayShot(0f, 0f);
            }
            else
            {
                GameObject arrowShotOut = objectPooler.SpawnFromPool("arrow_seeker", firePoint.position, firePoint.rotation);
                arrowShotOut.GetComponent<GoldenArrow>().arrowSpeed = 35f;
                arrowShotOut.GetComponent<GoldenArrow>().damage = damagePoint;
                arrowShotOut.GetComponent<GoldenArrow>().critMult = critMultiplier;
                if (piercingPerkEnabled) arrowShotOut.GetComponent<GoldenArrow>().isPiercingArrow = true;
            }


        }
        else if ((bm.currBow.bowName == "The Spore"))
        {
            if (overDriveMode) CreateSprayShot();
            else
            {
                GameObject arrowShotOut = objectPooler.SpawnFromPool("arrow_spore", firePoint.position, firePoint.rotation);
                // arrowShotOut.GetComponent<GrowthArrow>().childrenOfMain = false;
                arrowShotOut.GetComponent<GrowthArrow>().arrowSpeed = 23f;
                arrowShotOut.GetComponent<GrowthArrow>().damage = damagePoint;
                arrowShotOut.GetComponent<GrowthArrow>().critMult = critMultiplier;
                if (piercingPerkEnabled) arrowShotOut.GetComponent<GrowthArrow>().isPiercingArrow = true;
            }

        }
        else if (bm.currBow.bowName == "The Claw")
        {
            ClawShot();
        }
        else if (bm.currBow.bowName == "The Leech")
        {
            LeechShot(0f, Random.Range(80, 100));
            // LeechShot(-0.1f, Random.Range(75, 105));


        }
        else
        {
            //GameObject arrowShotOut = Instantiate(arrow, firePoint.position, firePoint.rotation);
            GameObject arrowShotOut = objectPooler.SpawnFromPool("arrow_reg", firePoint.position, firePoint.rotation);
            arrowShotOut.GetComponent<Arrow>().arrowSpeed = 21f;
            arrowShotOut.GetComponent<Arrow>().damage = damagePoint;
            arrowShotOut.GetComponent<Arrow>().critMult = critMultiplier;
            if (bm.currBow.bowName == "The Long Shot") arrowShotOut.GetComponent<Arrow>().arrowType = Arrow.ArrowType.LONG_SHOT;
            if (piercingPerkEnabled) arrowShotOut.GetComponent<Arrow>().isPiercingArrow = true;

            // player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized;
            // player.lastHitTime = Time.time - 1f;
        }
        
        
        if (bm.currBow.bowName == "The Seeker" && overDriveMode)
        {

        }
        else if (burstPerkEquipped)
        {
            if (Random.Range(0, 100) > 20) player.currStamina -= sDecrease;
        }
        else { player.currStamina -= sDecrease; }
        





    }

    void ChargeUp()
    {
        Vector2 mousePosition = new Vector2(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y);
        if (firePoint == null)
        {
            Debug.LogError("No Firepoint found!!!!");
        }
        else
        {

            Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
            RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 50, notToHit);


            ChargeUpEffect();
            controlsHandler.SwitchTo(ControlsHandler.AttackType.DASH);


            if (hit.collider != null)
            {
                OnCollide(hit.collider);
            }
        }


    }

    public bool CanAffordToShootChargeUp => player.currStamina - chargeShotCost >= 0;

    void ChargeUpEffect()
    {
        playerChargeUpReadySound = false;            
        if (!tutorialManager.hasCompletedChargeUpTutorial)
        {
            tutorialManager.FinishedTip(TutorialManager.TipsSpecify.ChargeUp);
            if (!characterMenu.BLESSINGS_SHOWING) headerPopup.FinishedChargeUpTip();
        }
        
        if (bm.currBow.bowName != "The Spore") soundManager.Play("explode3", 0);

        StartCoroutine(cameraShake.Shake(0.07f, 15f));

        player.currStamina -= chargeShotCost;
        chargeUpStatusOrbs.SetTrigger("released");

        if (bm.currBow.bowName == "The Seeker")
        {
            if (lastOverDriveTime > 4f)
            {
                StartCoroutine(SeekerOverdrive());
            }

            isCharging = false;
        }
        else if (bm.currBow.bowName == "The Spore")
        {
            if (lastOverDriveTime > 4f)
            {
                StartCoroutine(SporeOverdrive());

            }
            isCharging = false;
        }
        else if (bm.currBow.bowName == "The Claw")
        {
            if (lastOverDriveTime > 4f)
            {
                StartCoroutine(ClawOverdrive());
            }

            isCharging = false;
        }
        else if (bm.currBow.bowName == "The Rammer")
        {

            RammerChargeUpShot();
            player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 15;
            isCharging = false;
        }
        else if (bm.currBow.bowName == "The Long Shot")
        {

            LongShotChargeUpShot();
            player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 10;
            isCharging = false;
        }
        else if (bm.currBow.bowName == "The Leech")
        {
            // LeechShot(0.2f, 90);
            // LeechShot(-0.2f, 90);

            StartCoroutine(LeechOverDrive());
            GameObject leechSpike = Instantiate(arrow, firePoint.position, firePoint.rotation);
            leechSpike.GetComponent<LeechSpike>().arrowSpeed = 15f;
            leechSpike.GetComponent<LeechSpike>().damage = chargeUpDamage;
            leechSpike.GetComponent<LeechSpike>().pushFor = 1000f;
            player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 20;
            isCharging = false;
        }
        else
        {
            //GameObject chargedBlast = Instantiate(chargeUpEffect, firePoint.position, firePoint.rotation);

            GameObject chargedBlast = objectPooler.SpawnFromPool("chargeUpReg", firePoint.position, firePoint.rotation);

            chargedBlast.GetComponent<Arrow>().arrowSpeed = 15f;
            chargedBlast.GetComponent<Arrow>().damage = chargeUpDamage;
            chargedBlast.GetComponent<Arrow>().pushFor = 1000f;
            chargedBlast.GetComponent<Arrow>().isChargeUp = true;
            isCharging = false;
            player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 5;
        }
    }

    // Upgrades

    public void UpgradeWeapon()
    {
        GetComponent<SpriteRenderer>().sprite = bm.currBow.bowImage;
        damagePoint = bm.currBow.arrowDamage;
        dashDamage = bm.currBow.dashDamage;
        critMultiplier = bm.currBow.critMultiplier;
        cooldown = bm.currBow.cooldown;
        arrow = bm.currBow.arrowProj;
        chargeUpEffect = bm.currBow.chargeUpProj;
        lightcookie = bm.currBow.bowLightCookie;
        pushForce = bm.currBow.pushForce;
        chargeUpDamage = bm.currBow.chargeUpDamage;
        dashColor = bm.currBow.dashColor;
        CHARGE_COOLDOWN_LIMIT = bm.currBow.chargeCooldown;

        if (bm.tierIndex > 1)
        {
            eyesLight.color = bm.currBow.dashColor;
            player.bandana.color = bm.currBow.dashColor;
            player.bandanaLight.color = bm.currBow.lightTextColor;
        }


        chargeUpIndicLights.color = bm.currBow.dashColor;
    }


    IEnumerator SporeOverdrive()
    {
        float timeElapsed = 0.0f;
        overDriveMode = true;
        GameManager.instance.SporeOverdriveVolume.SetActive(true);
        cooldown = 0.4f;
        while (timeElapsed <= 0.1f)
        {
            if (PauseMenu.GameIsPaused)
            {
                GameManager.instance.SporeOverdriveVolume.SetActive(false);
                overDriveMode = false;
                yield break;
            }
            if (canShootAgain && (playerCurrStamina - sDecrease >= 0))
            {

                lastShot = true;
                Shoot();
                StartCoroutine(ShootingDelay());
            }
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        GameManager.instance.SporeOverdriveVolume.SetActive(false);
        overDriveMode = false;
    }

    void LeechShot(float adjustment, int z)
    {
        Vector2 adjustedPosition = new Vector2(firePoint.position.x + adjustment, firePoint.position.y);

        Quaternion adjustedRotation = transform.rotation * Quaternion.Euler(0, 0, z);
        //GameObject arrowShotOut = Instantiate(arrow, firePoint.position, adjustedRotation);

        GameObject leechArrow = objectPooler.SpawnFromPool("arrow_leech", adjustedPosition, adjustedRotation);

        //GameObject leechArrow = Instantiate(chargeUpEffect, adjustedPosition, adjustedRotation);
        // leechArrow.GetComponent<Arrow>().arrowSpeed = 15f;

        leechArrow.GetComponent<Arrow>().damage = damagePoint;
        leechArrow.GetComponent<Arrow>().critMult = critMultiplier;
        leechArrow.GetComponent<Arrow>().pushFor = 1000f;
        leechArrow.GetComponent<Arrow>().isChargeUp = false;

        if (piercingPerkEnabled) leechArrow.GetComponent<Arrow>().isPiercingArrow = true;
        //leechArrow.transform.localScale = new Vector3(3f, 3f, 1f);
    }

    IEnumerator LeechOverDrive()
    {
        float timeElapsed = 0.0f;
        overDriveMode = true;
        GameManager.instance.LeechOverDriveVolume.SetActive(true);
        
        while (timeElapsed <= 0.5f)
        {
            if (PauseMenu.GameIsPaused)
            {

                overDriveMode = false;
                GameManager.instance.LeechOverDriveVolume.SetActive(false);

                yield break;
            }

            timeElapsed += Time.deltaTime;
            yield return null;

        }
        overDriveMode = false;
        GameManager.instance.LeechOverDriveVolume.SetActive(false);
    }

    IEnumerator SeekerOverdrive()
    {
        float timeElapsed = 0.0f;
        overDriveMode = true;
        float realCooldown = cooldown;
        GameManager.instance.OverDriveVolume.SetActive(true);

        while (timeElapsed <= 1.5f)
        {
            if (PauseMenu.GameIsPaused)
            {
                cooldown = realCooldown;
                overDriveMode = false;
                GameManager.instance.OverDriveVolume.SetActive(false);

                yield break;
            }
            if (canShootAgain)
            {

                lastShot = true;
                Shoot();
                StartCoroutine(ShootingDelay());
            }
            else if (canShootAgain)
            {
                cooldown = realCooldown;
                overDriveMode = false;
                GameManager.instance.OverDriveVolume.SetActive(false);

                yield break;
            }
            cooldown = 0.1f;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        GameManager.instance.OverDriveVolume.SetActive(false);
        cooldown = realCooldown;
        overDriveMode = false;
    }

    void SeekerSprayShot(float posAdj, float angleAdj)
    {
        // Meant To Create One Of the Seeker Shots
        Vector2 adjustedPosition = new Vector2(firePoint.position.x, firePoint.position.y + posAdj);

        Quaternion adjustedRotation = transform.rotation * Quaternion.Euler(0, 0, 90 + angleAdj);

        GameObject arrowShotOut = Instantiate(chargeUpEffect, adjustedPosition, adjustedRotation);
        arrowShotOut.GetComponent<CircleCollider2D>().radius = 0.5f;
        arrowShotOut.GetComponent<GoldenArrow>().isChargeUp = true;
        if (posAdj == 0 && angleAdj == 0) arrowShotOut.GetComponent<GoldenArrow>().arrowSpeed = 35f;
        else arrowShotOut.GetComponent<GoldenArrow>().arrowSpeed = 40f;

    }

    IEnumerator ShootingDelay()
    {
        float timePassed = 0;
        canShootAgain = false;
        while (timePassed < cooldown)
        {

            if (PauseMenu.GameIsPaused)
            {
                canShootAgain = true;
                yield break;
            }

            canShootAgain = false;
            timePassed += Time.deltaTime;

            yield return null;
        }

        canShootAgain = true;
    }
    void CreateSprayShot()
    {

        Quaternion adjustedRotation = transform.rotation * Quaternion.Euler(0, 0, 90);
        //GameObject chargedBlast = Instantiate(chargeUpEffect, firePoint.position, adjustedRotation);
        GameObject chargedBlast = objectPooler.SpawnFromPool("sporeBlast", firePoint.position, adjustedRotation);

        chargedBlast.GetComponent<GrowthArrow>().damage = chargeUpDamage;
        chargedBlast.GetComponent<GrowthArrow>().isChargeUp = true;
        // chargedBlast.GetComponent<GrowthArrow>().childrenOfMain = true;

        player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 10;
    }
    void RandomShootingNoise(float adjustment)
    {
        int randNum = Random.Range(0, 111);
        if (randNum > 90)
        {
            soundManager.Play("shoot_1", adjustment);
        }
        else if (randNum > 70)
        {
            soundManager.Play("shoot_1", adjustment);
        }
        else if (randNum > 50)
        {
            soundManager.Play("shoot_3", adjustment);
        }
        else if (randNum > 30)
        {
            soundManager.Play("shoot_1", adjustment);
        }
        else
        {
            soundManager.Play("shoot_5", adjustment);
        }
    }


    void CreateRammerShotChunk()
    {
        Quaternion adjustedRotation = transform.rotation * Quaternion.Euler(0, 0, Random.Range(50, 131));


        // GameObject chargedBlast = Instantiate(chargeUpEffect, firePoint.position, adjustedRotation);
        GameObject chargedBlast = objectPooler.SpawnFromPool("chargeUpRammerChunk", firePoint.position, adjustedRotation);


        chargedBlast.GetComponent<Arrow>().arrowSpeed = 15f;
        chargedBlast.GetComponent<Arrow>().damage = chargeUpDamage + 3;
        chargedBlast.GetComponent<Arrow>().pushFor = 1000f;
        chargedBlast.GetComponent<Arrow>().isChargeUp = true;
        chargedBlast.transform.localScale = new Vector3(Random.Range(2.4f, 4f), Random.Range(2.4f, 4f), 1);

    }
    void RammerChargeUpShot()
    {
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
        CreateRammerShotChunk();
    }

    void LongShotChargeUpShot()
    {
        // GameObject chargedBlast = Instantiate(chargeUpEffect, firePoint.position, firePoint.rotation);
        GameObject chargedBlast = objectPooler.SpawnFromPool("chargeUpLongShot", firePoint.position, firePoint.rotation);

        chargedBlast.GetComponent<Arrow>().arrowSpeed = 25f;
        chargedBlast.GetComponent<Arrow>().damage = chargeUpDamage;
        chargedBlast.GetComponent<Arrow>().pushFor = 1000f;
        chargedBlast.GetComponent<Arrow>().isChargeUp = true;
        chargedBlast.transform.localScale = new Vector3(4f, 2f, 1f);
    }


    void CreateClawShot(int z, bool isDashShard)
    {
        Quaternion adjustedRotation = transform.rotation * Quaternion.Euler(0, 0, z);
        Vector2 adjustedPosition = new Vector2(firePoint.position.x, firePoint.position.y + Random.Range(-0.5f, 0.5f));
        // GameObject arrowShotOut = Instantiate(arrow, firePoint.position, adjustedRotation);
        // GameObject arrowShotOut = objectPooler.SpawnFromPool("arrow_claw", firePoint.position, adjustedRotation);
        GameObject arrowShotOut = objectPooler.SpawnFromPool("arrow_claw", adjustedPosition, adjustedRotation);

        arrowShotOut.GetComponent<Arrow>().arrowSpeed = 20f;
        arrowShotOut.GetComponent<Arrow>().damage = damagePoint;
        arrowShotOut.GetComponent<Arrow>().critMult = critMultiplier;

        arrowShotOut.GetComponent<Arrow>().isDashOfClaw = isDashShard;
        if (piercingPerkEnabled) arrowShotOut.GetComponent<Arrow>().isPiercingArrow = true;
        //player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 50;
    }
    void ClawShot()
    {
        
        

        // if (Random.Range(0, 10) > 6)
        // {
        //     CreateClawShot(Random.Range(100, 105), false);
        //     CreateClawShot(Random.Range(75, 80), false);
        // }
        CreateClawShot(Random.Range(95, 100), false);
        CreateClawShot(90, false);
        CreateClawShot(Random.Range(80, 85), false);
       

    }
    IEnumerator ClawOverdrive()
    {

        float timeElapsed = 0.0f;
        //float timeSinceCursorDetection = 0.0f;
        float normalDashDistance = player.dashDistance;
        float normalDashIncr = player.dashIncr;
        player.dashDistance = 0.15f;
       // Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        //Vector2 playerPosition = player.transform.position;
        GameManager.instance.ClawOverDriveVolume.SetActive(true);
        lastOverDriveTime = 0.0f;
       
        insaneModeActive = true;
        player.dashIncr = 0.25f;
        while (timeElapsed < 0.25f)
        {

            if (PauseMenu.GameIsPaused)
            {
                GameManager.instance.ClawOverDriveVolume.SetActive(false);
                player.dashDistance = normalDashDistance;
                player.isDashing = false;
                insaneModeActive = false;
                yield break;
            }
            //  mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

            if (Time.time - ComboManager.instance.recentDashKillTime <= 0.1f)
            {
                timeElapsed = 0.1f;
                // player.dashIncr = 0.29f; CreateClawShot(Random.Range(65, 116), true);
                // CreateClawShot(Random.Range(65, 116), true);
                // CreateClawShot(Random.Range(65, 116), true);
                CreateClawShot(50, true);
                CreateClawShot(130, true);
               
            }

            player.dashIncr -= 0.02f;

            //mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            //  float angleOfDash = Mathf.Atan2(mousePosition.y - playerPosition.y, mousePosition.x - playerPosition.x) * 180 / Mathf.PI;

            //    Quaternion dashDirToEuler = Quaternion.Euler(0, 0, angleOfDash);

            //     Vector2 dashDirection = dashDirToEuler * Vector2.right;
            
            // if (!player.isDashing) StartCoroutine(player.DashMode(dashDirection));
            if (!player.isDashing) StartCoroutine(player.DashMode(controlsHandler.dashDirection));

            timeElapsed += Time.deltaTime;
            yield return null;
        }
        
        player.dashIncr = normalDashIncr;
        player.dashDistance = normalDashDistance;
        insaneModeActive = false;
        GameManager.instance.ClawOverDriveVolume.SetActive(false);
    }

}
