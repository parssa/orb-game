﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class DialogueManager : MonoBehaviour
{

	public static DialogueManager instance;

	void Awake()
	{
		instance = this;
	}
	
	public Animator animator;


	public TextMeshProUGUI nameText;

	[Header("Dialogue Mode")]
	public GameObject dialogueContent;
	public TextMeshProUGUI dialogueText;

	[Header("Weapons Mode")]
	public GameObject weaponsContent;
	private bool weaponChoiceMode;
	public BowManager bm;
	public TextMeshProUGUI bowDesc;
	public TextMeshProUGUI chargeUpHeader;
	public TextMeshProUGUI chargeUpText;
	public TextMeshProUGUI classHeader;
	public TextMeshProUGUI classText;

	
	public Queue<string> sentences;

	//private bool notPrintingCauseFormatting = false;

	// Use this for initialization
	void Start () {
		sentences = new Queue<string> ();
		weaponChoiceMode = false;
		dialogueContent.SetActive(true);
		weaponsContent.SetActive(false);
	}

	public void StartDialogue (Dialogue dialogue) {

		if (dialogue.isPartOfWeaponChoice) weaponChoiceMode = true;
		else weaponChoiceMode = false;
		
		// animator.SetBool ("IsOpen", true);

		if (weaponChoiceMode)
		{
			dialogueContent.SetActive(false);
			weaponsContent.SetActive(true);

			if (dialogue.leftChoice) 
			{
				nameText.text = bm.LeftOption().bowName;
				nameText.color = bm.LeftOption().lightTextColor;
				
				bowDesc.text = bm.LeftOption().description;
				bowDesc.color = bm.LeftOption().darkTextColor;
				
				
				classHeader.color = bm.LeftOption().darkTextColor;
				classText.text = bm.LeftOption().weaponClass.ToString();
				classText.color = bm.LeftOption().lightTextColor;
				
				chargeUpHeader.color = bm.LeftOption().darkTextColor;
				chargeUpText.color = bm.LeftOption().lightTextColor;
				chargeUpText.text = bm.LeftOption().chargeUpDescription;

			}
			if (!dialogue.leftChoice)
			{
				nameText.text = bm.RightOption().bowName;
				nameText.color = bm.RightOption().lightTextColor;
				
				bowDesc.text = bm.RightOption().description;
				bowDesc.color = bm.RightOption().darkTextColor;
				
				classHeader.color = bm.RightOption().darkTextColor;
				classText.text = bm.RightOption().weaponClass.ToString();
				classText.color = bm.RightOption().lightTextColor;
				
				chargeUpHeader.color = bm.RightOption().darkTextColor;
				chargeUpText.color = bm.RightOption().lightTextColor;
				chargeUpText.text = bm.RightOption().chargeUpDescription;
			}
		}
		else
		{
			dialogueContent.SetActive(true);
			weaponsContent.SetActive(false);
			
			nameText.text = dialogue.name;
		}
		
		


		sentences.Clear ();

		
		foreach (string sentence in dialogue.sentences) {

			sentences.Enqueue (sentence);

		}

		DisplayNextSentence ();

	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0) {
			
			EndDialogue ();
			return;

		}

		string sentence = sentences.Dequeue ();
		StopAllCoroutines ();
		StartCoroutine (TypeSentence (sentence));


	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray()) {

			// if (letter == '<')
			// {
			// 	notPrintingCauseFormatting = true;
			// }
			// if (letter == '>')
			// {
			// 	notPrintingCauseFormatting = false;
			// }
			// if (!notPrintingCauseFormatting && letter != '>') dialogueText.text += letter;
			dialogueText.text += letter;
			
			yield return null;

		}
	}

	public void EndDialogue()
	{
		animator.SetBool ("IsOpen", false);

	}
}
