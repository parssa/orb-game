﻿using UnityEngine;

public struct Damage  {

	public Vector3 origin;
	public int damageAmount;
	public float pushForce;
	public bool isCrit;
	public enum damageType
	{
		Arrow, 
		Dash, 
		Orb, 
		Turret,
		ChargeAttack
	}
	public damageType originType;

}
