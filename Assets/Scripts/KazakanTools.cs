﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;





namespace ParssaProgramming
{
    
    
    public class ParssaAudio : MonoBehaviour
    {
        /// <summary>
        /// The same as "PlayClipAtPoint" however can edit values such as AudioMixerGroup, pitch, etc...
        /// </summary>
        /// <param name="clip"></param> the clip to play
        /// <param name="pos"></param> position at which to play, normally transform.position
        /// <returns></returns>
        public static AudioSource PlayAudioClip(AudioClip clip, Vector3 pos) 
        {

           
            GameObject tempGO = new GameObject("TempAudio"); // create the temp object
            tempGO.transform.position = pos; // set its position
            
            AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
            aSource.clip = clip; // define the clip
            // set other aSource properties here, if desired
            aSource.spatialBlend = 0.5f;
	    
	    
            aSource.Play(); // start the sound
            Destroy(tempGO, clip.length); // destroy object after clip duration
            return aSource; // return the AudioSource reference
        }
    }
}
