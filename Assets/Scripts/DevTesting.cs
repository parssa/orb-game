﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DevTesting : MonoBehaviour
{

    public Player player;
    public static bool KILL_ALL = false;
    public Orb orb;
    public GameManager gameManager;
    public BowManager bowManager;
    public PerkManager perkManager;
    public Animator bowLightAnim;

    public static bool DEVMODEACTIVE = false;
    // public UnityEvent killAllEvent;
    
    
    // private void Start()
    // {
    //     if (killAllEvent == null)
    //     {
    //         killAllEvent = new UnityEvent();
    //     }
    // }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha1)) gameManager.UnlockRequestedRegion(GameManager.Regions.BOTTOM);
        if (Input.GetKeyDown(KeyCode.Alpha2)) gameManager.UnlockRequestedRegion(GameManager.Regions.LEFT);
        if (Input.GetKeyDown(KeyCode.Alpha3)) gameManager.UnlockRequestedRegion(GameManager.Regions.RIGHT);
        if (Input.GetKeyDown(KeyCode.Alpha4)) gameManager.UnlockRequestedRegion(GameManager.Regions.TOP);
       
        if ((Input.GetKey(KeyCode.Return) && Input.GetKey(KeyCode.P) && !DEVMODEACTIVE))
        {
            DEVMODEACTIVE = true;


            bowLightAnim.SetBool("devModeActive", true);
            DamagePopup.Create(player.transform.position, 10, true, DamagePopup.DEVMODE);
        }

        if (DEVMODEACTIVE)
        {
            if (Input.GetKeyDown(KeyCode.M)) gameManager.GrantSouls(150);

            if (Input.GetKeyDown(KeyCode.N)) KillAll();

            if (Input.GetKeyDown(KeyCode.H))
            {

                bowManager.tierIndex = 1;
                bowManager.chosenType = Bow.ClassType.RANGER;
                bowManager.UpgradeBow(0);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                bowManager.tierIndex = 1;
                bowManager.chosenType = Bow.ClassType.RANGER;
                bowManager.UpgradeBow(1);
            }

            if (Input.GetKeyDown(KeyCode.K))
            {

                bowManager.tierIndex = 1;
                bowManager.chosenType = Bow.ClassType.BRUTE;
                bowManager.UpgradeBow(0);
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                bowManager.tierIndex = 1;
                bowManager.chosenType = Bow.ClassType.BRUTE;
                bowManager.UpgradeBow(1);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                player.Heal(100);
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                orb.RestoreTheOrb(100);
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                perkManager.GrantPerkToPlayer(PerkManager.Perk.PerkType.HEALTH_REGEN);
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                perkManager.GrantPerkToPlayer(PerkManager.Perk.PerkType.AGILITY);
            }

            if (Input.GetKeyDown(KeyCode.Y))
            {
                perkManager.GrantPerkToPlayer(PerkManager.Perk.PerkType.RESISTANCE);
            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                perkManager.GrantPerkToPlayer(PerkManager.Perk.PerkType.STAMINA_FAST);
            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                GameManager.instance.shrineSouls += 40;
            }

        }
    }

    void Start()
    {
        foreach (GameObject g in devButtons)
        {
            g.SetActive(false);
        }
    }

    
    public void DevTestingButton()
    {
        if (!DEVMODEACTIVE)
        {
            DEVMODEACTIVE = true;
        }
        gameManager.GrantSouls(UnityEngine.Random.Range(40, 74));
        player.Heal(100);
        orb.RestoreTheOrb(100);
        GameManager.instance.shrineSouls += UnityEngine.Random.Range(24, 32);

        foreach (GameObject g in devButtons)
        {
            g.SetActive(true);
        }
        
    }

    public GameObject[] devButtons;

    public void KillAll()
    {
        Debug.Log("PRESSED");
        // if (!KILL_ALL) KILL_ALL = true;
        // if (KILL_ALL) KILL_ALL = false;

    }
    
    
    


    }
