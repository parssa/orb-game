﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenArrow : MonoBehaviour, IPooledObject
{
    public float arrowSpeed = 20f;
    private CameraMotor cameraShake;

    private AudioSource audioSource;
    private AudioClip impact;

    private BoxCollider2D hitbox;

    private CircleCollider2D seekingZone;
    private bool lockedTarget;
    private Vector2 projectilePosition;

    private Vector2 startingPosition;


    private Transform targetTransform;
    private Vector2 targetPosition;

    public int damage;
    public int critMult;
    public float pushFor = 2f;


    private float timeAlive = 0.0f;
    private float breakTime = 0.35f;

    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;

    public bool isChargeUp = false;
    public bool isPiercingArrow = false;

    private int numEnemiesHit = 0;
    private Transform firstEnemyHit;
    private float timeMade = 0f;

    // Start is called before the first frame update
    void Start()
    {

        lockedTarget = false;

        impact = SoundManager.instance.GetSounds("ArrowHit").clip;

        //arrowSpeed = 30f;
        damage = Weapon.instance.damagePoint;
        if (isChargeUp) damage = Weapon.instance.chargeUpDamage;
        critMult = Weapon.instance.critMultiplier;
        cameraShake = CameraMotor.instance;
        hitbox = GetComponent<BoxCollider2D>();
        seekingZone = GetComponent<CircleCollider2D>();

        projectilePosition = transform.position;

        audioSource = GetComponent<AudioSource>();
        transform.localScale = new Vector3(2f, 2f, 1f);

        if (isChargeUp) breakTime = 0.3f;

    }

    public void OnObjectSpawn()
    {
        numEnemiesHit = 0;
        firstEnemyHit = null;
        timeMade = Time.time;

        projectilePosition = transform.position;
        transform.localScale = new Vector3(2f, 2f, 1f);
        timeAlive = 0.0f;

        if (isChargeUp) breakTime = 0.2f;

        damage = Weapon.instance.damagePoint;
        critMult = Weapon.instance.critMultiplier;
        lockedTarget = false;

        targetTransform = null;

        arrowSpeed = 20f;
        // Erase array on respawn;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }
    }



    // Update is called once per frame
    void Update()
    {

        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(4f, 4f, 1f), Time.deltaTime * 3f);
        if (lockedTarget && timeAlive < 0.1f)
        {
            lockedTarget = false;
        }
        if (!lockedTarget)
        {
            transform.Translate(Vector3.right * Time.deltaTime * arrowSpeed);

        }
        else if (lockedTarget)
        {
            if (targetTransform == null)
            {
                lockedTarget = false;
                targetPosition = new Vector2(1, 1);
            }

            Vector2 moveDelta = (targetPosition - projectilePosition).normalized;
            float dist = Vector2.Distance(transform.position, targetPosition);

            if (dist < 1f)
            {
                Debug.Log("Close");

                transform.position += new Vector3(moveDelta.x, moveDelta.y, 0).normalized * Time.deltaTime;
            }
            else transform.position += new Vector3(moveDelta.x, moveDelta.y, 0).normalized * (Time.deltaTime * arrowSpeed);
            //Vector2 moveDelta = Vector2.Lerp(transform.position, targetPosition, Time.deltaTime * arrowSpeed).normalized;



            Vector3 relativePos = new Vector3(targetPosition.x, targetPosition.y, 0) - transform.position;
            Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
            //Debug.Log(rotatedVectorToTarget);
            Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);
            transform.rotation = movementRotation;
        }



        seekingZone.OverlapCollider(filter, hits);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            SeekingEnemy(hits[i]);
            //Cleans array
            hits[i] = null;

        }

        if (isActiveAndEnabled)
        {
            hitbox.OverlapCollider(filter, hits);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i] == null)
                    continue;


                OnCollide(hits[i]);
                //Cleans array
                hits[i] = null;

            }
        }


        timeAlive += Time.deltaTime;
        if (timeAlive > breakTime)
        {
            BreakArrow();
        }

    }

    protected virtual void OnCollide(Collider2D coll)
    {
        int damageToDeal = 0;

        if (!transform.gameObject.activeSelf) return;
        if (coll.CompareTag("Fighter") || (coll.tag == "Shrine") || coll.CompareTag("Guardian")|| coll.CompareTag("Boss"))
        {
            if ((coll.tag == "Shrine") && ((coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN) ||
            (coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.CLOSED)))
            {
                return;
            }

            if ((coll.tag == "Guardian") && !coll.transform.GetComponent<Guardian>().activated) return;
            else if ((coll.tag == "Boss") && !coll.transform.GetComponent<Boss>().activated) return;
            int randNum = Random.Range(0, 100);
            if (randNum > 90)
            {
                damageToDeal = Random.Range(damage, (damage * 2) + critMult);
            }
            else if (randNum > 70)
            {
                damageToDeal = Random.Range(damage, damage + critMult + 2);
            }
            else
            {
                damageToDeal = Random.Range(1, damage + 1);
            }

            StartCoroutine(cameraShake.Shake(0.1f, 10f));

            Damage.damageType arrowType = Damage.damageType.Arrow;


            Damage dmg = new Damage
            {
                damageAmount = damageToDeal,
                origin = transform.position,
                pushForce = pushFor,
                isCrit = damageToDeal >= (damage * 2),
                originType = arrowType

            };

            if (coll.gameObject != null)
            {
                numEnemiesHit += 1;
                coll.SendMessage("RecieveDamage", dmg);
            }
            if (isPiercingArrow)
            {
                if (firstEnemyHit == null)
                {
                    PlayHitSound();
                }
                else
                {
                    if (coll.transform != firstEnemyHit)
                    {
                        PlayHitSound();
                    }
                }
            }
            else
            {
                PlayHitSound();
            }

            
            if (!isChargeUp) BreakArrow();
        }

    }

    void BreakArrow()
    {
        targetTransform = null;
        lockedTarget = false;

        if ((isPiercingArrow) && (numEnemiesHit < 2) && (Time.time - timeMade < breakTime))
        {
            damage = 2;
            critMult = 1;
            Debug.Log("havent done enough killing");
            return;
        }
        
        gameObject.SetActive(false);
    }

    protected virtual void SeekingEnemy(Collider2D coll)
    {

        if (coll.CompareTag("Fighter"))
        {
            if (!lockedTarget)
            {
                lockedTarget = true;
                targetTransform = coll.transform;
                targetPosition = targetTransform.position;
                arrowSpeed = 40f;
            }


        }
    }
    
    void PlayHitSound()
    {
        if (GameManager.instance.mastVol <= -78f) return;
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 2 - GameManager.instance.mastVol * 10);
        AudioSource.PlayClipAtPoint(impact, soundPos);
    }





}
