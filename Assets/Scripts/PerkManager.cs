﻿using System.Collections;
using System.Collections.Generic;
using CloudOnce;
using UnityEngine;


public class PerkManager : MonoBehaviour
{

    public static PerkManager instance;
    public Player player;
    public HubLayout hubLayout;
    private int numGuardiansKilled;
    public List<Guardian> allGuardians;
    public static bool AllPerksAcquired = false;

    public int NumGuardiansKilled => numGuardiansKilled;

    void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class Perk
    {
        public string perkName;
        public bool enabled = false;
        public enum PerkType
        {
            HEALTH_REGEN,
            AGILITY,
            RESISTANCE,
            STAMINA_FAST,
        }
        public PerkType perkType;
        public Sprite perkSprite;

        public string perkDescription;
        public Color lightPerkColor;
        public Color boldPerkColor;

    }
    public Perk[] perks;


    public Color PerkColor(Perk.PerkType perkType)
    {
        return FetchPerk(perkType).boldPerkColor;
    }

    // Get perk based on Enum
    Perk FetchPerk(Perk.PerkType perkType)
    {
        switch (perkType)
        {
            case Perk.PerkType.HEALTH_REGEN:
                return perks[0];

            case Perk.PerkType.AGILITY:
                return perks[1];

            case Perk.PerkType.RESISTANCE:
                return perks[2];

            case Perk.PerkType.STAMINA_FAST:
                return perks[3];

            default:
                return perks[0];
        }
    }

    // Grant Perk To Player
    public bool GrantPerkToPlayer(Perk.PerkType perkType)
    {
        StrengthenTheHerd();
        Perk currPerk = FetchPerk(perkType);
        if (!currPerk.enabled)
        {
            
            currPerk.enabled = true;
            player.RefreshPerkList(currPerk);
            hubLayout.AddPerkToPerksDisplay(currPerk.perkSprite);
            hubLayout.PerkAcquiredPopup(currPerk.perkSprite, currPerk);
            CharacterMenu.instance.AddPerkToPerksDisplayInPlayerMenu(currPerk.perkSprite);

            //player.perkList.Add(currPerk);
            return true;
        }
        // else it is already enabled;

        return false;
    }


    [System.Serializable]
    public class Blessing
    {
        public string blessingName;
        public bool enabled = false;
        public enum Type
        {

            TURRET_MULTISHOT,
            TURRET_RAPID_ONE, // NI
                              //   TURRET_RAPID_TWO, // NI
            TURRET_EXPLODE,
            TURRET_STRENGTH, // NI
            TURRET_CHEAP,

            POWERUP_INCREASE,
            POWERUP_DUALITY,
            POWERUP_STAMINA_MORE, // NI
            POWERUP_HEALTH_REGEN, // NI
            POWERUP_HEALTH_MORE, // NI
            POWERUP_MAGNETIC, // NI
            POWERUP_ALTAR, // NI

            PLAYER_ENDWAVE_HEAL_ONE,
            //   PLAYER_ENDWAVE_HEAL_TWO, // NI
            PLAYER_PIERCING_ONE, // NI
            PLAYER_GUIDANCE_ONE, // NI
            PLAYER_BURST_SHOT, // NI

            NULL_BLESSING,
        }
        public Type blessingType;
        public Sprite blessingIcon;

        public string blessingDescription;
        public Color lightBlessingColor;
        // public Color boldBlessingColor;


    }

    //public Blessing[] allBlessings;
    public Queue<Blessing> activeBlessings = new Queue<Blessing>();
    public Blessing[] turretBlessings;
    public Blessing[] powerupBlessings;
    public Blessing[] playerBlessings;

    public Blessing nullBlessing;

    public Blessing[] currBlessings = new Blessing[3];



    //public int numBlessingsEnabled = 0;

    private int numBlessingsEquipped = 0;
    
    public int numBlessingSlotsAvailable = 1;
    public Animator platformLight;

    private int numOneEnabled = 0;
    private int numTwoEnabled = 0;
    private int numThreeEnabled = 0;

    private bool ranOutOfOption1 = false;
    private bool ranOutOfOption2 = false;
    private bool ranOutOfOption3 = false;


    void Start()
    {
        int shuffleNum = Random.Range(2, 4);
        for (int a = 1; a <= shuffleNum; a++)
        {
            GenerateOptions();
        }
            

    }

    public void GenerateOptions()
    {

        // Blessing[] optionFromEachGroup = new Blessing[3];

        
        
        Blessing randBlessing = turretBlessings[Random.Range(0, turretBlessings.Length)];
        if (randBlessing.enabled)
        {
            Debug.Log("Contains1");
            while (randBlessing.enabled)
            {
                if (numOneEnabled >= turretBlessings.Length)
                {
                    ranOutOfOption1 = true;
                    break;
                }

                randBlessing = turretBlessings[Random.Range(0, turretBlessings.Length)];
            }

        }

        if (ranOutOfOption1) randBlessing = nullBlessing;

        currBlessings[0] = randBlessing;

        randBlessing = powerupBlessings[Random.Range(0, powerupBlessings.Length)];
        if (randBlessing.enabled)
        {
            Debug.Log("Contains2");
            while (randBlessing.enabled)
            {
                if (numTwoEnabled >= powerupBlessings.Length)
                {
                    ranOutOfOption2 = true;
                    break;
                }
                randBlessing = powerupBlessings[Random.Range(0, powerupBlessings.Length)];
            }

        }

        if (ranOutOfOption2) randBlessing = nullBlessing;
        currBlessings[1] = randBlessing;

        randBlessing = playerBlessings[Random.Range(0, playerBlessings.Length)];
        if (randBlessing.enabled)
        {
            Debug.Log("Contains3");
            while (randBlessing.enabled)
            {
                if (numThreeEnabled >= playerBlessings.Length)
                {
                    ranOutOfOption3 = true;
                    break;
                }
                randBlessing = playerBlessings[Random.Range(0, playerBlessings.Length)];
            }

        }
        if (ranOutOfOption3) randBlessing = nullBlessing;
        currBlessings[2] = randBlessing;


        // Now we have an array of three options

    }


    public void ActivateBlessing(Blessing chosenBlessing)
    {
        activeBlessings.Enqueue(chosenBlessing);
        platformLight.SetBool("blessingOffered", false);
        // Debug.Log(activeBlessings.Count);
        chosenBlessing.enabled = true;
        Debug.Log(chosenBlessing.blessingName.ToString());
        CharacterMenu.instance.AddBlessingToOrbPage(chosenBlessing.blessingIcon, activeBlessings.Count - 1);
        numBlessingsEquipped += 1;
        if (numBlessingsEquipped >= 7)
        {
            if (!Achievements.SoBlessed.IsUnlocked)
            {
                Achievements.SoBlessed.Unlock();
            }
        }

        // GenerateOptions();
        switch (chosenBlessing.blessingType)
        {
            case Blessing.Type.TURRET_MULTISHOT:
                numOneEnabled += 1;
                GameManager.instance.TurretMultiBlessing();
                return;


            case Blessing.Type.TURRET_CHEAP:
                numOneEnabled += 1;
                GameManager.instance.TurretCheapBlessing();
                return;


            case Blessing.Type.TURRET_EXPLODE:
                numOneEnabled += 1;
                GameManager.instance.TurretExplodeBlessing();
                return;

            case Blessing.Type.PLAYER_ENDWAVE_HEAL_ONE:
                numThreeEnabled += 1;
                WaveSpawner.instance.endOfWaveHealPlayerBlessing = true;
                return;

            case Blessing.Type.POWERUP_INCREASE:
                numTwoEnabled += 1;
                PowerupManager.instance.odds = 10;
                return;

            case Blessing.Type.PLAYER_BURST_SHOT:
                numThreeEnabled += 1;
                Weapon.instance.burstPerkEquipped = true;
                return;

            case Blessing.Type.POWERUP_MAGNETIC:
                numTwoEnabled += 1;
                PowerupManager.instance.magneticPowerups = true;
                return;

            case Blessing.Type.POWERUP_DUALITY:
                numTwoEnabled += 1;
                PowerupManager.instance.ReplaceWithDuality();
                return;
            
            case Blessing.Type.PLAYER_PIERCING_ONE:
                numThreeEnabled += 1;
                Weapon.instance.piercingPerkEnabled = true;
                return;


            default:
                Debug.Log(chosenBlessing.ToString());
                return;

        }

    }

    public void StrengthenTheHerd()
    {

        numGuardiansKilled += 1;
        EnemyManager.instance.YolkEnemies(1, 0, 0.05f);
        if (numGuardiansKilled >= 4)
        {
            AllPerksAcquired = true;

            if (!Achievements.Supercharged.IsUnlocked)
            {
                Achievements.Supercharged.Unlock();
            }
        }
        for (int a = 0; a < allGuardians.Count; a++)
        {
            if (allGuardians[a] == null) continue;
            allGuardians[a].GetComponent<Guardian>().regularSpeed += 0.3f;
            allGuardians[a].GetComponent<Guardian>().num_shot_till_decide += 10;
            allGuardians[a].GetComponent<Guardian>().maxHitpoint += 100;
            allGuardians[a].GetComponent<Guardian>().hitpoint = allGuardians[a].GetComponent<Guardian>().maxHitpoint;


        }
    }






}
