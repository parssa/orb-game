﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeechSpike : MonoBehaviour
{
    private CameraMotor cameraShake;

    private Bow currentBow;

    private AudioClip impact;
    private AudioSource audioSource;

    private BoxCollider2D hitbox;


    [Header("Damage Values")]
    public float arrowSpeed = 20f;
    public int damage;
    public int critMult;
    public float pushFor = 2f;
    //private int numEnemiesHit;


    //private float breakTime = 2f;
    //private float timeAlive;

    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;

    public LineRenderer lineRenderer;
    private Vector3[] positions;

    private float distanceTravelled;
    private bool retracting = false;
    private Vector2 currPos;
    private Vector2 startingPos;

    // Start is called before the first frame update
    void Start()
    {
        cameraShake = Camera.main.GetComponent<CameraMotor>();

        currentBow = BowManager.instance.currBow;
        audioSource = GetComponent<AudioSource>();
        hitbox = GetComponent<BoxCollider2D>();

        impact = SoundManager.instance.GetSounds("ArrowHit").clip;
        audioSource.Pause();

       // numEnemiesHit = 0;
        
        //timeAlive = 0.0f;

        lineRenderer.enabled = true;
        lineRenderer.useWorldSpace = true;
        lineRenderer.positionCount = 2;
        positions = new Vector3[2];
        positions[1] = new Vector3(Weapon.instance.firePoint.transform.position.x, Weapon.instance.firePoint.transform.position.y, 0.0f);
        positions[0] = transform.position;

        lineRenderer.SetPositions(positions);
        


        currPos = transform.position;
        startingPos = transform.position;
        distanceTravelled = 0.0f;
        //lineRenderer.useWorldSpace = true;
        GameManager.instance.LeechOverDriveVolume.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        currPos = transform.position;
        
        distanceTravelled = Vector2.Distance(startingPos, currPos);
        

        if (!retracting) transform.Translate(Vector3.right * Time.deltaTime * arrowSpeed * 1f);
        else transform.position = Vector2.MoveTowards(transform.position, Weapon.instance.firePoint.transform.position, 0.6f);

        if (distanceTravelled >= 3 && !retracting) 
        {   
            retracting = true;  
            distanceTravelled = 3f;
            startingPos = Weapon.instance.firePoint.transform.position;
           
        }
        
        if (retracting) startingPos = Weapon.instance.firePoint.transform.position;

        if (retracting && ((distanceTravelled <= 0.2f ) || (currPos == new Vector2(Weapon.instance.firePoint.transform.position.x, Weapon.instance.firePoint.transform.position.y))))
        {

         
            GameManager.instance.LeechOverDriveVolume.SetActive(false);
            Destroy(gameObject);
        } 
        //if (retracting) Destroy(gameObject, 0.5f);

       

        hitbox.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;

        }
        positions[1] = new Vector3(Weapon.instance.firePoint.transform.position.x, Weapon.instance.firePoint.transform.position.y, 0.0f);
        positions[0] = transform.position;
        lineRenderer.SetPositions(positions);
        //Destroy(gameObject, breakTime);
    }

    protected virtual void OnCollide(Collider2D coll)
    {
        int damageToDeal = 0;
        if (coll.CompareTag("Fighter") || coll.CompareTag("Guardian"))
        {
            bool randNum = Random.Range(0, 100) > 70;
            if (randNum)
            {
                damageToDeal = Random.Range(damage, damage * critMult + 1);
            } else
            {
                damageToDeal = damage;
            }
            
            StartCoroutine(cameraShake.Shake(0.1f, 10f));

            Damage.damageType arrowType = Damage.damageType.ChargeAttack;
            Damage dmg = new Damage
            {
                damageAmount = damageToDeal,
                origin = transform.position,
                pushForce = pushFor,
                isCrit = damageToDeal >= damage * critMult,
                originType = arrowType,

            };

            if (dmg.isCrit && damageToDeal > 1)
            {
                SoundManager.instance.Play("CriticalHit", 0);
            }
            if (damageToDeal > 15)  Player.instance.Heal(1);
            else  Player.instance.Heal(1);
            var position = transform.position;
            Vector3 soundPos = new Vector3(position.x, position.y, 2 - GameManager.instance.mastVol * 10);
            AudioSource.PlayClipAtPoint(impact, soundPos);
            coll.SendMessage("RecieveDamage", dmg);
            Destroy(gameObject, 0.5f);
            GameManager.instance.LeechOverDriveVolume.SetActive(false);
        }
    }

}
