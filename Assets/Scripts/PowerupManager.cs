﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour
{
    public static PowerupManager instance;

    private PerkManager perkManager;
    private TutorialManager tutorialManager;

    private HeaderPopup headerPopup;


    public float pickupCreationTime = 0f;
    public const float POWERUP_CREATION_COOLDOWN = 5f;

    public bool canSpawnPowerup = true;

    private List<GameObject> potential_drops = new List<GameObject>();

    private GameObject healthDrop;
    
    private GameObject orbHealthDrop;
    private GameObject staminaDrop;

    public GameObject blessingDrop;
    public int numKilledSinceLastDrop = 0;
    private int intervalTillNext = 10;
    public bool blessingPickedUp = true;

    public int odds = 5;
    public int oddsBlessing = 3;
    private int numBlessingsSpawned = 1;

    public bool magneticPowerups = false;
    public bool dualityPowerups = false;

    private Vector3 altarPosition = new Vector3(0.066f, 1.29f, 0f);


    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        potential_drops.Add(Resources.Load("StaminaDrop") as GameObject); // 0
        potential_drops.Add(Resources.Load("HealthDrop") as GameObject); // 1
        potential_drops.Add(Resources.Load("OrbPickup") as GameObject); // 2

        healthDrop = Resources.Load("HealthDrop") as GameObject;
       
        orbHealthDrop = Resources.Load("OrbPickup") as GameObject;
        staminaDrop = Resources.Load("StaminaDrop") as GameObject;

        headerPopup = HeaderPopup.instance;
        tutorialManager = TutorialManager.instance;
        perkManager = PerkManager.instance;


    }


    public void DropLoot(Vector2 pos)
    {
        int rnGesus = Random.Range(0, 100);
        numKilledSinceLastDrop += 1;



        // If it is the second last enemy, drop something
        if (WaveSpawner.instance.NumEnemiesLeftInWave() == 2)
        {

            //Instantiate(potential_drops[Random.Range(0, potential_drops.Count - 1)], pos, transform.rotation);
            GameObject drop = Instantiate(DecideDrop(), pos, transform.rotation);
            drop.GetComponent<PowerUp>().isMagnetic = magneticPowerups;
            pickupCreationTime = Time.time;
        }
        else if (rnGesus <= odds)
        {
            if (Time.time - pickupCreationTime > POWERUP_CREATION_COOLDOWN)

            {

                //Instantiate(potential_drops[Random.Range(0, potential_drops.Count - 1)], pos, transform.rotation);
                GameObject drop = Instantiate(DecideDrop(), pos, transform.rotation);
                drop.GetComponent<PowerUp>().isMagnetic = magneticPowerups;
                pickupCreationTime = Time.time;
            }
        }
        else if (CanDropBlessing)
        {
            
            if (perkManager.numBlessingSlotsAvailable > perkManager.activeBlessings.Count)
            {
                if (numKilledSinceLastDrop > intervalTillNext * 3)
                {
                    if (blessingPickedUp) SpawnBlessing();

                }
                else if (numKilledSinceLastDrop > intervalTillNext)
                {
                    rnGesus = Random.Range(0, 100);
                    if (rnGesus <= oddsBlessing)
                    {
                        if (blessingPickedUp) SpawnBlessing();

                    }

                }
            }
            
            
        }
    }

    private bool CanDropBlessing => (tutorialManager.hasCompletedTurretTutorial);


    GameObject DecideDrop()
    {

        float currHealth = Player.instance.GetHealth();
        float maxHealth = Player.instance.GetMaxHealth();
        float healthPercent = (currHealth / maxHealth) * 100;

        float currOrbHealth = OrbHitBox.instance.hitpoint;
        float maxOrbHealth = OrbHitBox.instance.maxHitpoint;
        float orbHealthPercent = (currOrbHealth / maxOrbHealth) * 100;

        // Debug.Log("Health Percent: " + healthPercent);
        // Debug.Log("Orb Health Percent: " + orbHealthPercent);

        // Urgent Need of Certain Powerup
        if (healthPercent < 20) return healthDrop;
        if (orbHealthPercent < 20) return orbHealthDrop;

        // Everything is Okay
        if ((orbHealthPercent >= 80 && healthPercent >= 80) || (Player.instance.currStamina <= 1)) return staminaDrop;

        // Which One Do you nee more
        if ((orbHealthPercent < healthPercent) && (healthPercent < 60)) return orbHealthDrop;
        else if ((orbHealthPercent >= healthPercent) && (healthPercent < 60)) return healthDrop;

        // Safety Net
        if (healthPercent < 85) return healthDrop;
        else if (orbHealthPercent < 85) return orbHealthDrop;
        else return staminaDrop;
    }

    void SpawnBlessing()
    {
        perkManager.platformLight.SetBool("blessingOffered", true);
        Instantiate(blessingDrop, altarPosition, transform.rotation);
        numKilledSinceLastDrop = 0;
        intervalTillNext += 10 * numBlessingsSpawned;

        blessingPickedUp = false;

        CharacterMenu.instance.BlessingAtAltar(true);

        if (!headerPopup.gameObject.activeSelf)
        {
            headerPopup.gameObject.SetActive(true);
            HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
            unlockedPrompt.title = "BLESSING OFFERED";
            unlockedPrompt.text = "Collect at Altar";
            unlockedPrompt.type = HeaderPopup.PromptType.ORB;
            
            unlockedPrompt.textColor = Color.white;

            HeaderPopup.instance.PlayPrompt(unlockedPrompt);
        }
    }

    public void ReplaceWithDuality()
    {
        healthDrop = Resources.Load("DualityDrop") as GameObject;
        orbHealthDrop = Resources.Load("DualityDrop") as GameObject;
    }
}
