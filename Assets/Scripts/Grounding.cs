﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounding : MonoBehaviour
{
    public bool collidingWithWorldBorder = false;

    private Vector2 circleCenterPos;
    
    public Vector2 collisionPosition = Vector2.zero;
    public Vector2 whereCollisionCameFrom;

    private Transform playerTransform;
    private void Start()
    {
        circleCenterPos = transform.position;
        playerTransform = transform.parent;
    }

    void CalculateCollisionEntryAngle(Vector2 collisionPos)
    {
        
    }
    
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 12) // 12 is world border layer
        {
            
            collidingWithWorldBorder = true;
            collisionPosition = collision.transform.position;
          //  Debug.Log(collisionPosition);
            whereCollisionCameFrom = (collision.contacts[0].point - (Vector2)transform.position);

            var testerVar = new Vector2(collision.contacts[0].point.x - playerTransform.position.x,
                collision.contacts[0].point.y - playerTransform.position.y );
            Debug.Log( testerVar);
            // Debug.Log(whereCollisionCameFrom);
            if (transform.parent.GetComponent<Player>().isDashing) transform.parent.GetComponent<Player>().isDashing = false;
        }   
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 12)
        {
           // Debug.Log("Collision Exited");
            collidingWithWorldBorder = false;
        }
    }
}
