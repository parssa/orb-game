﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class WeaponPanel : MonoBehaviour
{
    [Header ("Weapon (Current)")]
#pragma warning disable 0649
	[SerializeField] private BowManager bm;
#pragma warning restore 0649
	public Image currWeaponSprite;
	public TextMeshProUGUI bowName;
	public TextMeshProUGUI bowDescription;
	public TextMeshProUGUI currWeaponDamage;
	public TextMeshProUGUI currWeaponCritDamage;
	public TextMeshProUGUI currWeaponDashDamage;
	public TextMeshProUGUI currWeaponRpm;


	[Header("Weapon (Button)")]
	public TextMeshProUGUI weaponFee;
	

	[Header("Weapon Choice Section")]

	public Animator weaponChoice;
	public GameObject choiceOne;
	public GameObject choiceTwo;
	public Image choiceOneImage;
	public TextMeshProUGUI choiceOneName;
	public Image choiceTwoImage;
	public TextMeshProUGUI choiceTwoName;
	public Image weaponPanelCover;
	public int leftOptionNum;
	public int rightOptionNum;
	private bool showCover;

	private bool hoveringOverLeftOption = false;
	private bool hoveringOverRightOption = false;
    // Start is called before the first frame update
    void Start()
    {
        bowName.text = bm.currBow.bowName;
		bowDescription.text = bm.currBow.description;
		currWeaponSprite.sprite = bm.currBow.bowImage;
		currWeaponDamage.text = " DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponCritDamage.text = " CRIT DAMAGE: " + (bm.currBow.arrowDamage * bm.currBow.critMultiplier).ToString();
		currWeaponDashDamage.text = " DASH DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponRpm.text = " RPM: " + (60 / bm.currBow.cooldown).ToString();

        weaponPanelCover.color = Color.clear;
		showCover = false;
		weaponFee.text = bm.bowPrices[bm.weaponIndex].ToString();
    }

    // Update is called once per frame
    void Update()
    {
        weaponChoice.SetBool("leftHover", hoveringOverLeftOption);
		weaponChoice.SetBool("rightHover", hoveringOverRightOption);
		if (showCover)
		{
			
			weaponPanelCover.color = Color.Lerp(Color.clear, Color.white, 0.1f);
			
		}
		if (!showCover)
		{
			weaponPanelCover.color = Color.clear;
		}
    }
    public void OnWeaponUpgradeClick()
	{
		choiceOneImage.sprite = bm.LeftOption().bowImage;
		choiceOneName.text = bm.LeftOption().bowName;
		choiceTwoImage.sprite = bm.RightOption().bowImage;
		choiceTwoName.text = bm.RightOption().bowName;

		if (BowManager.instance.TryUpgradeBow())
        {
			// SUSPECT
			

			weaponChoice.SetTrigger("show");
			hoveringOverLeftOption = true;
			hoveringOverRightOption = false;

			showCover = true;

			bowName.text = bm.currBow.bowName;
			bowDescription.text = bm.currBow.description;
			currWeaponSprite.sprite = bm.currBow.bowImage;
			currWeaponDamage.text = " DAMAGE: " + bm.currBow.arrowDamage.ToString();
			currWeaponCritDamage.text = " CRIT DAMAGE: " + (bm.currBow.arrowDamage * bm.currBow.critMultiplier).ToString();
			currWeaponDashDamage.text = " DASH DAMAGE: " + bm.currBow.arrowDamage.ToString();
			currWeaponRpm.text = " RPM: " + (60 / bm.currBow.cooldown).ToString();

			if (BowManager.instance.MaxWeapon())
            {
				Debug.Log("TRIGGERED NOW");
			} else
            {
				weaponFee.text = bm.bowPrices[bm.weaponIndex].ToString();
			}
		}
	}

	public void OnLeftChoiceClick() 
	{
		bm.UpgradeBow(0);
		
		choiceOne.transform.SetAsLastSibling();

		bowName.text = bm.currBow.bowName;
		bowDescription.text = bm.currBow.description;
		currWeaponSprite.sprite = bm.currBow.bowImage;
		currWeaponDamage.text = " DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponCritDamage.text = " CRIT DAMAGE: " + (bm.currBow.arrowDamage * bm.currBow.critMultiplier).ToString();
		currWeaponDashDamage.text = " DASH DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponRpm.text = " RPM: " + (60 / bm.currBow.cooldown).ToString();

		weaponChoice.SetTrigger("leftSelected");
		showCover = false;
	}

	public void OnLeftChoiceHover()
	{
		Debug.Log("Hovering Over Left");
		hoveringOverLeftOption = true;
		
		
	}

	public void OnLeftChoiceExit()
	{
		Debug.Log("Exited Left");
		hoveringOverLeftOption = false;

		
	}

	public void OnRightChoiceClick() 
	{
		bm.UpgradeBow(1);

		choiceTwo.transform.SetAsLastSibling();

		bowName.text = bm.currBow.bowName;
		bowDescription.text = bm.currBow.description;
		currWeaponSprite.sprite = bm.currBow.bowImage;
		currWeaponDamage.text = " DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponCritDamage.text = " CRIT DAMAGE: " + (bm.currBow.arrowDamage * bm.currBow.critMultiplier).ToString();
		currWeaponDashDamage.text = " DASH DAMAGE: " + bm.currBow.arrowDamage.ToString();
		currWeaponRpm.text = " RPM: " + (60 / bm.currBow.cooldown).ToString();
		weaponChoice.SetTrigger("rightSelected");
		showCover = false;
	}

	public void OnRightChoiceHover()
	{
		Debug.Log("Hovering Over Right");
		hoveringOverRightOption = true;
		  
		
	}

	public void OnRightChoiceExit()
	{
		Debug.Log("Exited Right");
		hoveringOverRightOption = false;
	
		
	}

}
