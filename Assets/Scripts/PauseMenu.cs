﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using TMPro;

public class PauseMenu : MonoBehaviour
{


    public static bool GameIsPaused = false;
    public Animator fadeIntoGamePanel;

    public static PauseMenu instance;
    // -- SINGLETON REFERENCES -- 
    private Player player;
    private CharacterMenu characterMenu;
    private ControlsHandler controls;
    private Weapon weapon;
    public GameManager gameManager;

    public GameObject pauseMenuUI;
    public GameObject PauseMenuPopup;

    public GameObject OptionsMenuPopup;

    // public CharacterMenu characterMenu;
    // public static bool PlayMusic = false;
    public AudioSource musicPlayer;
    public Toggle muteMusicToggle;
    public Toggle aimAssistToggle;

    public Animator resumeButtonAnimator;
    public Animator restartAnimator;
    public TextMeshProUGUI restartTex;
    public TextMeshProUGUI restartTexGameOver;
    public TextMeshProUGUI quitGameText;
    public Animator optionsAnimator;
    public Animator quitAnimator;
    public Animator backAnimator;

    [Space] public Animator mainMenuGameOverAC;
    public Animator restartGameoVerAC;
    

    public AudioMixer audioMixer;
    public float mastVol;
    public Slider volumeSlider;

    public TMP_Dropdown resolutionDropdown;
    Resolution[] resolutions;

    public static bool AdjustingControls = false;
    //public InputButton chargeButton;

    public string feedbackUrl;
    // public Image feedbackSprite;
    // private Color destination;    


    [Header("Mobile")] public TextMeshProUGUI controlsTypeText;



    private float sceneLoadedTime = 0f;

    private float awakePlayedTime = 0f;
    // private CharacterMenu characterMenu;
    // private bool timeIsSlowed = false;


    private void Awake()
    {
        instance = this;
        awakePlayedTime = Time.time;
    }

    void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if ((resolutions[i].width == Screen.currentResolution.width) &&
                (resolutions[i].height == Screen.currentResolution.height)) currentResolutionIndex = i;




        }
        
        countdownTex.color = Color.clear;

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        pauseMenuUI.SetActive(false);



        player = Player.instance;
        characterMenu = CharacterMenu.instance;
        weapon = Weapon.instance;
        // gameManager = GameManager.instance;
        controls = ControlsHandler.instance;
        controlsTypeText.SetText(controls.currLayout.ToString());


        ToggleMusic(PlayerPrefs.GetInt("playMusic", 1) == 1);
        ToggleAimAssist(PlayerPrefs.GetInt("toggleAim", 1) == 1);
        ToggleHapticsFX(PlayerPrefs.GetInt("toggleHaptics", 1) == 1);

        Resume();
        // if (!PlayerPrefs.HasKey("playMusic")) // if preference hasnt been set
        // {
        //     ToggleMusic(PlayMusic);
        // }
        // else 
        // {
        //     int _toPlay = PlayerPrefs.GetInt("playMusic");
        //     if (_toPlay == 0) ToggleMusic(false);
        //     else ToggleMusic(true);
        //    
        // }

    }



    private void OnEnable()
    {

        sceneLoadedTime = Time.time;
        // Debug.Log($"RanNormal, {PlayerPrefs.GetFloat("sliderVolume")} and: {PlayerPrefs.GetFloat("volumePref")}");  
        volumeSlider.value = PlayerPrefs.GetFloat("sliderVolume");
        muteMusicToggle.isOn = PlayerPrefs.GetInt("playMusic", 1) == 1;
        StartCoroutine(WaitThenFadeIn());
    }

    IEnumerator WaitThenFadeIn()
    {
        
        yield return new WaitForSeconds(0.3f);

        float daVol = PlayerPrefs.GetFloat("volumePref", 0);
        audioMixer.SetFloat("masterVol", daVol);
        gameManager.mastVol = daVol;
        
        
        fadeIntoGamePanel.SetTrigger("sceneEnter");
        


    }


    // void OnApplicationPause(bool isApplicationPause)
    // {
    //     if (isApplicationPause)
    //     {
    //         Pause();
    //         Debug.Log("Paused!");
    //     } else
    //     {
    //         Debug.Log("Resumed");
    //     }
    //
    //
    // }


    // void SetColor()
    // {
    //     destination = new Color((byte) Random.Range(0f, 1f), (byte) Random.Range(0f, 1f), (byte) Random.Range(0f, 1f),
    //         1);
    //
    //     StartCoroutine(ChangeColor());
    // }

    // IEnumerator ChangeColor()
    // {
    //     while (feedbackSprite.color != destination)
    //     {
    //         if (feedbackSprite.color == destination)
    //         {
    //             SetColor();
    //             yield break;
    //         }
    //         feedbackSprite.color = Color.Lerp(feedbackSprite.color, destination, 0.1f);
    //         yield return null;
    //     }
    //     
    //     
    // }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.GameOver) return;

            // if ((!GameIsPaused) &&
            //     (!CharacterMenu.instance.shopIsActive) &&
            //     (!Player.instance.isDashing) &&
            //     (!Weapon.instance.lastShot) && !GameManager.GameOver)
            if ((!GameIsPaused) &&
                (!characterMenu.shopIsActive) &&
                (!player.isDashing) &&
                (!weapon.lastShot) && !GameManager.GameOver)
            {
                Pause();

            }
            else
            {
                Resume();

            }
        }
    }

    public void Resume()
    {
        if (AdjustingControls) AdjustingControls = false;


        if (GameManager.GameOver)
        {
            return;

        }

        if (pauseMenuUI.activeSelf) // Pause Menu open
        {

            pauseMenuUI.SetActive(false);
            if (!CharacterMenu.instance.shopIsActive && !CharacterMenu.instance.BLESSINGS_SHOWING)
            {
                Time.timeScale = 1f;
                GameIsPaused = false;
            }
        }
        else if (CharacterMenu.instance.shopIsActive) // Pause menu closed, character menu closed
        {
            Time.timeScale = 1f;
            GameIsPaused = false;
        }
        else if (!CharacterMenu.instance.shopIsActive && !pauseMenuUI.activeSelf)
        {
            Time.timeScale = 1f;
            GameIsPaused = false;
        }




        // pauseMenuUI.SetActive(false);



    }

    public void Pause()
    {
        // GameManager.instance.IntroduceFade();
        if (GameManager.GameOver)
        {
            return;
        }

        pauseMenuUI.SetActive(true);
        PauseMenuPopup.transform.SetAsLastSibling();
        Time.timeScale = 0.0001f;
        GameIsPaused = true;
        restartTex.SetText("RESTART");
        restartTexGameOver.SetText("RESTART");
        quitGameText.SetText("MAIN MENU");
        RestartPressed = false;
        QuitPressed = false;


    }

    public void ShopMenuPause()
    {
        Time.timeScale = 0.0001f;
        GameIsPaused = true;
    }

    public void ShopMenuResume()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
    }


    public void TriggerGameOverState()
    {
        GameIsPaused = true;
        Time.timeScale = 0.0001f;
    }


    private bool RestartPressed = false;
    private bool QuitPressed = false;
    private bool MainMenuPressed = false;

    public PauseMenu(float mastVol)
    {
        this.mastVol = mastVol;
    }

    public void Restart()
    {
        if (!RestartPressed)
        {
            RestartPressed = true;
            restartTex.SetText("CONFIRM?");
            restartTexGameOver.SetText("CONFIRM?");
        }
        else
        {

            gameManager.RestartGame();
            Resume();
        }

    }

    public void RestartGameOver()
    {

        RestartPressed = true;

        gameManager.RestartGame();
        Resume();


    }

    public void MainMenuButton()
    {
        if (!MainMenuPressed)
        {
            MainMenuPressed = true;
            quitGameText.SetText("CONFIRM?");
        }
        else
        {
            ShopMenuResume();
            gameManager.BackToMainMenu();

        }
    }

    public void ToggleMute()
    {
        if (gameManager.disableAllSounds)
        {
            gameManager.disableAllSounds = false;
        }
        else if (!gameManager.disableAllSounds)
        {
            gameManager.disableAllSounds = true;
        }
    }

    public void OptionsButton()
    {
        if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        {
            controlsTypeText.SetText(controls.currLayout.ToString());
        }

        OptionsMenuPopup.transform.SetAsLastSibling();


    }

    public void ExitOptions()
    {
        PauseMenuPopup.transform.SetAsLastSibling();

    }


    public void QuitGame()
    {
        if (!QuitPressed)
        {
            QuitPressed = true;
            quitGameText.SetText("CONFIRM?");
        }
        else
        {
            Application.Quit();
        }
    }

    public void OnResumeButtonOver()
    {
        resumeButtonAnimator.SetBool("hover", true);
    }

    public void OnResumeButtonExit()
    {
        resumeButtonAnimator.SetBool("hover", false);
    }

    public void OnRestartOver()
    {
        restartAnimator.SetBool("hover", true);
    }

    public void OnRestartExit()
    {
        restartAnimator.SetBool("hover", false);
    }

    public void OnOptionsOver()
    {
        optionsAnimator.SetBool("hover", true);
    }

    public void OnOptionsExit()
    {
        optionsAnimator.SetBool("hover", false);
    }

    public void OnQuitOver()
    {
        quitAnimator.SetBool("hover", true);
    }

    public void OnQuitExit()
    {
        quitAnimator.SetBool("hover", false);
    }

    public void OnBackOver()
    {
        backAnimator.SetBool("hover", true);
    }

    public void OnBackExit()
    {
        backAnimator.SetBool("hover", false);
    }
    

    public void OnMainMenuGameOverOver()
    {
        mainMenuGameOverAC.SetBool("hover", true);
    }
    
    public void OnMainMenuGameOverExit()
    {
        mainMenuGameOverAC.SetBool("hover", false);
    }

    public void OnRestartGameOverOver()
    {
        restartGameoVerAC.SetBool("hover", true);
    }
    
    public void OnRestartGameOverExit()
    {
        restartGameoVerAC.SetBool("hover", false);
    }
    
    

    public void SetVolume(float volume)
    {



        if (Time.time - sceneLoadedTime <= 0.2f)
        {
            // Debug.Log($"RanSTART, {PlayerPrefs.GetFloat("sliderVolume", 1f)} and: {PlayerPrefs.GetFloat("volumePref")}");    
            // audioMixer.SetFloat("masterVol", actualVolume);
            // PlayerPrefs.SetFloat("volumePref", actualVolume);
            volumeSlider.value = PlayerPrefs.GetFloat("sliderVolume", 1f);
            float presetVolume = PlayerPrefs.GetFloat("volumePref", 0);
            // Debug.Log(presetVolume);
            audioMixer.SetFloat("masterVol", presetVolume);
            // audioMixer.SetFloat("masterVol", )}
        }
        else
        {

            PlayerPrefs.SetFloat("sliderVolume", volume);
            float actualVolume = Mathf.Log10(volume) * 20;
            audioMixer.SetFloat("masterVol", actualVolume);
            PlayerPrefs.SetFloat("volumePref", actualVolume);
            gameManager.mastVol = actualVolume;
            // Debug.Log($"RanNormal, {PlayerPrefs.GetFloat("sliderVolume")} and: {PlayerPrefs.GetFloat("volumePref")}");  
        }
        // float actualVolume = Mathf.Log10(volume) * 20;



    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex + 1);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void ToggleControlsMobile()
    {
        ControlsHandler.MobileLayout newLayout = controls.LayoutSwitch();
        controlsTypeText.SetText(newLayout.ToString());
    }

    public void ToggleAdjustments()
    {
        AdjustingControls = true;

    }


    public void ToggleMusic(bool toPlay)
    {
        // PlayMusic = toPlay;



        if (toPlay)
        {
            PlayerPrefs.SetInt("playMusic", 1);
            musicPlayer.volume = 0.1f;
        }
        else
        {
            musicPlayer.volume = 0f;
            PlayerPrefs.SetInt("playMusic", 0);
        }


    }


    public void ToggleAimAssist(bool toAim)
    {
        PlayerPrefs.SetInt("toggleAim", toAim ? 1 : 0);
    }


    public void ToggleHapticsFX(bool toRumble)
    {
        PlayerPrefs.SetInt("toggleHaptics", toRumble ? 1 : 0);
    }

    public void FeedbackButton()
    {
        Application.OpenURL(feedbackUrl);
    }


    public void MainMenuGameOver()
    {
        ShopMenuResume();
        gameManager.BackToMainMenu();
    }

    public TextMeshProUGUI countdownTex;

    public IEnumerator CountdownToPlayMode()
    {
        countdownTex.color = Color.white;
        float timeElapsed = 0f;
        while (timeElapsed <= 3f)
        {
            
            timeElapsed += Time.unscaledDeltaTime;
            if (timeElapsed < 1f)
            {
                if (timeElapsed <= 0.1f) countdownTex.fontSize = 248.3f;
                else countdownTex.fontSize = Mathf.Lerp(countdownTex.fontSize, 0f,  Time.unscaledDeltaTime);
 
                countdownTex.SetText("3");
            } else if (timeElapsed < 2f)
            {
                countdownTex.SetText("2");
                if (timeElapsed <= 1.1f) countdownTex.fontSize = 248.3f;
                else countdownTex.fontSize = Mathf.Lerp(countdownTex.fontSize, 0f,  Time.unscaledDeltaTime);
            }
            else
            {
                if (timeElapsed <= 2.1f) countdownTex.fontSize = 248.3f;
                else countdownTex.fontSize = Mathf.Lerp(countdownTex.fontSize, 0f,   Time.unscaledDeltaTime);
                countdownTex.SetText("1");
            }

            yield return null;
        }
        Debug.Log("time passed");
        gameManager.ResurrectTheTeam();
        countdownTex.color = Color.clear;
    }

}
