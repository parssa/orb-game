﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


public abstract class Mover : Fighter
{

    [Header("Mover Values")]
    [HideInInspector] public Animator anim;
    public float playerSpeed = 1.3f;
    [HideInInspector] public float regularSpeed;
    //----
    private Transform targetForEnemy;
    protected BoxCollider2D boxCollider;
    protected Vector2 moveDelta;
    protected RaycastHit2D hit;

    protected float ySpeed = 10f;
    protected float xSpeed = 10f;

   

    // This is really only for the player :P

    public float dashDistance = 3f;
    public float dashIncr = 0.9f;

    private const float PUSHED_SPEED_MULTIPLIER = 2.3f;

    public bool isGettingKnockedBack;
    public bool isRecovering;

    private float timeSinceLastKnockback;
    private float knockBackEffectCooldown;

    //private bool countdownTillScatterBegun;
    private bool isScattering;
    private bool scatterCourotInProgress;

    public float pushRecSpeed = 0.1f;
    public bool isWalking = false;

    public bool isPlayer = false;

    public ControlsHandler controlsHandler;

    private Player _player;

    private float actualRegularSpeed = 3f;
    protected virtual void Start()
    {

        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
        regularSpeed = playerSpeed;
        timeSinceLastKnockback = 0f;
        knockBackEffectCooldown = 0.05f;
        isGettingKnockedBack = false;
        isRecovering = false;
        pushRecoverySpeed = 0.03f;

        //countdownTillScatterBegun = false;
        isScattering = false;





        if (transform.TryGetComponent(out Enemy enemy))
        {
            if (enemy.orbTargetter)
            {
                targetForEnemy = GameManager.instance.orbTransform;

            }
            else targetForEnemy = GameManager.instance.player.transform;

            EnemyFaceTarget();
        }
        else targetForEnemy = GameManager.instance.player.transform;

        actualRegularSpeed = regularSpeed;


        if (isPlayer)
        {
            controlsHandler = ControlsHandler.instance;
            _player = GetComponent<Player>();
        }


    }

    protected virtual void UpdateMotor(Vector2 input)
    {
        if (this.CompareTag("Player")) PlayerFaceCursor();
        else EnemyFaceTarget();


        if (!isScattering) moveDelta = input;
        if (this.CompareTag("Player"))
        {
            if (isGettingKnockedBack) moveDelta = pushDirection;
            else timeSinceLastKnockback += Time.deltaTime;
        }
        else timeSinceLastKnockback += Time.deltaTime;


        if (this.CompareTag("Fighter"))
        {
            if (this.GetComponent<Enemy>().forceFollowing &&
                this.GetComponent<Enemy>().thisEnemyIs != EnemyManager.EnemyEnum.SKULL_ORB_TARGET)
            {
                // regularSpeed = 3.5f;
                regularSpeed = GetComponent<Enemy>().reallyFarAway ? UnityEngine.Random.Range(5f, 6f) : 3.5f;

            }
            else if (this.GetComponent<Enemy>().thisEnemyIs != EnemyManager.EnemyEnum.SLUGGER)
            {
                if (GetComponent<Enemy>().InCrowd)
                {
                    regularSpeed = GetComponent<Enemy>().blueprintSpeed + UnityEngine.Random.Range(-0.2f, 0.3f) ;
                }
                else
                {
                    regularSpeed = GetComponent<Enemy>().blueprintSpeed;
                }
                
            }
        }
        if (!isGettingKnockedBack)
        {

            if ((pushDirection != Vector2.zero) && (timeSinceLastKnockback >= knockBackEffectCooldown))
            {
                TakeHit();
                timeSinceLastKnockback = 0.0f;
            }
        }
        if (moveDelta == Vector2.zero && pushDirection != Vector2.zero)
        {
            moveDelta = pushDirection;
        }


        // Vector2 moveDir = new Vector2(moveDelta.x, moveDelta.y).normalized;
        if (isGettingKnockedBack)
        {
            if (this.CompareTag("Player"))
            {
                moveDelta = pushDirection;
            }
            else
            {
                moveDelta /= 2;
                moveDelta += pushDirection;
            }


            if (pushDirection != Vector2.zero)
            {
                // pushDirection.x /= 10f;
                // pushDirection.y /= 10f;
                
                float interpolationRatio = Time.time - lastHitTime;
                pushDirection = Vector2.MoveTowards(pushDirection, Vector2.zero, interpolationRatio);
            }
        }
        else playerSpeed = regularSpeed;

        isWalking = (Mathf.Abs(moveDelta.x) + Mathf.Abs(moveDelta.y)) > 0;

        if (isPlayer)
        {
            // Check y axis

            // Debug.Log("Y:" + moveDelta.y  * playerSpeed);
            // Debug.Log("X:" + moveDelta.x * playerSpeed);

            hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y * Time.deltaTime * playerSpeed), LayerMask.GetMask("Actor", "Blocking", "WorldBorder"));
            if (hit.collider != null) isWalking = false;
            // Check x axis
            hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x * Time.deltaTime * playerSpeed), LayerMask.GetMask("Actor", "Blocking", "WorldBorder"));
            if (hit.collider != null) isWalking = false;
            
            if (isWalking && _player.inWall)
            {
            
                
                
                if (_player.collEnterVec.x != 0 && (Mathf.Abs(_player.collEnterVec.x) > 0.1f))
                {
                    //Debug.Log("X : Entered At" + _player.collEnterVec.x + " and is trying to go" + moveDelta.x);
                    if (moveDelta.x != 0)
                    {
                        if ((_player.collEnterVec.x < 0 && moveDelta.x < 0) || (_player.collEnterVec.x > 0 && moveDelta.x > 0))
                        {
                            
                            // if (_player.collEnterVec.x < 0 && moveDelta.x < 0) moveDelta.x = 0.5f;
                            // else moveDelta.x = -0.5f;
            
                        }
                    }
                    
                    if (moveDelta.x == 0) isWalking = true;
                }
            
                if (_player.collEnterVec.y != 0 && (Mathf.Abs(_player.collEnterVec.y) > 0.1f))
                {
                    // Debug.Log("Y : Entered At" + _player.collEnterVec.y + " and is trying to go" + moveDelta.y);
                    if (moveDelta.y != 0)
                    {
                        if ((_player.collEnterVec.y < 0 && moveDelta.y < 0) || (_player.collEnterVec.y > 0 && moveDelta.y > 0))
                        {
                            // isWalking = false;
                            if (moveDelta.y == -1) moveDelta.y = 0f;
                            else moveDelta.y = 0.5f;
                            
                        }
                    }
                    
                    if (moveDelta.y == 0) isWalking = true;
                }
            }

        }
        else
        {
            // Check y axis
            if (transform.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.GUARDIAN || transform.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.TURBO_KING || transform.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.BOSS) isWalking = true;
            else if (!transform.GetComponent<Enemy>().reachedEndOfPath) isWalking = true;
            else
            {
                hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y * Time.deltaTime * playerSpeed), LayerMask.GetMask("WorldBorder"));
                if (hit.collider != null)
                {
                    isWalking = false;
                    //  if (!transform.GetComponent<Enemy>().reachedEndOfPath) isWalking = true;
                }
                // Check x axis
                hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x * Time.deltaTime * playerSpeed), LayerMask.GetMask("WorldBorder"));
                if (hit.collider != null)
                {
                    isWalking = false;

                }
            }


        }




        // Conclusion
        if (isWalking)
        {

            transform.position += new Vector3(moveDelta.x, moveDelta.y, 0).normalized * Time.deltaTime * playerSpeed;

        }

        // Animator Values
        anim.SetBool("isWalking", isWalking);


    }

    protected virtual void HandleDash(Vector2 dashDirection)
    {

        // if (isPlayer)
        // {
        //     if (_player.inWall)
        //     {
        //         Player.instance.isDashing = false;
        //         return;
        //     }
        //
        //
        // }

        Vector2 playerPosition = transform.position;
        Vector2 afterDashPosition = playerPosition += dashDirection.normalized * 0.5f;
        if (!CompareTag("Player")) transform.position = afterDashPosition;
        else
        {
          //  Vector2 ACTUAL_DASH_DIRECTION = 
            float distanceFromCurrToDashPosition = Vector2.Distance(transform.position, afterDashPosition);


            // Debug.DrawRay(transform.position, afterDashPosition, Color.white, 0.1f);
            // if (Physics2D.Raycast(transform.position, afterDashPosition, distanceFromCurrToDashPosition, LayerMask.GetMask("WorldBorder")).collider == null)
            
            // if (_player.inWall)
            // {
            //     Player.instance.isDashing = false;
            //     return;
            // }
            // else 
           if (Physics2D.Linecast(transform.position, afterDashPosition, LayerMask.GetMask("WorldBorder")).collider == null)
            {
                // transform.position = afterDashPosition;
                RaycastHit2D hitX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(dashDirection.x, 0), distanceFromCurrToDashPosition, LayerMask.GetMask("WorldBorder"));
                RaycastHit2D hitY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, dashDirection.y), distanceFromCurrToDashPosition, LayerMask.GetMask("WorldBorder"));

                if ((hitX.collider == null && hitY.collider == null)) transform.position = afterDashPosition;
                else if (hitX.collider == null && hitY.collider != null)
                { Debug.Log("X Dispatch");
                    transform.position = transform.position = new Vector2(afterDashPosition.x, transform.position.y);
                } else if (hitY.collider == null && hitY.collider != null)
                { Debug.Log("Y Dispatch");
                    transform.position = transform.position = new Vector2(transform.position.x, afterDashPosition.y);
                } 

                
                // else
                // {
                //     Debug.Log("No Go Go.");
                //     Player.instance.isDashing = false;
                // }
            }
            else
            {
                Debug.Log("else");
                Player.instance.isDashing = false;
            }
        }
    }


    // Animator SetFloats
    #region 
    void PlayerFaceCursor()
    {
        // Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        // Vector2 playerPosition = transform.position;
        
        if (!PauseMenu.GameIsPaused)
        {

            anim.SetFloat("x", controlsHandler.currVec.x);
            // Debug.Log(controlsHandler.currVec.x);
            anim.SetFloat("y", controlsHandler.currVec.y);
        } 
    }

    void EnemyFaceTarget()
    {
        if (transform.TryGetComponent(out Enemy enemy))
        {
            if (enemy.orbTargetter)
            {
                targetForEnemy = GameManager.instance.orbTransform;

            }
            else targetForEnemy = GameManager.instance.playerTransform;
        }
        Vector2 playerPosRespectToEnemy = (targetForEnemy.position - transform.position).normalized;

        // if (pushDirection != Vector2.zero)
        // {
        //     Debug.Log((-1*pushDirection.x) > 0);
        //     anim.SetFloat("x", pushDirection.x);
        //     anim.SetFloat("y", pushDirection.y);
        // } else
        // {
        //     anim.SetFloat("x", playerPosRespectToEnemy.x);
        //     anim.SetFloat("y", playerPosRespectToEnemy.y);
        // }

        anim.SetFloat("x", playerPosRespectToEnemy.x);
        anim.SetFloat("y", playerPosRespectToEnemy.y);
        
       
    }
    #endregion

    // Knockback Functions
    #region 
    void TakeHit()
    {
        
            
            if (!isGettingKnockedBack && KnockbackMayContinue()) StartCoroutine(TakeKnockback(pushDirection, 0.1f));
            
        
    }

    public IEnumerator TakeKnockback(Vector2 pushDirection, float duration)
    {
        float timeElapsed = 0.0f;
        isGettingKnockedBack = true;
        playerSpeed = regularSpeed * 2;


        while (timeElapsed <= pushRecoverySpeed)
        {

            if ((PauseMenu.GameIsPaused))
            {
                pushDirection = Vector2.zero;
                playerSpeed = regularSpeed;
                isGettingKnockedBack = false;
                yield break;
            }

            if (pushDirection == Vector2.zero)
            {

                isGettingKnockedBack = false;
                playerSpeed = regularSpeed;
                yield break;
            }

            if (CompareTag("Player"))
            {
                //     Vector2 potentialPushDirection = pushDirection.normalized;
                //     RaycastHit2D hitX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(potentialPushDirection.x, 0), potentialPushDirection.x, LayerMask.GetMask("Blocking"));
                //     RaycastHit2D hitY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, potentialPushDirection.y), potentialPushDirection.y, LayerMask.GetMask("Blocking"));
                // if (KnockbackMayContinue()) playerSpeed = playerSpeed * 1.02f;
                // if (hitX.collider == null && hitY.collider == null)
                // {
                //     playerSpeed = playerSpeed * 1.02f;

                // }
                if (KnockbackMayContinue()) playerSpeed = playerSpeed * 1.02f;
                else yield break;
                // {
                //     Debug.Log("Cond Hit In Mover");
                //     isGettingKnockedBack = false;
                //     pushDirection = Vector2.zero;
                //     playerSpeed = regularSpeed;
                //     yield break;
                // }
            }
            
            timeElapsed += Time.deltaTime;
            yield return null;
        }
     //   pushRecoverySpeed = pushRecSpeed;
        if (CompareTag("Player")) StartCoroutine(Recovery());
        else playerSpeed = regularSpeed;

        pushDirection = Vector2.zero;

        isGettingKnockedBack = false;
    }

    bool KnockbackMayContinue()
    {
        Vector2 potentialPushDirection = pushDirection.normalized;
        RaycastHit2D hitX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(potentialPushDirection.x, 0), potentialPushDirection.x * playerSpeed, LayerMask.GetMask("WorldBorder"));
        RaycastHit2D hitY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, potentialPushDirection.y), potentialPushDirection.y * playerSpeed, LayerMask.GetMask("WorldBorder"));

        // RaycastHit2D hitX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(pushDirection.x, 0), pushDirection.x, LayerMask.GetMask("Blocking"));
        // RaycastHit2D hitY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, pushDirection.y), pushDirection.y, LayerMask.GetMask("Blocking"));


        Vector2 futureLocation = pushDirection.normalized;
        Vector2 dist = (Vector2)transform.position + futureLocation;


        // Debug.DrawRay(transform.position, futureLocation, Color.blue, 0.1f);

        RaycastHit2D blocking = Physics2D.Linecast(transform.position, futureLocation, LayerMask.GetMask("WorldBorder"));

        if (hitX.collider == null && hitY.collider == null && blocking.collider == null)
        {
            playerSpeed = playerSpeed * 1.02f;
            return true;

        }
        else
        {

            isGettingKnockedBack = false;
            // isWalking = false;
            pushDirection = Vector2.zero;
            playerSpeed = regularSpeed;
            return false;
        }
    }

    IEnumerator Recovery()
    {
        isRecovering = true;
        float timeElapsed = 0.0f;
        while (playerSpeed >= regularSpeed)
        {
            if (pushDirection != Vector2.zero) moveDelta = pushDirection;
            playerSpeed -= 0.02f;
            timeElapsed += Time.deltaTime;
            if (PauseMenu.GameIsPaused || timeElapsed > 0.1f)
            {
                playerSpeed = regularSpeed;
                isRecovering = false;
                yield break;
            }

            yield return null;
        }
        playerSpeed = regularSpeed;
        isRecovering = false;
    }
    #endregion

}