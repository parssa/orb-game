﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireballHitBox : Collidable
{

	//public RangedEnemy rangedEnemy;

	private GameObject fireball;
	private CameraMotor cameraShake;
	public int critMultiplier = 3;
	public bool isGuardianBlast= false;

	// Damage
	public int damage = 4;
	public float pushForce = 10;

    protected override void Start()
    {
        base.Start();
        cameraShake = CameraMotor.instance;
        fireball = transform.parent.gameObject;
		
        //if (fireball.GetComponent<EnemyFireball>().isGuardianBlast) isGuardianBlast =true;
        damage = Random.Range(1, 3);
        if (isGuardianBlast) damage = 7;
    }

	



    protected override void OnCollide(Collider2D coll)
	{
		if (coll.CompareTag("Laser") && coll.name != "weapon" && !isGuardianBlast)
		{
			//Destroy(coll.transform.gameObject);
			fireball.GetComponent<EnemyFireball>().BreakArrow();
		}
		if (coll.name == "Player")
		{

			// Create a new damage object, before sending it to the player 
			int damageToDeal = Random.Range(1, damage);
			Damage dmg = new Damage
			{
				damageAmount = Random.Range(1, damage),
				origin = transform.position,
				pushForce = pushForce,
				isCrit = damageToDeal >= 5
			};

			coll.SendMessage("RecieveDamage", dmg);

			//transform.parent.gameObject.GetComponent<SpriteRenderer>();
			//StartCoroutine(cameraShake.Shake(0.05f, 10f));
			// Destroy(fireball);
			fireball.GetComponent<EnemyFireball>().BreakArrow();
            
		}
	}
}
