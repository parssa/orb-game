﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbPickUp : PowerUp
{
    protected override void OnCollect()
    {
		
		if (!collected)
		{

			GameObject particleEffect = Instantiate(Resources.Load("OnOrbParticles") as GameObject, transform.position, transform.rotation);
			Destroy(particleEffect, 1f);
			collected = true;
			GameManager.instance.GrantPoints(10);
			DamagePopup.Create(Player.instance.transform.position, 5, true, DamagePopup.ORBPICK);
			Player.instance.pickedUpOrb = true;
			SoundManager.instance.Play("Powerup", 0);
			Destroy(gameObject);
		}
	}
	
}
