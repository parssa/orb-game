﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianFlyUpBehaviour : StateMachineBehaviour
{
    Vector2 currPos;
    Vector2 abovePos;
    Vector3 positionInVector3Form;
    public float speed;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<Guardian>().immuneTime = 100f;
        currPos = animator.transform.position;
        abovePos = new Vector2(currPos.x, currPos.y + 7f);
        positionInVector3Form = new Vector3(abovePos.x, abovePos.y, animator.transform.position.z);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       animator.transform.position = Vector2.MoveTowards(animator.transform.position, abovePos, speed * Time.deltaTime);
       if (animator.transform.position == positionInVector3Form)
       {
           animator.GetComponent<Guardian>().currState = Guardian.GuardianState.DECIDE;
           
       } 
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<Guardian>().immuneTime = 0.1f;
        animator.GetComponent<Guardian>().currState = Guardian.GuardianState.DECIDE;
    }

}
