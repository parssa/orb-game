﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CinemaController : MonoBehaviour
{

    public static CinemaController instance;   
    
    public Camera cam;
    private float cameraSize;
    
    /* CINEMATIC STATE (PlayerPref)
    
     -1: Has Never been played Before.
     0: Go Straight to game
     1: Go Back to Main Menu
    
    */

    public Image backupFade;
    public TextMeshProUGUI skippingTex;
    
    
    
    public int currScene = 0;
    public GameObject[] scenes;

    public Animator MainThingAC;
    
    
    void Awake()
    {

        instance = this;
        
        currScene = 0;
        skippingTex.color = Color.clear;
    }

    private void DetermineCameraSize()
    {
        // if (Application.isMobilePlatform)
        // {
        //     
        //     
        //     Debug.Log("Is Mobile");
        //     
        //     
        // if (SystemInfo.deviceModel.Contains("iPad"))
        // {
        //     MainThingAC.SetBool("isMobile", false);
        //     cameraSize = 5.3f;
        // }
        //     else
        //     {
        //         MainThingAC.SetBool("isMobile", true);
        //         cameraSize = 4.23f;
        //     }
        // }
        // else
        // {
        //     MainThingAC.SetBool("isMobile", false);
        //     cameraSize = 5f;
        // }
        cam.orthographicSize = cameraSize;
        
        if (SystemInfo.deviceModel.Contains("iPad"))
        {
            MainThingAC.SetBool("isMobile", false);
            cameraSize = 5.3f;
        }
        else
        {
            MainThingAC.SetBool("isMobile", true);
            cameraSize = 4.23f;
        }
        cam.orthographicSize = cameraSize;
        ;

    }


    public void PlayScene(int sceneToPlay)
    {
        currScene = sceneToPlay;
        for (int s = 0; s < scenes.Length; s++)
        {
            scenes[s].SetActive(s == sceneToPlay);
            
        }
        
        MainThingAC.SetInteger("currScene", currScene);
    }


    public void FinishedFinalScene()
    {
        if (PlayerPrefs.GetInt("CinematicState") == 1)
        {
            PlayerPrefs.SetInt("CinematicState", 1);
            SceneManager.LoadScene("mainMenu");
        }
        else
        {
            PlayerPrefs.SetInt("CinematicState", 1);
            SceneManager.LoadScene("beginning");
        }
    }
    
    void Start()
    {
        DetermineCameraSize();
        PlayScene(currScene);
    }

    private float lastMousePressedTime = 0f;
    private int numPressedDown = 0;
    private const float DoubleClickThreshold = 0.4f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Backspace))
        {
            FinishedFinalScene();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Debug.Log("mouse down");
            numPressedDown += 1;
            
            if (Time.time - lastMousePressedTime <= DoubleClickThreshold && numPressedDown >= 2)
            {
                if (!fadeOutCoroutStarted) StartCoroutine(FadeThenEndScene());

            }
            else
            {
                Debug.Log("Not fast enough");
                lastMousePressedTime = Time.time;
            }
                
        }
        
        
    }


    private bool fadeOutCoroutStarted = false;

    IEnumerator FadeThenEndScene()
    {
        fadeOutCoroutStarted = true;
        skippingTex.color = Color.white;
        float timeStarted = Time.time;
        while (backupFade.color != Color.black)
        {
            if (backupFade.color == Color.black) break;
            backupFade.color = Color.Lerp(backupFade.color, Color.black, Time.deltaTime * 3);

            if (Time.time - timeStarted >= 1.5f)
            {
                backupFade.color = Color.black;
                break;
            }
            
            yield return null;
        }
        
        FinishedFinalScene();
        Debug.Log("faded!");
    }
    

}
