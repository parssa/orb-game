﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SpawnPoint : Fighter
{
    // References
    private WaveSpawner wSpawner;
    private SoundManager soundManager;
    public Animator animator;
    private ObjectPooler objectPooler;
    

    private Transform ShrineObj;
    private Transform TopSpinner;
    private Transform areaLightObj;
#pragma warning disable 0649
    [SerializeField] private Transform HealthCanvas;
#pragma warning restore 0649
    private SpriteRenderer spriteRenderer;
    private Animator topSpinAnimator;
    private AudioClip hitClip;
    
    
    // Lights
    private Light2D pointLight;
    private Light2D spriteLight;

    private const float LIGHT_OFF = 0.8f;
    private const float LIGHT_ON = 2.5f;

    // Queue of enemies that the spawnPoint has to Spawn;
    public Queue<Transform> spawnQueue;


    private bool newShrine = true;


    public enum SpawnerState
    {
        CLOSED, // Waiting to be opened
        OPENING, // Activated when closed and enemies are put in queue
        SPAWNING, // Spawning when spawning enemies
        WAITING,
        RELOADING,
        CLOSING,
        BROKEN,
    }

    public const float TIME_TO_OPEN = 0.5f;
    public float timeOpened = 0.0f;

    public float intervalSpawning = 5f;
    public float timeSpawned = 0.0f;

    public const float TIME_TO_CLOSE = 0.7f;
    public float timeStartedClosing = 0.0f;

    public const float DEPOSIT_RATE = 0.1f;
    public float timeLastDepositied = 0.0f;

    public SpawnerState currState;

    public bool shrineIsActive;

    public int indexInWaveSpawnerArray;

    [Header("Shrine Values'")]
    public bool atCapacity;
    public int shrineLevel;
    public List<Sprite> shrineSprites;



    const float REG_SPIN_RATE = 1f;
    const float FASTEST_SPIN_RATE = 2.5f;


    public RectTransform healthBar;
    public RectTransform damageBar;
    private Vector3 diffVecHealth;
   // private bool showingHealthBar = false;

   private GameObject deathParticles;

    private bool brokenFormTriggered = false;


    public GameManager.Regions shrineRegion = GameManager.Regions.MAIN;
    public bool currRegionUnlocked = false;
    public Animator tutorialArrow;
    
    private void Awake()
    {
        newShrine = true;
        indexInWaveSpawnerArray = 99;
    }

    // Start is called before the first frame update
    void Start()
    {
        wSpawner = WaveSpawner.instance;
        objectPooler = ObjectPooler.instance;
        soundManager = SoundManager.instance;
        hitClip = soundManager.GetSounds("shrineHit").clip;
        
        
        if (shrineRegion == GameManager.Regions.MAIN)
        {
            currRegionUnlocked = true;
        }
        

        // spriteRenderer = GetComponent<SpriteRenderer>();
        // animator = GetComponent<Animator>();

        animator = GetComponent<Animator>();
        TopSpinner = transform.GetChild(1);
        ShrineObj = transform.GetChild(2);
        areaLightObj = transform.GetChild(0);
        spriteRenderer = ShrineObj.GetComponent<SpriteRenderer>();
        //HealthCanvas = transform.GetChild(4);
        spriteLight = ShrineObj.GetComponent<Light2D>();


        pointLight = transform.GetChild(0).transform.GetComponent<Light2D>();
        topSpinAnimator = transform.GetChild(1).GetComponent<Animator>();

        animator.SetBool("awake", false);
        HealthCanvas.gameObject.SetActive(false);
        TopSpinner.gameObject.SetActive(false);
        ShrineObj.gameObject.SetActive(false);
        areaLightObj.gameObject.SetActive(false);

        spriteLight.intensity = 1.18f;

        pointLight.intensity = LIGHT_OFF;

        atCapacity = false;
        currState = SpawnerState.CLOSED;

        spawnQueue = new Queue<Transform>();
        //  shrineSprites = new List<Sprite>();
        shrineLevel = 0;

        hitpoint = 15;
        maxHitpoint = 15;

        shrineIsActive = false;
        HideArrow();

        deathParticles = Resources.Load("OnShrineBreak") as GameObject;

    }

    // Update is called once per frame
    void Update()
    {

        if (shrineLevel > 0)
        {
            spriteRenderer.sprite = shrineSprites[1];
            animator.SetInteger("level", shrineLevel);
        }
        else
        {
            spriteRenderer.sprite = shrineSprites[0];
            animator.SetInteger("level", shrineLevel);
        }

        // If Closed, and has enemies in queue, open it
        if ((currState == SpawnerState.CLOSED) && (spawnQueue.Count > 0))
        {
            //  animator.SetBool("awake", true);
            currState = SpawnerState.OPENING;
            //timeOpened = Time.time;
        }
        // else if ((currState == SpawnerState.OPENING) && (Time.time - timeOpened >= TIME_TO_OPEN))
        else if ((currState == SpawnerState.OPENING) && ((wSpawner.State == WaveSpawner.SpawnState.SPAWNING) || (wSpawner.State == WaveSpawner.SpawnState.WAITING)))
        {
            //currState = SpawnerState.SPAWNING;
            // animator.SetBool("awake", true);
            StartCoroutine(SpawnEnemies());
            topSpinAnimator.speed = 1f;
        }
        else if (currState == SpawnerState.WAITING && (spawnQueue.Count > 0) && ((wSpawner.State == WaveSpawner.SpawnState.SPAWNING) || (wSpawner.State == WaveSpawner.SpawnState.WAITING)))
        {
           // Debug.Log("Reactivated");
            StartCoroutine(SpawnEnemies());
            // animator.SetBool("awake", true);
        }
        else if (currState == SpawnerState.CLOSING)
        {
            if (spawnQueue.Count != 0)
            {
                currState = SpawnerState.SPAWNING;

            }
            else if (Time.time - timeStartedClosing > TIME_TO_CLOSE) currState = SpawnerState.CLOSED;
        }
        else if (currState == SpawnerState.BROKEN)
        {
            // BrokenForm();
            animator.SetBool("awake", false);
        }

        if (currState == SpawnerState.CLOSED || currState == SpawnerState.CLOSING || currState == SpawnerState.BROKEN)
        {
            shrineIsActive = false;
        }
        else
        {
            shrineIsActive = true;
            animator.SetBool("broke", false);
        } 

        if (hitpoint <= 0) 
        {
            animator.SetBool("awake", false);
            animator.SetBool("broke", true);
            shrineIsActive = false;
        }

       // if (hitpoint == 0) 

        // if (currState == SpawnerState.OPENING)
        if (shrineIsActive)
        {
            spriteRenderer.color = Color.white;
            pointLight.intensity = LIGHT_OFF;
            spriteLight.intensity = 1.18f;
            // HealthCanvas.gameObject.SetActive(true);
            areaLightObj.gameObject.SetActive(true);
            TopSpinner.gameObject.SetActive(true);
            ShrineObj.gameObject.SetActive(true);
            animator.SetBool("awake", true);



        }
        

        if (spawnQueue.Count > 3 * (shrineLevel + 1)) atCapacity = true;
        else atCapacity = false;


        if (shrineLevel > 0 && shrineLevel < 2) intervalSpawning = 4f;
        else if (shrineLevel <= 4) intervalSpawning = 3f;
        else intervalSpawning = 2f;

        if (shrineIsActive && hitpoint < maxHitpoint)
        {
            brokenFormTriggered = false;
            HealthCanvas.gameObject.SetActive(true);
        } 
        else HealthCanvas.gameObject.SetActive(false);


        float currHealth = hitpoint;
        float maxHealth = maxHitpoint;
        float diffHealth = currHealth / maxHealth;
        diffVecHealth = new Vector3(diffHealth, 1, 1);

        if (diffVecHealth.x <= healthBar.localScale.x)
        {
            healthBar.localScale = diffVecHealth;
        }
        else
        {
            healthBar.localScale = Vector3.Lerp(healthBar.localScale, diffVecHealth, 0.1f);
        }

        damageBar.localScale = Vector3.Lerp(damageBar.localScale, diffVecHealth, 0.1f);

        if (currState == SpawnerState.BROKEN || currState == SpawnerState.CLOSED) spriteRenderer.sprite = shrineSprites[2];
        else if (shrineLevel <= 1) spriteRenderer.sprite = shrineSprites[0];
        else spriteRenderer.sprite = shrineSprites[1];

    }

    public bool AddToQueue(Transform enemyToAdd)
    {
        if(newShrine && currRegionUnlocked)
        {
            wSpawner.num_enemies_added_to_stack += 1;
            wSpawner.numEnemiesInsideShrines += 1;
            spawnQueue.Enqueue(enemyToAdd);
            newShrine = false;
            return true;
        }
        
        if (!atCapacity && (currState != SpawnerState.BROKEN) && currRegionUnlocked)
        {
            
            wSpawner.num_enemies_added_to_stack += 1;
            wSpawner.numEnemiesInsideShrines += 1;
            spawnQueue.Enqueue(enemyToAdd);
            newShrine = false;
            return true;
        }
        return false;
    }


    protected override void RecieveDamage(Damage dmg)
    {
        
        if (dmg.isCrit)
        {
            topSpinAnimator.SetTrigger("gotHit");
            topSpinAnimator.SetFloat("xPos", (dmg.origin.x - TopSpinner.position.x));
        }

        // var pitchMod = Random.Range(0.5f, 1.4f);
        // var position = transform.position;
        // Vector3 soundPos = new Vector3(position.x, position.y, 10 - GameManager.instance.mastVol * 10);
        //
        // AudioSource.PlayClipAtPoint(hitClip, soundPos);

        // Debug.Log($"{pitchMod}");
        
        animator.SetTrigger("gotHit");
        base.RecieveDamage(dmg);
    }
    
    void SpawnEnemy()
    {

        if ((wSpawner.State == WaveSpawner.SpawnState.SPAWNING || wSpawner.State == WaveSpawner.SpawnState.WAITING) && (spawnQueue.Count > 0))
        {

            if (Time.time - timeSpawned >= intervalSpawning)
            {
                topSpinAnimator.speed = REG_SPIN_RATE;
            }
            else
            {
                if (topSpinAnimator.speed < FASTEST_SPIN_RATE) topSpinAnimator.speed += 0.2f;
                else topSpinAnimator.speed = FASTEST_SPIN_RATE;
            }

        }
        // trigger spawn animation
    }

    public void EmptyShrine()
    {
        int numInside = spawnQueue.Count;

        
        if (numInside > 0)
        {
            Debug.Log("Dumping Enemies bc there was " + numInside);
            for (int a = 0; a < numInside; a++)
            {
                spawnQueue.Dequeue();
            }
        }
    }

    

    void BrokenForm()
    {
        brokenFormTriggered = true;
        shrineIsActive = false;
        int numEnemiesInsideThisShrine = spawnQueue.Count;
        //wSpawner.enemies_defeated_during_wave += numEnemiesInsideThisShrine;
        
        // for (int a = 0; a < numEnemiesInsideThisShrine; a++)
        // {
        //     spawnQueue.Dequeue();
        //     wSpawner.numEnemiesInsideShrines -= 1;
        //     CreateSoul();
        // }
        if (numEnemiesInsideThisShrine > 0) 
        {
            Debug.Log("Had to Do some quick redesignation");
            wSpawner.ReassignEnemies(spawnQueue);
            wSpawner.numEnemiesInsideShrines -= numEnemiesInsideThisShrine;
            wSpawner.num_enemies_added_to_stack -= numEnemiesInsideThisShrine;
            wSpawner.enemies_defeated_during_wave += numEnemiesInsideThisShrine;
        }

        
       
        for (int a = 0; a <= numEnemiesInsideThisShrine; a++)
        {
          
            CreateSoul();
        }
        animator.SetBool("awake", false);

        // if ((spawnQueue.Count > 0))
        // {

        //     spawnQueue.Dequeue();
        //     wSpawner.enemies_defeated_during_wave += 1;
        //     wSpawner.numEnemiesInsideShrines -= 1;
        //     //Instantiate(Resources.Load("soulCollectable") as GameObject, transform.position, transform.rotation);
        //     // animator.SetBool("awake", true);
            
        // }
    }


    protected override void Death()
    {

        
        if (!brokenFormTriggered) BrokenForm();
        currState = SpawnerState.BROKEN;
        GameManager.instance.GrantPoints(100);
        
        HideArrow();
        wSpawner.numShrinesBroken += 1;
        
        Vector3 randPos = new Vector3(Random.Range(-1f, 1f) + transform.position.x, Random.Range(-1f, 1f) + transform.position.y, 1);
        
        wSpawner.shrinesAvailable.SetValue(null, indexInWaveSpawnerArray);
        indexInWaveSpawnerArray = 99;
        // for (int a = 0; a < spawnQueue.Count; a++)
        // {
        //     spawnQueue.Dequeue();
        //     wSpawner.enemies_defeated_during_wave += 1;
        //     wSpawner.numEnemiesInsideShrines -= 1;
        //     randPos = new Vector3(Random.Range(-1f, 1f) + transform.position.x, Random.Range(-1f, 1f) + transform.position.y, 1);
        //     Instantiate(Resources.Load("soulCollectable") as GameObject, randPos, transform.rotation);
        // }
        //
        //if (spawnQueue.Count > 0) wSpawner.ReassignEnemies(spawnQueue);

        //Instantiate(Resources.Load("ShrineSoul") as GameObject, transform.position, transform.rotation);
        objectPooler.SpawnFromPool("shrineSoul", randPos, transform.rotation);
        
        spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.clear, Time.deltaTime);
        pointLight.intensity = Mathf.Lerp(pointLight.intensity, 0f, Time.deltaTime);
        spriteLight.intensity = Mathf.Lerp(spriteLight.intensity, 0f, Time.deltaTime);
        areaLightObj.gameObject.SetActive(false);
        //    HealthCanvas.gameObject.SetActive(false);

        //  TopSpinner.gameObject.SetActive(false);


        // while (spawnQueue.Count > 0)
        // {


        //     spawnQueue.Dequeue();

        //     wSpawner.enemies_defeated_during_wave += 1;
        //     wSpawner.numEnemiesInsideShrines -= 1;
        //     // randPos = new Vector3(Random.Range(-1f, 1f) + transform.position.x, Random.Range(-1f, 1f) + transform.position.y, 1);
        //     // Instantiate(Resources.Load("soulCollectable") as GameObject, randPos, transform.rotation);



        // }


        // Debug.Log("Stopped lopp");
        objectPooler.SpawnFromPool("shrineBreakParticles", transform.position, transform.rotation);

        CreateSoul();
        CreateSoul();
        CreateSoul();
        CreateSoul();
        CreateSoul();


        TopSpinner.gameObject.SetActive(false);
        areaLightObj.gameObject.SetActive(false);
        // HealthCanvas.gameObject.SetActive(false);
        shrineLevel = 0;

        base.Death();

    }

    IEnumerator SpawnEnemies()
    {
        currState = SpawnerState.SPAWNING;
        animator.SetBool("awake", true);
        while (spawnQueue.Count > 0)
        {
            
            
            if (!shrineIsActive)
            {
                //BrokenForm();
                
                    // wSpawner.ReassignEnemies(spawnQueue);
                if (!brokenFormTriggered) BrokenForm();
                yield break;
            }
            wSpawner.numEnemiesInsideShrines -= 1;


            
            wSpawner.num_enemies_spawned_curr_wave += 1;
            Transform enemy = spawnQueue.Dequeue();
            Vector3 randPos = new Vector3(Random.Range(-1f, 1f) + transform.position.x, Random.Range(-1f, 1f) + transform.position.y, 1);

            string enemyTag = enemy.GetComponent<Enemy>().thisEnemyIs.ToString();
            // if (enemy.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.SKULL_REG || enemy.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.SKULL_REG) objectPooler.SpawnFromPool("skull_regular", randPos, transform.rotation);
            // else if (enemy.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.RANGED_REG)  
            //Debug.Log(enemyTag);
            objectPooler.SpawnFromPool(enemyTag, randPos, transform.rotation);
            //Instantiate(enemy, randPos, transform.rotation);
            timeSpawned = Time.time;
            //animator.SetTrigger("spawnedEnemy");

            yield return new WaitForSeconds(1f / intervalSpawning);
        }
        if (currState != SpawnerState.BROKEN) currState = SpawnerState.WAITING;


    }

    public float NumEnemiesInside()
    {
        return spawnQueue.Count;
    }

    public void LevelUp()
    {
        if (Random.Range(0, 10) > 7)
        {
            shrineLevel += 1;
            WaveSpawner.ShrineStat thisShrineStats = wSpawner.FetchShrineStats(shrineLevel);
            // maxHitpoint = thisShrineStats.maxHitpoint;
            maxHitpoint += Random.Range(2, 7);
            if (maxHitpoint >= 35) maxHitpoint = 30;
            hitpoint = maxHitpoint;
            intervalSpawning = thisShrineStats.intervalSpawning;    
        }
        
    }

    void CreateSoul()
    {
        Vector3 randPos = new Vector3(Random.Range(-0.3f, 0.3f) + transform.position.x, Random.Range(-0.3f, 0.3f) + transform.position.y, 1);
        //GameObject soul = Instantiate(Resources.Load("soulCollectable") as GameObject, transform.position, transform.rotation);
        GameObject soul = objectPooler.SpawnFromPool("soul", transform.position, transform.rotation);
        soul.GetComponent<FlyToPlayer>().shootOutDir = lastPushDirection.normalized;
        Vector2 randShootOutDir = new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f));
        soul.GetComponent<FlyToPlayer>().shootOutDir = randShootOutDir;
        soul.GetComponent<FlyToPlayer>().cameFromShrine = true;
    }

    public void ShowArrow()
    {
        
        tutorialArrow.SetBool("tutorial", true);
    }

    public void HideArrow()
    {
        
        tutorialArrow.SetBool("tutorial", false);
    }

}
