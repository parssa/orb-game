﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAnimation : MonoBehaviour
{

    public Animator anim;
    public Animator weaponLight;
    const string SHOT_ANIM = "shotArrow";

    private void Start()
    {
        weaponLight.SetInteger("weaponLevel", Weapon.instance.weaponLevel);
    }
    // Update is called once per frame
    void Update()
    {

        weaponLight.SetInteger("weaponLevel", Weapon.instance.weaponLevel);
        if (Weapon.instance.justUpgradedWeapon)
        {
            Debug.Log("weaponLevel" + Weapon.instance.weaponLevel);
            weaponLight.SetInteger("weaponLevel", Weapon.instance.weaponLevel);
            Weapon.instance.justUpgradedWeapon = false;
        }
        if (Weapon.instance.lastShot)
        {
           
            anim.SetTrigger("shotArrow");
        }


        if (Player.instance.godModeActive)
        {
            weaponLight.SetBool("godModeActive", true);
            anim.SetTrigger("godMode");
        
        } 
        else
        {
            weaponLight.SetBool("godModeActive", false);
            
        }

        
    }
}
