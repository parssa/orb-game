﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRadius : MonoBehaviour
{
    private CircleCollider2D radiusCollider;
    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;
    public bool playerInRadius = false;
    public Transform[] targets = new Transform[5];

    private void Start()
    {
        radiusCollider = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        radiusCollider.OverlapCollider(filter, hits);



        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;
        }


    }

    protected virtual void OnCollide(Collider2D coll)
    {


        // Suspect
        if (!transform.parent.GetComponent<Turret>().activated)
        {
            if (coll.CompareTag("Player"))
            {
                StartCoroutine(PlayerLeft());

            }
        }
        
        if (transform.parent.GetComponent<Turret>().activated)
        {
            if (coll.CompareTag("Fighter"))
            {
                for (int i = 0; i < targets.Length; i++)
                {
                    if (targets[i] != null)
                        continue;

                    if (coll.transform.TryGetComponent(out Enemy enemy))
                    {
                        if (!enemy.targeted)
                        {
                            targets[i] = coll.transform;
                            // enemy.targeted = true;
                            // enemy.wasTargettedByTurret = true;
                            // enemy.turretThatTargettedIt = transform.parent;

                        }
                    }

                    




                }

            }
        }
        
    }


    IEnumerator PlayerLeft()
    {
        float timeElapsed = 0f;
        while (timeElapsed <= 4f)
        {

            playerInRadius = true;
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        playerInRadius = false;
    }
}
