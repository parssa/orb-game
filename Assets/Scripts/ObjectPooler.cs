﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    public static ObjectPooler instance;
    void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    public bool shouldExpand = true;

    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();    
        foreach(Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int a = 0; a < pool.size; a++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        // Check if key exists
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + "doesn't exist");
            return null;
        }



        //GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        
        // Peek to see if object is active already
        GameObject objectToSpawn = poolDictionary[tag].Peek();

        if (!objectToSpawn.activeInHierarchy) 
        {

            objectToSpawn = poolDictionary[tag].Dequeue();
            //Debug.Log("Object already active in scene! " + objectToSpawn.name);
           
        }
        else
        {
            Debug.Log("NOT ENOUGH " + objectToSpawn.name);
            if (shouldExpand)
            {
                Debug.Log("Expanding Payload of " + objectToSpawn.name);
                GameObject obj = (GameObject)Instantiate(objectToSpawn);
                obj.SetActive(false);
               // poolDictionary[tag].Enqueue(obj);
                objectToSpawn = obj;
            } else
            {
                Debug.Log("NOT ENOUGH " + objectToSpawn.name);
                Debug.Log("shouldExpand is Disabled");
            }
        }
        
        

        



        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        IPooledObject pooledObj = objectToSpawn.GetComponent<IPooledObject>();

        if (pooledObj != null)
        {
            pooledObj.OnObjectSpawn();
        }


        poolDictionary[tag].Enqueue(objectToSpawn); // Suspect.

        return objectToSpawn;
    }

}
