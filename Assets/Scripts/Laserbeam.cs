﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laserbeam : MonoBehaviour
{

   // private float defDistanceRay = 0.4f;
    private Transform firePoint;
#pragma warning disable 0649
    [SerializeField] private LineRenderer lineRenderer;
#pragma warning restore 0649
    public Vector2 targetPosAcc =Vector2.zero;

    //Transform firePoint;

   
    void Start()
    {
         Draw2DRay(transform.position, transform.position);

    }
    public void ShootLaser(Vector2 targetPos)
    {

        targetPosAcc = targetPos;

        if (Physics2D.Raycast(transform.position, targetPos))
        {
            RaycastHit2D _hit = Physics2D.Raycast(transform.position, targetPos);
            
          //  particleObj.transform.position = _hit.point;
          //  pSystem.Play();
            if (_hit.collider.TryGetComponent(out Enemy enemy))
            {
                
                Damage dmg = new Damage
                {
                    damageAmount = 1,
                    origin = transform.position,
                    pushForce = 0f,
                    isCrit = false,
                    originType = Damage.damageType.Turret,

                };
                Draw2DRay(transform.position, targetPos);
                _hit.collider.SendMessage("RecieveDamage", dmg);
            }
            

            Debug.Log("A");
        }
        else
        {
            // Draw2DRay(transform.position, transform.right * defDistanceRay);
            // pSystem.Pause();


            Draw2DRay(transform.position, transform.position);
            Debug.Log("B");
        }
    }

    void Draw2DRay(Vector2 startPos, Vector2 endPos)
    {
        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(1, endPos);
    }
    
}
