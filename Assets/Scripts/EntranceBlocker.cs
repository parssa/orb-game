﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntranceBlocker : MonoBehaviour
{
    public string regionName = "Lower Sector";
   

    void Unlocked()
    {
        HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
        unlockedPrompt.title = regionName;
        unlockedPrompt.text = "Region unlocked";
        unlockedPrompt.type = HeaderPopup.PromptType.EXCLAMATE;
        unlockedPrompt.textColor = Color.white;

       
        HeaderPopup.instance.gameObject.SetActive(true);
        HeaderPopup.instance.PlayPrompt(unlockedPrompt);
    }

    
}
