﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Audio;


public class AdManager : MonoBehaviour, IUnityAdsListener
{

    public static AdManager instance;
    private GameManager gm;
    private float volumeBeforeAd;
    public AudioMixer mainMixx;
    
    void Awake()
    {
        instance = this;
    }
    
    
    private string appStoreID = "3804060";
    private string playStoreID = "3804061";

    private string interstitialAd = "video";
    private string rewardedVideoAd = "rewardedVideo";

    public bool isTargetPlayStore;
    public bool isTestAd;

    
    
    
    private void Start()
    {
        gm = GameManager.instance;
        Advertisement.AddListener(this);
        InitializeAdvertisement();
    }

    private void InitializeAdvertisement()
    {
        if (isTargetPlayStore) 
        {
            Advertisement.Initialize(playStoreID, isTestAd);
            return;
        }
        
        Advertisement.Initialize(appStoreID, isTestAd);
    }
// Debug.Log("Running cloud version...");
        // Cloud.Leaderboards.LoadScores(leaderboardID: "com.Kazakan.leaderboard",  callback: scores =>
        // {
        //     int _temprank = -1;
        //     int counter = 0;
        //     // debugPlayerIDTex.SetText(user);
        //     Debug.Log("Callback");
        //     if (scores.Length > 0)
        //     {
        //         Debug.Log($"Retrieved {scores.Length} scores");
        //         numPlayersFound = scores.Length;
        //
        //         
        //         
        //         string myScores = "Leaderboard:\n";
        //         foreach (IScore score in scores)
        //         {
        //             myScores += "\t userID:" + score.userID + " leaderboardID" + score.leaderboardID + "userRank: " +
        //                         score.rank;
        //             
        //         }
        //
        //         Debug.Log(myScores);
        //         
        //         // Filter the score with the username
        //         for (int i = 0; i < scores.Length; i++)
        //         {
        //             counter++;
        //             Debug.Log("Comparing with player on board");
        //           
        //             if (user == scores[i].userID)
        //             {
        //                
        //                 Debug.Log("Hit this player");
        //                 _temprank = counter;
        //                 hitThisPlayer = true;
        //                 // rankingInWorld = scores[i].rank;
        //                 Debug.Log(_temprank);
        //                 rankingInWorld = _temprank; 
        //                 rank(scores[i].rank);
        //                 break; //TRY return
        //             }
        //
        //             
        //         }
        //     }
        //
        //     if (_temprank == -1)
        //     {
        //         Debug.Log("Did not hit player return length of leaderboard");
        //         rankingInWorld = scores.Length;
        //         rank(scores.Length);
        //     }
        //     else
        //     {
        //         Debug.Log("Hit rank of player, returning temprank.");
        //         Debug.Log(_temprank);
        //         rankingInWorld = _temprank;
        //         rank(_temprank);
        //     }
        //     
        //     if (numPlayersFound < 2 && numTimesSearched < 5)
        //     {
        //         Debug.Log("Getting ready to rerun");
        //         Invoke("LoadScoresFromHighScore", 0.5f);
        //     }
        //     
        //     
        //     
        //     
        // });

    public void PlayInterstitialAd()
    {
        // Check if the Ad is ready
        // IF the ad is ready, deploy it.
        if (!Advertisement.IsReady(interstitialAd)) return;;
        Advertisement.Show(interstitialAd);
    }


    public void PlayRewardedVideoAd()
    {
        if (!Advertisement.IsReady(rewardedVideoAd)) return;;
        Advertisement.Show(rewardedVideoAd);
    }

    public void OnUnityAdsReady(string placementId)
    {
        
        // Kinda Useless.
        
        // throw new System.NotImplementedException();
    }

    public void OnUnityAdsDidError(string message)
    {
        // 
        
        // throw new System.NotImplementedException();
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        
        // Mute Game HERE 
        
        Debug.Log("Ad did start");
        volumeBeforeAd = gm.mastVol;
        mainMixx.SetFloat("masterVol", -80);
            

        // Take in what volue volume was before




        // throw new System.NotImplementedException();
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {

        
        switch (showResult)
        {
            case ShowResult.Finished:
                // Finished Ad!
                if (placementId == rewardedVideoAd)
                {
                    Debug.Log("Reward the Player!");
                    mainMixx.SetFloat("masterVol", volumeBeforeAd);
                    GameManager.instance.HailMaryAdFinished();
                }

                if (placementId == interstitialAd)
                {
                    Debug.Log("Finished Interstit.");
                    mainMixx.SetFloat("masterVol", volumeBeforeAd);
                    GameManager.instance.HailMaryAdFinished();
                }
                
                break;
            case ShowResult.Failed:
                
                Debug.Log("Failed Ad");
                // Failed Ad!
                break;
            case ShowResult.Skipped:
                
                Debug.Log("Skipped Ad.");
                GameManager.instance.HailMaryAdFinished();
                // Skipped Ad >:(
                break;
        }
        
        
        // throw new System.NotImplementedException();
    }
}
