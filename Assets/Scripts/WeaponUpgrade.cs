﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeaponUpgrade 
{
    public string upgradeName;

    public enum UpgradeType
    {
        ARROW_DAMAGE,
        DASH_DAMAGE,
        CHARGE_DAMAGE,
        CRIT_MULT,
        CHARGE_COOLDOWN
    }   
    public UpgradeType upgradeType;

    public List<int> upgradeCosts;
    public int[] damageUpgradeIntervals; 
    [HideInInspector] public int numUpgraded = 0;
    [HideInInspector] public bool maxUpgraded = false;
    public float[] cooldownUpgradeIntervals;

}
