﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

    private Func<Vector3> GetCameraFollowPositionFunc;
    private Camera cam;


    public static CameraMotor instance;
    void Awake()
    {
        instance = this;
        cam = GetComponent<Camera>();
        
    }

    private void Start()
    {
        if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        {
            // Set Mobile size
        
            cam.orthographicSize = 4.8f;
            if (ControlsHandler.instance.tabletDevice)
            {
                // set ipad size
                cam.orthographicSize = 6.7f;
            }
        }
        else
        {
            // set computer size
            cam.orthographicSize = 5.6f;
        }
        
        
        
        
        
        
        // if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        // {
        //     cam.orthographicSize = 4.7f;
        // } else cam.orthographicSize = 5.5f;
    }


    public void Setup(Func<Vector3> GetCameraFollowPositionFunc)
    {
        this.GetCameraFollowPositionFunc = GetCameraFollowPositionFunc;

    }

    void Update()
    {
        Vector3 cameraFollowPosition = GetCameraFollowPositionFunc();
        cameraFollowPosition.z = transform.position.z;

        Vector3 cameraMoveDir = (cameraFollowPosition - transform.position).normalized;
        float distance = Vector3.Distance(cameraFollowPosition, transform.position);
        float cameraMoveSpeed = 3f;

        if (distance > 0)
        {
            Vector3 newCameraPosition = transform.position + cameraMoveDir * (distance * cameraMoveSpeed * Time.deltaTime);

            float distanceAfterMoving = Vector3.Distance(newCameraPosition, cameraFollowPosition);
            if (distanceAfterMoving > distance)
            {
                // We've overshot the player
                newCameraPosition = cameraFollowPosition;
            }

            transform.position = newCameraPosition;

        }


    }

    public IEnumerator Shake(float duration, float magnitude)
    {
        

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            if (PauseMenu.GameIsPaused)
                yield break;

            if (!PauseMenu.GameIsPaused)
            {
                float xShake = UnityEngine.Random.Range(-0.01f, 0.01f) * magnitude;
                float yShake = UnityEngine.Random.Range(-0.01f, 0.01f) * magnitude;


                transform.position = new Vector3(transform.position.x + xShake, transform.position.y + yShake, transform.position.z);


                elapsed += Time.deltaTime;

                yield return null;

            }
            
        }

        //transform.position = originalPos;
    }





}
