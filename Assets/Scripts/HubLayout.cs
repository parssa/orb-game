﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// using Unity.UNetWeaver;
using UnityEngine.Serialization;

public class HubLayout : MonoBehaviour
{

    public GameObject menu;
    private CharacterMenu characterMenu;
    public static HubLayout instance;
    public WaveSpawner waveSpawner;
    private Player player;
    
    [Header("Stamina Bar")]
    // Stamina Bar
    public TextMeshProUGUI staminaText;
    public RectTransform staminaBar;
    public RectTransform staminaDamageBar;
    public Image staminaImage;

    [Header("Score Num")]
    // Score Num
    public TextMeshProUGUI scoreText;

    private ComboManager comboManager;
    public BowManager bowManager;
    private GameManager gameManager;
    
    
    public int displayScore;

    public float maxFontSize;
    public float normalFontSize;

    // Souls Num
    [Header("Souls Num")]
    public TextMeshProUGUI soulsText;
    public TextMeshProUGUI shrineSoulsText;
    public TextMeshProUGUI soulsTextMobile;
    public TextMeshProUGUI shrineSoulsTextMobile;
    public int displayShrineSouls;
    public int displaySouls;
    public float maxSoulsFontSize;
    public float normalSoulsFontSize;
    public float maxSoulsFontSizeMobile;
    public float normalSoulsFontSizeMobile;
    public Image soulsImageDesktop;
    public Image shrineSoulsImageDesktop;
    public Image soulsImageMobile;
    public Image shrineSoulsImageMobile;
    

    [Header("Health Bar")]
    // Health Bar
    public RectTransform healthBar;
    public RectTransform damageBar;
    public TextMeshProUGUI healthText;
    public Image healthImage;
    public Image healthHitImage;

    [Header("Orb Bar")]
    // Health Bar
    public RectTransform orbBar;
    public RectTransform orbDamageBar;
    public TextMeshProUGUI orbHealthText;
    public Image orbImage;

    [Header("Wave Bar")]
    // Wave Bar
    public RectTransform waveBar;
    private WaveUpdaterText waveUpdaterText;
    //public Image waveBarBackPieceImage;

    private Image waveBarImage;
    private float timeInBetweenWaves;
    private Color normalWaveColor;
    private Color intermissionBarColor;


    [Header("Effect Indicators")]
    //public Image orbModeImage;
    public TextMeshProUGUI eventPrompt;
    private Color regularEventTextColor;
    private Color pingPongedTextColor;
    public Animator shopPromptAnim;
    public TextMeshProUGUI shopPromptText;

    private Vector3 diffVecStamina;
    private Vector3 diffVecHealth;
    private Vector3 diffOrbVecHealth;
    private Vector3 diffVec;
    private Vector3 currVec;
    private Vector3 completion;

    [Header("Mute Button")]
    public Image volumeCondition;
    public Sprite mutedSprite;
    public Sprite unmutedSprite;

    [Header("Perk Slots")]
    public Image perkSlotOne;
    public Image perkSlotTwo;
    public Image perkSlotThree;
    public Image perkSlotFour;

    [Header("Perk Acquired Popup")]
    public GameObject perkAcquiredPopup;
    public bool perkPopupShowing = false;
    private const float TIME_TILL_HIDE_PERKPOPUP = 5f;
    public float timePerkAcquired;
    public Image perkIconImage;
    public TextMeshProUGUI perkDescText;
    public TextMeshProUGUI perkAcquiredText;
    
    [Space]
    // public GameObject fadeIntoScene;

    
    public GameObject mobileControls;
    private int indexOfMobileControls; // We use this to refer to where we need to move the controls back after editing
    
    private void Awake()
    {
        instance=this;
        waveUpdaterText = WaveUpdaterText.instance;
    
        // fadeIntoScene.SetActive(true);

        staminaImage.color = Color.clear; // S  tamina Icon
        healthImage.color = Color.clear; // Health Icon
        healthHitImage.color = Color.clear; // Health Damaged Icon
        orbImage.color = Color.clear; // Orb Mode Image

        waveBarImage = waveBar.GetComponent<Image>();
        normalWaveColor = new Color32(106, 40, 45, 255);
        waveBarImage.color = normalWaveColor;
        //waveBarBackPieceImage.color = normalWaveColor;
        intermissionBarColor = new Color32(193, 94, 65, 255);


        normalFontSize = scoreText.fontSize; // scoreFont
        maxFontSize = 95f; // enlargedScoreFont

        // Souls Font
        normalSoulsFontSize = soulsText.fontSize; // soulsFont
        maxSoulsFontSize = 60f; // Enlarged Souls Font

        normalSoulsFontSizeMobile = 45f;
        maxSoulsFontSizeMobile = 100f;
        

        regularEventTextColor = eventPrompt.color; // PRESS SPACEBAR TO ACTIVATE
        pingPongedTextColor = new Color(1f, 1f, 1f, 1f);
      //  eventPrompt.alpha = 0f;

        comboManager = ComboManager.instance;

        timePerkAcquired = 0f;



    }

    private void Start()
    {
        
        gameManager = GameManager.instance;
        player = gameManager.player;
        displayScore = gameManager.GetCurrPoints();
        displaySouls = gameManager.GetCurrSouls();
        
        displayShrineSouls = gameManager.shrineSouls;
        timeInBetweenWaves = waveSpawner.timeBetweenWaves;

        characterMenu = CharacterMenu.instance;
        perkSlotFour.color = Color.clear;
        perkSlotThree.color = Color.clear;
        perkSlotTwo.color = Color.clear;
        perkSlotOne.color = Color.clear;

        perkAcquiredPopup.SetActive(false);


        

        menu.SetActive(true);
        mobileControls.transform.SetAsFirstSibling();
        purchaseTurretButton.SetActive(false);
        purchaseGuardianButton.SetActive(false);
        // if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        // {
        //     
        //     Debug.Log("mobile version");
        //     // Text Desktop
        //     soulsText.color = Color.clear; 
        //     shrineSoulsText.color = Color.clear;
        //     
        //     // Images Desktop
        //     soulsImageDesktop.color = Color.clear;
        //     shrineSoulsImageDesktop.color = Color.clear;
        //     
        //     // Text Mobile
        //     soulsTextMobile.color = Color.white;
        //     shrineSoulsTextMobile.color = Color.white;
        //     
        //     // Images Mobile
        //     soulsImageMobile.color = Color.white;
        //     shrineSoulsImageMobile.color = Color.white;
        // }
        // else
        // {
        //     Debug.Log("desktop version");
        //     // Text Desktop
        //     soulsText.color = Color.white; 
        //     shrineSoulsText.color = Color.white;
        //     
        //     // Images Desktop
        //     soulsImageDesktop.color = Color.white;
        //     shrineSoulsImageDesktop.color = Color.white;
        //     
        //     // Text Mobile
        //     soulsTextMobile.color = Color.clear;
        //     shrineSoulsTextMobile.color = Color.clear;
        //     
        //     // Images Mobile
        //     soulsImageMobile.color = Color.clear;
        //     shrineSoulsImageMobile.color = Color.clear;
        // }

        // if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        // {
            
        // }

    }
    void Update()
    {

        UpdateMenu();
        // if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        // {
            if (turretSlotTaken)
            {
                if (!turretSlot.CloseToPlayer || gameManager.GetCurrSouls() < turretSlot.activateCost)
                {
                    RemoveTurretFromSlot();
                }
            }

            if (guardianSlotTaken)
            {
                if (!statueSlot.CloseToPlayer || statueSlot.Activated)
                {
                    RemoveStatueFromSlot();
                }
            }
        // }
    }

    // Update Character Information
    public void UpdateMenu()
    {

        // --- MUTE BUTTON ---
        #region
        // if (gameManager.disableAllSounds)
        // {
        //     volumeCondition.sprite = mutedSprite;
        // }

        // else if (!gameManager.disableAllSounds)
        // {
        //     volumeCondition.sprite = unmutedSprite;
        // }


        #endregion


        // --- EFFECT INDICATORS --- 


        //Stamina Pickup
        #region
        if (player.staminaRegenEffecting)
        {
            staminaImage.color = Color.white;
        }
        else if (staminaImage.color != Color.clear)
        {
            staminaImage.color = Color.Lerp(staminaImage.color, Color.clear, Time.deltaTime);
        }
        else
        {
            staminaImage.color = Color.clear;
        }
        #endregion

        // Health Pickup
        #region
        if (player.healthRegenEffecting)
        {
            healthImage.color = Color.white;
        }
        else if (healthImage.color != Color.clear)
        {
            healthImage.color = Color.Lerp(healthImage.color, Color.clear, Time.deltaTime);
        }
        else
        {
            healthImage.color = Color.clear;
        }
        #endregion

        // --- HEALTH BAR --
        #region
        float currHealth = player.GetHealth();
        float maxHealth = player.GetMaxHealth();
        healthText.text = currHealth.ToString() + "/" + maxHealth.ToString();


        float diffHealth = currHealth / maxHealth;
        diffVecHealth = new Vector3(diffHealth, 1, 1);




        if (diffVecHealth.x <= healthBar.localScale.x)
        {
            healthBar.localScale = diffVecHealth;
        }
        else
        {
            healthBar.localScale = Vector3.Lerp(healthBar.localScale, diffVecHealth, 0.1f);
        }

        damageBar.localScale = Vector3.Lerp(damageBar.localScale, diffVecHealth, 0.1f);

        if (player.gotHit)
        {
            healthHitImage.color = Color.white;
            player.gotHit = false;
        }
        else
        {
            if (healthHitImage.color != Color.clear)
            {
                healthHitImage.color = Color.Lerp(healthHitImage.color, Color.clear, 0.1f);
            }
            else
            {
                healthHitImage.color = Color.clear;
            }
        }



        #endregion


        // --- ORB HEALTH BAR --
        #region
        float currOrbHealth = OrbHitBox.instance.hitpoint;
        float orbMaxHealth = OrbHitBox.instance.maxHitpoint;
        orbHealthText.text = currOrbHealth.ToString() + "/" + orbMaxHealth.ToString();
        if (OrbHitBox.instance.gotHit)
        {
            orbImage.color = Color.white;
            OrbHitBox.instance.gotHit = false;
        }
        else
        {
            if (orbImage.color != Color.clear)
            {
                orbImage.color = Color.Lerp(orbImage.color, Color.clear, 0.1f);
            }
            else
            {
                orbImage.color = Color.clear;
            }
        }

        float diffOrbHealth = currOrbHealth / orbMaxHealth;
        diffOrbVecHealth = new Vector3(diffOrbHealth, 1, 1);

        //healthBar.localScale = new Vector3 (diffHealth, 1, 1);


        if (diffOrbVecHealth.x <= orbBar.localScale.x)
        {
            orbBar.localScale = diffOrbVecHealth;
        }
        else
        {
            orbBar.localScale = Vector3.Lerp(orbBar.localScale, diffOrbVecHealth, 0.1f);
        }

        orbDamageBar.localScale = Vector3.Lerp(orbDamageBar.localScale, diffOrbVecHealth, 0.1f);

        #endregion

        // --- WAVE BAR ---
        #region


        float currEnemies = waveSpawner.curr_num_enemies;
        float waveEnemies = waveSpawner.NumEnemiesForWave();

        float waveStatus = waveSpawner.NumEnemiesLeftInWave() / waveEnemies;

        float timeLeft = Mathf.Floor(waveSpawner.WaveCountdown * 10) / 10;
        if (waveSpawner.State == WaveSpawner.SpawnState.TUTORIAL) timeLeft = 1f;
        if (timeLeft == 0.1)
        {
            timeLeft = 0f;
        }

        float waveLoadingStatus = 1 - (timeLeft / timeInBetweenWaves);
        if (waveStatus < 0)
        {
            waveUpdaterText.hitZeroScale = true;
            waveStatus = 0.1f;
        } 
        
        if (waveSpawner.State == WaveSpawner.SpawnState.COUNTING)
        {

            waveStatus = waveLoadingStatus;
            waveBarImage.color = intermissionBarColor;

        }
        else
        {
            if (waveBarImage.color != normalWaveColor)
            {
                waveBarImage.color = Color.Lerp(waveBarImage.color, normalWaveColor, 0.1f);
            }

        }
        //waveBarBackPieceImage.color = waveBarImage.color;
        completion = new Vector3(waveStatus, 1, 1);
        waveBar.localScale = Vector3.Lerp(waveBar.localScale, completion, 0.1f);
        #endregion


        // --- SCORE STATUS --- 
        #region
        if (gameManager.GetCurrPoints() == 0)
        {
            scoreText.text = "000000";
        }
        else if (displayScore < gameManager.GetCurrPoints())
        {
            StartCoroutine(ScoreUpdater());

        }
        else if (displayScore == gameManager.GetCurrPoints())
        {

            if (scoreText.fontSize > normalFontSize)
            {
                scoreText.fontSize = Mathf.Lerp(scoreText.fontSize, normalFontSize, 0.1f);
            }
            if (displayScore >= 1000)
            {
                scoreText.text = "00" + displayScore.ToString();
            }
            else if (displayScore >= 100)
            {
                scoreText.text = "000" + displayScore.ToString();
            }
            else if (displayScore >= 10)
            {
                scoreText.text = "0000" + displayScore.ToString();
            }
            else
            {
                scoreText.text = "00000" + displayScore.ToString();
            }
        }

        if (!comboManager.comboIsHappening)
        {
            if (scoreText.color != Color.white) scoreText.color = Color.Lerp(scoreText.color, Color.white, Time.deltaTime * 4f);
        }
        if (comboManager.comboIsHappening)
        {
            if (Time.time - comboManager.recentKillTime <= 0.2f) scoreText.color = Color.Lerp(scoreText.color, bowManager.currBow.darkTextColor, Time.deltaTime * 2f);
            else if (scoreText.color != bowManager.currBow.lightTextColor) scoreText.color = Color.Lerp(scoreText.color, bowManager.currBow.lightTextColor, Time.deltaTime * 3f);

        }
        #endregion


        // --- SOULS STATUS ---
        #region

        
        if (displaySouls != gameManager.GetCurrSouls())
        {
            StartCoroutine(SoulsUpdater());
        }
        else if (displaySouls == gameManager.GetCurrSouls())
        {
            if (soulsText.fontSize > normalSoulsFontSize)
            {
                soulsText.fontSize = Mathf.Lerp(soulsText.fontSize, normalSoulsFontSize, 0.1f);
                soulsText.text = displaySouls.ToString();
            }
            
            if (soulsTextMobile.fontSize > normalSoulsFontSizeMobile)
            {
                soulsTextMobile.fontSize = Mathf.Lerp(soulsTextMobile.fontSize, normalSoulsFontSizeMobile, 0.1f);
                soulsTextMobile.text = displaySouls.ToString();
            }
        }
        #endregion


        // TODO make this prettier but i dont care enough right now
        displayShrineSouls = gameManager.shrineSouls;
        shrineSoulsText.SetText(displayShrineSouls.ToString());
        shrineSoulsTextMobile.SetText(displayShrineSouls.ToString());
        // --- STAMINA BAR --- 
        #region
        float currStamina = player.currStamina;
        float maxStamina = player.maxStamina;
        float staminaStatus = currStamina / maxStamina;
        staminaText.text = currStamina.ToString() + "/" + maxStamina.ToString();
        diffVecStamina = new Vector3(staminaStatus, 1, 1);

        if (diffVecStamina.x <= staminaBar.localScale.x)
        {
            staminaBar.localScale = diffVecStamina;
        }
        else if (diffVecStamina.x > staminaBar.localScale.x)
        {
            staminaBar.localScale = Vector3.Lerp(staminaBar.localScale, diffVecStamina, 0.1f);
        }

        staminaDamageBar.localScale = Vector3.Lerp(staminaDamageBar.localScale, diffVecStamina, 0.1f);
        #endregion

        #region 
        if (perkPopupShowing)
        {
            if (Time.time - timePerkAcquired > TIME_TILL_HIDE_PERKPOPUP)
            {
                perkPopupShowing = false;
                perkAcquiredPopup.SetActive(false);
            }
        }
        #endregion


        if (PauseMenu.AdjustingControls)
        {
            mobileControls.transform.SetAsLastSibling();
        } else 
        {
            mobileControls.transform.SetAsFirstSibling();
        }

    }


    // private IEnumerator SoulsUpdater()
    // {
    //     float timePassed = 0;
    //     while (timePassed < 0.1)


    //     {
    //         if (PauseMenu.GameIsPaused)
    //             yield break;

    //         if (displaySouls < gameManager.GetCurrSouls())
    //         {
    //             displaySouls++;
    //             soulsText.fontSize = Mathf.Lerp(soulsText.fontSize, maxSoulsFontSize, 0.1f);
    //             soulsText.text = displaySouls.ToString();
    //         }
    //         if (displaySouls > gameManager.GetCurrSouls())
    //         {
    //             displaySouls--;
    //             soulsText.fontSize = Mathf.Lerp(soulsText.fontSize, maxSoulsFontSize, 0.1f);
    //             soulsText.text = displaySouls.ToString();
    //         }
    //         timePassed += Time.deltaTime;
    //         yield return null;
    //     }


    // }
     public IEnumerator SoulsUpdater()
    {
        
        while (displaySouls != gameManager.GetCurrSouls())
        {
            // if (PauseMenu.GameIsPaused && ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
            //     yield break;

            if (displaySouls < gameManager.GetCurrSouls())
            {
                displaySouls++;
                
                soulsText.fontSize = Mathf.Lerp(soulsText.fontSize, maxSoulsFontSize, 0.1f);
                soulsText.text = displaySouls.ToString();
                
                soulsTextMobile.fontSize = Mathf.Lerp(soulsTextMobile.fontSize, maxSoulsFontSizeMobile, 0.1f);
                soulsTextMobile.text = displaySouls.ToString();
                
                
            }
            if (displaySouls > gameManager.GetCurrSouls())
            {
                displaySouls--;
                soulsText.fontSize = Mathf.Lerp(soulsText.fontSize, maxSoulsFontSize, 0.1f);
                soulsText.text = displaySouls.ToString();
                
                soulsTextMobile.fontSize = Mathf.Lerp(soulsTextMobile.fontSize, maxSoulsFontSizeMobile, 0.1f);
                soulsTextMobile.text = displaySouls.ToString();
            }
            
            yield return new WaitForSecondsRealtime(0.01f);
        }


    }

    private IEnumerator ScoreUpdater()
    {
        float timePassed = 0;

        while (timePassed < 0.5)
        {
            if (PauseMenu.GameIsPaused)
                yield break;

            if (displayScore < gameManager.score)
            {
                displayScore++;
                scoreText.fontSize = Mathf.Lerp(scoreText.fontSize, maxFontSize, 0.3f);
                if (displayScore >= 1000)
                {
                    scoreText.text = "00" + displayScore.ToString();
                }
                else if (displayScore >= 100)
                {
                    scoreText.text = "000" + displayScore.ToString();
                }
                else if (displayScore >= 10)
                {
                    scoreText.text = "0000" + displayScore.ToString();
                }
                else
                {
                    scoreText.text = "00000" + displayScore.ToString();
                }


            }

            timePassed += Time.deltaTime;
            yield return null;
        }
    }


    public void AddPerkToPerksDisplay(Sprite perkIcon)
    {
        if (player.perkList.Count == 1)
        {
            perkSlotOne.sprite = perkIcon;
            perkSlotOne.color = Color.white;
        }
        if (player.perkList.Count == 2)
        {
            perkSlotTwo.sprite = perkIcon;
            perkSlotTwo.color = Color.white;
        }
        if (player.perkList.Count == 3)
        {
            perkSlotThree.sprite = perkIcon;
            perkSlotThree.color = Color.white;
        }
        if (player.perkList.Count == 4)
        {
            perkSlotFour.sprite = perkIcon;
            perkSlotFour.color = Color.white;
        }
    }

    public void ShowSomething()
    {
        if (bowManager.CanAfford())
        {
            OpenPromptWeapon();
            StartCoroutine(ClosePromptAfterCoupleSeconds());
        } else if (gameManager.orb.CanAfford())
        {
            OpenPromptOrb();
            StartCoroutine(ClosePromptAfterCoupleSeconds());
        }
    }


    IEnumerator ClosePromptAfterCoupleSeconds()
    {
        yield return new WaitForSeconds(4f);
        shopPromptAnim.SetTrigger("closePrompt");
    }

    public void OpenPromptWeapon()
    {
        shopPromptText.SetText("Weapon Upgrade!");
        shopPromptAnim.SetTrigger("openPrompt");
        characterMenu.TriggerWeaponMenu();
    }

    public void OpenPromptOrb()
    {
        shopPromptText.SetText("Orb Upgrade!");
        shopPromptAnim.SetTrigger("openPrompt");
        characterMenu.TriggerOrbMenu();
    }

    public void CloseShopPrompt()
    {
        shopPromptAnim.SetTrigger("closePrompt");
    }

    public void PerkAcquiredPopup(Sprite perkIcon, PerkManager.Perk acquiredPerk)
    {
        perkIconImage.sprite = perkIcon;
        //PerkManager.Perk acquiredPerk = PerkManager.instance.FetchPerk(perkType);
        perkAcquiredText.color = acquiredPerk.lightPerkColor;
        perkDescText.color = acquiredPerk.boldPerkColor;
        perkDescText.SetText(acquiredPerk.perkDescription);
    
        timePerkAcquired = Time.time;
        perkPopupShowing = true;
        perkAcquiredPopup.SetActive(true);

    }

    public bool turretSlotTaken = false;
    public Turret turretSlot = null;
    public bool displayingActivateButton = false;
    public void ActivateNearbyTurret()
    {
        if (turretSlot.TryActivate())
        {
            RemoveTurretFromSlot();
            purchaseTurretButton.SetActive(false);
        }
    }

    public GameObject purchaseTurretButton;
    

    void RemoveTurretFromSlot()
    {
        turretSlotTaken = false;
        purchaseTurretButton.SetActive(false);
        if (TutorialManager.instance.hasCompletedTurretTutorial) turretSlot.ArrowAnimatorSetState(false);
        turretSlot = null;
        
        
    }

    public void SetTurretAsSlot(Turret _thisTurret)
    {
        // if (ControlsHandler.instance.currPlatform != ControlsHandler.CurrPlatform.MOBILE) return;
        if (turretSlotTaken) return;
        turretSlot = _thisTurret;
        turretSlotTaken = true;
        turretSlot.ArrowAnimatorSetState(true);
        purchaseTurretButton.SetActive(true);
        
    }

    public GameObject purchaseGuardianButton;
    public bool guardianSlotTaken = false;
    [HideInInspector] public PerkStatue statueSlot;

    public void SetPerkStatueAsSlot(PerkStatue _thisStatue)
    {
        // if (ControlsHandler.instance.currPlatform != ControlsHandler.CurrPlatform.MOBILE) return;;
        if (guardianSlotTaken) return;
        statueSlot = _thisStatue;
        guardianSlotTaken = true;
        purchaseGuardianButton.SetActive(true);
    }

    public void ActivateNearbyGuardian()
    {
        if (statueSlot.CanActivate())
        {
            statueSlot.TryActivate();
        }
    }

    void RemoveStatueFromSlot()
    {
        
        guardianSlotTaken = false;
        purchaseGuardianButton.SetActive(false);
        statueSlot = null;
    }

    
    public Boss boss;
    public GameObject activateBossButton;
    
    public void ActivateBossButtonTrigger()
    {
        boss.ActivateBoss();
    }
    
    
    
    


}

