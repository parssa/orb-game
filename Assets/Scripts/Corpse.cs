﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour, IPooledObject
{
    private GameObject corpseObj;
    private SpriteRenderer spriteRenderer;
    public Sprite corpseSprite;

    public EnemyManager.EnemyEnum thisCorpseIs;

    public float lifespan;
    public Vector2 lastPushDir;

    private Vector2 startPos;
    private float moveAmount = 0.4f;

    private float timeMade;
    public Vector3 startScale = new Vector3(3, 3, 1);

    private SoundManager soundManager;
    private List<AudioClip> deathSounds = new List<AudioClip>();
    private List<AudioClip> bigDeathSounds = new List<AudioClip>();

    // Start is called before the first frame update

    void Awake()
    {
        soundManager = SoundManager.instance;
        deathSounds.Add(soundManager.GetSounds("enemy_death_1").clip);
        deathSounds.Add(soundManager.GetSounds("enemy_death_2").clip);
     //   deathSounds.Add(soundManager.GetSounds("enemy_death_3").clip);
        deathSounds.Add(soundManager.GetSounds("enemy_death_4").clip);
        deathSounds.Add(soundManager.GetSounds("enemy_death_5").clip);
    }
    void Start()
    {
        corpseObj = transform.GetChild(0).gameObject;
        spriteRenderer = corpseObj.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = corpseSprite;

        lifespan = Random.Range(3f, 5f);
        startPos = transform.position;

        float angleAdj = Random.Range(10f, -10f);
        Quaternion corpseRot = transform.rotation * Quaternion.Euler(0, 0, angleAdj);
        corpseObj.transform.rotation = corpseRot;

        timeMade = Time.time;

        transform.localScale = startScale;


        

    }

    public void OnObjectSpawn()
    {
        corpseObj = transform.GetChild(0).gameObject;
        moveAmount = 0.4f;

        transform.GetChild(1).GetComponent<ParticleSystem>().Play();

        //spriteRenderer = corpseObj.GetComponent<SpriteRenderer>();
        

        lifespan = Random.Range(3f, 5f);
        startPos = transform.position;

        float angleAdj = Random.Range(10f, -10f);
        Quaternion corpseRot = transform.rotation * Quaternion.Euler(0, 0, angleAdj);
        corpseObj.transform.rotation = corpseRot;

        timeMade = Time.time;
        transform.localScale = startScale;

        if (GameManager.instance.mastVol > -80f)
        {
            Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
           
            var shootSound = PlayClipAt(deathSounds[Random.Range(0, deathSounds.Count)], soundPos);
            shootSound.pitch = Random.Range(0.65f, 1f);
            shootSound.volume = 0.2f;
            shootSound.reverbZoneMix = 0.078819f;
            
            
            // AudioSource.PlayClipAtPoint(deathSounds[Random.Range(0, deathSounds.Count)], soundPos);
        } else Debug.Log("Game has no vol rn");
        // Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
        // AudioSource.PlayClipAtPoint(deathSounds[Random.Range(0, deathSounds.Count)], soundPos);
    }

    // Update is called once per frame
    void Update()
    {
        moveAmount = Mathf.Lerp(moveAmount, 0f, Time.deltaTime * lifespan);
        Vector2 moveTowardV = new Vector2(transform.position.x + lastPushDir.x, transform.position.y + lastPushDir.y);
        transform.position = Vector2.MoveTowards(transform.position, moveTowardV, moveAmount);

        // spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.clear, 3 * Time.deltaTime);
        //Destroy(gameObject, 5f);
        spriteRenderer.sprite = corpseSprite;
        if (Vector2.Distance(startPos, transform.position) >= 4 || Vector2.Distance(startPos, transform.position) <= -4)
        {
            // transform.GetChild(1).gameObject.SetActive(false);
            // Destroy(gameObject, lifespan);
            KillCorpse();
        }

        if (Time.time - timeMade > 2f && timeMade != 0f)
        {
            
            KillCorpse();

        }


        //transform.localScale = Vector3.Lerp( new Vector3(3f, 3f, 1f), new Vector3(2f, 2f, 1f), Mathf.PingPong(Time.deltaTime * 2, 10f));
        //spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.clear, lifespan* Time.deltaTime);
        // Destroy(gameObject, lifespan + 0.2f);
    }

    void KillCorpse()
    {
        //transform.GetChild(1).gameObject.SetActive(false);
        transform.localScale = startScale;
        startPos = transform.position;
        timeMade = 0f;

        gameObject.SetActive(false);
    }
    
    AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        
        
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.spatialBlend = 0.5f;
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }
}
