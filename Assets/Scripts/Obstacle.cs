﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public ContactFilter2D filter;
    private PolygonCollider2D polyCollider;

    private const string BEHIND_LAYER = "Above Floor Behind";
    private const string FRONT_LAYER = "Above Floor In Front";

    private Collider2D[] hits = new Collider2D[10];

    [HideInInspector]
    public SpriteRenderer this_Sprite;

    [HideInInspector]
    public Sprite standardSprite;

    public Color regularColor;
    public Color fadedColor;

    public bool alwaysInFront = true;


    public bool isShrine = false;
    public bool isGuardian = false;
   
    [HideInInspector] public bool bossIsActive = false;

    [HideInInspector] public bool playerContact = false;
    [HideInInspector]public bool enemyContact = false;




    protected virtual void Start()
    {
        polyCollider = GetComponent<PolygonCollider2D>();
        this_Sprite = GetComponent<SpriteRenderer>();
        regularColor = this_Sprite.color;
        fadedColor = regularColor;
        fadedColor.a = 0.6f;
        standardSprite = this_Sprite.sprite;

        if (isShrine) fadedColor.a = 0.3f;




    }

    protected virtual void Update()
    {

        // if (isGuardian)
        // {
        //     if (GetComponent<Guardian>().activated)
        //         return;
        // }
        // Debug.Log("NOTSKIPPING");
        // Collisions

        if (bossIsActive) return;

        polyCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            OnCollide(hits[i]);

            //Cleans array
            hits[i] = null;
        }
        if (isShrine && transform.parent.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN) this_Sprite.color = Color.clear;
        else if (playerContact)
        {
            enemyContact = false;
            if (!alwaysInFront)
            {

                this_Sprite.sortingLayerName = FRONT_LAYER;
            }

            StartCoroutine(FadeOut());

        }
        else if (!playerContact)
        {
            if (enemyContact && !isShrine) this_Sprite.sortingLayerName = FRONT_LAYER;
            if (!alwaysInFront)
            {

                this_Sprite.sortingLayerName = BEHIND_LAYER;
            }

            StartCoroutine(FadeIn());

        }





    }
    protected virtual void OnCollide(Collider2D coll)
    {

        // Not Much

        if (bossIsActive) return;

        if (coll.CompareTag("Player"))
        {
            playerContact = true;
        }
        if (coll.CompareTag("Fighter"))
        {
            enemyContact = true;
        }else if (!coll.CompareTag("Fighter")) enemyContact = false;

    }

    public IEnumerator FadeOut()
    {
        float timePassed = 0;
        //fadeOutIsDone = false;
        while (timePassed < 0.1)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            if (isShrine && !transform.parent.GetComponent<SpawnPoint>().shrineIsActive)
            {
                yield break;
            }
            if (this_Sprite.color != fadedColor)
            {
                this_Sprite.color = Color.Lerp(this_Sprite.color, fadedColor, 0.1f);
            }

            timePassed += Time.deltaTime;
            yield return null;
        }
        playerContact = false;
        //fadeOutIsDone = true;
    }

    public IEnumerator FadeIn()
    {
        float timePassed = 0;
        //fadeInIsDone = false;
        while (timePassed < 0.07)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            if (isShrine && !transform.parent.GetComponent<SpawnPoint>().shrineIsActive)
            {
                yield break;
            }
            if (this_Sprite.color != regularColor)
            {
                this_Sprite.color = Color.Lerp(this_Sprite.color, regularColor, 0.1f);
            }

            timePassed += Time.deltaTime;
            yield return null;
        }
        //fadeInIsDone = true;

    }

    public void BossIsActive()
    {
        this_Sprite.color = Color.clear;
    }
}
