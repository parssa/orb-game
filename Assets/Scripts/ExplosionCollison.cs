﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionCollison : Collidable, IPooledObject
{

    // Damage
    public int damage = 3;
    public int critMultiplier = 2;
    public float pushForce = 5;
    //private float box_Collider_x;
    private float timeAlive;
    //  private float box_Collider_y;

    private CameraMotor cameraShake;

    private ParticleSystem explosion;
    private List<ParticleCollisionEvent> collisionEvents;

    public bool isPlayerShot = false;
    public bool isShrineExplosion = false;
    public bool isTurretShot = false;

    public enum ExplosionType
    {
        NEUTRAL,
        TURRET_SHOT,
        PLAYER_SHOT,
        RED_ENEMY_DEATH,
        GUARDIAN_DEATH,
        SHRINE_BREAK
    }

    public ExplosionType explosionType;

    private int numCollidersInfluenced = 0;
    private const int CAP_COLLIDERS = 15;

    [SerializeField]private float particleSystemDuration = 0.5f;

    private void Awake()
    {
        timeAlive = 0f;
        //cameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMotor>();
        cameraShake = CameraMotor.instance;
        //box_Collider_x = GetComponent<BoxCollider2D>().size.x;
        //  box_Collider_x = 0.0001f;
        //box_Collider_y = GetComponent<BoxCollider2D>().size.y;
        //  box_Collider_y = 0.0001f;

        //  GetComponent<BoxCollider2D>().size = new Vector2(box_Collider_x, box_Collider_y);

        explosion = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();



        //  
    }

    public void OnObjectSpawn()
    {
        if (!explosion.isPlaying)
        {

            explosion.Play();
        }
        StartCoroutine(cameraShake.Shake(0.05f, 10f));
        timeAlive = Time.time;

        numCollidersInfluenced = 0;
        //box_Collider_x = 0.0001f;
        //box_Collider_y = 0.0001f;
        //GetComponent<BoxCollider2D>().size = new Vector2(box_Collider_x, box_Collider_y);

    }


    protected override void Update()
    {
        base.Update();


        if (Time.time - timeAlive >= particleSystemDuration)
        {

            DestroyExplosion();

        }
        if (numCollidersInfluenced > CAP_COLLIDERS)
        {
            Debug.Log("Hit Too Many!");
            DestroyExplosion();
        }

    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = explosion.GetCollisionEvents(other, collisionEvents);


        Collider2D coll = other.GetComponent<Collider2D>();

        if (explosionType == ExplosionType.RED_ENEMY_DEATH)
        {
            if (!coll.gameObject.activeInHierarchy) return;
            if ((coll.CompareTag("Fighter")) || (coll.name == "Player"))
            {
                //Debug.Log(other.name);

                // Create a new damage object, before sending it to the player 
                int damageToDeal = Random.Range(damage, damage * critMultiplier + 1);
                Damage dmg = new Damage
                {
                    damageAmount = Random.Range(damage, damage * critMultiplier),
                    origin = transform.position,
                    pushForce = pushForce,
                    isCrit = damageToDeal >= damage * critMultiplier
                };

                if (coll.gameObject != null) coll.SendMessage("RecieveDamage", dmg);
                else Debug.Log("NULL GAMEOBJECT EXPLOSION");

                numCollidersInfluenced += 1;

            }
        } else if (explosionType == ExplosionType.PLAYER_SHOT)
        {
            if ((coll.CompareTag("Fighter") || (coll.CompareTag("Shrine") ) || (coll.CompareTag("Guardian") ) || coll.CompareTag("Boss")))
            {
                // Debug.Log(other.name);
                if (!coll.gameObject.activeInHierarchy) return;
                // Create a new damage object, before sending it to the player 
                int damageToDeal = Random.Range(damage, damage * critMultiplier + 1);

                Damage dmg = new Damage
                {
                    damageAmount = Random.Range(damage, damage * critMultiplier),
                    origin = transform.position,
                    pushForce = pushForce,
                    isCrit = damageToDeal >= damage * critMultiplier
                };

                DamagePopup.Create(transform.position, damageToDeal, false, DamagePopup.DAMAGE);



                if (coll.gameObject != null) coll.SendMessage("RecieveDamage", dmg);
                else Debug.Log("NULL GAMEOBJECT EXPLOSION");

                numCollidersInfluenced += 1;
            }
        }


        int i = 0;
        while (i < numCollisionEvents)
        {
            i++;

        }
    }

    void DestroyExplosion()
    {
        //isShrineExplosion = false;
        numCollidersInfluenced = 0;
        gameObject.SetActive(false);
    }

}