﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

//using UnityEngine.EventSystems;


public class Turret : Fighter
{
    private Transform playerTransform;
    private Camera cam;
    private Weapon weapon;
    private ControlsHandler controlsHandler;
    private CharacterMenu characterMenu;
    public Transform thisTarget = null;

    public AudioMixer mainMixx;
    private CameraMotor cameraShake;
    private SpriteRenderer currSprite;
    private Animator turretAnimator;

    private PolygonCollider2D polyCollider;
    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;

    [Header("Sprites")]
    // Sprites
    public Sprite highlightedSprite;
    public Sprite activatedSprite;
    public Sprite highlightedSpriteRejected;
    public Sprite highlightedSpriteWorked;
    private Sprite deactiveSprite;
    [Space]

    // Activation
    public int activateCost = 20;
    public bool activated = false;
    public bool mouseIsHovering = false;
    
    public bool projectilesExplode = false;

    private float mouseLeftAt = 0.0f;



#pragma warning disable 0649
    [SerializeField] private CursorManager.CursorType cursorType;
#pragma warning restore 0649
    [Header("FirePoint")]
    public GameObject turretFirepoint;
    public Transform tFirepointTransform;
    public TriggerRadius turretRadius;
    private GameObject projectileGameObj;


    [Header("Shooting Logic")]
    public float startTimebtwShots;
    public Transform[] targetsToShootAt = new Transform[5];
    private float timebtwShots;

    [Header("Lightz")]
    public GameObject areaLightObj;
    private Light2D areaLight;
    private float normalAreaLightIntensity;

    [Header("Canvas Stuff")]
    public GameObject healthBarObj;
    public RectTransform healthBar;
    public RectTransform damageBar;

    public GameObject costImageObj;
    public Sprite costImageReg;
    public Sprite costImageDeclined;
    public Sprite costImageAccepted;

    private Vector3 diffVecHealth;
    private float timeLastShowedHealthBar;
    private const float TIME_TILL_HIDE_BAR = 1f;

    public enum TurretType
    {
        REGULAR,
        BURST,
        BEAM
    }

    public TurretType thisTurret = TurretType.REGULAR;
    private ObjectPooler objectPooler;
    public bool isShooting = false;

    public Laserbeam thisLaserbeam;
    public TextMeshProUGUI costText;

    public Light2D faceLight;
    
    
    private float activeFaceLightIntensity = 1.1f;
    private float deactiveFaceLightIntensity = 0.7f;
    private float isShootingFaceLightIntensity = 2f;


    public GameObject armourObj;
    public Light2D armourLight;
    public Animator armourAnim;
    public Animator tutorialArrow;

    private HubLayout hubLayout;


    void Start()
    {
        cameraShake = CameraMotor.instance;
        weapon = Weapon.instance;
        cam = Camera.main;
        
        controlsHandler = ControlsHandler.instance;
        //projectileGameObj = Resources.Load("TurretProjectile") as GameObject;
        objectPooler = ObjectPooler.instance;
        hubLayout = HubLayout.instance; 
        
        // Self References
        currSprite = GetComponent<SpriteRenderer>();
        polyCollider = GetComponent<PolygonCollider2D>();
        turretAnimator = GetComponent<Animator>();

        // Logical bools
        activated = false;
        mouseIsHovering = false;

        // Things that don't show when deactivated;
        deactiveSprite = currSprite.sprite;
        turretFirepoint.SetActive(false);
        areaLightObj.SetActive(false);
        healthBarObj.SetActive(false);
        costImageObj.SetActive(false);

        // Shooting Logic
        targetsToShootAt = turretRadius.targets;
        tFirepointTransform = turretFirepoint.transform;
        timebtwShots = 0;

        // Light Logic
        normalAreaLightIntensity = 1f;
        areaLight = areaLightObj.GetComponent<Light2D>();
        areaLight.intensity = 0.6f;

        playerTransform = Player.instance.transform;
        costText.SetText(activateCost.ToString());

        faceLight.intensity = deactiveFaceLightIntensity;
        armourObj.SetActive(false);
        armourLight.intensity = 1f;
        
        characterMenu = CharacterMenu.instance;

    }


    public bool CloseToPlayer => Vector2.Distance(transform.position, playerTransform.position) <= 2f;
    void Update()
    {
        turretAnimator.SetBool("isActive", activated);

        if (!activated)
        {
            lastImmune = 100f;

        }




        if (CloseToPlayer && !hubLayout.turretSlotTaken && !activated)
        {
            if (GameManager.instance.GetCurrSouls() >= activateCost) hubLayout.SetTurretAsSlot(this);
        }
        
        if (CloseToPlayer)
        {
            if (!activated) costText.SetText(activateCost.ToString());
            else
            {
                timeLastShowedHealthBar = Time.time;
            }
        }

        // if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        // {
            // mouseIsHovering = Vector2.Distance(transform.position, playerTransform.position) <= 1.5f;
        
        // }
        // if (mouseIsHovering)
        // {
        //
        //     if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE)
        //     {
        //         Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        //         
        //         if (Vector2.Distance(transform.position, (Vector2)mousePos) > 3)
        //         {
        //             mouseIsHovering = false;
        //             mouseLeftAt = Time.time;
        //             CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
        //         }
        //     }
        //     if (mouseIsHovering)
        //     {
        //         costText.SetText(activateCost.ToString());
        //         timeLastShowedHealthBar = Time.time;
        //         if (Input.GetKeyDown(KeyCode.Mouse0))
        //         {
        //             TryActivate();
        //
        //         }
        //     }
        //
        //     
        // }



        if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE)
        {
            if (mouseIsHovering)
            {
                //CursorManager.Instance.SetActiveCursorType(cursorType);
            
                if (!activated)
                {
                    currSprite.sprite = highlightedSprite;
                    costImageObj.SetActive(true);
                }
            } else
            {
                // Weapon.instance.hoveringOver = false;
            
                //CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
            
                if (!activated && (Time.time- mouseLeftAt > 0.4f))
                {
                    currSprite.sprite = deactiveSprite;
                    costImageObj.SetActive(false);
                }
            }
        }
        
        


        if (areaLight.intensity <= 0.1f) areaLightObj.SetActive(false);

        if (activated && (hitpoint <= 0))
        {
            UnActivate();
        }

        if (activated)
        {
            if (costImageObj.activeSelf) costImageObj.SetActive(false);
            //targetsToShootAt = turretRadius.targets;
            if (Time.time - timeLastShowedHealthBar <= TIME_TILL_HIDE_BAR)
            {

                healthBarObj.SetActive(true);


            }
            else healthBarObj.SetActive(false);

            float currHealth = hitpoint;
            float maxHealth = maxHitpoint;
            float diffHealth = currHealth / maxHealth;
            diffVecHealth = new Vector3(diffHealth, 1, 1);

            if (diffVecHealth.x <= healthBar.localScale.x)
            {

                healthBar.localScale = diffVecHealth;
            }
            else
            {
                healthBar.localScale = Vector3.Lerp(healthBar.localScale, diffVecHealth, 0.1f);
            }

            damageBar.localScale = Vector3.Lerp(damageBar.localScale, diffVecHealth, 0.1f);


            for (int a = 0; a < turretRadius.targets.Length; a++)
            {
                if (turretRadius.targets[a] != null)
                {
                    if (!turretRadius.targets[a].gameObject.activeInHierarchy)
                    {
                        // Debug.Log("Null");
                        continue;
                    }
                    if (turretRadius.targets[a].TryGetComponent(out Enemy enemy))
                    {
                        if (!enemy.isDead && turretRadius.targets[a].gameObject.activeInHierarchy)
                        {
                            targetsToShootAt[a] = turretRadius.targets[a];

                        }
                    }

                }
            }

            for (int t = 0; t < targetsToShootAt.Length; t++)
            {
                if (targetsToShootAt[t] != null)
                {
                    GoAfterTarget(targetsToShootAt[t], t);
                } 
            }

            if (thisTarget != null)
            {
                if (!thisTarget.gameObject.activeInHierarchy)
                {
                    thisTarget = null;
                    if (isShooting) isShooting = false;
                    // Debug.Log("deed done");
                }
            }

            if (isShooting)
            {
                faceLight.intensity = isShootingFaceLightIntensity;

            } else faceLight.intensity = activeFaceLightIntensity;



        }

        polyCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;
        }
    }


    public void ArrowAnimatorSetState(bool cond)
    {
        tutorialArrow.SetBool("tutorial", cond);
    }

    protected virtual void OnCollide(Collider2D coll)
    {
        if (activated)
        {
            if (coll.CompareTag("Fighter"))
            {
                //StartCoroutine(cameraShake.Shake(0.1f, 3f));
                Damage dmg = new Damage
                {
                    damageAmount = 1,
                    origin = transform.position,
                    pushForce = 100f,
                    originType = Damage.damageType.Turret,
                };
                timeLastShowedHealthBar = Time.time;

                coll.SendMessage("RecieveDamage", dmg);
            }

        }
    }
    private void UnActivate()
    {
        activated = false;
        lastImmune = 100f;
        turretFirepoint.SetActive(false);
        currSprite.sprite = deactiveSprite;
        GameManager.instance.numTurretsActivated -= 1;
        areaLightObj.SetActive(false);
        healthBarObj.SetActive(false);
        faceLight.intensity = deactiveFaceLightIntensity;

    }

    private void Activate()
    {

        activated = true;
        lastImmune = 0f;
        turretFirepoint.SetActive(true);
        hitpoint += (maxHitpoint - hitpoint);
        // barCanvas.SetActive(true);
        currSprite.sprite = activatedSprite;
        GameManager.instance.numTurretsActivated += 1;
        GameManager.instance.GrantPoints(30);
        DamagePopup.Create(transform.position, 30, true, DamagePopup.POINTS);
        //DamagePopup.Create(transform.position, activateCost, false, DamagePopup.SOULS);
        areaLightObj.SetActive(true);
        areaLight.intensity = normalAreaLightIntensity;
        ArrowAnimatorSetState(false);
        faceLight.intensity = activeFaceLightIntensity;

    }

   
    
    public bool TryActivate()
    {
        if (CanActivate())
        {
            SoundManager.instance.Play("turretAcceoted", 0);
            return true;

        }

        if (!CanActivate() && !activated)
        {
            SoundManager.instance.Play("declinedTurret", 0);
            // if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE)
            // {
                StopCoroutine(PlayRejectedSprite());
                StartCoroutine(PlayRejectedSprite());
            // }
            
            //DamagePopup.Create(transform.position, 30, true, DamagePopup.SPECIALT);
            return false;
        }

        return false;
    }

    public bool EligibleToUpgrade => (GameManager.instance.GetCurrSouls() >= activateCost) && !activated;

    private bool CanActivate()
    {
        if (GameManager.instance.GetCurrSouls() >= activateCost && !characterMenu.shopIsActive)
        {
            if ((!activated))
            {
                //TODO Play Activated noise
                
                timeLastShowedHealthBar = Time.time;
                GameManager.instance.souls -= activateCost;
                StartCoroutine(PlayAcceptedSprite());
                Activate();
                return true;
            }
        }
        // TODO Play Declined noise
        return false;
    }

    void GoAfterTarget(Transform target, int index)
    {
       // turretRadius.targets[index] = null;
       targetsToShootAt[index] = null;

        // if (target.TryGetComponent(out Enemy enemy))
        // {
        //     if (!enemy.isDead && target.gameObject.activeSelf)
        //     {
        //         StartCoroutine(GoingAfterTarget(target));

        //     }
        // }

        if (target.gameObject.activeInHierarchy && !isShooting )
        {
            thisTarget = target;
            StartCoroutine(GoingAfterTarget(target));
        }

        // else if (target.TryGetComponent(out RangedEnemy rEnemy))
        // {
        //     if (!rEnemy.isDead)
        //     {


        //     }
        // }

        // else if (target.TryGetComponent(out RedEnemy scaryRed))
        // {
        //     if (!scaryRed.isDead)
        //     {
        //         StartCoroutine(GoingAfterTarget(target));

        //     }
        // }

        // else if (target.TryGetComponent(out HighMageEnemy aggroBoiPew))
        // {
        //     if (!aggroBoiPew.isDead)
        //     {
        //         StartCoroutine(GoingAfterTarget(target));


        //     }
        // }
    }

    IEnumerator PlayRejectedSprite()
    {
        float timeElapsed = 0f;
        while (timeElapsed < 0.2f)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            costImageObj.GetComponent<Image>().sprite = costImageDeclined;
            currSprite.sprite = highlightedSpriteRejected;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        costImageObj.GetComponent<Image>().sprite = costImageReg;
        costImageObj.SetActive(false);
        currSprite.sprite = highlightedSprite;
    }

    IEnumerator PlayAcceptedSprite()
    {
        float timeElapsed = 0f;
        while (timeElapsed < 0.3f)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            costImageObj.GetComponent<Image>().sprite = costImageAccepted;
            currSprite.sprite = highlightedSpriteWorked;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        costImageObj.GetComponent<Image>().sprite = costImageReg;
        costImageObj.SetActive(false);
        currSprite.sprite = activatedSprite;
    }

    IEnumerator GoingAfterTarget(Transform target)
    {
        float timeElapsed = 0f;
        isShooting = true;
        while (timeElapsed <= 3f)
        {
            if (
                target == null ||
                WaveSpawner.instance.currState == WaveSpawner.SpawnState.COUNTING
                || !target.gameObject.activeInHierarchy ||
                activated == false)
            {
                target.GetComponent<Enemy>().targeted = false;
                isShooting = false;
                yield break;
            }

            float dist = Vector2.Distance(transform.position, target.position);
            if (dist > 5)
            {
                // Debug.Log("Too far");
                target.GetComponent<Enemy>().targeted = false;
                isShooting = false;
                yield break;
            } 


            // if (thisTarget == null)
            // {
            //     isShooting = false;
            //     yield break;

            // }


            // if (timebtwShots <= 0)
            // {
            //     // shoot that hoe

            //     GameObject tProjectile = Instantiate(projectileGameObj, tFirepointTransform.position, tFirepointTransform.rotation);
            //     tProjectile.GetComponent<TurretProjectile>().targetTransform = target;
            //     timebtwShots = startTimebtwShots;
            // }
            // else
            // {
            //     timebtwShots -= Time.deltaTime;
            // }


            HandleShooting(target);

            timeElapsed += Time.deltaTime;
            yield return null;
        }


        if (thisTarget != null)
        {
            StartCoroutine(GoingAfterTarget(thisTarget));
            isShooting = true;

        }
        else isShooting = false;


    }

    void OnMouseOver()
    {
        mouseIsHovering = true;
        CursorManager.Instance.SetActiveCursorType(cursorType);
        weapon.hoveringOver = true;
        // CursorManager.Instance.SetActiveCursorType(cursorType);
        // Weapon.instance.hoveringOver = true;
        // if (!activated)
        // {
        //     currSprite.sprite = highlightedSprite;
        //     costImageObj.SetActive(true);
        // }


    }

    public void OnMouseEnter()
    {
     //   Debug.Log("On Mouse Ebter");
        mouseIsHovering = true;
        weapon.hoveringOver = true;
       
        CursorManager.Instance.SetActiveCursorType(cursorType);

    }

    public void OnMouseExit()
    {

        // Weapon.instance.hoveringOver = false;
        mouseIsHovering = false;
        weapon.hoveringOver = false;
        mouseLeftAt = Time.time;
        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);

        //Debug.Log("On Mouse Exit");
      
    }

    void HandleShooting(Transform target)
    {
        if (thisTurret == TurretType.REGULAR)
        {
            if (timebtwShots <= 0)
            {
                // shoot that hoe

                // GameObject tProjectile = Instantiate(projectileGameObj, tFirepointTransform.position, tFirepointTransform.rotation);
                // tProjectile.GetComponent<TurretProjectile>().targetTransform = target;
                SpawnProjectile(target, false, new Vector3(2, 2, 1));
                timebtwShots = startTimebtwShots;
                
                
                
                
                PlayTurretShootSound(false);
               
            }
            else
            {
                timebtwShots -= Time.deltaTime;
            }
        }
        else if (thisTurret == TurretType.BURST)
        {
            if (timebtwShots <= 0)
            {
                // shoot that hoe

                // GameObject tProjectile = Instantiate(projectileGameObj, tFirepointTransform.position, tFirepointTransform.rotation);
                // tProjectile.GetComponent<TurretProjectile>().targetTransform = target;
                SpawnProjectile(thisTarget, true, new Vector3(1.5f, 1.5f, 1));
                SpawnProjectile(thisTarget, true, new Vector3(1.9f, 1.9f, 1));
                SpawnProjectile(thisTarget, true, new Vector3(1.8f, 1.8f, 1));
                timebtwShots = startTimebtwShots;
                PlayTurretShootSound(true);


            }
            else
            {
                timebtwShots -= Time.deltaTime;
            }
        }
        else if (thisTurret == TurretType.BEAM)
        {

            float x = thisTarget.position.x;
            float y = thisTarget.position.y;

            Quaternion adjustedRotation = Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));

            // thisLaserbeam.transform.rotation = adjustedRotation;
            thisLaserbeam.ShootLaser(new Vector2(x, y));
        }
    }


    void SpawnProjectile(Transform target, bool rand, Vector3 scale)
    {
        if (!isShooting) return;

        Vector3 pos = tFirepointTransform.position;
        if (rand)
        {
            pos = new Vector3(tFirepointTransform.position.x + Random.Range(-0.7f, 0.7f), tFirepointTransform.position.y + Random.Range(-0.7f, 0.7f), tFirepointTransform.position.z);
        }
        //   GameObject tProjectile = Instantiate(projectileGameObj, tFirepointTransform.position, tFirepointTransform.rotation);
        GameObject tProjectile = objectPooler.SpawnFromPool("turretProj", pos, tFirepointTransform.rotation);
        tProjectile.transform.localScale = scale;
       // tProjectile.GetComponent<TurretProjectile>().targetTransform = thisTarget;
        tProjectile.GetComponent<TurretProjectile>().thisTurret = this.transform.GetComponent<Turret>();

        tProjectile.GetComponent<TurretProjectile>().targetPosition = target.position;
        
        tProjectile.GetComponent<TurretProjectile>().projectilePosition = pos;

    }

    IEnumerator ShootingLaser(Transform target)
    {
        float timeElapsed = 0.0f;
        while (timeElapsed <= 1f)
        {

        }

        yield return null;
    }

    public void EquipArmour(int level)
    {
        armourObj.SetActive(true);
        armourLight.intensity = 1f;
        armourAnim.SetInteger("level", level);


    }


    void PlayTurretShootSound(bool isTriple)
    {
        if (GameManager.instance.mastVol <= -80f) return;

        if (isTriple)
        {
            Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
            AudioClip shootClip = SoundManager.instance.GetSounds("turretShootTriple").clip;
            var shootSound = PlayClipAt(shootClip, soundPos);
            shootSound.pitch = Random.Range(0.7f, 1f);
            shootSound.volume = 0.15f;
            shootSound.reverbZoneMix = 0.078819f;
            shootSound.outputAudioMixerGroup = mainMixx.outputAudioMixerGroup;
        }
        else
        {
            Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
            AudioClip shootClip = SoundManager.instance.GetSounds("turretShootPew").clip;
            var shootSound = PlayClipAt(shootClip, soundPos);
            shootSound.pitch = Random.Range(0.9f, 1.1f);
            shootSound.volume = 0.1f;
            shootSound.reverbZoneMix = 0.078819f;
            shootSound.outputAudioMixerGroup = mainMixx.outputAudioMixerGroup;    
        }
        
    }
    
    
    AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        
        
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.spatialBlend = 0.5f;    
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }


}
