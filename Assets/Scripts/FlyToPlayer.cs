﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class FlyToPlayer : MonoBehaviour, IPooledObject
{

    public ContactFilter2D filter;
    private BoxCollider2D boxCollider;
    private Collider2D[] hits = new Collider2D[10];
    private AudioClip onCollectClip;
    private AudioMixerGroup onCOllectMixx;


    private SoundManager sm;
    private Transform playerTransform;
    
    public bool isShrineSoul = false;
    public bool cameFromShrine = false;
    
    private float distanceTravelRate = 0.4f;
    private const float INTERVAL_FOR_SEEKING = 0.1f;
    private float lastSeeked;

    private Vector2 playerPos;

    public Vector2 shootOutDir = Vector2.zero;
    

 //   bool collected = false;
    //private bool trajectoryExplosion = true;


    void Start()
    {
        //   base.Start();

        boxCollider = GetComponent<BoxCollider2D>();
        sm = SoundManager.instance;;
        //
        // if (isShrineSoul)
        // {
        //     distanceTravelRate = 0.2f;
        //     transform.localScale = new Vector3(2.5f, 2.5f, 1f);
        // } 
        playerTransform = Player.instance.transform;
        playerPos = new Vector2(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(-0.5f, 0.5f));
        lastSeeked = Time.time;
        onCollectClip = sm.GetSounds("soul_pickup").clip;
        onCOllectMixx = sm.audioMixx;

    }

    public void OnObjectSpawn()
    {
        playerPos = new Vector2(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(-0.5f, 0.5f));
        playerTransform = Player.instance.transform;
       // collected = false;

        if (shootOutDir != Vector2.zero) playerPos = new Vector2(transform.position.x + shootOutDir.x, transform.position.y + shootOutDir.y);
        else playerPos = playerTransform.position;
        if (isShrineSoul)
        {
            distanceTravelRate = 0.5f;
            transform.localScale = new Vector3(2.5f, 2.5f, 1f);
        }
        lastSeeked = Time.time;

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }
    }

    void Update()
    {

        transform.position = Vector2.MoveTowards(transform.position, playerPos, distanceTravelRate);

        if (Time.time - lastSeeked >= INTERVAL_FOR_SEEKING)
        {
            lastSeeked = Time.time;
            playerPos = playerTransform.position;
        }
        if (transform.gameObject.activeInHierarchy)
        {
            boxCollider.OverlapCollider(filter, hits);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i] == null)
                    continue;


                OnCollide(hits[i]);
                //Cleans array
                hits[i] = null;
            }
        }
    }

    void OnCollide(Collider2D coll)
    {
        if ((coll.name == "Player"))
            OnCollect();
    }

    void OnCollect()
    {

      //  collected = true;
      
        // SoundManager.instance.Play("soul_pickup", 0);
        
        
        PlayCollectedSound();
        
        
        if (isShrineSoul)
        {
            GameManager.instance.GrantShrineSouls(1);
            DamagePopup.Create(transform.position, 1, true, DamagePopup.SPECIALT); 
        }
        else
        {
            DamagePopup.Create(transform.position, 1, true, DamagePopup.SOULS);
            GameManager.instance.GrantSouls(1);
        }

        gameObject.SetActive(false);
    }

    void PlayCollectedSound()
    {
        if (GameManager.instance.mastVol <= -79f)
        {
            Debug.Log(GameManager.instance.mastVol);
            return;
        }
        
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
        var shootSound = PlayClipAt(onCollectClip, soundPos);
        
        shootSound.pitch = sm.FetchSoulPitch(cameFromShrine); 
        shootSound.volume = 0.03f; 
        shootSound.reverbZoneMix = 0.078819f; 
        shootSound.outputAudioMixerGroup = onCOllectMixx;
    }
    
    AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        
        
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.spatialBlend = 0.5f;
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }
}
