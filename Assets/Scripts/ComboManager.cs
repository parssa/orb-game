﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboManager : MonoBehaviour
{
    public static ComboManager instance;

    void Awake()
    {
        instance = this;
    }

    public int streakCounter = 0;

    // Times
    [Header("Times")]
    public float recentKillTime;
    public float lastKillTime;
    public float timeOfSecondLastKill;
    public float recentDashKillTime;

    public bool comboIsHappening = false;
    public bool newKill = false;

    private const float DIFF_TO_START_COMBO = 1f;
    private const float DIFF_TO_STOP_COMBO = 2f; 

    [System.Serializable]
    public class ComboGate
    {
        public string gateName;
        public Color32 gateColor;
        public int killsToUnlock;
        public bool shownTextEntrance = false;

    }

    public ComboGate[] gates;
    public ComboGate currGate; 

    private int gateIndex;
    
    private int killsToNext;

    public Vector2 recentKillLocation;

    
    // Start is called before the first frame update
    void Start()
    {
        lastKillTime = 0.0f;
        timeOfSecondLastKill = 10f;
        gateIndex = 0;
        currGate = gates[gateIndex];
        killsToNext = currGate.killsToUnlock;
        
    }

    // Update is called once per frame
    void Update()
    {

        if (newKill)
        {
            
            newKill = false;
            timeOfSecondLastKill = lastKillTime;
            lastKillTime = recentKillTime;
            if (comboIsHappening)
            {
                streakCounter += 1;
                killsToNext -= 1;
            } 
        }
        
        if (comboIsHappening)
        {
            
            timeOfSecondLastKill += Time.deltaTime;

            if ((killsToNext <= 0) && (gateIndex < gates.Length - 1))
            {
                gateIndex += 1; 
                currGate = gates[gateIndex];
                killsToNext = currGate.killsToUnlock;
                if (!currGate.shownTextEntrance)
                {
                    currGate.shownTextEntrance = true;
                    DamagePopup.Create(recentKillLocation, 10, true, DamagePopup.SCOREGATE);

                }
                
            } 
            
            

            if (timeOfSecondLastKill - lastKillTime >= DIFF_TO_STOP_COMBO)
            {
                comboIsHappening = false;
                gateIndex = 0;
                currGate = gates[gateIndex];
                killsToNext = currGate.killsToUnlock;
                for (int a = 0; a < gates.Length; a++)
                {
                    gates[a].shownTextEntrance = false;
                }
            } 

        }

        if (!comboIsHappening && (timeOfSecondLastKill - recentKillTime <= DIFF_TO_START_COMBO))
        {
            comboIsHappening = true;
            gateIndex = 0;
            currGate = gates[gateIndex];
            streakCounter = 0;
            killsToNext = currGate.killsToUnlock;


        } 
    }


}
