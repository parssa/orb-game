﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class InputButton : MonoBehaviour
{
    private Button thisButton;

    
    Vector2 originalPos;

    private RectTransform background = null;

    private RectTransform baseRect = null;
    private Canvas canvas;
    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        thisButton = GetComponent<Button>();

        background = GetComponent<RectTransform>();
        baseRect = GetComponent<RectTransform>();
        canvas = GetComponentInParent<Canvas>();
        
        originalPos = baseRect.anchoredPosition;
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        HeldDown(eventData);
    }

    public void HeldDown(PointerEventData eventData)
    {
        
        if (PauseMenu.AdjustingControls)
        {
            OnDrag(eventData);
            baseRect.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        cam = null;
        if (canvas.renderMode == RenderMode.ScreenSpaceCamera)
            cam = canvas.worldCamera;

        Vector2 position = RectTransformUtility.WorldToScreenPoint(cam, background.position);
        Vector2 radius = background.sizeDelta / 2;
        //input = (eventData.position - position) / (radius * canvas.scaleFactor);
        //FormatInput();
       // HandleInput(input.magnitude, input.normalized, radius, cam);
        //handle.anchoredPosition = input * radius * handleRange;
    }

    public void ResetPosition()
    {
        baseRect.anchoredPosition = originalPos;
    }

    protected Vector2 ScreenPointToAnchoredPosition(Vector2 screenPosition)
    {
        Vector2 localPoint = Vector2.zero;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(baseRect, screenPosition, cam, out localPoint))
        {
            Vector2 pivotOffset = baseRect.pivot * baseRect.sizeDelta;
            return localPoint - (background.anchorMax * baseRect.sizeDelta) + pivotOffset;
        }
        return Vector2.zero;
    }
}
