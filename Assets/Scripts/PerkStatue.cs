﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal;

public class PerkStatue : MonoBehaviour
{
    [Header("Guardian References")]
    public Guardian thisGuardian;

    private HubLayout hub;
    
    private TutorialManager tutorialManager;
    public PerkManager.Perk.PerkType perkStatueGrants;
    private ControlsHandler controlsHandler;

    public bool mouseIsHovering = false;
    private bool activated = false;
    public bool Activated
    {
        get { return activated; }
    }
    public GameObject costImageObj;
    // public SpriteRenderer currSprite;

    // public Sprite highlightedSpriteRejected;
    public Sprite costImageDeclined;
    public Sprite costImageReg;
    public int cost;

    public GameObject[] shrineLights;
    public GameObject[] EntranceBlockers;
    

    public Teleporter teleporter;
    private Transform playerTransform;
    public ParticleSystem pSystem;

    public Animator tutorialArrow;


    public GameObject hoveringDetector;
    

    //private bool ranThroughTurnOns = false;

    // Start is called before the first frame update
    void Start()
    {
        if (cost == 0)
        {
            cost = 30;
        }
        tutorialManager = TutorialManager.instance;
        cost  = 15;
        costImageObj.SetActive(false);
        foreach(GameObject _light in shrineLights)
        {
            _light.SetActive(false);
        }

        playerTransform = GameManager.instance.playerTransform;
        //pSystem = GetComponentInChildren<ParticleSystem>();
        var main = pSystem.main;
        main.startColor = PerkManager.instance.PerkColor(perkStatueGrants);

        pSystem.Pause();


        hub = HubLayout.instance;
        controlsHandler = ControlsHandler.instance;
        //EntranceBlocker.SetActive(false);


    }


    public float dist => Vector2.Distance(transform.position, playerTransform.position);
    public bool CloseToPlayer => Vector2.Distance(transform.position, playerTransform.position) <= 3f;
    

    void Update()
    {
        if (CloseToPlayer)
        {
            if (!activated && !hub.guardianSlotTaken)
            {
                if (CanActivate()) hub.SetPerkStatueAsSlot(this);
            }
            
        }
        if (!mouseIsHovering && !activated)
        {
            // float dist = Vector2.Distance(transform.position, playerTransform.position);
            if (dist <= 5)
            {
                HaloLight(true);
                CostImage(true);
                if (dist <= 2) mouseIsHovering = true;
                else mouseIsHovering = false;
              

                if (!tutorialManager.hasCompletedGuardianTutorial)
                {
                    if (GameManager.instance.shrineSouls >= 15 && PerkManager.instance.NumGuardiansKilled == 0)
                    {
                    
                        tutorialArrow.SetBool("tutorial", true);
                        HeaderPopup.instance.gameObject.SetActive(true);
                        HeaderPopup.instance.PlayGuardianPrompt();
                    }
                }
                
                
                
                

            } else 
            {
                HaloLight(false);
                CostImage(false);
                tutorialArrow.SetBool("tutorial", false);
                //if (TutorialManager.TutorialCompleted&& !CharacterMenu.instance.BLESSINGS_SHOWING && !CharacterMenu.instance.BLESSING_BANNER_SHOULD_VISIBLE)HeaderPopup.instance.DisablePrompt();

            }
        }

        if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE && mouseIsHovering)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                TryActivate();
                
            }
        }

        // if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE)
        // {
        //     if (mouseIsHovering && !activated)
        //     {
        //         if (dist > 8)
        //         {
        //             Debug.Log("Too far");
        //             mouseIsHovering = false;
        //             CostImage(false);
        //             Weapon.instance.hoveringOver = false;
        //             CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
        //         }
        //         
        //     }
        // }

        // if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE && !activated)
        // {
        //     if (mouseIsHovering)
        //     {
        //         if (Input.GetKeyDown(KeyCode.Mouse0))
        //         {
        //             TryActivate();
        //         }
        //
        //     }
        // }



        
    }

    public void HaloLight(bool setTo)
    {
        shrineLights[1].SetActive(setTo);
    }

    public void CostImage(bool setTo)
    {
        costImageObj.SetActive(setTo);
    }
    

    public void GuardianKilled()
    {
        foreach(GameObject light in shrineLights)
        {
            light.SetActive(true);
        }
        teleporter.Activated();

        foreach(GameObject _e in EntranceBlockers)
        {
            _e.SetActive(false);
        }   
        pSystem.Play();
        AstarPath.active.Scan();
    }

    public void OnMouseEnter()
    {
        mouseIsHovering = true;
    }

    public void OnMouseOver()
    {
        
        mouseIsHovering = true;
        if (!activated)
        {
            
            mouseIsHovering = true;
            CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Select);
            Weapon.instance.hoveringOver = true;

            CostImage(true);
            

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                // if (CanActivate())
                // {
                //     // Everything regarding activation

                //     StartCoroutine(PlayAcceptedSprite());
                //     Activate();
                // }
                // else
                // {
                //    StopCoroutine(PlayRejectedSprite());
                //     StartCoroutine(PlayRejectedSprite());
                //     SoundManager.instance.Play("declinedTurret", 0);
                // }
                TryActivate();
            }

        }
    }

    public void TryActivate()
    {
        
        if (hub.guardianSlotTaken)
        {
            if (hub.statueSlot == this)
            {
                Debug.Log("MMM yes this floor is made of floor");
                    
            }
        }
        
        if (CanActivate())
        {
            // Everything regarding activation

            StartCoroutine(PlayAcceptedSprite());
            Activate();
        }
        else
        {
            StopCoroutine(PlayRejectedSprite());
            StartCoroutine(PlayRejectedSprite());
            SoundManager.instance.Play("declinedTurret", 0);
        }
    }

    public bool CanActivate()
    {
        if (GameManager.instance.shrineSouls >= cost)
        {
            return true;
        }
        return false;
    }

    void Activate()
    {
        GameManager.instance.shrineSouls -= cost;
        TutorialManager.instance.FinishedTip(TutorialManager.TipsSpecify.Guardian);
        thisGuardian.lightToActivateOnDeath = shrineLights[0];
        thisGuardian.ActivateGuardian(perkStatueGrants);
        activated = true;
        foreach (GameObject _e in EntranceBlockers)
        {
            _e.SetActive(true);
        }
        
        
        hoveringDetector.SetActive(false);

        tutorialArrow.SetBool("tutorial", false);
        HeaderPopup.instance.DisablePrompt();

        AstarPath.active.Scan();
         
           
    }

    

    public void OnMouseExit()
    {
      
        mouseIsHovering = false;
        CostImage(false);
        Weapon.instance.hoveringOver = false;
        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
      

    }


    IEnumerator PlayRejectedSprite()
    {
        float timeElapsed = 0f;
        while (timeElapsed < 0.2f)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            costImageObj.GetComponent<Image>().sprite = costImageDeclined;
            //currSprite.sprite = highlightedSpriteRejected;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        costImageObj.GetComponent<Image>().sprite = costImageReg;
        costImageObj.SetActive(false);
      
    }

    IEnumerator PlayAcceptedSprite()
    {
        float timeElapsed = 0f;
        while (timeElapsed < 0.3f)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
           
           
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        costImageObj.GetComponent<Image>().sprite = costImageReg;
        costImageObj.SetActive(false);
        
    }



    

}
