﻿using System.Collections;
using System.Collections.Generic;
using CloudOnce;
// using System.Runtime.Remoting.Messaging;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class WaveSpawner : MonoBehaviour
{

    public static WaveSpawner instance;
    public EnemyManager enemyManager;
    public HubLayout hub;
    private ObjectPooler objectPooler;
    public BackgroundAudioPlayer backgroundMusic;
    public bool trackSwitched = false;

    private void Awake()
    {
        instance = this;
    }

    private bool shrineTipPlayed = false;
    [System.Serializable] // <- Allows us to Edit Wave Values in Inspector
    public class Wave
    {
        public string name;
        public Transform[] enemy;
        public int numEnemies;
        public float rate;
        public int numShrines;
    }

    public enum SpawnState { SPAWNING, WAITING, COUNTING, TUTORIAL };
    /*
        SpawnState Documentation:
            SPAWNING: The WaveSpawner is choosing enemies based on the wave parameters and adding them to the enemyStack;
            WAITING: Checking to see if conditions met to go into COUNTING (Check if wave done)

    */

    private SpawnState state = SpawnState.COUNTING;
    public SpawnState State
    {
        get { return state; }
    }


    [Header("Waves")]
    public int waveNum; // Current Wave Number
    public Wave[] waves; // Array of all the waves 

    private int nextWave = 0;
    public int NextWave
    {
        get { return nextWave + 1; }
    }

    [Header("Enemy Logical Values")]
    public Stack<Transform> enemyStack;

    public float curr_num_enemies; // # of enemies in scene currently
    public int num_enemies_wave; // # of enemies needed to spawn in wave
    public int num_enemies_spawned_curr_wave; // # of enemies that have entered the scene during wave
    public int num_enemies_added_to_stack; // # of enemies added to the enemyStack;
    public int enemies_defeated_during_wave; // # of enemies that have died during wave;

    public int numShrinesBroken = 0; // This one is really just for the tutorial lol
    
    [HideInInspector] public int curr_ranged_spawned, curr_red_spawned, curr_high_mage_spawned, curr_slug_spawned, curr_turbo_spawned;



    [Header("Shrines")]
    public Transform[] spawnPoints; // Array of all Possible Shrine locations
    public List<Transform> spawnPointsInActiveAreas = new List<Transform>(); // List of all shrines in unlocked regions
    public Transform[] shrinesAvailable; // Array of Shrines Currently Being Used
    public int curr_num_shrines; // Current number of shrines being used
    public int numEnemiesInsideShrines = 0; // Amount of enemies still inside shrines

    // private bool bugCheck = false;
    public bool mainSpawningIsDone = false;
    public bool residualsDone = false;



    // private Stack<Transform> enemiesGotSpawned; // Unused



    // COUNTING STATE
    [HideInInspector] public float timeBetweenWaves;
    private float waveCountdown;
    public float WaveCountdown
    {
        get { return waveCountdown; }
    }

    public SpawnState currState;
    private Wave currWave;

    [System.Serializable]
    public class ShrineStat
    {
        public int shrineLevel;
        public int maxHitpoint;
        public float intervalSpawning;
    }

    public ShrineStat[] shrineStats;

    public bool endOfWaveHealPlayerBlessing = false;

    public int numIterated = 0;
    public static bool ShrineTipShowing = false;
    

    public TextMeshProUGUI currNumEnemiesT;
    public TextMeshProUGUI numEnemiesWaveT;
    public TextMeshProUGUI numEnemiesSpawnedCurrWaveT;
    public TextMeshProUGUI numEnemiesAddedStackT;
    public TextMeshProUGUI numEnemiesDefeatedDuringWaveT;

    public UnityEvent OnGuardianSummonKillEnemy;
    
    void Start()
    {

        // OnGuardianSummonKillEnemy.AddListener(Enemy.KillEnemy());
        
        objectPooler = ObjectPooler.instance;
        timeBetweenWaves = 3f;
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No Spawn Points referenced");
        }
        if (waves.Length == 0)
        {
            Debug.LogError("No Waves referenced");
        }
        waveCountdown = 0f;
        if (TutorialManager.TutorialInProgress)
        {
            //startedTheWaves = false;
            state = SpawnState.TUTORIAL;
            waveCountdown = 3f;
        }
        else
        {
            //startedTheWaves = true;
        }

        waveNum = 0;
        currWave = waves[0];

        currWave.numShrines = 5;
        curr_num_shrines = currWave.numShrines;
        shrinesAvailable = new Transform[currWave.numShrines];

        enemyStack = new Stack<Transform>();

        PutShrinesInUnlockedRegionInList(GameManager.Regions.MAIN);

        AddShrinesToArray();

        currState = state;
    }

    public void PutShrinesInUnlockedRegionInList(GameManager.Regions region)
    {
        foreach (Transform _t in spawnPoints)
        {
            if (_t.GetComponent<SpawnPoint>().shrineRegion == region)
            {
                spawnPointsInActiveAreas.Add(_t);
            }
        }
    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.N) && DevTesting.DEVMODEACTIVE)) WaveCompleted();
        currState = state;
        if (TutorialManager.TutorialInProgress) state = SpawnState.TUTORIAL;
        if (TutorialManager.TutorialCompleted && state == SpawnState.TUTORIAL)
        {
            state = SpawnState.COUNTING;
        }

        if (ShrineTipShowing)
        {
            if (numShrinesBroken != 0) ShrineTipTaskDone();
        }

        if (state == SpawnState.WAITING)
        {

            
            curr_num_enemies = GameObject.FindGameObjectsWithTag("Fighter").Length;

            num_enemies_wave = waves[0].numEnemies;




            if (curr_num_enemies > 0)
            {
                currNumEnemiesT.SetText(curr_num_enemies.ToString());
                currNumEnemiesT.color = Color.blue;
                numEnemiesWaveT.SetText(num_enemies_wave.ToString());
                numEnemiesSpawnedCurrWaveT.SetText(num_enemies_spawned_curr_wave.ToString());
                numEnemiesAddedStackT.SetText(num_enemies_added_to_stack.ToString());
                numEnemiesDefeatedDuringWaveT.SetText(enemies_defeated_during_wave.ToString());
                return;
            }

            // if (enemies_defeated_during_wave >= num_enemies_wave)
            // {
            //     Debug.Log("Thiiiiis guy,");
            //     // WaveCompleted();

            // }

            // {

            //     WaveCompleted();
            //     DesperationCleaning();
            // }


            if (!EnemyIsAlive())
            {
                WaveCompleted();
            }
            // else
            // {
            //     if (curr_num_enemies == 0 && num_enemies_spawned_curr_wave == num_enemies_wave) WaveCompleted();
            //     // return;
            // }

        }
        // if (waveCountdown <= 0 && (state != SpawnState.TUTORIAL))
        if (waveCountdown <= 0 && (state == SpawnState.COUNTING))

        {
            hub.CloseShopPrompt();
            if (state != SpawnState.SPAWNING)
            {
                // Start Spawning Wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            if (state == SpawnState.COUNTING)
            {
                waveCountdown -= Time.deltaTime;
                if (Input.GetKeyDown(KeyCode.N) && DevTesting.DEVMODEACTIVE) waveCountdown = 0f;
            }
            else if (state == SpawnState.TUTORIAL)
            {
                waveCountdown = 3f;
            }
        }

        currNumEnemiesT.SetText(curr_num_enemies.ToString());
        currNumEnemiesT.color = Color.yellow;
        numEnemiesWaveT.SetText(num_enemies_wave.ToString());
        numEnemiesSpawnedCurrWaveT.SetText(num_enemies_spawned_curr_wave.ToString());
        numEnemiesAddedStackT.SetText(num_enemies_added_to_stack.ToString());
        numEnemiesDefeatedDuringWaveT.SetText(enemies_defeated_during_wave.ToString());

        // if (state == SpawnState.SPAWNING && Input.GetKeyDown(KeyCode.N) && DevTesting.DEVMODEACTIVE) WaveCompleted();




    }

    void WaveCompleted()
    {
        // Debug.Log("Wave Completed!");
        DesperationCleaning();
        waveCountdown = timeBetweenWaves;
        state = SpawnState.COUNTING;
        hub.ShowSomething();

        //waveCountdown = timeBetweenWaves;
        waveNum += 1;
        mainSpawningIsDone = false;
        numEnemiesInsideShrines = 0;
        if (endOfWaveHealPlayerBlessing) GameManager.instance.PlayerHealEndWaveBlessing();

        
        if (waveNum > 8)
        {
            if (!Achievements.GoodDefence.IsUnlocked)
            {
                Achievements.GoodDefence.Unlock();
            }
        }
        if (waveNum > 13)
        {
            if (!Achievements.OneManArmy.IsUnlocked)
            {
                Achievements.OneManArmy.Unlock();
            }
        }

        if (waveNum > 48)
        {
            if (!Achievements.GreatestPlayerAlive.IsUnlocked)
            {
                Achievements.GreatestPlayerAlive.Unlock();
            }
        }
        
        
        if (waveNum >= 14 && !trackSwitched)
        {
            
            backgroundMusic.SwitchTrack(1);
            trackSwitched = true;
        }    
        
        if (nextWave + 1 > waves.Length - 1)
        {

            nextWave = 0;
            //Debug.Log("ALL WAVES COMPLETE! Looping...");


            for (int i = 0; i < waves.Length; i++)
            {
                waves[i].numShrines = 6;
                if (waveNum > 1)
                {
                    waves[i].numEnemies += 2;
                    waves[i].rate = 1.8f;
                }

                if (waveNum > 7)
                {
                    waves[i].numShrines = 8;
                    waves[i].numEnemies += Random.Range(0, 2);
                    waves[i].rate = 2;
                }

                if (waveNum > 9)
                {
                    waves[i].numShrines = 10;
                    waves[i].numEnemies += Random.Range(0, 3);
                    waves[i].rate = 2.1f;
                    if (waveNum == 10) enemyManager.YolkEnemies(1, 0, 0.05f);
                }

                if (waveNum > 14)
                {
                    waves[i].numShrines = 13;
                    waves[i].numEnemies += Random.Range(0, 2);
                    waves[i].rate = 2.5f;
                    if (waveNum == 15) enemyManager.YolkEnemies(2, 0, 0.1f);
                }

                if (waveNum >= 20)
                {
                    waves[i].rate = 2.8f;
                    if (waveNum == 20) enemyManager.YolkEnemies(5, 1, 0.2f);
                    else if (waveNum < 30) enemyManager.YolkEnemies(2, 1, 0.1f);
                    else enemyManager.YolkEnemies(0, 0, 0.01f);
                }
                
                
            }

        }
        else
        {
            nextWave++;
        }

        if (currWave.numShrines != shrinesAvailable.Length)
        {
            Transform[] tempArray = new Transform[currWave.numShrines];
            int valueLeftOffAt = 0;
            for (int a = 0; a < shrinesAvailable.Length; a++)
            {
                if (shrinesAvailable[a] == null)
                {
                    Debug.Log("null is being transferred");
                    tempArray.SetValue(null, a);
                }
                else if (shrinesAvailable[a].GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN)
                {
                    Debug.Log("this is broken, but being transferred.");
                }
                else
                {
                    tempArray.SetValue(shrinesAvailable[a], a);

                }
                valueLeftOffAt = a;


            }

            for (int index = 0; index < tempArray.Length; index++)
            {
                if (tempArray[index] == null)
                {
                    tempArray.SetValue(null, index);
                }
            }

            // for (int b = valueLeftOffAt; b < tempArray.Length; b++)
            // {
            //     tempArray.SetValue(null, b);
            // }

            shrinesAvailable = tempArray;
        }
        // Begin By Cleaning Array by replacing all null values with new shrines
        AddShrinesToArray();

        // Heal and Level Up Current Shrines
        HealShrines();
        currWave = waves[0];

        if (numShrinesBroken == 0 && !shrineTipPlayed)
        {
            if (waveNum >= 1 && TutorialManager.instance.ConditionsMetForShrineTutorial)
            {
                
                ShrineTutorialTip();
            }
            else
            {    
                
                Debug.Log($"Wave Num {waveNum} and Tips Done: {TutorialManager.instance.num_tips_done}");
            }
        }
        //if (shrines.Count < currWave.numShrines) AddShrineToQueue();

    }

    public void EnemyKilled()
    {
        enemies_defeated_during_wave += 1;

    }

    bool EnemyIsAlive()
    {
        //Debug.Log("have to check.");

        bool isAlive = true;



        if (curr_num_enemies > 0)
        {
            Debug.Log("USUED TRIGGERED 0");
            isAlive = false;
        }


        if (curr_num_enemies == 0 && enemies_defeated_during_wave >= num_enemies_wave)
        {
          
            isAlive = false;
        }

        //  Unused
        if (curr_num_enemies <= 0 && residualsDone)
        {
            Debug.Log("USUED TRIGGERED 1");
            residualsDone = false;
            return false;
        }

        if ((num_enemies_spawned_curr_wave < num_enemies_wave) && num_enemies_spawned_curr_wave == enemies_defeated_during_wave)
        {
            Debug.Log("Killed as many enemies that were brought up");
            isAlive = false;

        }

        if ((num_enemies_added_to_stack == num_enemies_spawned_curr_wave)
            && (num_enemies_added_to_stack == currWave.numEnemies)
            && (enemies_defeated_during_wave >= currWave.numEnemies))
        {
            //Debug.Log("A_0");
            if (curr_num_enemies <= 0)
            {
                //Debug.Log("A_1");
                isAlive = false;
            }
        }

        if (enemies_defeated_during_wave >= currWave.numEnemies)
        {
           // Debug.Log("B_0");
            if (curr_num_enemies <= 0)
            {

                //Debug.Log("B_1");
                isAlive = false;
            }
        }


        return isAlive;
    }

    // bool EnemyIsAlive()
    // {
    //     Debug.Log("have to check.");
    //     searchCountdown -= Time.deltaTime;
    //     bool isAlive = true;

    //     if (searchCountdown <= 0f)
    //     {
    //         searchCountdown = 1f;
    //         if (curr_num_enemies > 0)
    //         {
    //             Debug.Log("X_0");
    //             isAlive = false;
    //         }


    //         if (curr_num_enemies == 0 && enemies_defeated_during_wave >= num_enemies_wave)
    //         {
    //             Debug.Log("New Case");
    //             isAlive = false;
    //         }

    //         if (curr_num_enemies <= 0 && residualsDone)
    //         {
    //             Debug.Log("Force End");
    //             residualsDone = false;
    //             return false;
    //         }

    //         if ((num_enemies_spawned_curr_wave < num_enemies_wave) && num_enemies_spawned_curr_wave == enemies_defeated_during_wave)
    //         {
    //             Debug.Log("case hit, but who cares");
    //             isAlive = false;

    //         }

    //         if ((num_enemies_added_to_stack == num_enemies_spawned_curr_wave)
    //             && (num_enemies_added_to_stack == currWave.numEnemies)
    //             && (enemies_defeated_during_wave >= currWave.numEnemies))
    //         {
    //             Debug.Log("A_0");
    //             if (curr_num_enemies <= 0)
    //             {
    //                 Debug.Log("A_1");
    //                 isAlive = false;
    //             }
    //         }

    //         if (enemies_defeated_during_wave >= currWave.numEnemies)
    //         {
    //             Debug.Log("B_0");
    //             if (curr_num_enemies <= 0)
    //             {

    //                 Debug.Log("B_1");
    //                 isAlive = false;
    //             }
    //         }
    //     }

    //     return isAlive;
    // }


    public int NumEnemiesForWave()
    {
        return currWave.numEnemies;
    }

    public int NumEnemiesLeftInWave()
    {
        return NumEnemiesForWave() - enemies_defeated_during_wave;
    }

    // This is how to make a method that can wait a certain amount of seconds before continuing. (Also needs System.Collections)
    IEnumerator SpawnWave(Wave _wave)
    {
        num_enemies_spawned_curr_wave = 0;
        num_enemies_added_to_stack = 0;
        enemies_defeated_during_wave = 0;
        numEnemiesInsideShrines = 0;

        curr_ranged_spawned = 0;
        curr_red_spawned = 0;
        curr_high_mage_spawned = 0;
        curr_slug_spawned = 0;
        curr_turbo_spawned = 0;

        //Debug.Log("Spawning Wave: " + waveNum);


        // Spawning Logic
        state = SpawnState.SPAWNING;

        mainSpawningIsDone = false;

        //num_enemies_wave = _wave.enemy.Length;
        //for (int i = 0; i < _wave.numEnemies; i++)


        //numShrinesIteratedForSpawning = 0;

        num_enemies_wave = currWave.numEnemies;
        //while (num_enemies_spawned_curr_wave < _wave.numEnemies)
        //while (num_enemies_added_to_stack < _wave.numEnemies)
        while ((num_enemies_added_to_stack < num_enemies_wave))
        {
            int current_num_enemies = num_enemies_spawned_curr_wave - enemies_defeated_during_wave;
            int num_enemies_left_to_spawn = _wave.numEnemies - num_enemies_spawned_curr_wave;

            if (waveNum <= 3)
            {
                // First Waves
                SpawnEnemy(_wave.enemy[0]);

            }
            else if (waveNum <= 5)
            {
                // Introducing: Ranged
                int maxEnemiesOnBoard = waveNum + 5;

                if (num_enemies_left_to_spawn < 5)
                {

                    int randEnemyChooser = Random.Range(0, 10);
                    if ((randEnemyChooser > 7) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);
                    }

                }
                else if (current_num_enemies <= maxEnemiesOnBoard)

                {

                    int randEnemyChooser = Random.Range(0, 10);
                    if ((randEnemyChooser > 5) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);
                    }

                }


            }
            else if (waveNum <= 7)
            {
                // Introducing Red    
                int maxEnemiesOnBoard = waveNum + 3;

                if (num_enemies_left_to_spawn < 9)
                {

                    int randEnemyChooser = Random.Range(0, 10);

                    if ((randEnemyChooser > 6) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);
                    }

                }
                else if (current_num_enemies <= maxEnemiesOnBoard)
                {

                    int randEnemyChooser = Random.Range(0, 10);
                    if ((randEnemyChooser == 9) && (curr_red_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[2]);
                        curr_red_spawned += 1;
                    }
                    else if ((randEnemyChooser >= 6) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }

                }
            }
            else if (waveNum <= 10)
            {
                // Introducing, Aggro Mage  
                int maxEnemiesOnBoard = waveNum;

                if (num_enemies_left_to_spawn < 10)
                {

                    int randEnemyChooser = Random.Range(0, 10);

                    if ((randEnemyChooser > 7) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }


                }
                else if (current_num_enemies <= maxEnemiesOnBoard)

                {
                    int randEnemyChooser = Random.Range(0, 21);
                    if ((randEnemyChooser >= 19) && (curr_red_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[2]);
                        curr_red_spawned += 1;
                    }
                    else if ((randEnemyChooser >= 15) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else if ((randEnemyChooser >= 13) && (curr_high_mage_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[3]);
                        curr_high_mage_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }


                }
            }
            else if (waveNum <= 13)
            {
                // Introducing, Slugger.  
                int maxEnemiesOnBoard = waveNum - 2;


                if (num_enemies_left_to_spawn < 10)
                {

                    int randEnemyChooser = Random.Range(0, 10);

                    if ((randEnemyChooser > 7) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }

                }
                else if (current_num_enemies <= maxEnemiesOnBoard)

                {
                    int randEnemyChooser = Random.Range(0, 30);
                    if ((randEnemyChooser >= 28) && (curr_slug_spawned < waveNum - 8))
                    {
                        SpawnEnemy(_wave.enemy[4]);
                        //Debug.Log("Slug Incoming");
                        curr_red_spawned += 4;
                    }
                    else if ((randEnemyChooser >= 26) && (curr_red_spawned < waveNum - 5))
                    {
                        SpawnEnemy(_wave.enemy[2]);
                        curr_red_spawned += 1;
                    }
                    else if ((randEnemyChooser >= 24) && (curr_ranged_spawned < waveNum - 7))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else if ((randEnemyChooser > 22) && (curr_high_mage_spawned < waveNum - 10))
                    {
                        SpawnEnemy(_wave.enemy[3]);
                        curr_high_mage_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }


                }
            }
            else
            {
                // Introducing, Slugger.  
                int maxEnemiesOnBoard = waveNum;



                if (waveNum < 17) maxEnemiesOnBoard = 17;



                if (num_enemies_left_to_spawn < 10)
                {

                    int randEnemyChooser = Random.Range(0, 10);

                    if ((randEnemyChooser > 7) && (curr_ranged_spawned < waveNum))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else if (randEnemyChooser == 4)
                    {
                        SpawnEnemy(_wave.enemy[4]);
                        curr_slug_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }

                }
                else if (current_num_enemies <= maxEnemiesOnBoard)

                {
                    // Introducing Turbo King
                    int randEnemyChooser = Random.Range(0, 32);
                    if ((randEnemyChooser >= 30) && (curr_turbo_spawned < waveNum - 11))
                    {
                        SpawnEnemy(_wave.enemy[5]);
                        //Debug.Log("Slug Incoming");
                        curr_red_spawned += 4;
                    }
                    else if ((randEnemyChooser >= 28) && (curr_slug_spawned < waveNum - 8))
                    {
                        SpawnEnemy(_wave.enemy[4]);
                        //Debug.Log("Slug Incoming");
                        curr_red_spawned += 4;
                    }
                    else if ((randEnemyChooser >= 26) && (curr_red_spawned < waveNum - 5))
                    {
                        SpawnEnemy(_wave.enemy[2]);
                        curr_red_spawned += 1;
                    }
                    else if ((randEnemyChooser >= 24) && (curr_ranged_spawned < waveNum - 7))
                    {
                        SpawnEnemy(_wave.enemy[1]);
                        curr_ranged_spawned += 1;
                    }
                    else if ((randEnemyChooser > 22) && (curr_high_mage_spawned < waveNum - 10))
                    {
                        SpawnEnemy(_wave.enemy[3]);
                        curr_high_mage_spawned += 1;
                    }
                    else
                    {
                        SpawnEnemy(_wave.enemy[0]);

                    }

                }
            }

            // if (numShrinesIteratedForSpawning >= currWave.numShrines)
            // {

            //     RefreshQueue();
            // }

            // if (enemyStack.Count == 0) state = SpawnState.WAITING;

            // Don't think about it. 22:18 How to Make Wave Spawner in Unity 5 - Part 1/2
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        // num_enemies_spawned_curr_wave += 1;

        // Instantiate(_enemy, _sp.position, _sp.rotation);


        // Wait until Player Kills all Enemies
        mainSpawningIsDone = true;
        state = SpawnState.WAITING;
        // Debug.Log("CoroutRUNNING");
        yield break; // Returns nothing
    }


    void SpawnEnemy(Transform _enemy)
    {
        /*
        This script adds the enemy taken by parameter and adds it to the enemyStack, and then will remove the
        first enemy in the stack in the queue and assign it a spawn point. If spawn point is not found, it will run 
        AddShrinesToArray and not remove any enemy from the stack until AddShrinesToArray has replaced all null
        values of Shrines with new Shrines.
        */



        enemyStack.Push(_enemy);
        Transform _sp = shrinesAvailable[Random.Range(0, shrinesAvailable.Length)]; // Choose a Random Spawn Point

        if (_sp == null)
        {
            AddShrinesToArray();
            return;
        }



        Transform _enemy_to_add = enemyStack.Pop();

        if (numIterated > 20)
        {
            Debug.LogWarning("Hit Over 20. Killing Process To Prevent StackOverFlow");

            num_enemies_wave -= 1;
            return;

        }

        // If it can take enemy, add it 
        if (_sp.GetComponent<SpawnPoint>().AddToQueue(_enemy_to_add))
        {
            // _enemy to add is added inside of AddToQueue Func
            numIterated = 0;
        }
        else  // If not, rerun the function to find a different spawn point
        {
            numIterated += 1;
            SpawnEnemy(_enemy_to_add);
        }

    }


    IEnumerator ResidualSpawns()
    {
        state = SpawnState.SPAWNING;
        int amountLeftToSpawn = num_enemies_wave - num_enemies_spawned_curr_wave;
        int currResidualSpawned = 0;
        Debug.Log("Num needed to spawn to end residuals:" + amountLeftToSpawn);
        if (amountLeftToSpawn < 2)
        {
            Debug.Log("Mmmmmm nah not worth it");
            residualsDone = true;
            state = SpawnState.WAITING;
            num_enemies_spawned_curr_wave = num_enemies_wave;
            yield break;
        }

        while (currResidualSpawned < amountLeftToSpawn)
        {
            SpawnEnemy(currWave.enemy[0]);
            currResidualSpawned += 1;
            Debug.Log(currResidualSpawned);

            yield return new WaitForSeconds(0.1f);
        }
        Debug.Log("Residuals Done!");
        residualsDone = true;

        state = SpawnState.WAITING;


    }



    public string GetCurrState()
    {
        return state.ToString();
    }


    public void AddShrinesToArray()
    {

        // for (int a = 0; a < currWave.numShrines; a++)
        for (int a = 0; a < currWave.numShrines; a++)
        {

            if (shrinesAvailable[a] == null)
            {


                Transform _sp = spawnPointsInActiveAreas[Random.Range(0, spawnPointsInActiveAreas.Count)];

                if (_sp.GetComponent<SpawnPoint>().indexInWaveSpawnerArray == 99)
                {
                    // Suspect.

                    _sp.GetComponent<SpawnPoint>().currState = SpawnPoint.SpawnerState.OPENING;
                    _sp.GetComponent<SpawnPoint>().indexInWaveSpawnerArray = a;
                    _sp.GetComponent<SpawnPoint>().shrineLevel = 0;
                    _sp.GetComponent<SpawnPoint>().animator.SetBool("awake", true);

                    shrinesAvailable.SetValue(_sp, a);
                }
                else
                {
                    // Debug.Log(_sp.name + " is already in array");
                }



            }
        }
    }

    void HealShrines()
    {

        for (int a = 0; a < currWave.numShrines; a++)
        {
            if (shrinesAvailable[a] != null)
            {
                if (shrinesAvailable[a].GetComponent<SpawnPoint>().hitpoint <= 0)
                {
                    if (shrinesAvailable[a].GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN)
                    {
                        Debug.Log(shrinesAvailable[a].name + "has zero hp and broken");

                    }
                    else
                    {
                        Debug.Log(shrinesAvailable[a].name + "has zero hp and is +" + shrinesAvailable[a].GetComponent<SpawnPoint>().currState);
                    }

                }
                if (shrinesAvailable[a].GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN)
                {
                    Debug.Log(shrinesAvailable[a].name + "is generally broken");

                }


                shrinesAvailable[a].GetComponent<SpawnPoint>().currState = SpawnPoint.SpawnerState.WAITING;
                shrinesAvailable[a].GetComponent<SpawnPoint>().LevelUp();
            }
        }

    }

    public ShrineStat FetchShrineStats(int shrineLvl)
    {
        ShrineStat returnedStatSheet = shrineStats[0];
        foreach (ShrineStat statBlueprint in shrineStats)
        {
            if (statBlueprint.shrineLevel >= shrineLvl)
            {
                returnedStatSheet = statBlueprint;
            }
        }

        return returnedStatSheet;
    }

    public void ReassignEnemies(Queue<Transform> _enemyQueue)
    {
        Debug.Log("Reassigning Enemies...");
        for (int a = 0; a < _enemyQueue.Count; a++)
        {
            // enemies_defeated_during_wave += 1;

            //enemyStack.Push(_enemyQueue.Dequeue());

        }
        // if (state == SpawnState.WAITING) state = SpawnState.SPAWNING;
        //Debug.Log("Enemies Re added to Stack");
    }

    public void DesperationCleaning()
    {
        if (numEnemiesInsideShrines > 0)
        {
            Debug.Log("Oh Dear god...");
        }
       // else Debug.Log("A-OK");

    }



    public void ShrineTutorialTip()
    {
        ShrineTipShowing = true;
        // Debug.Log("activating shrine tutorial tip.");
        shrineTipPlayed = true;
        foreach (Transform _shrine in shrinesAvailable)
        {
            if (_shrine == null) continue;
            
            _shrine.GetComponent<SpawnPoint>().ShowArrow();
        }
        HeaderPopup.instance.gameObject.SetActive(true);
        HeaderPopup.instance.PlayShrinePrompt();
        
    }

    public void ShrineTipTaskDone()
    {
        if (ShrineTipShowing)
        {
            HeaderPopup.instance.FinishedShrinePrompt();
            TutorialManager.instance.FinishedTip(TutorialManager.TipsSpecify.Shrine);
            
            foreach (Transform _shrine in shrinesAvailable)
            {
                if (_shrine == null) continue;
                _shrine.GetComponent<SpawnPoint>().HideArrow();
            }    
            
           
        }
    }
}
