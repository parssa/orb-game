﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AimAssister : MonoBehaviour
{

    public bool lockedTarget = false;
    private float distFromTarget = 100f;
    
#pragma warning disable 0649
    [SerializeField] private Transform shootRaycastFrom;
#pragma warning restore 0649

    private CircleCollider2D detectionZone;
    public Transform closestEnemy = null;
    
    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;


    private ControlsHandler controlsHandler;
    // Start is called before the first frame update
    void Start()
    {
        detectionZone = GetComponent<CircleCollider2D>();
        controlsHandler = ControlsHandler.instance;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (PlayerPrefs.GetInt("toggleAim", 1) == 0) return;
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE) return;
        detectionZone.OverlapCollider(filter, hits);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;


            CalculateEnemyDist(hits[i]);
            //Cleans array
            hits[i] = null;

        }

        Vector3 outwardsFromPos = shootRaycastFrom.TransformDirection(Vector3.right);
        
        
        // Debug.DrawRay(shootRaycastFrom.position, outwardsFromPos, Color.red);
        RaycastHit2D enemyRayCast = Physics2D.Raycast(shootRaycastFrom.position, outwardsFromPos, 8f, LayerMask.GetMask("Actor"));

        if (enemyRayCast.collider != null)
        {
            // Debug.Log("RaycastHitSomething");
            if (enemyRayCast.collider.gameObject.TryGetComponent(out Enemy enemy))
            {
                // Debug.Log("is an enemy");
                if (!enemy.isDead)
                {
                    // Debug.Log("Locked in");
                    if (IsClosestEnemy(enemy.transform)) LockIn(enemy.transform);
                }
                    
            } 
        }
        
        
        if (lockedTarget)
        {
            if (!closestEnemy.gameObject.activeInHierarchy)
            {
                // Debug.Log("old target dead.");
                lockedTarget = false;
                distFromTarget = 100f;
            } else if (closestEnemy != null)
            {
                if (Vector2.Distance(transform.position, closestEnemy.position) >= 10f)
                {
                    // Debug.Log("TOo far");
                    lockedTarget = false;
                    distFromTarget = 100f;
                }
            }
            else
            {
                if (closestEnemy.GetComponent<Enemy>().isDead) Debug.Log("aww man");
            }
        }
    }

    void CalculateEnemyDist(Collider2D coll)
    {
        if (coll.transform == closestEnemy) return;
        if (coll.CompareTag("Fighter"))
        {
            // Debug.Log("We got one.");
            
            // float targetDist = Vector2.Distance(transform.position, coll.transform.position);
            // if (targetDist <= distFromTarget)
            // {
            //     // Debug.Log("This dude's even closer!");
            //     LockIn(coll.transform);
            // }
            
            // if (IsClosestEnemy(coll.transform)) LockIn(coll.transform);
        }
    }


    private bool IsClosestEnemy(Transform enemyTransform)
    {
        float targetDist = Vector2.Distance(transform.position, enemyTransform.position);
        return targetDist <= distFromTarget;
    }
    

    void LockIn(Transform newTarget)
    {
        if (newTarget != null)
        {
            lockedTarget = true;
            closestEnemy = newTarget;
            
        }
    }
}
