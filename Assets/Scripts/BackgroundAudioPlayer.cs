﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioPlayer : MonoBehaviour
{
    public AudioClip[] backgroundMusicTracks;
    private AudioSource backgroundMusicSource;


    private void Start()
    {
        backgroundMusicSource = GetComponent<AudioSource>();
    }
    
    
    
    public void SwitchTrack(int index)
    {
        StartCoroutine(FadeOutCurrentTrack(backgroundMusicTracks[index]));
    }

    IEnumerator FadeOutCurrentTrack(AudioClip switchedTrack)
    {
        backgroundMusicSource.loop = false;
        while (backgroundMusicSource.isPlaying)
        {
            if (PlayerPrefs.GetInt("playMusic") == 1)
            {
                backgroundMusicSource.volume = Mathf.Lerp(backgroundMusicSource.volume, 0.01f, Time.deltaTime);
                if (backgroundMusicSource.volume <= 0.02f)
                {
                    Debug.Log("Too Quiet");
                    LoadInNewTrack(switchedTrack);
                    yield break;
                }
            }
            else
            {
                Debug.Log("Lol no audio playing anyways so,,,");
                LoadInNewTrack(switchedTrack);
                yield break;
            }
            yield return null;
        }
        Debug.Log("Song Ended");
        
        
    }

    void LoadInNewTrack(AudioClip newTrack)
    {
        Debug.Log("new Track in!");
        backgroundMusicSource.clip = newTrack;
        backgroundMusicSource.loop = true;
        backgroundMusicSource.Play();
        if (PlayerPrefs.GetInt("playMusic") == 1) StartCoroutine(FadeInNewTrack());
    }

    IEnumerator FadeInNewTrack()
    {
        while (backgroundMusicSource.volume < 0.1f)
        {
            Debug.Log("Fadin IN");
            backgroundMusicSource.volume = Mathf.Lerp(backgroundMusicSource.volume, 0.1f, Time.deltaTime);
            yield return null;
        }
    }
}
