﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Collidable : MonoBehaviour
{

    public ContactFilter2D filter;
    private BoxCollider2D boxCollider;

    private Collider2D[] hits = new Collider2D[10];


    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    protected virtual void Update()
    {
        // Collisions
        if (this.isActiveAndEnabled)
        {
            boxCollider.OverlapCollider(filter, hits);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i] == null)
                    continue;


                OnCollide(hits[i]);
                //Cleans array
                hits[i] = null;
            }
        }
    }

    public void ResetHitbox()
    {
        // Erase array on respawn;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }
    }

    protected virtual void OnCollide(Collider2D coll)
    {
        //emoty
    }
}
