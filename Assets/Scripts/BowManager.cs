﻿using System.Collections;
using System.Collections.Generic;
using CloudOnce;
using UnityEngine;

public class BowManager : MonoBehaviour
{

    public static BowManager instance;
    

    private GameManager gm;
    private Weapon weapon;
   
    [Header("Bows")]
    public Bow[] TierOneBows;
    public Bow[] TierTwoBows;
    public Bow[] TierThreeBows;
    public Bow currBow;

    [Header("Tier Values")]
    public List<int> bowPrices;
    public int tierIndex;
    public int weaponIndex;
    //public bool isLeech;

    [Header("Individual Upgrades")]
    public WeaponUpgrade[] upgrades;

    public Bow.ClassType chosenType;

    [HideInInspector] public int absoluteDamage = 25;
    [HideInInspector] public int absoluteDashDamage = 25;
    [HideInInspector] public int absoluteChargeDamage = 25;
    [HideInInspector] public int absoluteCritDamage = 25;
    [HideInInspector] public float absoluteChargeCooldown = 4f;
    
#pragma warning disable 0649
	[SerializeField] private Animator weaponLightAnimator;
#pragma warning restore 0649
    
    public bool maxBow = false;
    
    void Awake() {
        instance = this;
        
        //isLeech = false;
        
        tierIndex = 0;
        weaponIndex = 0;
        currBow = TierOneBows[0];
       
        gm = GameManager.instance;
        weapon = Weapon.instance;
        weaponLightAnimator.SetInteger("tierlevel", 0);
        weaponLightAnimator.SetInteger("choice", 0);
        absoluteDamage = 25;
        absoluteDashDamage = 25;
        absoluteChargeDamage = 25;
        absoluteCritDamage = 25;
        
    }

    // Start is called before the first frame update
    void Start()
    {
       chosenType = Bow.ClassType.NEUTRAL;
       bowVolume = gm.PurpleVolume;
    }

    public void UpgradeBow(int option) 
    {
        weaponIndex += 1;
        if (tierIndex == 0) 
        {
            
            

            if (option == 1) 
            {
                currBow = RightOption();
                chosenType = Bow.ClassType.BRUTE;
            }
            else
            {
                currBow = LeftOption();
                chosenType = Bow.ClassType.RANGER;
            } 
            tierIndex = 1;
        }
        else if (tierIndex == 1) 
        {
            
            CharacterMenu.instance.maxBowAchieved = true;
            if (chosenType == Bow.ClassType.BRUTE)
            {
                Player.instance.dashDistance += 0.6f;
                if (option == 0)
                {
                    //isLeech = true;
                    currBow = LeftOption();
                }
                if (option == 1) currBow = RightOption();
            }
            if (chosenType == Bow.ClassType.RANGER)
            {
                if (option == 0) currBow = LeftOption();
               
                if (option == 1) currBow = RightOption();
            }
            tierIndex = 2;
      
        }
        
        UpdateCurrBow();
        
        weapon.justUpgradedWeapon = true;
		weapon.weaponLevel++;
        weaponLightAnimator.SetInteger("tierlevel", tierIndex);
        weaponLightAnimator.SetInteger("choice", option);
      
        if (chosenType == Bow.ClassType.BRUTE) Player.instance.dashStaminaDecrease = 2;
        
    }   
    public bool CanAfford()
    {
        if (bowPrices.Count - 1 == weaponIndex) return false;
        return gm.souls >= bowPrices[weaponIndex];
    }

    public bool TryUpgradeBow() 
    {
        // is the weapon max level
		if (bowPrices.Count - 1 == weaponIndex)
		{
			maxBow = true;
			Debug.Log("Bibles preeeeeetty clear about that");
		}
		if ((gm.souls >= bowPrices[weaponIndex]) && !maxBow)
		{
			gm.souls -= bowPrices[weaponIndex];
			SoundManager.instance.Play("upgradeStat", 0);
			return true;

		}
		SoundManager.instance.Play("upgradeDeclined", 0);
		return false;
    }

    public bool MaxWeapon() {
        if (weaponIndex == 3) {
            return true;
        }
        return false;
    }

    public Bow LeftOption() 
    {
        if (tierIndex == 0) 
        {
            CharacterMenu.instance.leftOptionNum = 0;
            return TierTwoBows[0];
        } else if (tierIndex == 1) 
        {
            if (chosenType == Bow.ClassType.BRUTE) 
            {
                CharacterMenu.instance.rightOptionNum = 1;
                return TierThreeBows[1];
            }
            if (chosenType == Bow.ClassType.RANGER)
            {
                CharacterMenu.instance.leftOptionNum = 0;
                return TierThreeBows[0];
            }  
        }
        CharacterMenu.instance.leftOptionNum = 1;
        return TierThreeBows[1];
        
    }

    public Bow RightOption() 
    {
        if (tierIndex == 0) 
        {
            CharacterMenu.instance.rightOptionNum = 1;
            return TierTwoBows[1];
        } else if (tierIndex == 1) 
        {
            if (chosenType == Bow.ClassType.BRUTE)
            {
                CharacterMenu.instance.rightOptionNum = 3;
                return TierThreeBows[3];
            } 
            if (chosenType == Bow.ClassType.RANGER)
            {
                CharacterMenu.instance.rightOptionNum = 2;
                return TierThreeBows[2];
            } 
            
        }
        CharacterMenu.instance.rightOptionNum = 0;
        return TierThreeBows[0];
    }

    
    private float amountReducedOnCooldown = 0;
    public bool TryUpgradeAspect(int upgradeType)
    {
        // 0: arrow damage 
        // 1: dash damage
        // 2: charge damage
        // 3: crit mult
        if (!upgrades[upgradeType].maxUpgraded)
        {
           if (gm.souls >= upgrades[upgradeType].upgradeCosts[upgrades[upgradeType].numUpgraded]) 
           {
               gm.souls -= upgrades[upgradeType].upgradeCosts[upgrades[upgradeType].numUpgraded];
               SoundManager.instance.Play("playerUpgrade", 0);
               return true;
           }
           
        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }

    public void UpgradeAspect(int upgradeType)
    {
        
        
        if (upgrades[upgradeType].numUpgraded == 3)
        {
            upgrades[upgradeType].maxUpgraded = true;

        } else
        {
            upgrades[upgradeType].numUpgraded += 1;
        }
        TransferAspectBuffToBow(upgradeType);
        
        
    }

    
    void TransferAspectBuffToBow(int upgradeType)
    {
        if (upgradeType == 0)
        {
            int upgradesAdded = upgrades[0].numUpgraded;
            while (upgradesAdded > 1)
            {   
                upgradesAdded -= 1;
                currBow.arrowDamage -= upgrades[0].damageUpgradeIntervals[upgradesAdded];
            }

            currBow.arrowDamage += upgrades[0].damageUpgradeIntervals[upgrades[0].numUpgraded];
        } else if (upgradeType == 1)
        {
            int upgradesAdded = upgrades[1].numUpgraded;
            while (upgradesAdded > 1)
            {   
                upgradesAdded -= 1;
                currBow.dashDamage -= upgrades[1].damageUpgradeIntervals[upgradesAdded];
            }
            currBow.dashDamage += upgrades[1].damageUpgradeIntervals[upgrades[1].numUpgraded];
        } else if (upgradeType == 2)
        {
            int upgradesAdded = upgrades[2].numUpgraded;
            while (upgradesAdded > 1)
            {   
                upgradesAdded -= 1;
                currBow.chargeUpDamage -= upgrades[2].damageUpgradeIntervals[upgradesAdded];
            }
            currBow.chargeUpDamage += upgrades[2].damageUpgradeIntervals[upgrades[2].numUpgraded];
        } else if (upgradeType == 3)
        {
            int upgradesAdded = upgrades[3].numUpgraded;
            while (upgradesAdded >= 1)
            {   
                upgradesAdded -= 1;
                currBow.critMultiplier -= upgrades[3].damageUpgradeIntervals[upgradesAdded];
            }
            currBow.critMultiplier += upgrades[3].damageUpgradeIntervals[upgrades[3].numUpgraded];
        } else if (upgradeType == 4)
        {
            // int upgradesAdded = upgrades[4].numUpgraded;
            // while (upgradesAdded >= 1)
            // {
            //     upgradesAdded -= 1;
            //     currBow.chargeCooldown -= upgrades[4].cooldownUpgradeIntervals[upgradesAdded];
            //     
            // }
            
            
            
            // currBow.chargeCooldown += upgrades[4].cooldownUpgradeIntervals[upgrades[4].numUpgraded];
            int upgradesAdded = upgrades[4].numUpgraded;
            Debug.Log($"Amount reduced on cooldown before: {amountReducedOnCooldown}");
            currBow.chargeCooldown -= upgrades[4].cooldownUpgradeIntervals[upgradesAdded];
            amountReducedOnCooldown += upgrades[4].cooldownUpgradeIntervals[upgradesAdded];
            Debug.Log($"Amount reduced on cooldown after: {amountReducedOnCooldown}");
           


        }
        weapon.UpgradeWeapon();


    }


    public GameObject bowVolume;
    public bool isLongVolume = false;
    void UpdateCurrBow()
    {
        
        currBow.arrowDamage += upgrades[0].damageUpgradeIntervals[upgrades[0].numUpgraded];
        currBow.dashDamage += upgrades[1].damageUpgradeIntervals[upgrades[1].numUpgraded];
        currBow.chargeUpDamage += upgrades[2].damageUpgradeIntervals[upgrades[2].numUpgraded];
        currBow.critMultiplier += upgrades[3].damageUpgradeIntervals[upgrades[3].numUpgraded];
        currBow.chargeCooldown -=  amountReducedOnCooldown;
        
        if (tierIndex < 2)
        {
            isLongVolume = false;
            Debug.Log("Purple Bow");
        }
        else
        {
            isLongVolume = true;
            if (currBow.bowName == "The Leech")
            {
                bowVolume = gm.LeechOverDriveVolume;
                if (!Achievements.JustANibble.IsUnlocked) Achievements.JustANibble.Unlock();
            }
            else if (currBow.bowName == "The Spore")
            {
                bowVolume = gm.SporeOverdriveVolume;
                if (!Achievements.EcoFriendly.IsUnlocked) Achievements.EcoFriendly.Unlock();
            }
            else if (currBow.bowName == "The Seeker")
            {
                bowVolume = gm.OverDriveVolume;
                if (!Achievements.BlingBling.IsUnlocked) Achievements.BlingBling.Unlock();
            }
            else if (currBow.bowName == "The Claw")
            {
                
                bowVolume = gm.ClawOverDriveVolume;
                if (!Achievements.RawPower.IsUnlocked) Achievements.RawPower.Unlock();
            }
    
        }
        
        
        ControlsHandler.instance.ChangeChargeUpButtonColor();
        weapon.UpgradeWeapon();
    }

}


    


