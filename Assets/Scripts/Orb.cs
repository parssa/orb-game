﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
    [Header("References")]
    public CameraMotor cameraShake;
    public Animator orbEyeAnimator;
    private Animator orbAnimator;
    private OrbHitBox combatVals;
    private SpriteRenderer orbSprite;

    [HideInInspector]
    public ContactFilter2D filter;

    [Header("Orb Leveling")]
    public int orbLevel;

    [System.Serializable]
    public class OrbUpgradeAspect
    {
        public string context;
        public Color32 upgradeTexColor;
        public enum Type
        {
            Regional_WorldUnlock,
            Numerical_BlessingSlot,
            Numerical_BlessingChance,
            Numerical_TurretDamage,
            Numerical_TurretHealth,
            Numerical_OrbHealth,
            Numerical_OrbDamage,
        }

        public Type aspectType;
        public GameManager.Regions region;
        public int numericalFactor;
        
    }

    [System.Serializable]
    public class OrbUpgrade
    {
       // public int orbLevel;
        public int upgradeCost;
        public OrbUpgradeAspect[] upgrades;
    }

    public OrbUpgrade[] orbUpgrades;


    public float xAnim = 0f;
    public float yAnim = 0f;

    private Sprite normalSprite;

    [Space]
    [Space]
    [Header("Damaged Sprites")]
    public Sprite damagedOne;
    public Sprite damagedTwo;
    public Sprite damagedThree;
    public Sprite damagedFour;

    // Eye Follow
    private Func<Vector2> GetPlayerPositionFromFunc;
    private Vector2 playerPos;
    private Vector2 orbPos;
    private Vector2 playerDistanceNormalized;
    public void Setup(Func<Vector2> GetPlayerPositionFromFunc)
    {
        this.GetPlayerPositionFromFunc = GetPlayerPositionFromFunc;
    }

    public int[] orbUpgradeCosts;

    // Fighter Implementation


    private bool hasRestored = false;
    
    protected float lastImmune;
    protected Vector2 pushDirection;

    private int damageNum;
    private PolygonCollider2D polyCollider;
    private Collider2D[] hits = new Collider2D[10];

    public int blessingSlots = 1;
    public int emptySlots = 1;




    private void Awake()
    {
        orbPos = transform.position;
        polyCollider = GetComponent<PolygonCollider2D>();
        orbSprite = GetComponent<SpriteRenderer>();
        normalSprite = orbSprite.sprite;
        damageNum = 2;
        orbLevel = 0;
        combatVals = OrbHitBox.instance;
        orbAnimator = GetComponent<Animator>();
        

    }


    void Update()
    {

        // Eye Follow Player
        orbEyeAnimator.SetBool("playerCanSee", true);
        playerPos = GetPlayerPositionFromFunc();
        playerDistanceNormalized = (playerPos - orbPos).normalized;

        xAnim = playerDistanceNormalized.x;
        yAnim = playerDistanceNormalized.y;
        orbEyeAnimator.SetFloat("x", xAnim);
        orbEyeAnimator.SetFloat("y", yAnim);
        

        // Collision Detection For Player and Orb
        polyCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;
            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;
        }
  
        // If All Fout Turrets Active, Fully Heal Orb
        if (GameManager.instance.numTurretsActivated == 4)
        {
            if (!hasRestored)
            {
                RestoreTheOrb(30);
            }
        } else
        {
            if (hasRestored)
            {
                hasRestored = false;
            }
        }


    }

    public int UpgradeCost()
    {
        // return orbUpgradeCosts[orbLevel];
        if (orbLevel >= orbUpgrades.Length) return 100;
        return orbUpgrades[orbLevel].upgradeCost;
    }

    public bool CanAfford()
    {
        return GameManager.instance.souls >= UpgradeCost() && orbLevel < orbUpgrades.Length;
    }
    public bool TryLevelUp()
    {
        if (GameManager.instance.souls >= UpgradeCost() && orbLevel < orbUpgrades.Length)
        {
            SoundManager.instance.Play("upgradeStat", 0);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OrbLevelUp()
    {


        GameManager.instance.souls -= UpgradeCost();
        combatVals.maxHitpoint = UpgradedHealth();
        UnlockRegionOnUpgrade();
        UpgradeSlots();
        UpgradeBlessingChance();
        UpgradeTurretDamage();
        UpgradeTurretHealth();

        UpgradeOrbDamage();



        orbLevel += 1;
        GameManager.instance.TurretArmourEquip(orbLevel);

        RestoreTheOrb(100);

        // if (orbLevel == 1)
        // {
        //     GameManager.instance.UnlockRequestedRegion(GameManager.Regions.BOTTOM);
        // } else if (orbLevel == 2)
        // {
        //     GameManager.instance.UnlockRequestedRegion(GameManager.Regions.LEFT);
        // } else if (orbLevel == 3)
        // {
        //     GameManager.instance.UnlockRequestedRegion(GameManager.Regions.RIGHT);
        // } else if (orbLevel == 4)
        // {
        //     GameManager.instance.UnlockRequestedRegion(GameManager.Regions.TOP);
        // }
        
    }

    void UpgradeTurretDamage()
    {
        if (orbUpgrades[orbLevel].upgrades[3].aspectType == OrbUpgradeAspect.Type.Numerical_TurretDamage)
        {
            GameManager.instance.turretDamage += orbUpgrades[orbLevel].upgrades[3].numericalFactor;
        }
    }

    void UpgradeOrbDamage()
    {
        if (orbUpgrades[orbLevel].upgrades[2].aspectType == OrbUpgradeAspect.Type.Numerical_OrbDamage)
        {
            //  GameManager.instance.turretDamage += orbUpgrades[orbLevel].upgrades[1].numericalFactor;
            damageNum += orbUpgrades[orbLevel].upgrades[2].numericalFactor;

        }
    }   

    void UpgradeTurretHealth()
    {
        if (orbUpgrades[orbLevel].upgrades[1].aspectType == OrbUpgradeAspect.Type.Numerical_TurretHealth)
        {
            // PerkManager.instance.numBlessingSlotsAvailable += orbUpgrades[orbLevel].upgrades[1].numericalFactor;
            GameManager.instance.TurretHealthUpgrade(orbUpgrades[orbLevel].upgrades[1].numericalFactor);
        }
    }

    void UpgradeSlots()
    {
        if (orbUpgrades[orbLevel].upgrades[1].aspectType == OrbUpgradeAspect.Type.Numerical_BlessingSlot)
        {
            PerkManager.instance.numBlessingSlotsAvailable += orbUpgrades[orbLevel].upgrades[1].numericalFactor;
            //PerkManager.instance.numBlessingSlotsAvailable += 
        }
    }

    void UpgradeBlessingChance()
    {
        if (orbUpgrades[orbLevel].upgrades[2].aspectType == OrbUpgradeAspect.Type.Numerical_BlessingChance)
        {
            PowerupManager.instance.oddsBlessing += orbUpgrades[orbLevel].upgrades[2].numericalFactor;
        }
    }

    public int UpgradedHealth()
    {
        //return combatVals.maxHitpoint + 2*(orbLevel+1);
        if (orbUpgrades[orbLevel].upgrades[3].aspectType == OrbUpgradeAspect.Type.Numerical_OrbHealth)
        {
            return combatVals.maxHitpoint + orbUpgrades[orbLevel].upgrades[3].numericalFactor;
        } else return combatVals.maxHitpoint;
    }

    void UnlockRegionOnUpgrade()
    {
        if (orbUpgrades[orbLevel].upgrades[0].aspectType == OrbUpgradeAspect.Type.Regional_WorldUnlock)
        {
            if (orbUpgrades[orbLevel].upgrades[0].region != GameManager.Regions.MAIN)
            {
                GameManager.instance.UnlockRequestedRegion(orbUpgrades[orbLevel].upgrades[0].region);
            }
        }
    }


    protected virtual void OnCollide(Collider2D coll)
    {

        if (coll.CompareTag("Fighter"))
        {
            if (!coll.gameObject.activeInHierarchy) 
            {
               
                Debug.Log("NULL GAMEOBJECT ORB");
                
                return;
            }

            if (coll.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER || coll.GetComponent<Enemy>().thisEnemyIs == EnemyManager.EnemyEnum.RANGED_REG) return;
            StartCoroutine(cameraShake.Shake(0.1f, 4f));
            Damage dmg = new Damage
            {
                damageAmount = damageNum,
                origin = transform.position,
                pushForce = 1000f,
                originType = Damage.damageType.Orb,
            };

            if (coll.gameObject.activeInHierarchy) coll.SendMessage("RecieveDamage", dmg);
            else Debug.Log("NULL GAMEOBJECT ORB NO SENDMESSAGE");
            
        }

    }

    public void RestoreTheOrb(int healAmount)
    {
        orbSprite.sprite = normalSprite;
        
        if (combatVals.hitpoint + healAmount >= combatVals.maxHitpoint)
        {
			combatVals.hitpoint = combatVals.maxHitpoint;
        } else
        {
			combatVals.hitpoint += healAmount;
        }
        hasRestored = true;
    }


}
