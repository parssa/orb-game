﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal;

public class ControlsHandler : MonoBehaviour
{
    public static ControlsHandler instance;
    private Camera cam;
    private CameraMotor camMotor;
    private GameManager gm;

    public GameObject mobileInputControls;
    public CanvasGroup mobileCanvasGroup;
    
    public VariableJoystick movementJoystick;
    private BowManager bm;
    private Player player;
    private HubLayout hub;


    [Header("Charge Up Button")] 
    public AudioSource chargeUpWobbleAudioSrc;

    private float chargeUpVolReg;
    public GameObject chargeButton;
    public Color chargeUpSeekerButtonColor;
    
    [Header("Shooting")]
    public GameObject shootingJoystickObj;
    public GameObject shootingButtonObj;
    public VariableJoystick shootingJoystick;
    
    [Space]
    public Image shootingHandleImg;
    public Image shootingBackgroundImg;
    public Sprite shootingHandleReg;
    public Sprite shootingHandleLockedIn;
    public Sprite shootingHandleShot;
    public Sprite shootingHandleChargeUp;
    public Color shootingJoystickRegColor;
    
    [Space]
    public Image shootButtonImage;
    public Image dashButtonImage;
    private bool shootButtonPulsing = false;
    private bool dashButtonPulsing = false;
    
    private Transform bowTransform;
    private Transform firePointTransform;
    private Transform playerTransform;
    
    private Weapon weapon;
    public AimAssister aimAssister;
    

    public Vector2 currVec = new Vector2(1, 0);
    public Vector2 chargeVec = new Vector2(1, 0);

    public Vector2 dashDirection = Vector2.one;
    public Quaternion dashDirToEuler = Quaternion.Euler(0, 0, 90);
    
    public bool triedDashing = false;
    public enum AttackType
    {
        SHOOT,
        DASH,
        BLAST
    }
    public AttackType currAttack = AttackType.DASH;

    public enum MobileLayout
    {
        DUAL,
        SIMPLE,
    }
    public MobileLayout currLayout = MobileLayout.SIMPLE;

    public enum ChargeShotStatus
    {
        DISABLED,
        INJECTED,
        HELD_DOWN,
        RELEASED,
    }
    public ChargeShotStatus chargeShotStatus;
    
    public bool tabletDevice = false;

    // Super temporaru
    //public TextMeshProUGUI attackText;

    private bool holdingDownShootButton = false;
    private bool holdingDownChargeButton = false;

    public Image joystickImage;
    public Color joystickChargeUpColor;
    private Color joystickRegColor;
    public Color joystickDashColor;

    private Vector2 lastShootJoystickPosition = Vector2.zero;
    private bool shootJoystickInjected = false;
    private float releasedChargeShotJoystickTime = 0f;


    // private bool chargeUpEffectPingPongStarted = false;
    
    
    void Awake()
    {
        instance = this;
    }
    public enum CurrPlatform
    {
        MOBILE,
        DESKTOP,
        CONTROLLER,
    }
    public CurrPlatform currPlatform;

    // Start is called before the first frame update
    void Start()
    {
        // currPlatform = CurrPlatform.DESKTOP;
        currPlatform = CurrPlatform.MOBILE;
        
        
      
        
        
        
        cam = Camera.main;
        player = Player.instance;
        camMotor = CameraMotor.instance;
        gm = GameManager.instance;
        
        bm = BowManager.instance;
        weapon = Weapon.instance;
        hub = HubLayout.instance;
        
        if (Application.isMobilePlatform)
        {
            currPlatform = CurrPlatform.MOBILE;
            if (SystemInfo.deviceModel.Contains("iPad"))
            {
                tabletDevice = true;
                // HubLayout.instance.menu.transform.GetComponent<RectTransform>().localScale =  new Vector3(2.7f, 2.7f, 1f);
                HubLayout.instance.menu.transform.GetComponent<Animator>().SetBool("isTablet", true);
            }
            else tabletDevice = false;
        }
        
        
        
        // if (true) HubLayout.instance.menu.transform.GetComponent<RectTransform>().localScale =  new Vector3(2.7f, 2.7f, 1f);

        
        if (currPlatform == CurrPlatform.MOBILE)
        {
            
            Debug.Log("mobile version");
            mobileInputControls.SetActive(true);
            // Text Desktop
            hub.soulsText.color = Color.clear; 
            hub.shrineSoulsText.color = Color.clear;
            
            // Images Desktop
            hub.soulsImageDesktop.color = Color.clear;
            hub.shrineSoulsImageDesktop.color = Color.clear;
            
            // Text Mobile
            hub.soulsTextMobile.color = Color.white;
            hub.shrineSoulsTextMobile.color = Color.white;
            
            // Images Mobile
            hub.soulsImageMobile.color = Color.white;
            hub.shrineSoulsImageMobile.color = Color.white;
        }
        else
        {
            Debug.Log("desktop version");
            
            mobileInputControls.SetActive(false);
            // Text Desktop
            hub.soulsText.color = Color.white; 
            hub.shrineSoulsText.color = Color.white;
            
            // Images Desktop
            hub.soulsImageDesktop.color = Color.white;
            hub.shrineSoulsImageDesktop.color = Color.white;
            
            // Text Mobile
            hub.soulsTextMobile.color = Color.clear;
            hub.shrineSoulsTextMobile.color = Color.clear;
            
            // Images Mobile
            hub.soulsImageMobile.color = Color.clear;
            hub.shrineSoulsImageMobile.color = Color.clear;
        }
       

        bowTransform = weapon.transform;
        playerTransform = Player.instance.transform;

        firePointTransform = bowTransform.GetChild(0);
        joystickRegColor = joystickImage.color;
        SwitchTo(AttackType.DASH);
        
        // if (!PlayerPrefs.HasKey("layout"))
        // {
        //     PlayerPrefs.SetInt("layout", (int)currLayout);
        // } 
        //
        // currLayout = (MobileLayout)PlayerPrefs.GetInt("layout");
        //
        //
        //
        // if (currLayout == MobileLayout.SIMPLE)
        // {
        //     shootingJoystickObj.SetActive(false);
        //     shootingButtonObj.SetActive(true);
        //
        // } else if (currLayout == MobileLayout.DUAL)
        // {
        // shootingJoystickObj.SetActive(true);
        // shootingButtonObj.SetActive(false);
        // }


        PlayerPrefs.SetInt("layout", 0);
        shootingJoystickObj.SetActive(true);
        shootingButtonObj.SetActive(false);


        chargeUpVolReg = chargeUpWobbleAudioSrc.volume;
        chargeUpWobbleAudioSrc.volume = 0f;

    }

    void Update()
    {

        //  Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 playerPosition = playerTransform.position;
        float angleOfDash = Mathf.Atan2(firePointTransform.position.y - playerPosition.y, firePointTransform.position.x - playerPosition.x) * 180 / Mathf.PI;
        dashDirToEuler = Quaternion.Euler(0, 0, angleOfDash);
        dashDirection = dashDirToEuler * Vector2.right;
        
        if (currPlatform == CurrPlatform.DESKTOP)
        {
            if (Input.GetKeyDown(KeyCode.LeftShift)) triedDashing = true;

        }
        else
        {
            if (weapon.canReleaseCharge && !shootJoystickInjected)
            {
                chargeButton.SetActive(true);
                Color pingPongedColor = joystickImage.color;
                pingPongedColor.a = Mathf.Lerp(0.6f, 0.8f, Mathf.PingPong(Time.time, 1f));
                joystickImage.color = pingPongedColor;
            }
            else
            {
                holdingDownChargeButton = false;
                chargeButton.SetActive(false);
            }


            if (chargeShotStatus == ChargeShotStatus.HELD_DOWN)
            {
                Color pingPongedColor = shootingHandleImg.color;
                pingPongedColor.a = Mathf.Lerp(0.7f, 1f, Mathf.PingPong(2* Time.time, 1f));
                shootingHandleImg.color = pingPongedColor;
                bm.bowVolume.GetComponent<Volume>().weight =  Mathf.Lerp(0.1f, 0.4f,  Mathf.PingPong(Time.time, 1f));
                StartCoroutine(camMotor.Shake(0.1f, 2f));

                weapon.isCharging = true;
                if (shootingJoystick.DurationHeld() >= 0.1f)
                {
                    Debug.Log("Playing wobble");
                    chargeUpWobbleAudioSrc.volume = chargeUpVolReg;
                    if (Time.time - lastRumbleTime > 0.2f)
                    {
                        lastRumbleTime = Time.time;    
                        gm.MidVibrate();
                    }
                }
            }
            // if (currLayout == MobileLayout.DUAL)
            // {
            //     if (currAttack == AttackType.DASH)
            //     {

            //     //    float x = chargeUpJoystick.Horizontal;
            //         //float y = chargeUpJoystick.Vertical;
            //       //  triedDashing = (x != 0 || y != 0);


            //     }
            // }
            // else // SIMPLE LAYOUT
            // {
            //     // Charge button Stuff



            // }

        }


        // if (chargeUpJoystick.isHeldDown)
        // {
        //     if (weapon.canReleaseCharge) SwitchTo(AttackType.BLAST);
        //     else SwitchTo(AttackType.DASH);
        //     chargeVec = chargeUpJoystick.Direction;

        // }
        // if (shootingJoystick.isHeldDown)
        // {
        //     SwitchToShoot();
        // }


        if (Input.GetKeyDown(KeyCode.E)) CharacterMenu.instance.MenuTriggered();
    }

    



    public Vector2 GetInputVector()
    {
        float x = 0f;
        float y = 0f;
        if (currPlatform == CurrPlatform.MOBILE)
        {
            if (movementJoystick.isHeldDown)
            {
                x = movementJoystick.Horizontal;
                y = movementJoystick.Vertical;

                x = JoystickToKeyboardValue(x);
                y = JoystickToKeyboardValue(y);


                // Debug.Log($"X: {x}, Y: {y}");
                // if (movementJoystick.CanPlayVibration(x, y))
                // {
                //     gm.JoystickRumble();
                // }
            }
            
            
            return new Vector2(x, y);

        }
        else if (currPlatform == CurrPlatform.DESKTOP)
        {
            x = Input.GetAxisRaw("Horizontal");
            y = Input.GetAxisRaw("Vertical");

            
            
            return new Vector2(x, y);

        }



        Vector2 inputV = new Vector2(0, 0);
        
        return inputV;
    }

    private const float ThresholdVal = 0.25f;
    float JoystickToKeyboardValue(float inputVal)
    {
        if (inputVal >= -ThresholdVal && inputVal <= ThresholdVal) return 0;
        else if (inputVal < -ThresholdVal) return -1f;
        else if (inputVal > ThresholdVal) return 1f;
        
        return 0f;
    }

    public Vector2 GetBowVec()
    {
        Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        // return currVec = new Vector2(mousePos.x - playerTransform.position.x, mousePos.y - playerTransform.position.y);
        return new Vector2(mousePos.x - playerTransform.position.x, mousePos.y - playerTransform.position.y);

    }

    public Quaternion GetBowLookRotation()
    {
        float x = 0f;
        float y = 0f;
        if (currPlatform == CurrPlatform.MOBILE)
        {

            
            // if (holdChargePos)
            // {
            //     Debug.Log("Using last charge shot position");
            //     x = lastShootJoystickPosition.x;
            //     y = lastShootJoystickPosition.y; 
            //         
            //     // Debug.Log($"This is getting ran{currVec}");
            //     if (x != 0 && y != 0) currVec = new Vector2(x, y);
            //     return Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));
            // }
            //
            
            
            if (weapon.overDriveMode || weapon.insaneModeActive)
            {
                if (aimAssister.lockedTarget)
                {
                    var lockedTargetTransform = (aimAssister.closestEnemy.position - bowTransform.position).normalized;
                    x = lockedTargetTransform.x;
                    y = lockedTargetTransform.y;
                    if (x != 0 && y != 0)
                    {
                        currVec = new Vector2(x, y);
                        return Quaternion.LookRotation(Vector3.forward, currVec);

                    }
                }
                
            }
            if (currLayout == MobileLayout.DUAL)
            {    
              
                // if ((!shootingJoystick.isHeldDown && Time.time - releasedChargeShotJoystickTime <= 0.1f) || chargeShotStatus == ChargeShotStatus.RELEASED)
                // if (Time.time - releasedChargeShotJoystickTime <= 0.1f)
                // if (chargeShotStatus == ChargeShotStatus.RELEASED)
                
                
                
                // if (Time.time - releasedChargeShotJoystickTime <= 0.1f)
                // {
                //     Debug.Log("Using last charge shot position");
                //     x = lastShootJoystickPosition.x;
                //     y = lastShootJoystickPosition.y; 
                //     
                //     // Debug.Log($"This is getting ran{currVec}");
                //     if (x != 0 && y != 0) currVec = new Vector2(x, y);
                //     return Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));
                // }
                //

                // if (Time.time - releasedChargeShotJoystickTime < 0.1f || holdChargePos)
                // {
                //     x = lastShootJoystickPosition.x;
                //     y = lastShootJoystickPosition.y;
                //     if (x != 0 && y != 0) currVec = new Vector2(x, y);
                //     return Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));
                // }

                if (chargeShotStatus == ChargeShotStatus.RELEASED)
                {
                    gm.HeavyVibration();
                    chargeUpWobbleAudioSrc.volume = 0f;
                    if (shootingJoystick.releasedDirection != Vector2.zero)
                    {
                        x = lastShootJoystickPosition.x;
                        y = lastShootJoystickPosition.y;
                        if (x != 0 && y != 0) currVec = new Vector2(x, y);
                        
                        return Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));
                    }
                }

                
                if (shootingJoystick.isHeldDown)
                {
                    x = shootingJoystick.Horizontal;
                    y = shootingJoystick.Vertical;
                    if (aimAssister.lockedTarget)
                    {
                        var lockedTargetTransform = (aimAssister.closestEnemy.position - bowTransform.position).normalized;
                        x = lockedTargetTransform.x;
                        y = lockedTargetTransform.y;

                        var shootingDir = shootingJoystick.Direction;
                        var firepointPos = weapon.firePoint.position;
                        // float theta = Mathf.Atan2(lockedTargetTransform.y - shootingDir.y, lockedTargetTransform.x - shootingDir.x) * 180 / Mathf.PI;
                        
                        
                        float thetaLocked = Mathf.Atan2(lockedTargetTransform.y - firepointPos.y, lockedTargetTransform.x - firepointPos.x) * 180 / Mathf.PI;
                        float thetaJoystick = Mathf.Atan2(shootingDir.y - firepointPos.y, shootingDir.x - firepointPos.x) * 180 / Mathf.PI;
                        
                        float thetaDelta = Mathf.Abs(thetaJoystick - thetaLocked);
                        
                        if (thetaDelta >= 1.8f)
                        {
                           
                            x = shootingJoystick.Horizontal;
                            y = shootingJoystick.Vertical;
                        }
                        
                    }
                    
                    if (chargeShotStatus == ChargeShotStatus.HELD_DOWN &&
                        shootingJoystick.Direction != Vector2.zero)
                    {
                        var shootingDir = shootingJoystick.Direction;
                        x = shootingDir.x;
                        y = shootingDir.y;
                        lastShootJoystickPosition = shootingDir;
                    }
                    
                } else if (movementJoystick.Direction != new Vector2(0, 0))
                {


                    if (Time.time - releasedChargeShotJoystickTime < 0.1f)
                    {
                        
                        x = lastShootJoystickPosition.x;
                        y = lastShootJoystickPosition.y;
                    }
                    else
                    {
                        x = movementJoystick.Horizontal; 
                        y = movementJoystick.Vertical;
                    }
                    
                    
                } 
                // else if (Time.time - shootingJoystick.lastReleasedTime > 0.1f)
                // {
                //
                //     if (movementJoystick.Direction != new Vector2(0, 0))
                //     {
                //         x = movementJoystick.Horizontal;
                //         y = movementJoystick.Vertical;
                //     }
                // }

                //  

                if (x != 0 && y != 0) currVec = new Vector2(x, y);
                return Quaternion.LookRotation(Vector3.forward, new Vector2(x, y));

            }
            else
            {
                x = movementJoystick.Horizontal;
                y = movementJoystick.Vertical;
                
                if (holdingDownShootButton)
                {
                    Debug.Log("shooting");
                    if (aimAssister.lockedTarget)
                    {
                        var lockedTargetTransform = (aimAssister.closestEnemy.position - bowTransform.position).normalized;
                        x = lockedTargetTransform.x;
                        y = lockedTargetTransform.y;
                    }
                }
                
                
                    
                
                
               
                if (x != 0 && y != 0) currVec = new Vector2(x, y);
                
                
                return Quaternion.LookRotation(Vector3.forward, currVec);
            }

        }

        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");

        Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        currVec = new Vector2(mousePos.x - playerTransform.position.x, mousePos.y - playerTransform.position.y);
        return Quaternion.LookRotation(Vector3.forward, mousePos - bowTransform.position);
    }

    //private bool holdChargePos = false;
    // IEnumerator HoldLastChargeShot()
    // {
    //     Debug.Log("Holding");
    //     holdChargePos = true;
    //     yield return new WaitForSeconds(0.3f);
    //     holdChargePos = false;
    //     
    //     Debug.Log("Holding OFf");
    // }

    public bool PlayerShootingInput()
    {
        if (currPlatform == CurrPlatform.MOBILE)
        {

            if (currLayout == MobileLayout.DUAL)
            {
                // float x = shootingJoystick.Horizontal;
                // float y = shootingJoystick.Vertical;

                // if (currAttack == AttackType.SHOOT) return (x != 0 || y != 0);

                // else return false;
                // if (shootJoystickInjected)
                // {
                //     if (shootingJoystick.isHeldDown)
                // }


                if (chargeShotStatus == ChargeShotStatus.DISABLED)
                {
                    if (Time.time - player.lastShotTime <= 0.1f) shootingHandleImg.sprite = shootingHandleShot;
                    else shootingHandleImg.sprite = shootingJoystick.isHeldDown ? shootingHandleLockedIn : shootingHandleReg;

                    if (shootingJoystick.isHeldDown)
                    {
                        if (shootingJoystick.DurationHeld() <= 0.2f) return false;
                        else return true;
                    }
                    else
                    {
                        if (Time.time > 0.5f && Time.time - releasedChargeShotJoystickTime > 0.1f)
                        {
                            return (Math.Abs(shootingJoystick.lastReleasedTime - Time.time) < 0.1f);
                        }
                        

                    }
                    
                    

                    // return false;
                    
                    // return shootingJoystick.isHeldDown;
                    
                } else if (chargeShotStatus == ChargeShotStatus.INJECTED)
                {
                    if (shootingJoystick.isHeldDown && weapon.CanAffordToShootChargeUp)
                    {
                        chargeShotStatus = ChargeShotStatus.HELD_DOWN;
                        lastShootJoystickPosition = shootingJoystick.Direction;
                    }

                    return false;
                } else if (chargeShotStatus == ChargeShotStatus.HELD_DOWN)
                {
                    
                    if (!shootingJoystick.isHeldDown)
                    {
                        
                        chargeShotStatus = ChargeShotStatus.RELEASED;
                        releasedChargeShotJoystickTime = Time.time;
                        ChargeJoystickReleasedEffect();
                    }

                    return false;
                }

                return false;


            }
            else
            {
                if (currLayout == MobileLayout.SIMPLE)
                {
                    return holdingDownShootButton;
                }
            }
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            return true;
        }
        else return false;
    }

   
    


    public bool PlayerChargeShotInput()
    {
        if (currPlatform == CurrPlatform.MOBILE)
        {
            if (currLayout == MobileLayout.DUAL)
            {
                if (chargeShotStatus == ChargeShotStatus.RELEASED)
                {
                   // StartCoroutine(HoldLastChargeShot());
                    shootJoystickInjected = false;
                    releasedChargeShotJoystickTime = Time.time;
                    chargeShotStatus = ChargeShotStatus.DISABLED;
                    
                    shootingBackgroundImg.color = shootingJoystickRegColor;
                    shootingHandleImg.color = shootingJoystickRegColor;
                    
                    return true;
                }

                return false;
            }
            return holdingDownChargeButton;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1)) return true;
        else return false;
    }

    // private bool pressedChargeShotMobile = false;
    //
    // IEnumerator PressedChargeShot()
    // {
    //     pressedChargeShotMobile = true;
    //     // if (weapon.overDriveMode)
    //     yield return null;
    // }
    

    public void SwitchWeapon()
    {
        if (currAttack == AttackType.SHOOT)
        {
            //joystickImage.color = joystickDashColor;
            currAttack = AttackType.DASH;
        }
        else if (currAttack == AttackType.DASH)
        {
            currAttack = AttackType.BLAST;
            //joystickImage.color = joystickChargeUpColor;
        }
        else if (currAttack == AttackType.BLAST)
        {
            currAttack = AttackType.SHOOT;
            // joystickImage.color = joystickRegColor;
        }

        // attackText.SetText(currAttack.ToString());
    }

    public void SwitchTo(AttackType attack)
    {
        if (attack == AttackType.DASH)
        {
            //   joystickImage.color = joystickDashColor;
            currAttack = AttackType.DASH;
        }

        if (attack == AttackType.BLAST)
        {
            currAttack = AttackType.BLAST;
            // joystickImage.color = joystickChargeUpColor;
        }

        if (attack == AttackType.SHOOT)
        {
            currAttack = AttackType.SHOOT;
        }

        // attackText.SetText(currAttack.ToString());
    }

    public void SwitchToShoot()
    {
        currAttack = AttackType.SHOOT;
        // attackText.SetText(currAttack.ToString());
    }

    public void HoldingDownShoot()
    {
        holdingDownShootButton = true;
    }

    public void ReleasedShoot()
    {
        holdingDownShootButton = false;
    }

    public void HoldingDownDash()
    {
      //  holdingDownDashButton = true;
        triedDashing = true;
         
    }


    public void ReleasedDash()
    {
        //holdingDownDashButton = false;
        triedDashing = false;
    }

    public void HoldingDownCharge()
    {
        holdingDownChargeButton = true;

    }

    public void ReleasedCharge()
    {
        holdingDownChargeButton = false;

    }

    public MobileLayout LayoutSwitch()
    {
        MobileLayout returnedLayout = MobileLayout.DUAL;

        if (currLayout == MobileLayout.DUAL)
        {

            returnedLayout = MobileLayout.SIMPLE;
            shootingJoystickObj.SetActive(false);
            shootingButtonObj.SetActive(true);
        }
        else if (currLayout == MobileLayout.SIMPLE)
        {
            returnedLayout = MobileLayout.DUAL;
            shootingJoystickObj.SetActive(true);
            shootingButtonObj.SetActive(false);
        }
        currLayout = returnedLayout;

        PlayerPrefs.SetInt("layout", (int)currLayout);
        return returnedLayout;

    }

    public void ResetPositions()
    {
        movementJoystick.ResetPosition();
        shootingJoystick.ResetPosition();
    }

    
    
    
    
    
    public void HighlightShootButton()
    {
        if (!shootButtonPulsing) StartCoroutine(PulseShootButton());
    }

    IEnumerator PulseShootButton()
    {
        shootButtonPulsing = true;
        // Debug.Log("Starting Pulse Shoot");
        while (!PlayerShootingInput())
        {
            if (PlayerShootingInput())
            {
                // Debug.Log("Player Shot, Exitting corut");
                Color restoredColor = shootButtonImage.color;
                restoredColor.a = 0.5f;
                shootButtonImage.color = restoredColor;
                shootButtonPulsing = false;
                yield break;
            }
            
            Color pingPongedColor = shootButtonImage.color;
            pingPongedColor.a = Mathf.Lerp(0.6f, 0.8f, 3 * Mathf.PingPong(Time.time, 1f));
            shootButtonImage.color = pingPongedColor;
            
            yield return null;
            
        }
    }
    
    public void HighlightDashButton()
    {
        if (!dashButtonPulsing) StartCoroutine(PulseDashButton());
    }

    public void InjectShootJoystick()
    {
        if (weapon.CanAffordToShootChargeUp)
        {
            gm.MidVibrate();
            Debug.Log("Injected Shoot Joystick");
            shootJoystickInjected = true;
            chargeShotStatus = ChargeShotStatus.INJECTED;
            var color = joystickImage.color;
            shootingBackgroundImg.color = color;
            shootingHandleImg.color = color;
            ChargeJoystickInjectedEffect();
            chargeButton.SetActive(false);
        }
        
    }
    
    IEnumerator PulseDashButton()
    {
        dashButtonPulsing = true;
        Debug.Log("Starting Pulse Dash");
        while (!triedDashing)
        {
            if (triedDashing)
            {
                Debug.Log("Player dashed, Exitting corut");
                Color restoredColor = dashButtonImage.color;
                restoredColor.a = 0.5f;
                dashButtonImage.color = restoredColor;
                dashButtonPulsing = false;
                yield break;
            }
            
            Color pingPongedColor = dashButtonImage.color;
            pingPongedColor.a = Mathf.Lerp(0.6f, 0.8f, 3 * Mathf.PingPong(Time.time, 1f));
            dashButtonImage.color = pingPongedColor;
            
            yield return null;
            
        }
    }
    

    void ChargeJoystickInjectedEffect()
    {
        bm.bowVolume.SetActive(true);
        bm.bowVolume.GetComponent<Volume>().weight = Mathf.Lerp(0.1f, 0.3f, Time.deltaTime * 2);
        
    }

    private float lastRumbleTime = 0f;

    void ChargeJoystickHeldDownEffect()
    {
        bm.bowVolume.GetComponent<Volume>().weight =  Mathf.Lerp(0.6f, 0.9f, 3 * Mathf.PingPong(Time.time, 1f));
    }

    void ChargeJoystickReleasedEffect()
    {
        // bm.bowVolume.GetComponent<Volume>().weight = Mathf.Lerp(bm.bowVolume.GetComponent<Volume>().weight, 0f, Time.deltaTime);

        if (!bm.isLongVolume)
        {
            bm.bowVolume.GetComponent<Volume>().weight = 0f;
            bm.bowVolume.SetActive(false);
        }
    }

    public void ChangeChargeUpButtonColor()
    {
        if (bm.currBow.bowName != "The Seeker")
        {joystickImage.color = bm.currBow.dashColor;}
        else {joystickImage.color =chargeUpSeekerButtonColor;}
    }

    public void ChangeAlphaMobileControls()
    {
        if (mobileCanvasGroup.alpha <= 0.1f) mobileCanvasGroup.alpha = 1f;
        else mobileCanvasGroup.alpha = 0f;
    }

    [Space] public TextMeshProUGUI currentPlatformText;

    public void ToggleControlTTypes()
    {
        if (currPlatform == CurrPlatform.DESKTOP)
        {
            
            currPlatform = CurrPlatform.MOBILE;
            mobileInputControls.SetActive(true);
            currentPlatformText.SetText("MOBILE");
        }
        else
        {
            currPlatform = CurrPlatform.DESKTOP;
            mobileInputControls.SetActive(false);
            currentPlatformText.SetText("DESKTOP");
        }
        
    }
    
    
    
    // IEnumerator PulseShootButton()
    // {
    //     dashButtonPulsing = true;
    //     Debug.Log("Starting Pulse Dash");
    //     while (!triedDashing)
    //     {
    //         if (triedDashing)
    //         {
    //             Debug.Log("Player dashed, Exitting corut");
    //             Color restoredColor = dashButtonImage.color;
    //             restoredColor.a = 0.5f;
    //             dashButtonImage.color = restoredColor;
    //             dashButtonPulsing = false;
    //             yield break;
    //         }
    //         
    //         Color pingPongedColor = dashButtonImage.color;
    //         pingPongedColor.a = Mathf.Lerp(0.6f, 0.8f, 2 * Mathf.PingPong(Time.time, 1f));
    //         dashButtonImage.color = pingPongedColor;
    //         
    //         yield return null;
    //         
    //     }
    // }

    
    
    


}
