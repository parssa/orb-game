﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHitBox : Weapon {

	// Damage
//	public int[] damagePoint = {1, 2, 3, 4, 5, 6, 7};
//	public float[] pushForce = {2.0f, 2.2f, 2.5f, 3f, 3.2f, 3.6f, 4f};
	public Weapon weapon;
	


	protected override void Start()
    {
        base.Start();
        cameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMotor>();
		
		//anim = GetComponent<Animator>();
    }
    protected override void OnCollide(Collider2D coll)
	{
		if (coll.tag == "Fighter") {

			if (coll.name == "Player")
				return;

			//Create a new damage object, then we'll send it to the fighter we've hit
			Damage dmg = new Damage {
				//damageAmount = Random.Range(damagePoint, damagePoint * critMultiplier),
				damageAmount = damagePoint,
				origin = transform.position, 
				pushForce = pushForce
			};
			StartCoroutine(cameraShake.Shake(0.05f, 10f));
			//StartCoroutine(cameraShake.Shake(0.1f, 4f));
			//StartCoroutine(cameraShake.Shake(0.1f, 1f));
			coll.SendMessage ("RecieveDamage", dmg);
			Destroy(transform.parent.gameObject);

		}
	}
}