﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitBox : Collidable {

	// Damage
	public int damage = 1;
	public int critMultiplier = 2;
	public float pushForce = 20;

	public bool isSlugger = false;
	public bool isTurbo = false;

	//private Enemy.EnemyType enemyType;

	private CameraMotor cameraShake;

    private void Awake()
    {
		cameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMotor>();
		
	}

	

    protected override void OnCollide(Collider2D coll) {

		
        if (coll.CompareTag("Player"))
        {
			
			// Create a new damage object, before sending it to the player 
			Damage dmg = new Damage {
				damageAmount = Random.Range(damage, damage*critMultiplier),
				origin = transform.position, 
				pushForce = pushForce
			};
		
			StartCoroutine(cameraShake.Shake(0.1f, 1f));
			coll.SendMessage ("RecieveDamage", dmg);
		}

		if (coll.CompareTag("OrbHitBox") && (!isSlugger || !isTurbo))
		{
			

			// Create a new damage object, before sending it to the player 
			Damage dmg = new Damage
			{
				damageAmount = Random.Range(damage, damage * critMultiplier),
				origin = transform.position,
				pushForce = pushForce
			};
			StartCoroutine(cameraShake.Shake(0.1f, 1f));
			coll.SendMessage("RecieveDamage", dmg);
			
			
			
			GameObject particleEffect = Instantiate(Resources.Load("OrbHitParticle") as GameObject, transform.position, transform.rotation);
			Destroy(particleEffect, 0.3f);
		}

		if (coll.CompareTag("Structure"))
		{

			// Create a new damage object, before sending it to the player
			int damageToDeal = Random.Range(damage, damage * critMultiplier + 1);
			Damage dmg = new Damage
			{
				damageAmount = Random.Range(damage, damage * critMultiplier),
				origin = transform.position,
				pushForce = pushForce,
				isCrit = damageToDeal >= damage * critMultiplier
			};
			//StartCoroutine(cameraShake.Shake(0.1f, 1f));
			coll.SendMessage("RecieveDamage", dmg);
		}

	}

}
