﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Collectable
{
    private float timeOfdeath = 7f;
    private float timeAlive;
    private float startBlinking;

    public bool isBlessing = false;
    public bool isMagnetic = false;

    public Transform playerTransform;

    private SpriteRenderer powerUpSprite;

   

    private void Awake()
    {
        timeAlive = 0f;
        startBlinking = 4f;
        powerUpSprite = GetComponent<SpriteRenderer>();
     ///   
    }

    protected override void Start()
    {
        base.Start();
       if (isMagnetic) playerTransform = Player.instance.transform;
      
    }

  
    protected override void Update()
    {
        base.Update();

       

        if (!isBlessing)
        {
            timeAlive += Time.deltaTime;
            if (timeAlive >= startBlinking)
            {
                powerUpSprite.color = Color.Lerp(Color.white, Color.clear, Mathf.PingPong(Time.time * (timeAlive) / 2, 1));
            }
            
            if (timeAlive >= timeOfdeath)
            {
                Destroy(gameObject);
            }

            if (isMagnetic)
            {
                float diff = Vector2.Distance(transform.position, playerTransform.position);
                if (diff <= 5f)
                {
                    transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, 1/ diff);
                }
            }
        }



    }



}
