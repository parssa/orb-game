﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering.Universal;

public class Player : Mover
{

    public static Player instance;
 //   private ControlsHandler controlsHandler;
 private Camera cam;
    private Weapon playerWeapon;
    private ObjectPooler objectPooler;
    

    private CameraMotor cameraShake;
    private GameManager gm;
    public BowManager bm;

    [Header("References")]
#pragma warning disable 0649
    [SerializeField] private Animator staminaAnim;
    [SerializeField] private Animator eyeControllerAnim;
#pragma warning restore 0649
    public GameObject dashAnimObj;

    [Header("Stamina Values")]
    public int currStamina = 15;
    public int maxStamina = 15;

    public int dashStaminaDecrease = 3;
    public int shootStaminaDecrease = 1;
    public int staminaRegenNum = 1;

    private float timeSinceLastAction = 0f;
    private float timeSinceLastRegen = 0f;
    private float timeTakesToRegenStamina = 1f;
    private float intervalsBetweenRegens = 0.7f;
    // Buffs
    [Header("Pickup Buffs")]
    private float normalFirerate;
    private float normalImmuneTime;
    private float normalPlayerSpeed;

    public bool godModeActive = false;
    public bool pickedUpOrb = false;

    public int plusStamina = 5;
    public bool pickedUpStamina = false;
    public bool staminaRegenEffecting = false;


    public int plusHealth = 3;
    public bool pickedUpHealth = false;
    public bool pickedUpDuality = false;
    public bool healthRegenEffecting = false;
    private int staminaBeforeBoss;
    private float healthEffectTimer;

    // Upgrades
    [Header("Upgrades")]
    public int numUpgradedHealth;
    public int startingHealthBoost = 2;
    public int startingHealthCost = 4;

    public int numUpgradedStamina;
    public int startingStaminaBoost = 1;
    public int startingStaminaCost = 4;


    public int numUpgradedDash = 0;
    public int startingDashCost = 10;

    public int numUpgradedStaminaRegen = 0;
    public int startingStaminaRegenCost = 10;

    // Dash Stuff

    [Header("Dash Values")]
    public float dashNum;
    public float incrementOfDash;
    public bool isDashing = false;

    private float dashingSpeed = 5f;
    public float lastDashTime;
    public float lastShotTime = 0f;
    private bool firstDash = true;
    private const float DASH_COOLDOWN_TIME = 0.4f;

    private SpriteRenderer playerSprite;
    public bool gotHit = false;
    [HideInInspector] public float lastHit;

    //private int numPressedChance;

    private Color32 regularEyeColor;
    private Color32 redEyeColor;
    private Color32 greenEyeColor;
    private Color32 orangeEyeColor;
    private Color32 yellowEyeColor;

    public Light2D playerEyes;


    [Header("Perks")]
    public List<PerkManager.Perk> perkList;

    public bool healthPerk = false;
    private bool healthPerkCoroutInProgress = false;
    private bool canRegenHealth = false;

    public bool agilityPerk = false;
    private bool isSprinting = false;

    private float quickerMovementSpeed;
    public bool strengthPerk = false;
    public bool efficientPerk = false;
    
    private float actualImmunityTime = 0.1f;

    [HideInInspector] public Collider2D[] hits = new Collider2D[10];


    public bool inWall = false; // Rigidbody collision :c
#pragma warning disable 0649
    [SerializeField] private Grounding grounding;
#pragma warning restore 0649
    public Vector2 collEnterVec = Vector2.zero;

    public float hasChargeShotBefore = 0.0f;
    public SpriteRenderer bandana;
    public Light2D bandanaLight;
    private void Awake()
    {
        instance = this;
        gm = GameManager.instance;
    }

    protected override void Start()
    {
        base.Start();
        //controlsHandler = ControlsHandler.instance;

        cam = Camera.main;
        

        objectPooler = ObjectPooler.instance;

        playerSpeed = 4.3f;
        normalPlayerSpeed = playerSpeed;
        quickerMovementSpeed = normalPlayerSpeed * 1.4f;
        normalImmuneTime = actualImmunityTime;

        incrementOfDash = 0.2f;

        playerWeapon = Weapon.instance;
        normalFirerate = Weapon.instance.cooldown;
        playerSprite = GetComponent<SpriteRenderer>();
        //numPressedChance = 0;

        cameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMotor>();
        lastHit = 0f;



        dashNum = 1f;

        // Upgrades
        numUpgradedHealth = 0;
        numUpgradedStamina = 0;
        numUpgradedStaminaRegen = 0;

        regularEyeColor = new Color32(156, 76, 241, 255);
        redEyeColor = new Color32(229, 28, 28, 255);
        yellowEyeColor = new Color32(255, 194, 0, 255);
        orangeEyeColor = new Color32(255, 239, 0, 255);


        pushRecoverySpeed = 0.04f;

        perkList = new List<PerkManager.Perk>();
    }

    private void Update()
    {

        // float x = Input.GetAxisRaw("Horizontal");
        // float y = Input.GetAxisRaw("Vertical");
        inWall = grounding.collidingWithWorldBorder;
        if (inWall)
        {
            collEnterVec = grounding.whereCollisionCameFrom;

        }

        Vector2 inputVec = controlsHandler.GetInputVector();
       // Debug.Log(inputVec);
        
        if (pushDirection != Vector2.zero && !isGettingKnockedBack && !isRecovering)
        {

            if (inWall) pushDirection = Vector2.zero;
            inputVec = pushDirection;

        }

        UpdateMotor(new Vector3(inputVec.x, inputVec.y, 0));


       
        // if (Input.GetKeyDown(KeyCode.Space) && pickedUpOrb)
        // {
        // 	SoundManager.instance.Play("orbMode");
        // 	StartCoroutine(GodMode());
        // }

        //if (Input.GetKeyDown(KeyCode.Space)) StartCoroutine(AgilityPerk());

        // Pickups
        #region
        if (pickedUpOrb)
        {
            OrbHitBox.instance.Heal();
            pickedUpOrb = false;
        }

        if (pickedUpStamina)
        {
            gm.GrantPoints(20);
            DamagePopup.Create(transform.position, 20, true, DamagePopup.POINTS);
            DamagePopup.Create(transform.position, 5, true, DamagePopup.STAMINA);
            StartCoroutine(StaminaPickup());
            GameObject particleEffect = Instantiate(Resources.Load("OnStaminaParticles") as GameObject, transform.position, transform.rotation);
            Destroy(particleEffect, 1f);
        }

        if (pickedUpHealth)
        {
            gm.GrantPoints(20);
            DamagePopup.Create(transform.position, 20, true, DamagePopup.POINTS);
            DamagePopup.Create(transform.position, 5, true, DamagePopup.HEALTH);
            StartCoroutine(HealthPickup());
            GameObject particleEffect = Instantiate(Resources.Load("OnHealthParticles") as GameObject, transform.position, transform.rotation);
            Destroy(particleEffect, 1f);
        }

        if (pickedUpDuality)
        {
            gm.GrantPoints(40);
            DamagePopup.Create(transform.position, 40, true, DamagePopup.POINTS);
            DamagePopup.Create(transform.position, 5, true, DamagePopup.DUALITY);
            StartCoroutine(HealthPickup());
            OrbHitBox.instance.Heal();

        }
        #endregion

        if (playerWeapon.lastChargeShot)
        {
            hasChargeShotBefore = Time.time;
            playerWeapon.lastChargeShot = false;
            timeSinceLastAction = 0f;
            //pushDirection = (transform.position - playerWeapon.transform.GetChild(0).position).normalized / 4;
        }

        // Player Eye Color
        #region 
        // if (bm.tierIndex <= 1)
        // {
        //     playerEyes.color = regularEyeColor;
        //     playerEyes.intensity = 12f;
        // }
        // else
        // {
        //     playerEyes.color = bm.currBow.dashColor;
        //     playerEyes.intensity = 8f;
        // }
        #endregion

        if (healthPerk)
        {
            if ((Time.time - lastHitTime > 4f) && hitpoint < maxHitpoint && !healthPerkCoroutInProgress)
            {

                canRegenHealth = true;
                StartCoroutine(HealthPerk());
            }
            if (gotHit)
            {
                canRegenHealth = false;
                StopCoroutine(HealthPerk());
            }
        }
        // VOLUMES
        #region
        if (healthRegenEffecting && !gm.HealVolume.activeSelf) gm.IntroduceHeal();
        else if (!healthRegenEffecting && gm.HealVolume.activeSelf)
        {
            healthEffectTimer += Time.deltaTime;
            if (healthEffectTimer > 0.15f)
            {
                gm.ExitHeal();
            }
        }

        float healthRatio = (float)hitpoint / (float)maxHitpoint;
        if ( healthRatio < 0.3f)  gm.IntroduceGlitch();
        if (gm.GlitchVolume.activeSelf)
        {
            if ((float)hitpoint / (float)maxHitpoint >= 0.3f) gm.ExitGlitch();
        }

        if (gotHit && !gm.DamageVolume.activeSelf)
        {
            gm.IntroduceDamage();
            
        }


        #endregion

        if (playerWeapon.isCharging) timeSinceLastAction = 0f;

        if (playerWeapon.lastShot)
        {
            lastShotTime = Time.time;
            
            playerWeapon.lastShot = false;
            timeSinceLastAction = 0f;

        }
        if (isDashing)
        {
            timeSinceLastAction = 0f;

        }

        if (controlsHandler.triedDashing && (!PauseMenu.GameIsPaused))
        {

            controlsHandler.triedDashing = false;

            if (CanDash())
            {
                isDashing = true;

                SoundManager.instance.Play("Dash1", 0);
                StartCoroutine(DashMode(controlsHandler.dashDirection));
            }
        }
    
                
        



        // Stamina Regen based on Action
        timeSinceLastAction += Time.deltaTime;
        timeSinceLastRegen += Time.deltaTime;

        if ((timeSinceLastAction > timeTakesToRegenStamina) && (!pickedUpStamina))
        {
            if (timeSinceLastRegen > intervalsBetweenRegens)
            {
                RegenStamina(staminaRegenNum);
                timeSinceLastRegen = 0f;
            }
        }

         for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            OnCollide(hits[i]);
            // Cleans Array
            hits[i] = null;
        }
    }


    // Useless stuff
    #region
    public void OnLevelUp()
    {
        maxHitpoint++;
        hitpoint = maxHitpoint;
    }

    public int GetHealth()
    {
        int r = 1;
        r = hitpoint;
        return r;

    }

    public int GetMaxHealth()
    {
        int f = 1;
        f = maxHitpoint;

        return f;

    }

    #endregion

    // Health Upgrades
    #region
    public void UpgradeHealth()
    {
        maxHitpoint += UpgradeHealthNum();
        numUpgradedHealth += 1;

        plusHealth += Random.Range(0, 3) == 1 ? Random.Range(1, 3) : 0;
        int numToHeal = numUpgradedHealth * 3;
        Heal(numToHeal);


    }

    public int UpgradeHealthNum()
    {
        if (numUpgradedHealth == 0)
        {
            return startingHealthBoost;
        }
        if (numUpgradedHealth == 1)
        {
            return startingHealthBoost * 2;
        }

        else
        {
            return startingHealthBoost * (numUpgradedHealth + 1);
        }
    }

    public int UpgradeHealthCost()
    {
        if (numUpgradedHealth == 0)
        {
            return startingHealthCost;
        }
        if (numUpgradedHealth == 1)
        {
            return startingHealthCost * 2;
        }
        else
        {
            return startingHealthCost * (numUpgradedHealth + 1);
        }
    }
    #endregion

    // Stamina Upgrades
    #region
    public void UpgradeStamina()
    {

        maxStamina += UpgradeStaminaNum();
        numUpgradedStamina += 1;
        
        if (currStamina + numUpgradedStamina * 3 >= maxStamina)
        {
            currStamina = maxStamina;
        }
        else
        {
            currStamina += numUpgradedStamina * 3;
        }
    }

    public int UpgradeStaminaNum()
    {
        if (numUpgradedStamina == 0)
        {
            return startingStaminaBoost;
        }
        else
        {
            return startingStaminaBoost * (numUpgradedStamina + 1);
        }
    }

    public int UpgradeStaminaCost()
    {
        if (numUpgradedStamina == 0)
        {
            return startingStaminaCost;
        }
        else
        {
            return startingStaminaCost * (numUpgradedStamina + 1);
        }
    }

    #endregion

    // Stamina Regen Upgrade
    #region
    public void UpgradeStaminaRegen()
    {
        staminaRegenNum += UpgradeStaminaRegenNum();
        numUpgradedStaminaRegen += 1;

    }

    public int UpgradeStaminaRegenNum()
    {
        if (numUpgradedStaminaRegen == 0)
        {
            return 1;
        }
        else
        {
            return numUpgradedStaminaRegen;
        }

    }

    public int UpgradeStaminaRegenCost()
    {
        if (numUpgradedStaminaRegen == 0)
        {
            return startingStaminaRegenCost;
        }
        else
        {
            return startingStaminaRegenCost + ( (numUpgradedStaminaRegen+1) * 2);
        }
    }
    #endregion

    // Dash Upgrades
    #region

    public void UpgradeDash()
    {
        dashDistance += 0.6f;
        numUpgradedDash += 1;
        dashNum = numUpgradedDash;
        Weapon.instance.dashDamage += 2;


    }


   protected virtual void OnCollide(Collider2D coll)
    {
        if (coll.CompareTag("WorldBorder"))
        {
            //isWalking  = false;
            isDashing = false;
        }
        
    }
    public int UpgradeDashCost()
    {
        if (numUpgradedDash == 0)
        {
            return startingDashCost;
        }
        else
        {
            return startingDashCost * numUpgradedDash + 4;
        }
    }

    #endregion


    protected override void Death()
    {
        
        
        
        
        
        PlayerPrefs.SetInt("CurrentScore", GameManager.instance.GetCurrPoints());
        PlayerPrefs.SetInt("PlayerOrOrb", 0);


        int waveDied = WaveSpawner.instance.waveNum + 1;
        PlayerPrefs.SetInt("WaveOnDeath", waveDied);
        
        
        if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", GameManager.instance.GetCurrPoints());


        }
        else if (PlayerPrefs.GetInt("HighScore") < GameManager.instance.GetCurrPoints())
        {
            PlayerPrefs.SetInt("HighScore", GameManager.instance.GetCurrPoints());
        }
        
        if (!PlayerPrefs.HasKey("BestWave"))
        {
            PlayerPrefs.SetInt("BestWave", waveDied);


        }
        else if (PlayerPrefs.GetInt("BestWave") < waveDied)
        {
            PlayerPrefs.SetInt("BestWave", waveDied);
        }

        if (gm.CanOfferHailMary)
        {
            gm.HailMaryMonetized();
        }
        else
        {
            gm.GameIsOver();
        }
        
        
    }
    
    
    

    protected override void RecieveDamage(Damage dmg)
    {

        base.RecieveDamage(dmg);
        
        
        
        
        eyeControllerAnim.SetTrigger("gotHit");
        isSprinting = false;
        if (!isGettingKnockedBack)
        {
                
                Vector2 potentialPushDirection = new Vector2(transform.position.x - dmg.origin.x, transform.position.y - dmg.origin.y).normalized;
                RaycastHit2D hitX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(potentialPushDirection.x, 0), potentialPushDirection.x / dmg.pushForce, LayerMask.GetMask("Blocking", "WorldBorder"));
                RaycastHit2D hitY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, potentialPushDirection.y), potentialPushDirection.y / dmg.pushForce, LayerMask.GetMask("Blocking", "WorldBorder"));

                // if (hitX.collider == null && hitY.collider == null)
                // {
                    
                //     pushDirection = new Vector2(transform.position.x - dmg.origin.x, transform.position.y - dmg.origin.y).normalized / dmg.pushForce;
				// 	//playerSpeed = playerSpeed * 1.02f;
                    
                // } else
				// {
					
				// 	isGettingKnockedBack = false;
                //     pushDirection = Vector2.zero;
                //     playerSpeed = regularSpeed;
                  
				// }
        } 
    }


    private void RegenStamina(int regenNum)
    {
        if (currStamina < maxStamina)
        {
            if (currStamina + regenNum > maxStamina)
            {
                currStamina = maxStamina;

            }
            else
            {
                currStamina += regenNum;
            }

        }
    }

    private bool CanDash()
    {
        if ((currStamina < dashStaminaDecrease) || (playerWeapon.isCharging))
        {
            staminaAnim.SetTrigger("cantShoot");

            // if (Random.Range(0, 10) > 5)
            // {

            //     SoundManager.instance.Play("outofstam2", 0);
            // }
            // else
            // {

            //     SoundManager.instance.Play("outofstam3", 0);
            // }

            return false;
        }
        if (firstDash)
        {
            lastDashTime = Time.time;
            firstDash = false;
            return true;
        }
        else
        {
            float timeSinceLastDash = Time.time - lastDashTime;
            if (timeSinceLastDash >= DASH_COOLDOWN_TIME)
            {
                lastDashTime = Time.time;
                return true;
            }
            return false;
        }

    }

    // Dash 
    public IEnumerator DashMode(Vector2 dashDirection)
    {
        float timePassed = 0;
        
        gm.MidVibrate();
        
        
        if ((bm.currBow.bowName == "The Claw"))
        {
            if (!playerWeapon.insaneModeActive) currStamina -= dashStaminaDecrease;
        }
        else currStamina -= dashStaminaDecrease;


        Vector2 startingPos = transform.position;
        isDashing = true;
        immuneTime = 100f; // && !inWall
        while (Vector2.Distance(startingPos, transform.position) < dashDistance && !PauseMenu.GameIsPaused)
        {
            // if (inWall)
            // {
            //     
            //     immuneTime = normalImmuneTime;
            //     isDashing = false;
            //     regularSpeed = normalPlayerSpeed;
            //     yield break;
            // }

            if (Vector2.Distance(startingPos, transform.position) >= dashDistance)
            {
                immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (PauseMenu.GameIsPaused)
            {
                immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (isDashing == false )
            {
                immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }




            regularSpeed = dashingSpeed;
            lastImmune = Time.time;

            CreateDashShadow(0.3f, dashDirection.x, false);


            HandleDash(dashDirection);
            dashIncr += 0.2f;

            timePassed += Time.deltaTime;

            yield return null;
        }
        dashIncr = 0.9f;
        CreateDashShadow(0.3f, dashDirection.x, false);
        lastImmune = Time.time;
        StartCoroutine(ResidualImmunity());
        isDashing = false;


        regularSpeed = normalPlayerSpeed;
        if (agilityPerk) StartCoroutine(AgilityPerk());

    }

    void CreateDashShadow(float lifeSpan, float dashDir, bool isAgilityBuff)
    {
        Color colorOfDashShadow = new Color32(50, 50, 155, 10);
        if (!isAgilityBuff) colorOfDashShadow = playerWeapon.dashColor;


        if (dashDir < 0)
        {

            Quaternion flippedTransformrotation = Quaternion.Euler(0, 180, 0);
           // GameObject currDash = Instantiate(dashAnimObj, transform.position, flippedTransformrotation);
            //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
            
            GameObject currDash =objectPooler.SpawnFromPool("dashShadow", transform.position, flippedTransformrotation);
            currDash.GetComponent<DashAnimation>().lifespan = lifeSpan;
            currDash.GetComponentInChildren<Light2D>().color = colorOfDashShadow;
            currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);

           // Destroy(currDash, lifeSpan);
        }
        else
        {

           // GameObject currDash = Instantiate(dashAnimObj, transform.position, transform.rotation);
            GameObject currDash =objectPooler.SpawnFromPool("dashShadow", transform.position, transform.rotation);
            //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
             currDash.GetComponent<DashAnimation>().lifespan = lifeSpan;
            currDash.GetComponentInChildren<Light2D>().color = colorOfDashShadow;
            currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);
           // Destroy(currDash, lifeSpan);
        }

    }


    public IEnumerator GodMode()
    {
        float timePassed = 0;
        pickedUpOrb = false;
        godModeActive = true;
        staminaBeforeBoss = currStamina;
        normalFirerate = Weapon.instance.cooldown;
        gm.IntroduceGlitch();
        while (timePassed < 1 && !PauseMenu.GameIsPaused)
        {
            if (PauseMenu.GameIsPaused)
            {
                gm.ExitGlitch();
                yield break;
            }
            godModeActive = true;
            currStamina = maxStamina;
            playerSpeed = dashingSpeed;
            isDashing = true;

            immuneTime = 100f;
            playerWeapon.cooldown = 0.05f;

            timePassed += Time.deltaTime;

            yield return null;
        }

        godModeActive = false;

        gm.ExitGlitch();

        currStamina = maxStamina;
        playerWeapon.cooldown = normalFirerate;
        immuneTime = normalImmuneTime;
        isDashing = false;
        playerSpeed = normalPlayerSpeed;
    }

    public IEnumerator StaminaPickup()
    {
        float timePassed = 0;
        pickedUpStamina = false;
        staminaRegenEffecting = true;


        while (timePassed < 0.1)
        {
            if (PauseMenu.GameIsPaused)
                yield break;

            
            plusStamina += Random.Range(1, 5);
            
            if (currStamina + plusStamina >= maxStamina)
            {
                currStamina = maxStamina;
            }
            else
            {
                currStamina += plusStamina;
            }

            staminaRegenEffecting = true;

            timePassed += Time.deltaTime;

            yield return null;
        }

        staminaRegenEffecting = false;

    }

    public IEnumerator HealthPickup()
    {
        float timePassed = 0;
        pickedUpHealth = false;
        pickedUpDuality = false;
        healthRegenEffecting = true;


        while (timePassed < 0.1)
        {
            if (PauseMenu.GameIsPaused)
                yield break;

            Heal(plusHealth);

            healthRegenEffecting = true;

            timePassed += Time.deltaTime;

            yield return null;
        }
        healthRegenEffecting = false;



    }

    IEnumerator ResidualImmunity()
    {
        float timeElapsed = 0f;
        immuneTime = 100f;
        lastImmune = Time.time;

        while (timeElapsed < 0.1f)
        {
            if (PauseMenu.GameIsPaused)
            {
                immuneTime = normalImmuneTime;
                yield break;
            }


            immuneTime = 100f;
            lastImmune = Time.time;
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        immuneTime = normalImmuneTime;
    }

    public void Heal(int healAmount)
    {
        healthEffectTimer = 0f;
        if (hitpoint + healAmount >= maxHitpoint)
        {
            hitpoint = maxHitpoint;
        }
        else
        {
            hitpoint += healAmount;
        }
        // healthRegenEffecting = false;
    }

    public void RefreshPerkList(PerkManager.Perk newPerk)
    {
        perkList.Add(newPerk);
        //if (newPerk.perkType == PerkManager.Perk.PerkType.HEALTH_REGEN) healthPerkCollected = true;
        PerkManager.Perk.PerkType perkType = newPerk.perkType;
        switch (perkType)
        {
            case PerkManager.Perk.PerkType.HEALTH_REGEN:
                healthPerk = true;
                break;

            case PerkManager.Perk.PerkType.AGILITY:
                agilityPerk = true;
                //normalPlayerSpeed = quickerMovementSpeed;
                //regularSpeed = quickerMovementSpeed;
                break;

            case PerkManager.Perk.PerkType.RESISTANCE:

                strengthPerk = true;
                break;

            case PerkManager.Perk.PerkType.STAMINA_FAST:
                timeTakesToRegenStamina = 0.7f;
                intervalsBetweenRegens = 0.3f;
                efficientPerk = true;
                break;

            default:
                break;
        }
    }

    IEnumerator HealthPerk()
    {

        healthPerkCoroutInProgress = true;
        while (canRegenHealth)
        {

            if (hitpoint == maxHitpoint)
            {

                gm.ExitHeal();
                healthPerkCoroutInProgress = false;
                healthRegenEffecting = false;
                yield break;
            }
            if (gotHit)
            {

                gm.ExitHeal();

                healthRegenEffecting = false;
                healthPerkCoroutInProgress = false;
                canRegenHealth = false;
                yield break;
            }
            if (Time.time - lastHitTime < 3)
            {

                gm.ExitHeal();

                healthRegenEffecting = false;
                healthPerkCoroutInProgress = false;
                canRegenHealth = false;
                yield break;
            }


            gm.IntroduceHeal();

            GameObject particleEffect = Instantiate(Resources.Load("OnHealthParticles") as GameObject, transform.position, transform.rotation);
            Destroy(particleEffect, 0.5f);
            healthRegenEffecting = true;
            Heal(1);


            yield return new WaitForSeconds(0.5f);
        }
        healthPerkCoroutInProgress = false;
        healthRegenEffecting = false;
    }

    IEnumerator AgilityPerk()
    {
        isSprinting = true;
        float timeStartedSprinting = Time.time;
        float actualRegularSpeed = normalPlayerSpeed;
        float shadowDirection = cam.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
        CreateDashShadow(0.2f, shadowDirection, true);
        float timeSinceLastShadow = Time.time;
        while (isSprinting)
        {



            // shadowDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            // CreateDashShadow(0.1f, shadowDirection, true);

            if (gotHit)
            {
                isSprinting = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (isDashing)
            {
                isSprinting = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (moveDelta == Vector2.zero)
            {
                isSprinting = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }



            if (Time.time - timeStartedSprinting < 0.4f)
            {

                regularSpeed = quickerMovementSpeed;

                if (Time.time - timeSinceLastShadow > 0.05f)
                {
                    shadowDirection = cam.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
                    CreateDashShadow(0.3f, shadowDirection, true);

                    timeSinceLastShadow = Time.time;
                }

                //normalPlayerSpeed = quickerMovementSpeed;

                //regularSpeed = Mathf.Lerp(regularSpeed, quickerMovementSpeed, Time.deltaTime * 3);
            }
            else if (Time.time - timeStartedSprinting < 0.7f)
            {
                regularSpeed = Mathf.Lerp(regularSpeed, actualRegularSpeed, Time.deltaTime);
                if (Time.time - timeSinceLastShadow > 0.1f)
                {
                    shadowDirection = cam.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
                    CreateDashShadow(0.2f, shadowDirection, true);

                    timeSinceLastShadow = Time.time;
                }

            }
            else
            {
                regularSpeed = actualRegularSpeed;
                isSprinting = false;

                yield break;
            }

            yield return null;

        }

        regularSpeed = actualRegularSpeed;
        isSprinting = false;
    }

    // void OnCollisionEnter2D(Collision2D collision)
    // {
    //     Debug.Log("Collision Detected");
    // }

   
}
