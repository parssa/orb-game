﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class AnimationDelayer : MonoBehaviour
{


    private Animator delayerAc;
    
    
    // Start is called before the first frame update
    void Start()
    {
        delayerAc = GetComponentInChildren<Animator>();
        delayerAc.speed = 0f;
        float randDelayTime = UnityEngine.Random.Range(0.0f, 1.0f);
        StartCoroutine(DelayTIllPlay(randDelayTime));
    }

    IEnumerator DelayTIllPlay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        delayerAc.speed = 1f;
    }
}
