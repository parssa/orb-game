﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Boss : Enemy
{
    [Header("Boss Values")]
    public bool activated = false;

    private HubLayout hubLayout;
    private AudioSource src;
    private bool startedRainingHell = false;
    public GameObject chargeUpPortal;
    public bool playerInRange = false;
    public float distanceFromPlayer;

    public int lastAction = 99;
    public SpriteRenderer bossSprite;
    public Light2D bossSpriteLight;
    private SoundManager sm;

    [Header("Activation")]
    public bool isMobile = true;
    public bool mouseHoveringOver = false;
#pragma warning disable 0649
    [SerializeField] private CursorManager.CursorType cursorType;
#pragma warning restore 0649
    float timeActivated = 0;

    // References
    private ObjectPooler objectPool;
    private SpriteRenderer spriteRenderer;
    public Sprite activatedSprite;
    public Animator bossAnim;
    public Animator bossEyesAnim;

        
    // Values
    private float timeSinceDecisionMade = 0.0f;
    private float DECISION_MAKING_TIME = 2.9f;



    // Shooting
    int numShots = 0;
    int num_shot_till_decide = 5;
    float lastShotTime = 0.0f;
    float SHOT_DELAY_INTERVAL = 0.6f;
    public bool unleashingFire = false;
    float absAdjustment = 5f;
    public List<Transform> firepoints;


    public enum BossState
    {
        IDLE = 0,
        AWOKEN = 1,
        SHOOTING = 2,
        DASHING = 3,
        BEAM_CHARGEUP = 4,
        BEAM_SHOOTING = 5,
        BOMBS = 6,
        LUNGE = 7,
        DECIDE = 8
    }

    public BossState bossState;



    protected override void Start()
    {
        base.Start();
        spriteRenderer = GetComponent<SpriteRenderer>();
        bossState = BossState.IDLE;
        objectPool = ObjectPooler.instance;
        // if (ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE) isMobile = true;
        // else isMobile = false;
        chargeUpPortal.SetActive(false);
        hubLayout = HubLayout.instance;
        hubLayout.boss = this;
        
        src = GetComponent<AudioSource>();
        sm = SoundManager.instance;
    }

    // Update is called once per frame
    void Update()
    {

        //distanceFromPlayer = Vector2.Distance(transform.position, playerTransform.position);


        
        if (!activated)
        {
            // if (CanActivateBoss) Debug.Log("Cam Activa");
            
            
        
                if (DistanceFromPlayer <= 3 && CanActivateBoss)
                {
                    // if (Input.GetKeyDown(KeyCode.Mouse0)) ActivateBoss();
                    
                    // TODO Make activate show up
                    hubLayout.activateBossButton.SetActive(true);
                    
                }

                if (DistanceFromPlayer > 3 && CanActivateBoss)
                {
                    hubLayout.activateBossButton.SetActive(false);
                }
                
           
            // else Debug.Log("inst even mobile");
        }

        if (activated)
        {
            Vector2 playerPosRespectToEnemy = (playerTransform.position - transform.position).normalized;
            bossEyesAnim.SetFloat("x", playerPosRespectToEnemy.x);
            bossEyesAnim.SetFloat("y", playerPosRespectToEnemy.x);
            StateManagement();
        }


        boxCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;
            OnCollide(hits[i]);
            // Cleans Array
            hits[i] = null;
        }
       
    }

    void StateManagement()
    {
        switch (bossState)
        {
            case BossState.AWOKEN:
                //anim.SetTrigger("activated");
                AwokenState();
                break;

            case BossState.DECIDE:
                anim.SetTrigger("decide");
                DecideState();
                break;

            case BossState.BEAM_CHARGEUP:
                ChargeUpState();
                break;

            case BossState.SHOOTING:
                //anim.SetTrigger("shooting");
                ShootingState();
                break;

            case BossState.DASHING:
                DashingState();
                break;

            default:
                DecideState();
                break;
        }
    }

    void AwokenState()
    {
        if (Time.time - timeActivated > 1f)
        {
            StartCoroutine(ReturnToDeciding());
        }
    }

    void DecideState()
    {
        if (Time.time - timeSinceDecisionMade >= DECISION_MAKING_TIME)
        {
            if (lastAction != (int)BossState.SHOOTING && Random.Range(0, 2) == 1)
            {
                timeSinceDecisionMade = Time.time;
                bossState = BossState.SHOOTING;
                lastAction = (int)BossState.SHOOTING;
                Debug.Log("Chose to shoot");
                return;
            }
            
            if (InDashingRange && lastAction != (int)BossState.DASHING)
            {
                timeSinceDecisionMade = Time.time;
                bossState = BossState.DASHING;
                lastAction = (int)BossState.DASHING;
                Debug.Log("Chose to dash");
                return;
            }
            
            timeSinceDecisionMade = Time.time;
            bossState = BossState.BEAM_CHARGEUP;
            lastAction = (int)BossState.BEAM_CHARGEUP;
            Debug.Log("Chose to fuckin rain hell down unto player lul");


            // if (lastAction != (int)BossState.SHOOTING)
            // {
            //     timeSinceDecisionMade = Time.time;
            //     bossState = BossState.SHOOTING;
            //     lastAction = (int)BossState.SHOOTING;
            //     Debug.Log("Chose to shoot");
            //     return;
            // }
            
            

        }
        


        
        
        

    }

    void DashingState()
    {
        if (!startedChargingUp) StartCoroutine(ChargeUpIntoDash());
    }

    IEnumerator ChargeUpIntoDash()
    {
        startedChargingUp = true;
        
       

       

        yield return new WaitForSeconds(0.4f);;


        float angleOfDash = Mathf.Atan2(playerTransform.position.y - transform.position.y, playerTransform.position.x - transform.position.x) * 180 / Mathf.PI;
        Quaternion dashDirToEuler = Quaternion.Euler(0, 0, angleOfDash);
        Vector2 dashDirection = dashDirToEuler * Vector2.right;



        StartCoroutine(DashMode(dashDirection));

        // if (!fellOutOfRange)
        // {
        // 	Debug.Log("About To Dash");
        // 	HandleDash(dashDirection);

        // }
        alertSymbolLight.intensity = 0f;
        alertSymbol.color = Color.clear;
        startedChargingUp = false;

    }

    IEnumerator DashMode(Vector2 dashDirection)
    {
        float timePassed = 0;
        CreateDashShadow(0.1f, dashDirection.x);
        Vector2 startingPos = transform.position;
        isDashing = true;
        timeSinceLastDash = Time.time;
        anim.SetTrigger("dash");


        float normalPlayerSpeed = regularSpeed;
        regularSpeed = normalPlayerSpeed;

        dashDistance = 2f;
        while (Vector2.Distance(startingPos, transform.position) < dashDistance && !PauseMenu.GameIsPaused)
        {

            if (Vector2.Distance(startingPos, transform.position) >= dashDistance)
            {

                isDashing = false;

                yield break;
            }

            if (PauseMenu.GameIsPaused)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (isDashing == false)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            playerSpeed = 1f;
            //lastImmune = Time.time;

            //CreateDashShadow(0.3f, dashDirection.x);
            CreateDashShadow(0.1f, dashDirection.x);
            CreateDashShadow(0.1f, dashDirection.x);
            HandleDash(dashDirection);
            dashIncr += 0.1f;

            timePassed += Time.deltaTime;

            yield return null;
        }

        dashIncr = 0.3f;
        CreateDashShadow(0.1f, dashDirection.x);
        isDashing = false;
        regularSpeed = normalPlayerSpeed;
        playerSpeed = regularSpeed;
        StopCoroutine(ReturnToDeciding());
        StartCoroutine(ReturnToDeciding());


        void CreateDashShadow(float lifeSpan, float dashDir)
        {
            if (dashDir < 0)
            {

                Quaternion flippedTransformrotation = Quaternion.Euler(0, 180, 0);
                GameObject currDash = Instantiate(dashAnimObj, transform.position, flippedTransformrotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);


                Destroy(currDash, lifeSpan);
            }
            else
            {

                GameObject currDash = Instantiate(dashAnimObj, transform.position, transform.rotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);
                Destroy(currDash, lifeSpan);
            }

        }
    }

    IEnumerator ReturnToDeciding()
    {
        float timeElapsed = 0.0f;
        while (timeElapsed < 1f)
        {
            if (PauseMenu.GameIsPaused)
            {
                bossState = BossState.DECIDE;
                yield break;
            }


            timeElapsed += Time.deltaTime;
            yield return null;
        }
        bossState = BossState.DECIDE;
        numShots = 0;

    }

    void ShootingState()
    {
        if (!unleashingFire) StartCoroutine(UnleashFire());
    }

    IEnumerator UnleashFire()
    {
        numShots = 0;
        unleashingFire = true;


        while (numShots < num_shot_till_decide)
        {

            if (numShots > num_shot_till_decide)
            {
                StartCoroutine(ReturnToDeciding());
                unleashingFire = false;
                Debug.Log("thats enough");
                yield break;


            }
            if (Time.time - lastShotTime >= SHOT_DELAY_INTERVAL)
            {

                // shoot player
                lastShotTime = Time.time;
               
                absAdjustment = Mathf.PingPong(Time.time, 10f);




                if (GameManager.instance.mastVol <= -80f)
                {
                    Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
                    AudioClip shootClip = sm.GetSounds("enemyShot1").clip;
                

                    var shootSound = PlayClipAt(shootClip, soundPos);
                    shootSound.pitch = Random.Range(0.65f, 1f);
                    shootSound.volume = 0.05f;
                    shootSound.reverbZoneMix = 0.078819f;
                    shootSound.outputAudioMixerGroup = src.outputAudioMixerGroup;
                }
                

                CreateGuardianProjectile(-10f - (absAdjustment * -1));
                CreateGuardianProjectile(-7f - (absAdjustment * -1));
                CreateGuardianProjectile(10f + absAdjustment);
                CreateGuardianProjectile(7f + absAdjustment);

                
                numShots += 1;

                //enemyProj.transform.localScale = new Vector3(3, 3, 1);
            }

            yield return null;
        }
        unleashingFire = false;
        
        StartCoroutine(ReturnToDeciding());

    }

    void CreateGuardianProjectile(float adjuster)
    {


        Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90 + adjuster) * relativePos;
        Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

        // GameObject enemyProj = Instantiate(projectile, firstFirePoint.transform.position, movementRotation);
        Transform chosenPoint = firepoints[Random.Range(0, firepoints.Count)];
        GameObject enemyProj = objectPool.SpawnFromPool("guardianFireball", chosenPoint.position, movementRotation);
        enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANSHOT;
    }




    void ChargeUpState()
    {
        if (!startedRainingHell)StartCoroutine(ShootSomeChargeUps());
    }
    
    
    IEnumerator ShootSomeChargeUps()
    {
        // objectPool.SpawnFromPool("")
        startedRainingHell = true;
        chargeUpPortal.SetActive(true);
        int numChargeUpsShot = 0;
        while (numChargeUpsShot <= 5)
        {
            if (numChargeUpsShot >= 5)
            {
                
                StopCoroutine(ReturnToDeciding());
                startedRainingHell = false;
                chargeUpPortal.SetActive(false);
                StartCoroutine(ReturnToDeciding());
                yield break;
            }
            FireChargeUp(Random.Range(-10, 10));
            numChargeUpsShot += 1;
            yield return new WaitForSeconds(Random.Range(0.4f, 1f));
        }
        chargeUpPortal.SetActive(false);
        startedRainingHell = false;
        StopCoroutine(ReturnToDeciding());
        StartCoroutine(ReturnToDeciding());

    }
    
    
    void FireChargeUp(int z)
    {
        if (!startedRainingHell) return;
        
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
                
        AudioClip shootClip = sm.GetSounds("bigBlastShot").clip;
        
        var shootSound = PlayClipAt(shootClip, soundPos);
        shootSound.pitch = Random.Range(0.97f, 1.29f);
        shootSound.volume = 0.08f;
        shootSound.reverbZoneMix = 0.078819f;
        shootSound.outputAudioMixerGroup = src.outputAudioMixerGroup;
    
        
        
        
        Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
        Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);
        //GameObject shot = Instantiate(bigBlasts, firstFirePoint.transform.position, movementRotation);
        GameObject shot = objectPool.SpawnFromPool("guardianBlast", chargeUpPortal.transform.position, movementRotation);
        shot.transform.rotation = shot.transform.rotation * Quaternion.Euler(0, 0, z);
        shot.GetComponent<EnemyFireball>().isBossBlast = true;
        // shot.transform.localScale = new Vector3(5g, 2.5f, 1f);
        //shot.GetComponent<EnemyFireball>().isGuardianBlast = true;
        //shot.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANBLAST;
    }

    float DistanceFromPlayer => Vector2.Distance(transform.position, playerTransform.position);
    bool InDashingRange => DistanceFromPlayer >= 4;
    bool InShootingRange => DistanceFromPlayer < 4;

    public bool CanActivateBoss => PerkManager.AllPerksAcquired;

    public void ActivateBoss()
    {
        if (PerkManager.AllPerksAcquired)
        {
            Debug.Log("Activated Boss!");


            hubLayout.activateBossButton.SetActive(false);
            
            
            GetComponent<Obstacle>().bossIsActive = true;
            GetComponent<Obstacle>().BossIsActive();
            GetComponent<Obstacle>().enabled = false;

            bossSprite.sortingLayerName = "Actor";
            //bossSpriteLight.sortingLayerName = "Actor";

            SoundManager.instance.PlayOnce("orbMode");

            spriteRenderer.sortingLayerName = "Weapon";
            spriteRenderer.sortingOrder = 12;
            spriteRenderer.sprite = activatedSprite;
            timeActivated = Time.time;

            bossState = BossState.AWOKEN;
            activated = true;
            bossAnim.SetBool("activated", true);
        }
        
    }




    protected override void OnCollide(Collider2D coll)
    {
        if (activated && coll.CompareTag("Player") && bossState != BossState.AWOKEN)
        {
            spriteRenderer.sprite = activatedSprite;
            Damage dmg = new Damage
            {
                damageAmount = Random.Range(damage, damage * critMultiplier),
                origin = transform.position,
                pushForce = pushForce
            };



            StartCoroutine(cameraShake.Shake(0.1f, 1f));
            coll.SendMessage("RecieveDamage", dmg);
            Vector2 direction = new Vector2(transform.position.x - playerTransform.position.x, transform.position.y - playerTransform.position.y).normalized * dmg.damageAmount;
            Vector2 directionPlusPosition = new Vector2(direction.x + transform.position.x, transform.position.y + direction.y);
            transform.position = Vector2.MoveTowards(transform.position, directionPlusPosition, 0.3f);

        }
    }

    protected override void RecieveDamage(Damage dmg)
    {
        if (!activated) return;
        
        base.RecieveDamage(dmg);
    }

    void OnMouseOver()
    {
        
        mouseHoveringOver = true;
        CursorManager.Instance.SetActiveCursorType(cursorType);
        if (!activated && Input.GetKeyDown(KeyCode.Mouse0)) ActivateBoss();
    }

    void OnMouseExit()
    {
       
        mouseHoveringOver = false;
        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
    }

    public void OnMouseEnter()
    {
      
        
        //   Debug.Log("On Mouse Ebter");


    }
    
    



    protected override void Death()
    {
        isDead = true;
        // Instantiate(Resources.Load("OnDeathExplosion") as GameObject, transform.position, transform.rotation);

        GameObject deathExplosion = objectPool.SpawnFromPool("OnGuardianDeath", transform.position, transform.rotation);

        StartCoroutine(cameraShake.Shake(0.15f, 2f));

        SoundManager.instance.PlayOnce("explode1");
        GameManager.instance.GrantPoints(1000);


        
        Destroy(gameObject);
    }


    
    
}
