﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Arrow : MonoBehaviour, IPooledObject
{
    // General References
    private CameraMotor cameraShake;
    // private BowManager bm;
    private Bow currentBow;
    // Sounds
    private AudioClip impact;
    //private AudioSource audioSource;

    public bool isDashOfClaw = false;
    private BoxCollider2D hitbox;

    [Header("values")]
    public float arrowSpeed = 20f;
    public int damage;
    public int critMult;
    private float breakTime = 1.4f;
    public float pushFor = 2f;

    private float timeMade;

    public enum ArrowType
    {
        REG,
        CLAW,
        LEECH,
        LONG_SHOT
    }
    public ArrowType arrowType;

    public bool isChargeUp = false;
    private int numEnemiesHit;
    private Collider2D[] hits = new Collider2D[10];
    public ContactFilter2D filter;

    protected RaycastHit2D hit;
    public Vector3 startPos;
    public Vector3 moveDelta;

    private GameObject explosion;
    private Transform firstEnemyHit;


    public bool isPiercingArrow = false;
    //private GameObject particles;

    // Start is called before the first frame update
    void Start()
    {

       // explosion = Resources.Load("OnRegChargeUpBreak") as GameObject;

        cameraShake = CameraMotor.instance;

        currentBow = BowManager.instance.currBow;
        //audioSource = GetComponent<AudioSource>();
        hitbox = GetComponent<BoxCollider2D>();
        impact = SoundManager.instance.GetSounds("ArrowHit").clip;
       // audioSource.Pause();
        timeMade = Time.time;


        if (currentBow.bowName == "The Starter")
        {
            transform.localScale = new Vector3(2.6f, 2.6f, 1f);
            arrowSpeed = 23.5f;
            
        }

        switch (arrowType)
        {
            //string _currBowName = BowManager.instance.currBow.bowName;
            //if (_currBowName == "The Leech")
            case ArrowType.LEECH:
                // transform.localScale = new Vector3(0.5f, 0.5f, 1f);
                transform.localScale = new Vector3(0.5f, 0.5f, 1f);
                arrowSpeed = 24f;
                breakTime = 0.4f;
                break;
            case ArrowType.CLAW:
                breakTime = 0.5f;
                arrowSpeed = 24f;
                transform.localScale = new Vector3(2, 2, 1);
                break;
            case ArrowType.LONG_SHOT:
                transform.GetChild(1).gameObject.SetActive(true);
                break;
        }




    }
    public void OnObjectSpawn()
    {
        timeMade = Time.time;
        startPos = transform.position;
        numEnemiesHit = 0;
        firstEnemyHit = null;
        GetComponent<AudioSource>().Pause();


        switch (arrowType)
        {
            //string _currBowName = BowManager.instance.currBow.bowName;
            //if (_currBowName == "The Leech")
            case ArrowType.LEECH:
                // transform.localScale = new Vector3(0.5f, 0.5f, 1f);
                transform.localScale = new Vector3(0.5f, 0.5f, 1f);
                arrowSpeed = 24f;
                breakTime = 0.4f;
                break;
            case ArrowType.CLAW:
                breakTime = 0.5f;
                arrowSpeed = 24f;
                transform.localScale = new Vector3(2, 2, 1);
                break;
            case ArrowType.LONG_SHOT:
                transform.GetChild(1).gameObject.SetActive(true);
                break;
        }
        // Erase array on respawn;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }
    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(Vector3.right * (Time.deltaTime * arrowSpeed));
    
        if (isActiveAndEnabled)
        {
            hitbox.OverlapCollider(filter, hits);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i] == null)
                    continue;

                if (!transform.gameObject.activeSelf)
                {
                    hits[i] = null;
                    continue;

                }
                if (transform.gameObject.activeSelf) OnCollide(hits[i]);
                
                //Cleans array
                hits[i] = null;

            }
        }

        if (currentBow.bowName == "The Starter")
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(2.8f, 2.8f, 1f), Time.deltaTime * 3f);
            arrowSpeed = Mathf.Lerp(arrowSpeed, 20f, Time.deltaTime * 3f);

        }

        else if (currentBow.bowName == "The Claw")
        {

            if (Time.time - timeMade >= 0.2f)
            {
                critMult = Random.Range(0, 2);
                damage = Random.Range(1, 4);
            }
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1f, 1f, 1), Time.deltaTime * breakTime);
        }

        else if ((currentBow.bowName == "The Rammer" && isChargeUp))
        {
            breakTime = 0.3f;

            if (Time.time - timeMade >= breakTime)
            {
                critMult = 1;
                damage = 1;
            }
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.2f, 0.4f, 1), breakTime);
        }
        else if (currentBow.bowName == "The Leech" && isChargeUp)
        {
            // breakTime = 0.5f;

            if (Time.time - timeMade >= (breakTime / 2))
            {
                arrowSpeed = 18f;
                critMult = 2;
                damage = 2;
            }
            GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, Color.clear, Time.deltaTime * breakTime);
            Debug.Log("haha jk");
            //transform.localScale = Vector3.Lerp( new Vector3(12f, 1f, 1), new Vector3(1f, 1f, 1), breakTime);
        }
        else if (arrowType == ArrowType.LEECH && !isChargeUp)
        {
            breakTime = 0.4f;

            if (Time.time - timeMade >= (breakTime / 2))
            {
                critMult = 2;
                damage = 2;
            }
            // transform.localScale = Vector3.Lerp(new Vector3(0.5f, 0.5f, 1f), new Vector3(1f, 1f, 1), Time.deltaTime * (Time.time - timeMade));
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1.3f, 1.3f, 1f), Time.deltaTime * (Time.time - timeMade));

        } 
        if (arrowType == ArrowType.LONG_SHOT)
        {
            
            if (!transform.GetChild(1).gameObject.activeInHierarchy)
            {
                
                transform.GetChild(1).gameObject.SetActive(true);
            }
            
        }


        if (Time.time - timeMade > breakTime)
        {
            BreakArrow();
        }

    }

    void BreakArrow()
    {
        if (isPiercingArrow && numEnemiesHit == 1 && Time.time - timeMade < breakTime) 
        {
            damage = 2;
            critMult = 1;
            Debug.Log("havent done enough killing");
            return;
        }

        if (arrowType == ArrowType.LEECH) transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        else if (arrowType == ArrowType.CLAW)
        {
            breakTime = 0.2f;
            transform.localScale = new Vector3(2, 2, 1);
        }

        gameObject.SetActive(false);
        


    }

    protected virtual void OnCollide(Collider2D coll)
    {
        int damageToDeal = 0;
        if (Player.instance.strengthPerk) pushFor *= 3f;

        if (coll.CompareTag("Fighter") || (coll.CompareTag("Shrine")) || coll.CompareTag("Guardian") || coll.CompareTag("Boss"))
        {
            if (isPiercingArrow)
            {
                if (firstEnemyHit != null)
                {
                    if (firstEnemyHit == coll.transform) return;
                }
            }
            if ((coll.CompareTag("Shrine")) && ((coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN) ||
                                                (coll.transform.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.CLOSED)))
            {
                return;
            }

            if ((coll.CompareTag("Guardian")) && !coll.transform.GetComponent<Guardian>().activated) return;
            else if ((coll.CompareTag("Boss")) && !coll.transform.GetComponent<Boss>().activated) return;
            int randNum = Random.Range(0, 100);
            if (randNum > 70)
            {
                damageToDeal = Random.Range(damage, (damage * 2) + critMult);
            }
            else if (randNum > 40)
            {
                damageToDeal = Random.Range(damage, damage + critMult);
            }
            else
            {
                damageToDeal = damage;
            }

            StartCoroutine(cameraShake.Shake(0.1f, 10f));
            Damage.damageType arrowType = Damage.damageType.Arrow;
            if (isChargeUp) arrowType = Damage.damageType.ChargeAttack;

            Damage dmg = new Damage
            {
                damageAmount = damageToDeal,
                origin = transform.position,
                pushForce = pushFor,
                isCrit = damageToDeal >= (damage * 2),
                originType = arrowType,

            };

            if (dmg.isCrit && damageToDeal > 1)
            {
                SoundManager.instance.Play("CriticalHit", 0);
            }

            if (!isDashOfClaw)
            {
                if (isPiercingArrow)
                {
                    if (numEnemiesHit == 0) 
                    {
                        PlayHitSound();
                    } 
                } else
                {
                    PlayHitSound();
                }
                

            }
            //audioSource.PlayOneShot(impact, 1f);

            if (coll.gameObject != null)
            {
                numEnemiesHit += 1;
                coll.SendMessage("RecieveDamage", dmg);
            }
            else Debug.Log("NULL GAMEOBJECT");

            if (!isChargeUp)
            {

                BreakArrow();
            }
            if (isChargeUp && currentBow.bowName == "The Leech")
            {
                Player.instance.Heal(1);

            }
            if (isChargeUp && numEnemiesHit == 0)
            {

                
                if (currentBow.bowName != "The Long Shot")
                {
                    BreakArrow();
                    timeMade += 0.2f;
                }

            }
            if (isChargeUp && numEnemiesHit >= 1)
            {
                if (currentBow.bowName != "The Long Shot") BreakArrow();

            }

            if (isChargeUp && numEnemiesHit > 13)
            {
                if (currentBow.bowName == "The Long Shot") BreakArrow();

            }

            if (isChargeUp && currentBow.bowName == "The Starter")
            {
               // Instantiate(explosion, transform.position, transform.rotation);
                
                GameObject explosion = ObjectPooler.instance.SpawnFromPool("explosionPurple", transform.position, transform.rotation);
                explosion.GetComponent<ExplosionCollison>().damage = damage;
                BreakArrow();
            }

            
        }

    }

    void PlayHitSound()
    {
        if (GameManager.instance.mastVol <= -80f) return;
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 2 - GameManager.instance.mastVol * 10);
        AudioSource.PlayClipAtPoint(impact, soundPos);
    }
}
