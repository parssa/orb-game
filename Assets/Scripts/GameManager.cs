﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


using MoreMountains.NiceVibrations;
using UnityEngine.Audio;

// using UnityEngine.Rendering.PostProcessing;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public WaveSpawner waveSpawner;
    public bool hasWatchedAd = false;

    public static bool CanVibrate = false;
    public static bool GameOver = false;
    [Header("Camera Stuff")]
    public CameraMotor cameraFollow;
    public Transform playerTransform;

    [Header("Orb Stuff")]
    public Transform orbTransform;
    public Orb orb;
    public int numTurretsActivated;

    public HubLayout hubLayout;

    [Header("Resources")]
    public List<Sprite> playerSprites;
    public List<Sprite> weaponSprites;
    public List<int> weaponPrices;

    [Space]
    public int healthIndex;
    public List<int> healthPrices;
    public List<int> healthValues;

    [Space]
    public int staminaIndex;
    public List<int> staminaPrices;
    public List<int> staminaValues;

    [Space]
    public int staminaRegenIndex;
    public List<int> staminaRegenPrices;
    public List<int> staminaRegenValues;

    [Space]
    public List<int> dashUpgradePrices;
    public List<int> dashUpgradeValues;



    public List<GameObject> arrows;
    public List<int> xpTable;

    [Header("Volumes")]
    public Animator volumeAnim;

    public GameObject GlitchVolume;
    public GameObject PauseVolume;
    public GameObject DamageVolume;
    public GameObject HealVolume;
    public GameObject OverDriveVolume;
    public GameObject SporeOverdriveVolume;
    public GameObject ClawOverDriveVolume;
    public GameObject LeechOverDriveVolume;
    public GameObject TeleportVolume;
    public GameObject PurpleVolume;
    
    
    [Header("References")]
    public Player player;
    public Weapon weapon;
    public Transform damagePopupText;
    //public FloatingTextManager floatingTextManager;
    public GameObject HUD;
    public PauseMenu pauseMenu;
    public float mastVol;
    // Logic
    [Header("Logics")]
    public int souls;
    public int shrineSouls;
    public int experience;
    public int score;
    public int previousScore;
    public int turretDamage;
    public bool maxLevelWeapon = false;
    public bool disableAllSounds = false;

    public enum Regions
    {
        MAIN,
        BOTTOM,
        RIGHT,
        LEFT,
        TOP
    }

    [System.Serializable]
    public class WorldRegion
    {
        public string regionName;
        public Regions thisRegion;
        public bool unlocked;
        public GameObject[] blockingZones;
        public Torch[] torches;
        //public SpawnPoint[] regionSpawnPoints;

        public void EnableRegion()
        {
            unlocked = true;
            foreach (GameObject b in blockingZones)
            {
                b.SetActive(false);
            }

            foreach (Transform _sp in WaveSpawner.instance.spawnPoints)
            {
                if (_sp.GetComponent<SpawnPoint>().shrineRegion == thisRegion)
                {
                    _sp.GetComponent<SpawnPoint>().currRegionUnlocked = true;
                }
            }

            foreach (Torch _t in torches)
            {
                _t.activated = true;
            }


            HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
            unlockedPrompt.title = regionName;
            unlockedPrompt.text = "Region unlocked";
            unlockedPrompt.type = HeaderPopup.PromptType.EXCLAMATE;
            unlockedPrompt.textColor = Color.white;


            HeaderPopup.instance.gameObject.SetActive(true);
            HeaderPopup.instance.PlayPrompt(unlockedPrompt);
        }
    }

    public WorldRegion[] allRegions;

    public Turret[] allTurrets;

    private void Awake()
    {

        if (GameManager.instance != null)
        {
            Destroy(gameObject);
            return;
        }



        // Initial Game Setup
        instance = this;
        cameraFollow.Setup(() => playerTransform.position);
        orb.Setup(() => playerTransform.position);
        previousScore = 0;
        HUD.SetActive(true);

        // Turret Values
        numTurretsActivated = 0;
        turretDamage = 2;

        foreach (WorldRegion _w in allRegions)
        {
            if (!_w.unlocked)
            {
                foreach (GameObject _g in _w.blockingZones)
                {
                    _g.SetActive(true);
                }
            }
        }


        // PauseVolume.SetActive(false);
        GlitchVolume.SetActive(false);
        HealVolume.SetActive(false);
        DamageVolume.SetActive(false);
        OverDriveVolume.SetActive(false);
        SporeOverdriveVolume.SetActive(false);
        ClawOverDriveVolume.SetActive(false);
        LeechOverDriveVolume.SetActive(false);
        TeleportVolume.SetActive(false);
        PurpleVolume.SetActive(false);
        hailMaryprompt.SetActive(false);

        mastVol = PlayerPrefs.GetFloat("actualVolume");
        
        
        Vibration.Init();
        CanVibrate = Vibration.HasVibrator();
        // CanVibrate = false;

    }
    
    
    

    public void TurretHealthUpgrade(int numUpgrade)
    {
        foreach (Turret _t in allTurrets)
        {
            _t.maxHitpoint += numUpgrade;
            _t.hitpoint = _t.maxHitpoint;
        }
    }


    public void ShortVibrate()
    {
        // Debug.Log("Short");
        if (!CanVibrate) return;
        if (PlayerPrefs.GetInt("toggleHaptics") == 0) return;
        Vibration.VibratePop();
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    public void MidVibrate()
    {
        // Debug.Log("Mid");
        if (!CanVibrate) return;
        if (PlayerPrefs.GetInt("toggleHaptics") == 0) return;
        Vibration.VibratePeek();
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void HeavyVibration()
    {
        // Debug.Log("Heavy");
        if (!CanVibrate) return;
        if (PlayerPrefs.GetInt("toggleHaptics") == 0) return;
        Vibration.VibratePop();
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    public void VibrateTriple()
    {
        // Debug.Log("Triple!");
        if (!CanVibrate) return;
        if (PlayerPrefs.GetInt("toggleHaptics") == 0) return;
        Vibration.VibrateNope();
    }


    public void JoystickRumble()
    {
        if (!CanVibrate) return;
        if (PlayerPrefs.GetInt("toggleHaptics") == 0) return;
        // MMVibrationManager.ContinuousHaptic(0.32f, 0.09f, 0.03f);
        MMVibrationManager.TransientHaptic(0.44f, 0.3f);
    }
    
    

    public void TurretArmourEquip(int _level)
    {
        hubLayout.CloseShopPrompt();
        foreach (Turret _t in allTurrets)
        {
            _t.EquipArmour(_level);
        }
    }

    public void TurretsShowTutorialArrow(bool show)
    {
        foreach (Turret _t in allTurrets)
        {
            _t.tutorialArrow.SetBool("tutorial", show);
        }
    }

    //Floating Text
    // public void ShowText(string msg, float fontSize, Color color, Vector3 position, Vector3 motion, float duration)
    // {


    //     floatingTextManager.Show(msg, 70f, color, position, motion, 0.5f);
    // }

    void ChangeInSouls()
    {
        hubLayout.StartCoroutine(hubLayout.SoulsUpdater());
    }

    public bool TryIncreaseHealth()
    {
        hubLayout.CloseShopPrompt();
        if (souls >= Player.instance.UpgradeHealthCost())
        {

            souls -= Player.instance.UpgradeHealthCost();
            Player.instance.UpgradeHealth();
            SoundManager.instance.Play("playerUpgrade", 0);
            ChangeInSouls();
            return true;
        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }

    public bool TryIncreaseStamina()
    {
        hubLayout.CloseShopPrompt();
        if (Player.instance.UpgradeStaminaCost() <= souls)
        {
            souls -= Player.instance.UpgradeStaminaCost();
            Player.instance.UpgradeStamina();
            SoundManager.instance.Play("playerUpgrade", 0);
            ChangeInSouls();
            return true;
        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }

    public bool TryIncreaseDash()
    {
        hubLayout.CloseShopPrompt();
        if (Player.instance.UpgradeDashCost() <= souls)
        {
            souls -= Player.instance.UpgradeDashCost();
            Player.instance.UpgradeDash();
            SoundManager.instance.Play("playerUpgrade", 0);
            ChangeInSouls();
            return true;
        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }

    // Upgrade Stamina Regen
    public bool TryIncreaseStaminaRegen()
    {
        hubLayout.CloseShopPrompt();
        if (Player.instance.UpgradeStaminaRegenCost() <= souls)
        {
            souls -= Player.instance.UpgradeStaminaRegenCost();
            Player.instance.UpgradeStaminaRegen();
            SoundManager.instance.Play("playerUpgrade", 0);
            ChangeInSouls();
            hubLayout.CloseShopPrompt();
            return true;
        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }


    // Upgrade Weapon
    public bool TryUpgradeWeapon()
    {


        // is the weapon max level
        if (weaponPrices.Count - 1 == weapon.weaponLevel)
        {
            maxLevelWeapon = true;



        }

        if ((souls >= weaponPrices[weapon.weaponLevel]) && !maxLevelWeapon)
        {


            souls -= weaponPrices[weapon.weaponLevel];
            weapon.UpgradeWeapon();
            SoundManager.instance.Play("upgradeStat", 0);
            ChangeInSouls();
            return true;

        }
        SoundManager.instance.Play("upgradeDeclined", 0);
        return false;
    }

    public bool NoNextWeapon()
    {
        if (weapon.weaponLevel + 1 == weaponSprites.Count)
        {
            return true;
        }
        return false;
    }

    // Experience System
    public int GetCurrentLevel()
    {
        int r = 0;
        int add = 0;

        while (experience >= add)
        {

            add += xpTable[r];
            r++;

            if (r == xpTable.Count) // Max Level
                return r;
        }

        return r;
    }
    public int GetXpToLevel(int level)
    {
        int r = 0;
        int xp = 0;

        while (r < level)
        {

            xp += xpTable[r];
            r++;
        }

        return xp;
    }
    public void GrantXp(int xp)
    {
        int currLevel = GetCurrentLevel();
        experience += xp;

        if (currLevel < GetCurrentLevel())
        {
            OnLevelUp();
        }
    }
    public void OnLevelUp()
    {

    }

    public void GameIsOver()
    {
        PlayerPrefs.SetInt("NEW_BUILD__", 1);
        if (!GameOver) GameOver = true;
        pauseMenu.TriggerGameOverState();
        
        
        // AdManager.instance.PlayInterstitialAd();
        // if (Random.Range(0, 5) == 2) AdManager.instance.PlayInterstitialAd();

    }

    private void OnEnable()
    {
        // Debug.Log("Scene has been loaded");
        
    }

    public void RestartGame()
    {
        GameOver = false;
        SceneManager.LoadScene("beginning");
    }
    
    public void BackToMainMenu()
    {
        GameOver = false;
        SceneManager.LoadScene("mainMenu");
    }
    
    


    // Points System
    public int GetCurrPoints()
    {
        return score;

    }
    public void GrantPoints(int points)
    {
        // TODO Combo Implementation
        previousScore = score;
        score += points;

    }

    // Souls System
    public int GetCurrSouls()
    {
        return souls;
    }

    public void GrantSouls(int soulsToGrant)
    {
        souls += soulsToGrant;
    }

    public void GrantShrineSouls(int soulsToGrant)
    {
        shrineSouls += soulsToGrant;
    }

    public void IntroduceGlitch()
    {
        //GlitchVolume.SetActive(true);
        //	Debug.Log("triggered");
        volumeAnim.SetBool("lowHealth", true);
        //GlitchVol.weight = Mathf.Lerp(GlitchVol.weight, maxGlitchWeight, 0.1f);
    }
    public void ExitGlitch()
    {
        //	GlitchVolume.SetActive(false);
        //	Debug.Log("Exited");
        volumeAnim.SetBool("lowHealth", false);
        // GlitchVol.weight = Mathf.Lerp(GlitchVol.weight, regGlitchWeight, 0.1f);
    }

    public void IntroduceDamage()
    {
        //StartCoroutine(IntroduceDamageCorout());
        volumeAnim.SetTrigger("gotHit");

    }
    public void ExitDamage()
    {
        DamageVolume.SetActive(false);

    }
    public void IntroduceHeal()
    {
        HealVolume.SetActive(true);

    }
    public void ExitHeal()
    {
        HealVolume.SetActive(false);


    }


    IEnumerator IntroduceDamageCorout()
    {
        float timeElapsed = 0.0f;
        DamageVolume.SetActive(true);
        while (timeElapsed < 0.2f)
        {
            if (PauseMenu.GameIsPaused)
            {
                ExitDamage();
                yield break;
            }
            timeElapsed += Time.deltaTime;
            yield return null;

        }
        ExitDamage();


    }

    // Saving and Loading
    public void SaveState()
    {

        string s = "";

        s += "0" + "|";
        s += souls.ToString() + "|";
        s += experience.ToString() + "|";
        s += "0";


        PlayerPrefs.SetString("SaveState", s);
        //Debug.Log ("SaveState");



    }
    public void LoadState(Scene s, LoadSceneMode mode)
    {

        if (PlayerPrefs.HasKey("SaveState"))
            return;

        string[] data = PlayerPrefs.GetString("SaveState").Split('|');

        //Change Player Skin
        souls = int.Parse(data[1]);
        experience = int.Parse(data[2]);
    }


    public void UnlockRequestedRegion(Regions region)
    {
        foreach (WorldRegion _w in allRegions)
        {
            if (_w.thisRegion == region)
            {
                waveSpawner.PutShrinesInUnlockedRegionInList(region);
                _w.EnableRegion();

            }
        }

        AstarPath.active.Scan();
    }

    public void TurretMultiBlessing()
    {
        foreach (Turret t in allTurrets)
        {
            t.thisTurret = Turret.TurretType.BURST;
        }
    }


    public void TurretCheapBlessing()
    {
        foreach (Turret t in allTurrets)
        {
            t.activateCost = 10;
        }
    }

    public void TurretExplodeBlessing()
    {
        foreach (Turret t in allTurrets)
        {
            t.projectilesExplode = true;
        }
    }

    public void PlayerHealEndWaveBlessing()
    {
        int maxHp = Player.instance.maxHitpoint;
        int currHp = Player.instance.hitpoint;

        int diff = maxHp - currHp;

        // int healNum = (int)(Mathf.Round(1 - ratio) * maxHp);
        int healNum = diff <= 10 ? 5 : Random.Range(4, 8);
        Player.instance.Heal(healNum);
    }

    public void HailMaryMonetized()
    {
        /*    0: Check if :
         *        a) is on mobile
         *        b) hasn't already watched an ad 
              1. Play Prompt to Watch Ad
                If player chooses No: 
                   1. save scores
                   2. gm.EndGame()
                ELSE:
                  if ad completed succesfully or failed
                    1. ressurect()
                    2. penalize
                   else gm.EndGame()
        */
        if (!CanOfferHailMary) return;
        Debug.Log("Can Show Prompt!");
        
        pauseMenu.ShopMenuPause();
        hailMaryprompt.SetActive(true);
        
        // show prompt 
    }

    
    public bool CanOfferHailMary =>
        ControlsHandler.instance.currPlatform == ControlsHandler.CurrPlatform.MOBILE 
        && !hasWatchedAd 
        && !ControlsHandler.instance.tabletDevice;

   public void HailMaryMonetizedAccepted()
   {
       hailMaryprompt.SetActive(false);
       // AdManager.instance.PlayRewardedVideoAd();
       AdManager.instance.PlayInterstitialAd();

   }

   public void HailMaryMonetizedRejected()
   {

       Debug.Log("Rejected Offering");
       hailMaryprompt.SetActive(false);

       GameIsOver();
   }

   public void HailMaryAdFinished()
   {
       hasWatchedAd = true;
       player.hitpoint = player.maxHitpoint;
       OrbHitBox.instance.hitpoint = OrbHitBox.instance.maxHitpoint;
       StartCoroutine(PauseMenu.instance.CountdownToPlayMode());
       // Instantiate(hailMaryExplosion, orbTransform.position, orbTransform.rotation);

   }

   public void ResurrectTheTeam()
   {
       player.hitpoint = player.maxHitpoint;
       OrbHitBox.instance.hitpoint = OrbHitBox.instance.maxHitpoint;
       EnemyManager.instance.YolkEnemies(1, 1, 0.1f);
       pauseMenu.ShopMenuResume();
       Debug.Log("Explision checkkkk");
       ObjectPooler.instance.SpawnFromPool("resBoom", playerTransform.position, Quaternion.identity);
       ObjectPooler.instance.SpawnFromPool("resBoom", orbTransform.position, Quaternion.identity);
   }


   public GameObject hailMaryprompt;
   public GameObject hailMaryExplosion;

   // public enum HailMaryChoice
   // {
   //     CHOOSING,
   //     ACCEPTED,
   //     REJECTED
   // }
   //
   // public HailMaryChoice playerChoice;

}
