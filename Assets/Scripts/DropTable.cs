﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTable : MonoBehaviour
{
    
    public int chanceOfDropping = 70;
    private int rnGesus;
    private int specialLoot;

    public void DropLoot(List<GameObject> potentialDrops, GameObject specialItem)
    {
        rnGesus = Random.Range(0, 100);
        if (rnGesus <= chanceOfDropping)
        {
            specialLoot = Random.Range(0, 10);
            if (specialLoot >= 7)
            {
                Instantiate(specialItem, transform.position, transform.rotation);
            } else if (potentialDrops.Count > 0)
            {
                Instantiate(potentialDrops[Random.Range(0, potentialDrops.Count - 1)], transform.position, transform.rotation);
            }

            
        }
    }




}
