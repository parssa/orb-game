﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering.Universal;

public class OrbHitBox : Fighter
{

	public static OrbHitBox instance;
	public bool gotHit = false;
	public Animator orbAnimator;
	public Animator orbEyeAnimator;

    private float lastSpawnTime = 0.0f;
    private const float SPAWN_TIME_DELAY = 0.1f;
    private ObjectPooler objectPooler;
    private Light2D playerBandanaLight;

    private SoundManager sm;
#pragma warning disable 0649
    [SerializeField] private AudioMixer mainMixx;
#pragma warning restore 0649
    private AudioClip[] orbHitClips;

    private void Awake()
    {
		instance = this;
        
    }

	private void Start()
	{
        objectPooler = ObjectPooler.instance;
        playerBandanaLight = Player.instance.bandanaLight;
        
        sm = SoundManager.instance;

        orbHitClips = new[]
        {
	        sm.GetSounds("orbHit1").clip,
	        sm.GetSounds("orbHit2").clip,
	        sm.GetSounds("orbHit3").clip,
	        sm.GetSounds("orbHit4").clip
			
        };
	}

    private void Update()
    {
		
		orbEyeAnimator.SetBool("gotHit", gotHit);


        float hitPointFloat = hitpoint;
        float maxHitPointFloat = maxHitpoint;
        float damagePerc = hitPointFloat / maxHitPointFloat;
		
        orbAnimator.SetFloat("percentDamage", damagePerc);

    }


    // private float lastSoundMadeHit = 0f;

    protected override void RecieveDamage(Damage dmg)
	{
		base.RecieveDamage(dmg);
		gotHit = true;
		if (Time.time - lastSpawnTime > SPAWN_TIME_DELAY)
		{
            objectPooler.SpawnFromPool("orbP", dmg.origin, transform.rotation);
            lastSpawnTime = Time.time;


            // if (Time.time - lastSoundMadeHit >= 0.5f && GameManager.instance.mastVol > -78f)
            // {
	           //  
	           //  Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
            //
	           //  AudioClip shootClip = orbHitClips[Random.Range(0, orbHitClips.Length)];
	           //  var shootSound = PlayClipAt(shootClip, soundPos);
	           //  shootSound.pitch = Random.Range(0.9f, 1f);
	           //  shootSound.volume = 0.1f;
	           //  shootSound.reverbZoneMix = 0.078819f;
	           //  shootSound.outputAudioMixerGroup = mainMixx.outputAudioMixerGroup;
	           //  lastSoundMadeHit = Time.time;
            // }
            
            
            StartCoroutine(FlashBandana());
            
            
            
		}
	}



    IEnumerator FlashBandana()
    {
	    float timeElapsed = 0f;
	    while (timeElapsed < 0.1f)
	    {
		    if (timeElapsed >= 0.1f)
		    {
			    playerBandanaLight.intensity = 0.5f;
			    yield break;
		    }

		    playerBandanaLight.intensity = 1.1f;
		    
		    timeElapsed += Time.deltaTime;
		    yield return null;
	    }
	    playerBandanaLight.intensity = 0.5f;
    }

	public void Heal()
	{
		int randHealAmount = Random.Range(7, 15);
		if (hitpoint + randHealAmount >= maxHitpoint)
        {
			hitpoint = maxHitpoint;
        } else
        {
			hitpoint += randHealAmount;
        }
	}

    protected override void Death()
    {
        
		PlayerPrefs.SetInt("CurrentScore", GameManager.instance.GetCurrPoints());
		PlayerPrefs.SetInt("PlayerOrOrb", 1);
		PlayerPrefs.SetInt("WaveOnDeath", WaveSpawner.instance.waveNum + 1);
		if (!PlayerPrefs.HasKey("HighScore"))
		{
			PlayerPrefs.SetInt("HighScore", GameManager.instance.GetCurrPoints());


		}
		else if (PlayerPrefs.GetInt("HighScore") < GameManager.instance.GetCurrPoints())
		{
			PlayerPrefs.SetInt("HighScore", GameManager.instance.GetCurrPoints());
		}
		
		int waveDied = WaveSpawner.instance.waveNum + 1;
		if (!PlayerPrefs.HasKey("BestWave"))
		{
			PlayerPrefs.SetInt("BestWave", waveDied);


		}
		else if (PlayerPrefs.GetInt("BestWave") < waveDied)
		{
			PlayerPrefs.SetInt("BestWave", waveDied);
		}
		
		
		if (GameManager.instance.CanOfferHailMary)
		{
			GameManager.instance.HailMaryMonetized();
		}
		else
		{
			GameManager.instance.GameIsOver();
		}
		GameManager.instance.GameIsOver();
		//SceneManager.LoadScene("gameOver");
	}
    
    
    AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
	    GameObject tempGO = new GameObject("TempAudio"); // create the temp object
	    tempGO.transform.position = pos; // set its position
        
        
	    AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
	    aSource.clip = clip; // define the clip
	    // set other aSource properties here, if desired
	    aSource.spatialBlend = 0.5f;
	    
	    
	    aSource.Play(); // start the sound
	    Destroy(tempGO, clip.length); // destroy object after clip duration
	    return aSource; // return the AudioSource reference
    }
}
