﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Bow 
{
    public string bowName; 
    public string description;
    
    public Sprite bowImage;
    public Sprite bowLightCookie;
    
    public enum ClassType
    {
        NEUTRAL,
        RANGER, 
        BRUTE, 
    }
    public ClassType weaponClass;
    [Range(0f, 2f)] public float cooldown;
    [Range(100f, 1000f)]public float pushForce;
    public int arrowDamage; 
    public int dashDamage;
    public int critMultiplier;
    public int chargeUpDamage;
    public GameObject arrowProj;
    public GameObject chargeUpProj;
    public Color32 dashColor;
    public string chargeUpDescription;
    public Color32 darkTextColor;
    public Color32 lightTextColor;
    public float chargeCooldown;


}
