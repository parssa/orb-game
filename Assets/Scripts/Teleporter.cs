﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : Collidable
{
    private GameObject EffectsObj;
    Animator animator;
    //BoxCollider2D coll;

    bool activated = false;
    bool teleporting = false;
    private float timeLastDetectedPlayer = 0f;
    private float teleportingFor = 0f; // How long theyvebeen on telelporters



    public Transform teleportsTo;

    // Start is called before the first frame update
    protected override void Start()

    {
        base.Start();

        EffectsObj = transform.GetChild(0).gameObject;
        animator = GetComponent<Animator>();
         animator.SetBool("active", activated);

    }

    public Torch[] torches;

    // Update is called once per frame
    protected override void Update()
    {

        base.Update();
       
        if (activated)
        {
            animator.SetBool("teleporting", teleporting);
            if (teleporting)
            {

                // Debug.Log("Player is on the teleporter");
                if (Time.time - timeLastDetectedPlayer > 0.2f)
                {
                    //Debug.Log("Player LEFT");


                    teleporting = false;

                }

                teleportingFor += Time.deltaTime;

                if (teleportingFor > 1.4f) Teleport();

            }
            else teleportingFor = 0f;
        }


    }

    protected override void OnCollide(Collider2D coll)
    {
        base.OnCollide(coll);
        if (coll.CompareTag("Player") && activated)
        {
            teleporting = true;
            timeLastDetectedPlayer = Time.time;


        }
    }

    void Teleport()
    {
        Debug.Log("Boom!");
        Player.instance.transform.position = teleportsTo.position;
        teleportingFor = 0f;
        StartCoroutine(TeleportingVolumeCoro());
    }

    IEnumerator TeleportingVolumeCoro()
    {
        float timeElapsed = 0.0f;
        GameManager.instance.TeleportVolume.SetActive(true);
        while (timeElapsed < 0.4f)
        {
            if (PauseMenu.GameIsPaused)
            {
                GameManager.instance.TeleportVolume.SetActive(false);
                yield break;

            }
            timeElapsed += Time.deltaTime;
            yield return null;

        }
        GameManager.instance.TeleportVolume.SetActive(false);

    }

    // void OnMouseOver()
    // {
    //     if (Input.GetKeyDown(KeyCode.Mouse0)) Activated();
    // }

    public void Activated()
    {
        activated = true;
         animator.SetBool("active", activated);
        if (torches.Length > 0)
        {
            foreach (Torch t in torches)
            {
                t.activated = true;
            }
        }

    }

}
