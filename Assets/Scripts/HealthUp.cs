﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUp : PowerUp
{
    public bool isDuality = false;
    protected override void Start()
    {
        base.Start();
        
    }

    protected override void OnCollect()
    {
       
        if (!collected)
        {
            collected = true;
            SoundManager.instance.Play("Powerup", 0);
            
            if (isDuality)
            {
                Player.instance.pickedUpDuality = true;
          //      PowerupManager.instance.dualityDropPickedUp = true;
                
            } else Player.instance.pickedUpHealth = true;
            Destroy(gameObject);
        }

    }
}
