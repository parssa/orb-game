﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledParticleSystem : MonoBehaviour, IPooledObject
{

    ParticleSystem pSystem;
    float timeMade;

    public float breakTime; // Make sure this is equal to duration in Particle System

    public void OnObjectSpawn()
    {

        timeMade = Time.time;
        pSystem = GetComponent<ParticleSystem>();
        pSystem.Play();
        StartCoroutine(BreakParticleSystemAfterPlay());


    }

    IEnumerator BreakParticleSystemAfterPlay()
    {
        while (Time.time - timeMade < breakTime)
        {
            // Wait for particle system to finish playing
            yield return null;
        }
     //   Debug.Log("Breaking Particle System now!");
        transform.gameObject.SetActive(false);
    }
}
