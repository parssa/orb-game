﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bush : MonoBehaviour
{
    private bool collisionDetected = false;
    private float lastDetectTime = 0.0f;
    private const float INVTERVAL_DETECT = 0.2f;

    // Colliders
    private BoxCollider2D hitbox;
    private Collider2D[] hits = new Collider2D[5];
    public ContactFilter2D filter;


    // Audio
    private AudioSource audioSource;
    private AudioClip impact;

    // Particle System
#pragma warning disable 0649
    [SerializeField] private ParticleSystem foliageParticles;  
    [SerializeField] private ParticleSystemRenderer foliageParticleRenderer;
    [SerializeField] private SpriteRenderer foliage;
#pragma warning restore 0649
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        hitbox = GetComponent<BoxCollider2D>();
        audioSource = GetComponent<AudioSource>();
        audioSource.Pause();
        impact = SoundManager.instance.GetSounds("ArrowHit").clip;

        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        hitbox.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {

            if (hits[i] == null)
                continue;
            OnCollide(hits[i]);
            //Cleans array
            hits[i] = null;
        }

        if (collisionDetected) 
        {
            
            // if (Random.Range(0, 11) > 5)
            // {
            //     SoundManager.instance.Play("DashImpactOne", 0);
            // }
            // else
            // {
            //     SoundManager.instance.Play("DashImpactTwo", 0);
            // }

            collisionDetected = false;
        }
        foliageParticleRenderer.sortingLayerName = foliage.sortingLayerName;
        foliageParticleRenderer.sortingOrder = foliage.sortingOrder - 1;
    }

    protected virtual void OnCollide(Collider2D coll)
    {   
            if ((coll.name == "Player" && Player.instance.isDashing) || 
                (coll.name == "weapon" && Player.instance.isDashing) || 
                (coll.CompareTag("Laser") && coll.name != "weapon"))
                {
                    OneShotDetect();
                }
           
        
    }


    void OneShotDetect()
    {
        if (Time.time - lastDetectTime >= INVTERVAL_DETECT)
        {
            lastDetectTime = Time.time;
            // player.pushDirection = (player.transform.position - gameObject.transform.GetChild(0).position).normalized / 100;
            collisionDetected = true;


            
            foliageParticles.Play();
            animator.SetTrigger("gotHit");

        }
    }
}
