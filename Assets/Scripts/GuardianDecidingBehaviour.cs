﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianDecidingBehaviour : StateMachineBehaviour
{
    Vector2 currPos;
    Vector2 moveTowardsPos;
    Vector3 positionInVector3Form;
    private float differenceInSway;

    public float speed;
    private float naturalSpeed;
    private float fasterSpeed;

    //private float timeSinceChangedBobDir = 0.0f;
    //private float intervalBetweenBobs = 0.1f;

    private const float THRESHOLD_Y = 2f;
    private const float THRESHOLD_X = 6f;
    private const float heightFromPlayer = 3.5f;
    private Transform playerTransform;

    private bool goingToPlayer = false;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        differenceInSway = -1f * 0.7f;
        currPos = animator.transform.position;
        moveTowardsPos = new Vector2(currPos.x, currPos.y + differenceInSway);
        playerTransform = Player.instance.transform;
        naturalSpeed = 3f;
        fasterSpeed = naturalSpeed + 1.5f;
        animator.transform.GetComponent<Guardian>().shadowObj.SetActive(true);
        //positionInVector3Form = new Vector3(moveTowardsPos.x, moveTowardsPos.y, animator.transform.position.z);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!animator.transform.GetComponent<Guardian>().isGettingKnockedBack && !animator.transform.GetComponent<Guardian>().unleashingFire) animator.transform.position = Vector2.MoveTowards(animator.transform.position, moveTowardsPos, speed * Time.deltaTime);

        positionInVector3Form = new Vector3(moveTowardsPos.x, moveTowardsPos.y, animator.transform.position.z);
        // if (animator.transform.GetComponent<Guardian>().numTimesHit > 10)
        // {
        //     numTimesHit = 0;

        // }

        currPos = animator.transform.position;




        if ((currPos.x - playerTransform.position.x > THRESHOLD_X) || (currPos.x - playerTransform.position.x < THRESHOLD_X * -1))
        {
            goingToPlayer = true;
            moveTowardsPos = new Vector2(playerTransform.position.x, moveTowardsPos.y);
            positionInVector3Form = new Vector3(moveTowardsPos.x, moveTowardsPos.y, animator.transform.position.z);
            speed = fasterSpeed;



        }
        if ((currPos.y - playerTransform.position.y > heightFromPlayer + THRESHOLD_Y) || (currPos.y - playerTransform.position.y < heightFromPlayer - THRESHOLD_Y))
        {
            goingToPlayer = true;
            moveTowardsPos = new Vector2(moveTowardsPos.x, playerTransform.position.y + heightFromPlayer);
            positionInVector3Form = new Vector3(moveTowardsPos.x, moveTowardsPos.y, animator.transform.position.z);
            speed = fasterSpeed;

        }

        if (animator.transform.position == positionInVector3Form)
        {
            currPos = animator.transform.position;
            speed = naturalSpeed;
        }

        if (goingToPlayer && ((currPos.x - playerTransform.position.x > THRESHOLD_X) || (currPos.x - playerTransform.position.x < THRESHOLD_X * -1) && (currPos.y - playerTransform.position.y > heightFromPlayer + THRESHOLD_Y) || (currPos.y - playerTransform.position.y < heightFromPlayer - THRESHOLD_Y)))
        {

            moveTowardsPos = new Vector2(playerTransform.position.x, playerTransform.position.y + heightFromPlayer);
            //animator.transform.position = Vector2.MoveTowards(animator.transform.position, new Vector2(playerTransform.position.x, playerTransform.position.y + heightFromPlayer), speed * Time.deltaTime);
        }
        else if (goingToPlayer && currPos == new Vector2(playerTransform.position.x, playerTransform.position.y + heightFromPlayer)) goingToPlayer = false;
    }

}
