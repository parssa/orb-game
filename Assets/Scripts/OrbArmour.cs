﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbArmour : MonoBehaviour
{
    public Sprite[] armourSprites;
    private SpriteRenderer armourRenderer;
#pragma warning disable 0649
    [SerializeField] private Orb orb;
#pragma warning restore 0649

    void Start()
    {
        armourRenderer = GetComponent<SpriteRenderer>();
        armourRenderer.color = Color.clear;
    }

    void Update()
    {

        if (orb.orbLevel >= 1)
        {
            armourRenderer.color = Color.white;
        }

    

        if (orb.orbLevel >= armourSprites.Length)
        {
            armourRenderer.sprite = armourSprites[armourSprites.Length- 1];
        } else if (orb.orbLevel > 0)

        {
           
            armourRenderer.sprite = armourSprites[orb.orbLevel - 1];
        }
       
       
    }
}
