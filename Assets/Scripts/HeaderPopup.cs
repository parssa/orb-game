﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HeaderPopup : MonoBehaviour
{
    public static HeaderPopup instance;

    private ControlsHandler controls;
    private CharacterMenu characterMenu;
    private bool headerIsOpen = false;

    public bool HeaderIsOpen => headerIsOpen;
    private Queue<Prompt> promptQueue = new Queue<Prompt>();

    void Awake()
    {
        instance = this;
    }

    public Sprite[] icons;

    public enum PromptType
    {
        EXCLAMATE,
        SOULS_ICON,
        STAMINA,
        ORB,
        SPECIAL,
        SHRINE_SOUL,
        CROSSHAIR
    }

    public enum PromptRespect
    {
        TUTORIAL, // Tutorial Prompts happen only in the beginning
        THROWAWAY, // Throwaway Prompts don't reappear when another prompt is played over it
        RESHELF, // Reshelf Prompts get added to the queue if another prompt plays over it, and returns once other prompt is done
    }

    public enum TipState
    {
        NOT_TIP,
        NOT_TASK,
        HAVENT_FINISHED,
        FINISHED
    }


    [System.Serializable]
    public class Prompt
    {
        public string title;
        public string text;
        public string mobileText;
        public PromptType type;
       // public int conditions;
        public Color textColor;
        public PromptRespect respect = PromptRespect.TUTORIAL;
        public TipState tipState = TipState.NOT_TIP;


    }

    private int numPromptsShown = 0;
    private float timePromptShown;

    public TextMeshProUGUI title;
    public TextMeshProUGUI bodyText;
    public Image icon;


    public Prompt[] prompts;
    public Prompt[] newPrompts;
    //public Prompt[] promptsMobile;

    public Prompt turretPrompt;
    public Prompt shopPrompt;
    public Prompt guardianPrompt;
    public Prompt chargeShotPrompt;
    public Prompt soulsPrompt;
    public Prompt shrinePrompt;

    public Prompt aimAssistPrompt;
    
    private Prompt lastDisplayedPrompt;
    // Start is called before the first frame update
    void Start()
    {

        controls = ControlsHandler.instance;
       
        prompts = newPrompts;
      

        if (!TutorialManager.TutorialCompleted) 
        {
            transform.gameObject.SetActive(true);
             PlayPrompt(prompts[0]);
        }
        else transform.gameObject.SetActive(false);
       
        timePromptShown = 0f;
        characterMenu = CharacterMenu.instance;



    }

    // // // Update is called once per frame
    // void Update()
    // {
    //    // if (TutorialManager.TutorialCompleted) transform.gameObject.SetActive(false);
    // }


    public void NextPrompt()
    {
        numPromptsShown += 1;
        if (numPromptsShown >= prompts.Length) transform.gameObject.SetActive(false);
        else PlayPrompt(prompts[numPromptsShown]);


    }

    public void PlayPrompt(Prompt prompt, bool addToQueue = false, bool isTip = false)
    {
        if (prompt.tipState == TipState.FINISHED)
        {
            Debug.Log($"Done{prompt.title}");
            DisablePrompt();
            return;
        }
        if (addToQueue && headerIsOpen)
        {
            if (!promptQueue.Contains(prompt) && lastDisplayedPrompt != prompt && prompt.tipState != TipState.FINISHED)
            {
                Debug.Log($"Added {prompt.title}");
                promptQueue.Enqueue(prompt);
            }
            
        }
        else
        {
            if (headerIsOpen)
            {
                if (lastDisplayedPrompt.respect == PromptRespect.RESHELF && prompt != lastDisplayedPrompt && prompt.tipState != TipState.FINISHED)
                {
                    Debug.Log($"Reshelf {lastDisplayedPrompt.title}");
                    promptQueue.Enqueue(lastDisplayedPrompt);
                }
            }
            
            
            headerIsOpen = true;
            title.SetText(prompt.title);

            if (prompt.mobileText == null)
            {
                // Debug.Log("There was no mobile equiv"); 
                prompt.mobileText = prompt.text;
            }
            
            // bodyText.SetText(Application.isMobilePlatform
            //     ? prompt.mobileText
            //     : prompt.text);
            
            bodyText.SetText(controls.currPlatform == ControlsHandler.CurrPlatform.MOBILE
                ? prompt.mobileText
                : prompt.text);

            bodyText.color = prompt.textColor;

            lastDisplayedPrompt = prompt;
            timePromptShown = Time.time;
            
            switch (prompt.type)
            {
                case PromptType.SOULS_ICON:
                    icon.sprite = icons[1];
                    break;
                case PromptType.EXCLAMATE:
                    icon.sprite = icons[0];
                    break;
                case PromptType.STAMINA:
                    icon.sprite = icons[2];
                    break;
                case PromptType.ORB:
                    icon.sprite = icons[3];
                    break;
                case PromptType.SHRINE_SOUL:
                    icon.sprite = icons[4];
                    break;
                case PromptType.CROSSHAIR:
                    icon.sprite = icons[5];
                    break;

            }


            if (TutorialManager.TutorialCompleted && !isTip) StartCoroutine(HidePromptUnscaledTime());
        }
        

       // if (TutorialManager.TutorialCompleted) transform.gameObject.SetActive(false);

        //if (prompt.type == PromptType.SOULS_ICON) icon = 
    }

    public Prompt FetchPrompt(PromptType promptType)
    {
        Prompt fetchedPrompt = prompts[0];
        foreach (Prompt p in prompts)
        {
            if (p.type == promptType) fetchedPrompt = p;
        }
        return fetchedPrompt;
    }

    public IEnumerator HidePromptUnscaledTime()
    {

        float timeElapsed = 0f;
        
        while (timeElapsed < 3f)
        {
            timeElapsed += 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }
        DisablePrompt();

    }

    IEnumerator HidePrompt()
    {
        

        while (Time.time - timePromptShown <= 3f)
        {
            if (Time.time - timePromptShown > 3f)
            {
                DisablePrompt();
            }
            yield return new WaitForSecondsRealtime(0.1f);
        }
        DisablePrompt();
    }

    public void DisablePrompt()
    {
        if (promptQueue.Count != 0)
        {
            PlayPrompt(promptQueue.Dequeue());
            
        }
        else
        {
            headerIsOpen = false;
            title.color = Color.white;

        
        
            gameObject.SetActive(false);
        }
        
    }


    public void FinishedChargeUpTip()
    {
        if (!headerIsOpen) DisablePrompt();
        else if (lastDisplayedPrompt == chargeShotPrompt) DisablePrompt();
    }

    public void FinishedShopTip()
    {
        if (!headerIsOpen) gameObject.SetActive(true);
        PlaySoulsPrompt();
    }

    public void PlayTurretPrompt()
    {
        if (turretPrompt.tipState == TipState.FINISHED)
        {
           // Debug.Log("Finished Turret Prompt, not going to play it");
            return;
        }
        
        PlayPrompt(turretPrompt, false, true);
    }
    
    public void PlayShopPrompt()
    {
        if (turretPrompt.tipState == TipState.FINISHED)
        {
            //Debug.Log("Finished Shop Prompt, not going to play it");
            return;
        }
        
        PlayPrompt(shopPrompt, true, true);
    }
    
    public void PlayChargePrompt()
    {
        if (chargeShotPrompt.tipState == TipState.FINISHED)
        {
          //  Debug.Log("Finished Charge Prompt, not going to play it");
            return;
        }
        
        PlayPrompt(chargeShotPrompt, true, true);
    }
    
    public void PlayGuardianPrompt()
    {
        if (guardianPrompt.tipState == TipState.FINISHED)
        {
           // Debug.Log("Finished Guardian Prompt, not going to play it");
            return;
        }
        
        PlayPrompt(guardianPrompt);
    }

    public void PlayShrinePrompt()
    {
         if (shrinePrompt.tipState == TipState.FINISHED) return;
         PlayPrompt(shrinePrompt, false, true);
    }

    public void FinishedShrinePrompt()
    {
        if (!headerIsOpen) DisablePrompt();
        else if (lastDisplayedPrompt == shrinePrompt) DisablePrompt();
    }

    public void PlaySoulsPrompt()
    {
        if (soulsPrompt.tipState == TipState.FINISHED) return;
        PlayPrompt(soulsPrompt);
    }

    public void CompletedTipPrompt(TutorialManager.TipsSpecify whichTip)
    {
        switch (whichTip)
        {
            case TutorialManager.TipsSpecify.Turret:
                turretPrompt.tipState = TipState.FINISHED;
                break;
            case TutorialManager.TipsSpecify.Shop:
                shopPrompt.tipState = TipState.FINISHED;
                break;
            case  TutorialManager.TipsSpecify.Guardian:
                guardianPrompt.tipState = TipState.FINISHED;
                break;
            case TutorialManager.TipsSpecify.ChargeUp:
                chargeShotPrompt.tipState = TipState.FINISHED;
                break;
            
            case TutorialManager.TipsSpecify.Shrine:
                shrinePrompt.tipState = TipState.FINISHED;
                break;
        }
    }
    

}
