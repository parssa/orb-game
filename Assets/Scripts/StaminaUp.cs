﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaUp : PowerUp
{
	
    protected override void OnCollect()
	{
		
		if (!collected)
		{
			collected = true;
			SoundManager.instance.Play("Powerup", 0);
			Player.instance.pickedUpStamina = true;
			Destroy(gameObject);
		}

	}
}
