﻿using System;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using Random = System.Random;


public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public Sounds[] sounds;
    public AudioMixerGroup audioMixx;

    
    private List<float> soundsPitchList = new List<float>();
    private int pitchIndex;
    
    
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

        foreach (Sounds s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.outputAudioMixerGroup = audioMixx;

        }

        float pitchVal = 0.65f;
        
        for (int a = 0; a <= 20; a++)
        {
            pitchVal += 0.01f;
            soundsPitchList.Add(pitchVal);    
        }
    }


    private float lastSoulCollectedTime = 0f;
    private const float SOUL_RESET_TIME = 2f;
    private const float SOUL_STEP_TIME = 0.15f;

    public float FetchSoulPitch(bool cameFromShrine = false)
    {
        if (cameFromShrine)
        {
            return 1f;
        }
        
        if (Time.time - lastSoulCollectedTime >= SOUL_RESET_TIME)
        { 
            pitchIndex = 0;
        } 
        else if (Time.time - lastSoulCollectedTime <= SOUL_STEP_TIME)
        {
            pitchIndex += 1;
        }

        lastSoulCollectedTime = Time.time;
        
        
        if (pitchIndex >= soundsPitchList.Count)
        {
            pitchIndex = 0;
        }

        float pitchVal = soundsPitchList[pitchIndex];
        
        
        return pitchVal;
    }

    public void Play(string name, float pitchMod)
    {

        Sounds s = Array.Find(sounds, sound => sound.soundName == name);
        if (pitchMod == 0)
        {
            pitchMod = UnityEngine.Random.Range(-0.7f, 0.8f);
        }

        s.pitch += pitchMod;
        s.source.Play();


    }



    public void PlayOnce(string name)
    {
        Sounds s = Array.Find(sounds, sound => sound.soundName == name);
        s.source.PlayOneShot(s.clip);
    }

    public Sounds GetSounds(string name)
    {
        return Array.Find(sounds, sound => sound.soundName == name);
    }


}
