﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EyeControl : MonoBehaviour
{

    //public Transform playerTransform;
    Animator anim;
    private Animator playerAnim;
    // private bool currSide = true;
    public static EyeControl instance;
    private ControlsHandler controlsHandler;

    private float currX;
    private float currY;

    void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        currX = 0;
        currY = 0;

        playerAnim = Player.instance.GetComponent<Animator>();

        controlsHandler = ControlsHandler.instance;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerFaceCursor();
        

    }

   


    void PlayerFaceCursor()
    {

        // Vector2 direction = controlsHandler.currVec;
        // if (currSide && direction.x < -0.01)
        // {
        //     currSide = false;
        // } else if (!currSide && direction.x >= 0.01)
        // {
        //     currSide = true;
        // }
        // currX = direction.x;
        // currY = direction.y;
        //
        //
        // anim.SetFloat("x", currX);
        // anim.SetFloat("y", currY);
        // anim.SetBool("rightSide", currSide);
        
        // Potential New Way
        
        // if (!PauseMenu.GameIsPaused)
        // {
        //     var animVec = controlsHandler.currVec;
        //     anim.SetFloat("x", animVec.x);
        //     anim.SetBool("rightSide", animVec.x > 0);
        //     anim.SetFloat("y", animVec.y);
        // }
        
        if (!PauseMenu.GameIsPaused)
        {
            
            anim.SetFloat("x", playerAnim.GetFloat("x"));
            anim.SetBool("rightSide", playerAnim.GetFloat("x") > 0);
            anim.SetFloat("y", playerAnim.GetFloat("y"));
        }
    }

    public Vector2 EyeAnimatorValues()
    {
        Vector2 values = new Vector2(currX, currY);


        return values;
    }

    



}
