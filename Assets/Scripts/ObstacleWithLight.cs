﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ObstacleWithLight : Obstacle
{
    public Light2D childLight;
    private float normalIntensity;
    private float fadedIntensity;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        normalIntensity = childLight.intensity;
        fadedIntensity = 1.11f;
        if (isShrine) fadedIntensity = 0.8f;

    }

    protected override void Update()
    {
        base.Update();
       // childLight.

        if (isShrine)
        {
            // if (transform.parent.GetComponent<SpawnPoint>().currState == SpawnPoint.SpawnerState.BROKEN)
            // {
            //     childLight.intensity = Mathf.Lerp(childLight.intensity, 0f, Time.deltaTime *2);
            //     transform.parent.GetChild(0).GetComponent<Light2D>().intensity = 0f;
            // }
            // else 
            // {
            //     transform.parent.GetChild(0).GetComponent<Light2D>().intensity = 1;
            // }
            if (!transform.parent.GetComponent<SpawnPoint>().shrineIsActive)
            {
                childLight.intensity = Mathf.Lerp(childLight.intensity, 0f, Time.deltaTime * 2);
                transform.parent.GetChild(0).GetComponent<Light2D>().intensity = 0.2f;
            }
            else
            {
                transform.parent.GetChild(0).GetComponent<Light2D>().intensity = 1;
            }

        }

        if (playerContact)
        {
            StartCoroutine(FadeOutLight());

        }
        if (!playerContact)
        {

            StartCoroutine(FadeInLight());

        }

    }

    public IEnumerator FadeOutLight()
    {
        float timePassed = 0;
        //fadeOutIsDone = false;
        while (timePassed < 0.1)
        {
            if (PauseMenu.GameIsPaused)
                yield break;

            if (isShrine && !transform.parent.GetComponent<SpawnPoint>().shrineIsActive)
            {
                yield break;
            }
            if (childLight.intensity != fadedIntensity)
            {
                childLight.intensity = Mathf.Lerp(childLight.intensity, fadedIntensity, 0.1f);
            }

            timePassed += Time.deltaTime;
            yield return null;
        }
        playerContact = false;
        //fadeOutIsDone = true;
    }

    public IEnumerator FadeInLight()
    {
        float timePassed = 0;
        //fadeInIsDone = false;
        while (timePassed < 0.07)
        {
            if (PauseMenu.GameIsPaused)
                yield break;
            if (isShrine && !transform.parent.GetComponent<SpawnPoint>().shrineIsActive)
            {
                yield break;
            }

            if (childLight.intensity != normalIntensity)
            {
                childLight.intensity = Mathf.Lerp(childLight.intensity, normalIntensity, 0.1f);
            }

            timePassed += Time.deltaTime;
            yield return null;
        }
        //fadeInIsDone = true;

    }
}
