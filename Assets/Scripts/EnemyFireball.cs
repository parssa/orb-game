﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class EnemyFireball : MonoBehaviour, IPooledObject
{

	private Vector2 playerLocation;
    private Vector2 fireballLocation;
	private Transform playerTransform;
	public ContactFilter2D filter;
    private float speed = 6f;

    private SpriteRenderer fireballSprite;
    private Color startingColor;
    //private float timeSinceMade = 0f;

    private float timeTillBreak = 2f;
    public bool isGuardianBlast = false;
    public bool isBossBlast = false;
	private BoxCollider2D hitbox;
	private Collider2D[] hits = new Collider2D[10];

    private Vector3 moveDelta;
    private Vector3 startPos;
    protected RaycastHit2D hit;

    public Light2D projLight;
    private float regProjLightIntensity = 1.06f;
    private float flashIntensity = 1.4f;
    
    public Gradient regGradient1;
    public Gradient regGradient2;
    [Space]
    public Gradient enragedGradient1;
    public Gradient enragedGradient2;

    private Gradient thisGradient;



    public enum FireballType
    {
        REG, 
        ENRAGED,
        GUARDIANBLAST,
        GUARDIANSHOT,
    } 
    public FireballType fireballType;

    private EnemyFireballHitBox thisFireballHitBox;

    public ParticleSystem pSystem;

    public float timeMade;


    public void OnObjectSpawn()
	{
		
		playerTransform = GameManager.instance.player.transform;
		playerLocation = playerTransform.position;
        fireballLocation = transform.position;
        startPos = transform.position; // Recently added
        thisFireballHitBox = transform.GetChild(0).GetComponent<EnemyFireballHitBox>();

        timeMade = Time.time;
        
        hitbox = transform.GetChild(0).GetComponent<BoxCollider2D>();

       

        if (!isGuardianBlast) transform.localScale = new Vector2(2f, 2f);

        fireballSprite = GetComponent<SpriteRenderer>();
        startingColor = fireballSprite.color;

        if (isGuardianBlast)
        {
            speed = 6;
            flashIntensity = 0.5f;
            timeTillBreak = 5f;
        }

        if (fireballType == FireballType.GUARDIANSHOT)
        {
            speed = Random.Range(6.1f, 8.5f);
        }
        thisGradient = regGradient2;
        var main = pSystem.main;
        if (fireballType == FireballType.REG)
        {
            main.startColor = new ParticleSystem.MinMaxGradient(regGradient1, regGradient2);
            thisGradient = regGradient2;
            speed = 5f;
        }
        else if (fireballType == FireballType.ENRAGED)
        {
            
            main.startColor = new ParticleSystem.MinMaxGradient(enragedGradient1, enragedGradient2);
            thisGradient = enragedGradient2;
            speed = Random.Range(6.1f, 8.5f);

        }


        if (isBossBlast)
        {
            speed += 0.6f;
        }
        
        


        // Erase array on respawn;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            hits[i] = null;
        }

        transform.GetChild(0).GetComponent<EnemyFireballHitBox>().ResetHitbox();
    }

	private void Update()
    {
        
        moveDelta = (transform.position - startPos).normalized;
        

        // hit = Physics2D.BoxCast(transform.position, hitbox.size, 0, new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y * Time.deltaTime * speed), LayerMask.GetMask("Blocking"));
        // if (hit.collider != null)
        // {
           
        //     BreakArrow();
            
        // }

        // // Check x axis
        // hit = Physics2D.BoxCast(transform.position, hitbox.size, 0, new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x * Time.deltaTime * speed), LayerMask.GetMask("Blocking"));
        // if (hit.collider != null)
        // {
            
        //     BreakArrow();
        // }
        transform.Translate(Vector3.right * (Time.deltaTime * speed));
        if (!isGuardianBlast) transform.localScale = Vector2.Lerp(transform.localScale, new Vector2(3.2f, 3.2f), Time.deltaTime * speed);
        
        // Check for overlaps
     
        // hitbox.OverlapCollider(filter, hits);
        // for (int i = 0; i < hits.Length; i++)
        // {
        //     if (hits[i] == null)
        //         continue;
 
        //     // Cleans array
        //     hits[i] = null;
        // }

        //timeSinceMade += Time.deltaTime;
        

        if (Time.time - timeMade > 0.1f && Time.time - timeMade <= 0.3f) projLight.intensity = flashIntensity;
        else projLight.intensity = regProjLightIntensity;

        if (Time.time- timeMade > 0.5f) speed  = 4f;

       
        
        
        if (fireballType != FireballType.GUARDIANBLAST)
        {
            fireballSprite.color = thisGradient.Evaluate((Time.time - timeMade) / timeTillBreak);
            projLight.color = thisGradient.Evaluate((Time.time - timeMade) / timeTillBreak);
        }
       

        if (Time.time - timeMade >= timeTillBreak) BreakArrow();

    }

    // protected virtual void OnCollide(Collider2D coll)
    // {
        
    //     if (coll.CompareTag("Player") || coll.CompareTag("Laser"))
    //     {
            
           
            
    //         //Destroy(gameObject);

    //         BreakArrow();
            
    //     }

    // }

    public void BreakArrow()
    {
        //GameObject particleEffect = Instantiate(Resources.Load("OnProjectileBreak") as GameObject, transform.position, transform.rotation);
        //Destroy(particleEffect, 0.3f);
        ObjectPooler.instance.SpawnFromPool("projBreakP", transform.position, transform.rotation);
        timeMade = Time.time;
        transform.gameObject.SetActive(false);
        //Destroy(gameObject);
    }
}
