﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;

    public enum EnemyEnum
    {
        SKULL_REG,
        SKULL_ORB_TARGET,
        SLUGGER,
        TURBO_KING,
        RANGED_REG,
        SKULL_RED,
        RANGED_ENRAGED,
        GUARDIAN,
        BOSS
    };

    [System.Serializable]
    public class EnemyBlueprint
    {
        public string _name; // Name of enemy
        public EnemyEnum enemyEnum; // Type of enemy
                                    //public bool orbTargetter = false; // Does it target the orb?
                                    // public Transform target; // Transform of it's target (Player / Orb)
                                    // public int hitpoint; // hitpoint
        public int maxHitpoint; // maxHitPoint

        public float regularSpeed; // normalSpeed
        public int damage; // Base Damage it deals
        public int pushForce;
        [Range(0f, 1f)]
        public float recoverySpeed;
        public Vector3 enemyScale;

        // public int indexInTargettingArray; // Index for retrieving objects index in array
    }

    public EnemyBlueprint[] blueprints;
    public GameObject enemyCorpse;

    void Awake()
    {
        instance = this;
    }

    public void YolkEnemies(int healthRaise, int damageBuff, float speedBoost)
    {
        
        foreach (EnemyBlueprint _e in blueprints)
        {
            _e.maxHitpoint += healthRaise;
            _e.damage += damageBuff;
            _e.regularSpeed += speedBoost;
        }
    }


    public EnemyBlueprint FetchValues(EnemyEnum thisEnum)
    {
        switch (thisEnum)
        {
            case EnemyEnum.SKULL_REG:

                return blueprints[0];

            case EnemyEnum.SKULL_ORB_TARGET:

                return blueprints[1];

            case EnemyEnum.SLUGGER:

                return blueprints[2];

            case EnemyEnum.TURBO_KING:

                return blueprints[3];

            case EnemyEnum.RANGED_REG:

                return blueprints[4];

            case EnemyEnum.SKULL_RED:
                // EnemyBlueprint redBlueprint = blueprints[5];
                // float randomSizeBoost = Random.Range(0.1f, 0.4f);
                // redBlueprint.enemyScale = new Vector3(redBlueprint.enemyScale.x + randomSizeBoost, redBlueprint.enemyScale.y + randomSizeBoost, redBlueprint.enemyScale.z);
                return blueprints[5];

            case EnemyEnum.RANGED_ENRAGED:

                return blueprints[6];

            case EnemyEnum.GUARDIAN:
            
                return blueprints[7];

            case EnemyEnum.BOSS:

                return blueprints[8];

            default:

                return blueprints[0];

        }
    }

}
