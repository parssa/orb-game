﻿using System.Collections;
using System.Collections.Generic;
using CloudOnce;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Guardian : Enemy
{
    [Header("Guardian Values")]
    public bool activated = false;
    private SpriteRenderer spriteRenderer;

    //private bool canMakeDecision = false;
    private float timeSinceDecisionMade = 0.0f;
    private float DECISION_MAKING_TIME = 5f;
    private bool secondPhase = false;
    public bool unleashingFire = false;

    private const float REG_INTESITY = 1.15f;

    float lastShotTime = 0.0f;
    float SHOT_DELAY_INTERVAL = 0.3f;
    public int numShots = 0;
    
    
    public int num_shot_till_decide = 10;

    public GameObject lightToActivateOnDeath;


    float shotAdjusterOne = -5f;
    float shotAdjusterTwo = 5f;
    float absAdjustment = 0;

    public enum GuardianState
    {
        IDLE,
        FLY_UP,
        DASHING,
        SHOOING,
        CHARGING_UP,
        DECIDE
    }


    public GuardianState currState;
    public GameObject shadowObj;
    public GameObject firstFirePoint;
    public GameObject bigBlasts;
    public Sprite activatedSprite;

    private ObjectPooler objectPool;

    public bool startedLoadingUpChargeBlast = false;
    public PerkManager.Perk.PerkType guardianOfThisPerk;

    private Color32 iconColor;

    private int lastChoiceNum = 99;


    public int numTimesHit = 0;
    protected override void Start()
    {
        base.Start();
        spriteRenderer = GetComponent<SpriteRenderer>();
        numShots = 0;
        currState = GuardianState.IDLE;
        iconColor = new Color32(250, 110, 0, 255);
        objectPool = ObjectPooler.instance;

        shadowObj.SetActive(false);
        //  boxCollider = GetComponent<BoxCollider2D>();
        //anim = GetComponent<>

    }

    void Update()
    {

        boxCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;
            OnCollide(hits[i]);
            // Cleans Array
            hits[i] = null;
        }
        if (activated)
        {
            StateManagement();
            GetComponent<Obstacle>().standardSprite = spriteRenderer.sprite;
        }

        if (hitpoint < (maxHitpoint/2) && !secondPhase) SecondPhase();

        if (secondPhase) transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(7, 7, 1), Time.deltaTime);
        if (isDashing)
        {
            alertSymbol.color = iconColor;
            if (alertSymbolLight.intensity >= 2f) alertSymbolLight.intensity = 1.5f;
            alertSymbolLight.intensity += 0.1f;
        }


    }

    void SecondPhase()
    {
        //transform.localScale = new Vector3(10, 10, 1);
        anim.speed = 1.4f;
        SHOT_DELAY_INTERVAL = 0.2f;
        DECISION_MAKING_TIME = 3.3f;
        secondPhase = true;
        regularSpeed += 1.5f;
        hitpoint = maxHitpoint;
        
    }

    void StateManagement()
    {
        switch (currState)
        {
            case GuardianState.FLY_UP:
                anim.SetTrigger("activated");
                FlyUpState();
                break;

            case GuardianState.DECIDE:
                anim.SetTrigger("deciding");
                DecideState();
                break;

            case GuardianState.CHARGING_UP:
                ChargeUpState();
                break;

            case GuardianState.SHOOING:
                anim.SetTrigger("shooting");
                ShootingState();
                break;

            case GuardianState.DASHING:
                DashingState();
                break;

            default:
                DecideState();
                break;
        }
    }

    protected override void RecieveDamage(Damage dmg)
    {
        if (!activated) return;
        
        base.RecieveDamage(dmg);
        numTimesHit += 1;
    }

    //     GameObject particleEffect = Instantiate(Resources.Load("RedEnemyHitParticle") as GameObject, transform.position, transform.rotation);
    //     Destroy(particleEffect, 0.3f);

    //     base.RecieveDamage(dmg);

    // }

    protected override void OnCollide(Collider2D coll)
    {
        if (activated && coll.CompareTag("Player") && currState != GuardianState.FLY_UP)
        {
            spriteRenderer.sprite = activatedSprite;
            Damage dmg = new Damage
            {
                damageAmount = Random.Range(damage, damage + 5),
                origin = transform.position,
                pushForce = pushForce
            };

           

            StartCoroutine(cameraShake.Shake(0.1f, 1f));
            coll.SendMessage("RecieveDamage", dmg);
            Vector2 direction = new Vector2(transform.position.x - playerTransform.position.x, transform.position.y - playerTransform.position.y).normalized * dmg.damageAmount;
            Vector2 directionPlusPosition = new Vector2(direction.x + transform.position.x, transform.position.y + direction.y);
            transform.position = Vector2.MoveTowards(transform.position, directionPlusPosition, 0.3f);
            
            
            if (currState == GuardianState.DASHING)
            {
                Debug.Log("Stopping the dash");
                StopCoroutine(ReturnToDeciding());
                StartCoroutine(ReturnToDeciding());
            }
        }


    }

    public void ActivateGuardian(PerkManager.Perk.PerkType perkGuardianGrants)
    {
        Debug.Log("Activated Guardian!");
        GetComponent<Obstacle>().enabled = false;

        SoundManager.instance.PlayOnce("orbMode");

        spriteRenderer.sortingLayerName = "Weapon";
        spriteRenderer.sortingOrder = 12;
        spriteRenderer.sprite = activatedSprite;
        currState = GuardianState.FLY_UP;
        activated = true;
        guardianOfThisPerk = perkGuardianGrants;
    }

    void IdleState()
    {

    }

    void FlyUpState()
    {
        // alertSymbolLight.intensity = Mathf.Lerp(1f, 0.2f, Time.deltaTime*3);
        
        alertSymbol.transform.gameObject.SetActive(true);
        alertSymbolLight.intensity = 1f;
        alertSymbol.color = iconColor;

    }

    void DecideState()
    {
        if (alertSymbol.color != Color.clear && !isDashing)
        {
            alertSymbol.color = Color.clear;
            alertSymbolLight.intensity = 0f;
        }

        if (numTimesHit >= 30)
        {
            Debug.Log("Getting Abused!");
            anim.speed = 1.6f;
            numTimesHit = 0;
            timeSinceDecisionMade = Time.time;
            currState = GuardianState.DASHING;
            lastChoiceNum = (int)GuardianState.DASHING;
        }

        if (Time.time - timeSinceDecisionMade > DECISION_MAKING_TIME)
        {
            if (secondPhase)
            {
                anim.speed = 1.4f;
                timeSinceDecisionMade = Time.time;
                int randIndexChosen = Random.Range(2, 5);
                if (randIndexChosen == lastChoiceNum)  randIndexChosen = Random.Range(2, 5);
                currState = (GuardianState)randIndexChosen;
                lastChoiceNum = randIndexChosen;
            }
            else
            {
                anim.speed = 1f;
                timeSinceDecisionMade = Time.time;
                int randIndexChosen = Random.Range(2, 4);
                if (randIndexChosen == lastChoiceNum)  randIndexChosen = Random.Range(2, 4);
                currState = (GuardianState)randIndexChosen;
                lastChoiceNum = randIndexChosen;
            }
        }
    }

    void ChargeUpState()
    {
        alertSymbol.transform.gameObject.SetActive(true);
        if (!startedLoadingUpChargeBlast) StartCoroutine(ChargeUp());
        currState = GuardianState.DECIDE;
    }

    void DashingState()
    {
        alertSymbol.transform.gameObject.SetActive(true);

        if (!startedChargingUp) StartCoroutine(ChargeUpIntoDash());

    }

    void ShootingState()
    {

        alertSymbol.transform.gameObject.SetActive(true);
        // StartFiring();
        if (!unleashingFire) StartCoroutine(UnleashFire());
    }

   

    // void StartFiring()
    // {
    //     if (numShots > num_shot_till_decide)
    //     {
    //         currState = GuardianState.DECIDE;
    //         numShots = 0;
    //         return;
    //     }

    //     //StartCoroutine(ShootingCorout());
    //     if (Time.time - lastShotTime >= SHOT_DELAY_INTERVAL)
    //     {

    //         // shoot player
    //         lastShotTime = Time.time;
    //         Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
    //         Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
    //         Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

    //        // GameObject enemyProj = Instantiate(projectile, firstFirePoint.transform.position, movementRotation);
    //         GameObject enemyProj = objectPool.SpawnFromPool("enemyFireball", firstFirePoint.transform.position, movementRotation);
    //         enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANSHOT;
    //         numShots += 1;
    //         Debug.Log("Shot "+ numShots);
    //         //enemyProj.transform.localScale = new Vector3(3, 3, 1);


    //     }

    //     alertSymbolLight.intensity = 1f;
    //     alertSymbol.color = iconColor;
    // }


    IEnumerator ChargeUp()
    {

        startedLoadingUpChargeBlast = true;
        // redskullLight.intensity = Mathf.Lerp(2.84f, 10f, Mathf.PingPong(Time.time, 0.5f));
        alertSymbolLight.intensity = 0.88f;
        float timeElapsed = 0.0f;
        alertSymbol.color = iconColor;
        while (timeElapsed < 1f)
        {
            // if (PauseMenu.GameIsPaused)
            // {
            //     startedLoadingUpChargeBlast = true;
            //     currState = Guardian.GuardianState.DECIDE;
            //     yield break;
            // }

            alertSymbolLight.intensity += 0.1f;
            redskullLight.intensity = Mathf.Lerp(1.14f, 2f, Mathf.PingPong(Time.time, 0.5f));
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        startedLoadingUpChargeBlast = false;
        redskullLight.intensity = REG_INTESITY;
        alertSymbolLight.intensity = 0f;
        alertSymbol.color = Color.clear;
        FireChargeUp(0);
        FireChargeUp(5);
        FireChargeUp(-5);
        currState = Guardian.GuardianState.DECIDE;
    }


    void FireChargeUp(int z)
    {
        Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
        Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);
        //GameObject shot = Instantiate(bigBlasts, firstFirePoint.transform.position, movementRotation);
        GameObject shot = objectPool.SpawnFromPool("guardianBlast", firstFirePoint.transform.position, movementRotation);
        shot.transform.rotation = shot.transform.rotation * Quaternion.Euler(0, 0, z);
        //shot.GetComponent<EnemyFireball>().isGuardianBlast = true;
        //shot.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANBLAST;
    }

    IEnumerator ChargeUpIntoDash()
    {
        startedChargingUp = true;
        float timeElapsed = 0.0f;
        alertSymbolLight.intensity = 0.88f;

        alertSymbol.color = iconColor;

        while (timeElapsed < 1f)
        {
            if (alertSymbolLight.intensity >= 1.72f) alertSymbolLight.intensity = 1.5f;
            alertSymbolLight.intensity += 0.1f;
            timeElapsed += Time.deltaTime;

            yield return null;

        }


        float angleOfDash = Mathf.Atan2(playerTransform.position.y - transform.position.y, playerTransform.position.x - transform.position.x) * 180 / Mathf.PI;
        Quaternion dashDirToEuler = Quaternion.Euler(0, 0, angleOfDash);
        Vector2 dashDirection = dashDirToEuler * Vector2.right;



        StartCoroutine(DashMode(dashDirection));

        // if (!fellOutOfRange)
        // {
        // 	Debug.Log("About To Dash");
        // 	HandleDash(dashDirection);

        // }
        alertSymbolLight.intensity = 0f;
        alertSymbol.color = Color.clear;
        startedChargingUp = false;

    }

    IEnumerator DashMode(Vector2 dashDirection)
    {
        float timePassed = 0;
        CreateDashShadow(0.1f, dashDirection.x);
        Vector2 startingPos = transform.position;
        isDashing = true;
        timeSinceLastDash = Time.time;



        float normalPlayerSpeed = regularSpeed;
        regularSpeed = normalPlayerSpeed;

        dashDistance = 4f;
        while (Vector2.Distance(startingPos, transform.position) < dashDistance && !PauseMenu.GameIsPaused)
        {

            if (Vector2.Distance(startingPos, transform.position) >= dashDistance)
            {

                isDashing = false;

                yield break;
            }

            if (PauseMenu.GameIsPaused)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (isDashing == false)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            playerSpeed = 1f;
            //lastImmune = Time.time;

            //CreateDashShadow(0.3f, dashDirection.x);
            CreateDashShadow(0.1f, dashDirection.x);
            CreateDashShadow(0.1f, dashDirection.x);
            HandleDash(dashDirection);
            dashIncr += 0.1f;

            timePassed += Time.deltaTime;

            yield return null;
        }

        dashIncr = 0.3f;
        CreateDashShadow(0.1f, dashDirection.x);
        isDashing = false;
        regularSpeed = normalPlayerSpeed;
        playerSpeed = regularSpeed;
        StopCoroutine(ReturnToDeciding());
        StartCoroutine(ReturnToDeciding());


        void CreateDashShadow(float lifeSpan, float dashDir)
        {
            if (dashDir < 0)
            {

                Quaternion flippedTransformrotation = Quaternion.Euler(0, 180, 0);
                GameObject currDash = Instantiate(dashAnimObj, transform.position, flippedTransformrotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);


                Destroy(currDash, lifeSpan);
            }
            else
            {

                GameObject currDash = Instantiate(dashAnimObj, transform.position, transform.rotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);
                Destroy(currDash, lifeSpan);
            }

        }
    }

    void CreateGuardianProjectile(float adjuster)
    {


        Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90 + adjuster) * relativePos;
        Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

        // GameObject enemyProj = Instantiate(projectile, firstFirePoint.transform.position, movementRotation);
        GameObject enemyProj = objectPool.SpawnFromPool("guardianFireball", firstFirePoint.transform.position, movementRotation);
        enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANSHOT;
    }

    IEnumerator UnleashFire()
    {
        numShots = 0;
        unleashingFire = true;

        alertSymbolLight.intensity = 1f;
        alertSymbol.color = iconColor;

        while (numShots < num_shot_till_decide )
        {

            if (numShots > num_shot_till_decide)
            {
                currState = Guardian.GuardianState.DECIDE;
                unleashingFire = false;
                Debug.Log("thats enough");
                yield break;


            }
            if (Time.time - lastShotTime >= SHOT_DELAY_INTERVAL)
            {

                // shoot player
                lastShotTime = Time.time;
                // Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
                // Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
                // Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

                // // GameObject enemyProj = Instantiate(projectile, firstFirePoint.transform.position, movementRotation);
                // GameObject enemyProj = objectPool.SpawnFromPool("guardianFireball", firstFirePoint.transform.position, movementRotation);
                // enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANSHOT;

                absAdjustment = Mathf.PingPong(Time.time, 5f);

                CreateGuardianProjectile( - 5f - (absAdjustment * -1));
                CreateGuardianProjectile(5f + absAdjustment);
                
                // if (shotAdjusterOne + 1f <= 0) shotAdjusterOne += 1f;
                // else if (shotAdjusterOne - 1f >= -6) shotAdjusterOne -= 1;
                Debug.Log("1: " + shotAdjusterOne);

                // if (shotAdjusterTwo - 1f >= 0) shotAdjusterTwo -= 1f;
                // else if (shotAdjusterTwo + 1f <= 6) shotAdjusterTwo += 1f;

                

                Debug.Log("2: " + shotAdjusterTwo);
                numShots += 1;
               
                //enemyProj.transform.localScale = new Vector3(3, 3, 1);
            }

            yield return null;
        }
        unleashingFire = false;     
        Debug.Log("Hit this");
        currState = Guardian.GuardianState.DECIDE;
        //StartCoroutine(ShootingCorout());
        // if (Time.time - lastShotTime >= SHOT_DELAY_INTERVAL)
        // {

        //     // shoot player
        //     lastShotTime = Time.time;
        //     Vector3 relativePos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, 0) - transform.position;
        //     Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
        //     Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

        //     // GameObject enemyProj = Instantiate(projectile, firstFirePoint.transform.position, movementRotation);
        //     GameObject enemyProj = objectPool.SpawnFromPool("enemyFireball", firstFirePoint.transform.position, movementRotation);
        //     enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.GUARDIANSHOT;
        //     numShots += 1;
        //     Debug.Log("Shot " + numShots);
        //     //enemyProj.transform.localScale = new Vector3(3, 3, 1);


        // }

        // alertSymbolLight.intensity = 1f;
        // alertSymbol.color = iconColor;
    }

    IEnumerator ReturnToDeciding()
    {
        float timeElapsed = 0.0f;
        while (timeElapsed < 1f)
        {
            if (PauseMenu.GameIsPaused)
            {
                currState = Guardian.GuardianState.DECIDE;
                yield break;
            }


            timeElapsed += Time.deltaTime;
            yield return null;
        }
        currState = Guardian.GuardianState.DECIDE;
        numShots = 0;

    }

    protected override void Death()
    {
        isDead = true;
        // Instantiate(Resources.Load("OnDeathExplosion") as GameObject, transform.position, transform.rotation);

        GameObject deathExplosion = objectPool.SpawnFromPool("OnGuardianDeath", transform.position, transform.rotation);
        
        StartCoroutine(cameraShake.Shake(0.15f, 2f));

        SoundManager.instance.PlayOnce("explode1");
        GameManager.instance.GrantPoints(1000);

        MakeSoul();
        MakeSoul();

        MakeSoul();
        MakeSoul();

        MakeSoul();
        MakeSoul();

        MakeSoul();
        MakeSoul();


        if (!Achievements.GuardianSlayer.IsUnlocked)
        {
            Achievements.GuardianSlayer.Unlock();
        }
        
        
        PerkManager.instance.GrantPerkToPlayer(guardianOfThisPerk);
        lightToActivateOnDeath.transform.parent.GetComponent<PerkStatue>().GuardianKilled();
        Destroy(gameObject);
    }

    void MakeSoul()
    {
        Vector3 randPos = new Vector3(Random.Range(-0.3f, 0.3f) + transform.position.x, Random.Range(-0.3f, 0.3f) + transform.position.y, 1);
        GameObject soul = objectPool.SpawnFromPool("soul", randPos, transform.rotation);
        Vector2 randShootOutDir = new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f));
        soul.GetComponent<FlyToPlayer>().shootOutDir = randShootOutDir;
        soul.GetComponent<FlyToPlayer>().cameFromShrine = true;
    }
}
