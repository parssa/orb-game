﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterMenu : MonoBehaviour
{

    public static CharacterMenu instance;
    private Player player;
    private GameManager gameManager;
    private SoundManager soundManager;

    public PauseMenu pauseMenu;
    private ControlsHandler controlsHandler;
    private HeaderPopup headerPopup;
    private GameObject headerPopupObj;
    public DialogueManager dialogueManager;

    public GameObject shopVolume;
    
    private HubLayout hubLayout;

    public Animator shop_animator;
    public Animator dialogue_animator;
    public bool shopIsActive = false;
    public bool shopHasBeenOpened = false;
    public bool hoveringOverButton = false;
    public bool maxBowAchieved = false;
    [Header("Tab Headers Buttons")]
    // Headers outside of the tabs, setactive == false when weapon page is open due to tiering popups
    public GameObject statsHeaderUniversal;
    public GameObject weaponHeaderUniversal;
    public GameObject orbHeaderUniversal;
    [Space]
    // Gameobjects grouping the buttons on each tab
    public GameObject playerTabButtons;
    public GameObject weaponTabButtons;
    public GameObject orbTabButtonsButtons;


    [Header("Player Tab")]
    public float maxHealthNum;
    public float maxStaminaNum;
    public float maxRegenNum;
    public float maxDashNum;
    public int indexOfSmallest = 0;

    public Color32 healthUpgradeColor;
    public Color32 staminaUpgradeColor;
    public Color32 dashPlayerUpgradeColor;
    public Color32 regenUpgradeColor;

    public TextMeshProUGUI playerClassHeader;
    public TextMeshProUGUI playerClassText;

    public TextMeshProUGUI playerSkillUpgradeHeader;
    public TextMeshProUGUI playerSkillSpecifcText;

    public GameObject scrollView;
    public Animator eyesUIAnim;
    private EyeControl eyeControl;
    public Image eyesImage;
    public Image bandanaImage;

    [Space]
    public GameObject healthGold;
    public GameObject staminaGold;
    public GameObject dashPowrGold;
    public GameObject regenGold;

    [Header("Perk Slots")]
    public GameObject perkSlotOne;
    public GameObject perkSlotTwo;
    public GameObject perkSlotThree;
    public GameObject perkSlotFour;

    [Header("Health Upgrade")]
    public TextMeshProUGUI maxHealth;
    public RectTransform maxHealthR;
    // public RectTransform maxHealthAfterUpgradeR;
    public TextMeshProUGUI newHealth;
    public TextMeshProUGUI healthFee;

    public int maximumHealthUpgrade;

    [Header("Stamina Upgrade")]
    public TextMeshProUGUI maxStamina;
    public RectTransform maxStaminaR;
    //public RectTransform maxStaminaAfterUpgradeR;
    public TextMeshProUGUI newStamina;
    public TextMeshProUGUI staminaFee;
    public int maximumStaminaUpgrade;

    [Header("Stamina Regen Upgrade")]
    public TextMeshProUGUI maxStaminaRegen;
    public RectTransform maxStaminaRegenR;
    //public RectTransform maxStaminaRegenAfterUpgradeR;
    public TextMeshProUGUI newStaminaRegen;
    public TextMeshProUGUI staminaRegenFee;
    public int maximumStaminaRegenUpgrade;


    [Header("Dash Upgrade")]
    public TextMeshProUGUI maxDash;
    public RectTransform maxDashR;
    //public RectTransform maxDashAfterUpgradeR;
    public TextMeshProUGUI newDash;
    public TextMeshProUGUI dashFee;
    public int maximumDashUpgrade;

    [Header("Weapon (Current)")]
#pragma warning disable 0649
    [SerializeField] private BowManager bm;
#pragma warning restore 0649
    public Image currWeaponSprite;
    public TextMeshProUGUI bowName;
    public TextMeshProUGUI bowDescription;
    public TextMeshProUGUI bowHeaderClass;
    public TextMeshProUGUI bowHeaderChargeUp;
    // public TextMeshProUGUI currWeaponDamage;
    // public TextMeshProUGUI currWeaponCritDamage;
    // public TextMeshProUGUI currWeaponDashDamage;
    // public TextMeshProUGUI currWeaponRpm;
    public TextMeshProUGUI classType;
    public TextMeshProUGUI chargeUpDescription;
    private Color lightBowTextColor;
    private Color darkBowTextColor;

    [Space]
    public GameObject arrDamageGold;
    public GameObject dashDamageGold;
    public GameObject chargeDamageGold;
    public GameObject critDamageGold;
    public GameObject weaponTierGold;

    [Header("Weapon (Button)")]
    public TextMeshProUGUI weaponFee;


    [Header("Weapon Choice Section")]
    public bool canClickWeaponUpgrade = true;

    public DialogueTrigger choiceOneDialogueTrigger;
    public DialogueTrigger choiceTwoDialogueTrigger;

    private int selectedWeapon = 3;
    public Animator weaponChoice;
    public GameObject choiceOne;
    public GameObject choiceTwo;
    public Image choiceOneImage;
    public TextMeshProUGUI choiceOneName;
    public Image choiceTwoImage;
    public TextMeshProUGUI choiceTwoName;
    public Image weaponPanelCover;
    public int leftOptionNum;
    public int rightOptionNum;
    public bool showCover;
    private float timeSinceTiered;

    //private bool hoveringOverLeftOption = false;
    //private bool hoveringOverRightOption = false;

    [Header("Weapon Upgrades")]
    public RectTransform arrowDamage;
    public RectTransform arrowDamageAfterUpgrade;
    public TextMeshProUGUI arrowDamagefee;
    [Space]
    public RectTransform dashDamage;
    public TextMeshProUGUI dashDamageFee;
    [Space]
    public RectTransform chargeDamage;
    public TextMeshProUGUI chargeDamageFee;
    [Space]
    public RectTransform critMultBar;
    public TextMeshProUGUI critMultFee;
    [Space] 
    public RectTransform chargeCooldownBar;
    public TextMeshProUGUI chargeCooldownFee;
    public GameObject chargeCooldownGold;
    [Header("Tabs")]
    public GameObject statsPanel;
    public GameObject weaponsPanel;
    public GameObject orbPanel;

    private int timesUpgradedWeapon;


    [Header("Orb Menu")]
    private Orb orb;
    private OrbHitBox orbhb;
    public Image orbImage;
    public TextMeshProUGUI orbLevel;
    public TextMeshProUGUI orbHealth;
    public TextMeshProUGUI turretDamage;
    public TextMeshProUGUI orbUpgradeCost;
    public Animator orbEyeAnim;
    public GameObject orbUpgradeGold;


    public GameObject blessingataltarT;
    public GameObject protectatallcostsT;

    public TextMeshProUGUI orbOnUpgradeTextOne;
    public TextMeshProUGUI orbOnUpgradeTextTwo;
    public TextMeshProUGUI orbOnUpgradeTextThree;
    public TextMeshProUGUI orbOnUpgradeTextFour;

    public Image[] blessingImages;
    public GameObject[] availableSlots;

    [Header("Orb Sprites")]
    public Sprite orbLev1;
    public Sprite orbLev2;
    public Sprite orbLev3;
    public Sprite orbLev4;


    [Header("Blessing Choices")]
    public GameObject blessingsObj;

    public GameObject blessingConfirmObj;
    public bool BLESSINGS_SHOWING = false;
    public Animator blessingAnimator;

    public Image blessingIconOne;
    public Image blessingIconTwo;
    public Image blessingIconThree;

    private int selectedBlessing = 4;
    // private float blessingChoiceChanged = 0.0f;
    private bool canActivateBlessing = true;

    public bool BLESSING_BANNER_SHOULD_VISIBLE = false;


    private Color32 bannerDefaultColorTitle;


    //  public PerkManager.Blessing[] currBlessings = new PerkManager.Blessing[3];




    private int currentCharacterSelection = 0;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {

        // weapon
        //hubLayout = HubLayout.instance;

        bannerDefaultColorTitle = new Color32(246, 235, 225, 255);

        timesUpgradedWeapon = 0;
        timeSinceTiered = 0f;

        player = Player.instance;
        gameManager = GameManager.instance;

        eyeControl = EyeControl.instance;
        controlsHandler = ControlsHandler.instance;
        headerPopup = HeaderPopup.instance;
        headerPopupObj = headerPopup.transform.gameObject;
        hubLayout = HubLayout.instance;
        soundManager = SoundManager.instance;

        healthGold.SetActive(false);
        staminaGold.SetActive(false);

        dashPowrGold.SetActive(false);
        regenGold.SetActive(false);


        arrDamageGold.SetActive(false);
        dashDamageGold.SetActive(false);
        chargeDamageGold.SetActive(false);
        critDamageGold.SetActive(false);
        weaponTierGold.SetActive(false);
        chargeCooldownGold.SetActive(false);

        playerTabButtons.SetActive(true);
        weaponTabButtons.SetActive(false);
        orbTabButtonsButtons.SetActive(false);

        lightBowTextColor = bm.currBow.lightTextColor;
        darkBowTextColor = bm.currBow.darkTextColor;

        bowName.text = bm.currBow.bowName;
        bowDescription.text = bm.currBow.description;
        bowDescription.color = darkBowTextColor;

        bowHeaderChargeUp.color = darkBowTextColor;
        bowHeaderClass.color = darkBowTextColor;

        playerClassHeader.color = darkBowTextColor;
        playerClassText.color = lightBowTextColor;
        playerClassText.SetText(bm.currBow.weaponClass.ToString());


        chargeUpDescription.color = lightBowTextColor;
        classType.color = lightBowTextColor;

        currWeaponSprite.sprite = bm.currBow.bowImage;

        weaponPanelCover.color = Color.clear;
        showCover = false;

        chargeUpDescription.SetText(bm.currBow.chargeUpDescription);
        weaponFee.text = bm.bowPrices[bm.weaponIndex].ToString();

        // health
        maxHealth.text = gameManager.player.GetMaxHealth().ToString();
        newHealth.text = (player.maxHitpoint + player.UpgradeHealthNum()).ToString();
        healthFee.text = player.UpgradeHealthCost().ToString();
        maximumHealthUpgrade = 5;

        // stamina
        maxStamina.text = player.maxStamina.ToString();
        newStamina.text = (player.maxStamina + player.UpgradeStaminaNum()).ToString();
        staminaFee.text = player.UpgradeStaminaCost().ToString();

        maximumStaminaUpgrade = 5;

        // stamina regen
        maxStaminaRegen.text = player.staminaRegenNum.ToString();
        newStaminaRegen.text = (player.staminaRegenNum + player.UpgradeStaminaRegenNum()).ToString();
        staminaRegenFee.text = player.UpgradeStaminaRegenCost().ToString();
        maximumStaminaRegenUpgrade = 3;

        // dash
        maxDash.text = player.dashNum.ToString();
        newDash.text = "1";
        dashFee.text = player.UpgradeDashCost().ToString();

        maximumDashUpgrade = 2;

        orb = gameManager.orb;
        orbhb = OrbHitBox.instance;
        orbLevel.SetText((orb.orbLevel + 1).ToString());
        orbHealth.SetText(orbhb.maxHitpoint.ToString());
        turretDamage.SetText(gameManager.turretDamage.ToString());
        // orbUpgradeCost.SetText(orb.UpgradeCost().ToString());


        arrowDamagefee.text = bm.upgrades[0].upgradeCosts[bm.upgrades[0].numUpgraded].ToString();
        dashDamageFee.text = bm.upgrades[1].upgradeCosts[bm.upgrades[1].numUpgraded].ToString();
        chargeDamageFee.text = bm.upgrades[2].upgradeCosts[bm.upgrades[2].numUpgraded].ToString();
        critMultFee.text = bm.upgrades[3].upgradeCosts[bm.upgrades[3].numUpgraded].ToString();
        chargeCooldownFee.SetText(bm.upgrades[4].upgradeCosts[bm.upgrades[4].numUpgraded].ToString());
        foreach (Image _i in blessingImages)
        {
            _i.color = Color.clear;
        }

        foreach (GameObject _g in availableSlots)
        {
            _g.SetActive(false);
        }

        PlayerSkillToUpgrade();
        FormatAllOrbUpgradeTexts();
        BlessingAtAltar(false);
        orbUpgradeGold.SetActive(false);
        blessingsObj.SetActive(false);
        shopVolume.SetActive(false);



    }

    void FormatAllOrbUpgradeTexts()
    {

        orbLevel.SetText((orb.orbLevel + 1).ToString());

        OrbUpgradeTextFormat(orbOnUpgradeTextOne, 0);
        OrbUpgradeTextFormat(orbOnUpgradeTextTwo, 1);
        OrbUpgradeTextFormat(orbOnUpgradeTextThree, 2);
        OrbUpgradeTextFormat(orbOnUpgradeTextFour, 3);

        for (int a = 0; a < PerkManager.instance.numBlessingSlotsAvailable; a++)
        {
            if (blessingImages[a].color == Color.clear) availableSlots[a].SetActive(true);
        }


        if (orb.orbLevel >= orb.orbUpgrades.Length)
        {
            orbUpgradeCost.SetText("MAX  ");
            orbUpgradeGold.SetActive(true);
        }
        else orbUpgradeCost.SetText(orb.UpgradeCost().ToString());

    }

    void OrbUpgradeTextFormat(TextMeshProUGUI textSlot, int index)
    {
        if (orb.orbLevel >= orb.orbUpgrades.Length)
        {
            textSlot.SetText("max tier orb");
            textSlot.color = lightBowTextColor;
            orbUpgradeCost.SetText("MAX  ");
            return;
        }
        textSlot.SetText(orb.orbUpgrades[orb.orbLevel].upgrades[index].context);
        textSlot.color = (orb.orbUpgrades[orb.orbLevel].upgrades[index].upgradeTexColor);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (shopIsActive && !GameManager.GameOver)
            {
                MenuTriggered();
            }

        }

        if (bm.tierIndex == 2)
        {
            weaponTierGold.SetActive(true);
            weaponFee.text = "MAX";
        }
        else weaponFee.text = bm.bowPrices[bm.tierIndex].ToString();

        Vector2 eyeControllerVals = controlsHandler.GetBowVec();
        eyesUIAnim.SetFloat("x", eyeControllerVals.x);
        eyesUIAnim.SetFloat("y", eyeControllerVals.y);


        if (orbPanel.activeSelf)
        {
            orbEyeAnim.SetFloat("x", orb.xAnim);
            orbEyeAnim.SetFloat("y", orb.yAnim);
        }

        eyesImage.color = BowManager.instance.currBow.darkTextColor;
        bandanaImage.color = BowManager.instance.currBow.dashColor;


        #region  Arrow Damage Bar


        arrowDamage.localScale = Vector3.Lerp(arrowDamage.localScale, FindLerpVal(bm.currBow.arrowDamage, bm.absoluteDamage), 0.1f);
        #endregion

        // Dash Damage Bar

        dashDamage.localScale = Vector3.Lerp(dashDamage.localScale, FindLerpVal(bm.currBow.dashDamage, bm.absoluteDashDamage), 0.1f);

        // Charge Damage Bar

        chargeDamage.localScale = Vector3.Lerp(chargeDamage.localScale, FindLerpVal(bm.currBow.chargeUpDamage, bm.absoluteChargeDamage), 0.1f);

        // Crit Damage Bar

        critMultBar.localScale = Vector3.Lerp(critMultBar.localScale, FindLerpVal(bm.currBow.critMultiplier, bm.absoluteCritDamage), 0.1f);
        
        // Charge Cooldown Bar

        chargeCooldownBar.localScale = Vector3.Lerp(chargeCooldownBar.localScale,
            FindLerpVal( 6.1f - bm.currBow.chargeCooldown, 6.1f - bm.absoluteChargeCooldown), 0.1f);

        classType.SetText(bm.currBow.weaponClass.ToString());
        chargeUpDescription.SetText(bm.currBow.chargeUpDescription);



        // Debug.Log(selectedBlessing);
        if (blessingsObj.activeInHierarchy)
        {
            BLESSINGS_SHOWING = true;
            SetBlessingAnimator(selectedBlessing);
        }
        else BLESSINGS_SHOWING = false;




        playerClassHeader.color = darkBowTextColor;
        playerClassText.color = lightBowTextColor;
        playerClassText.SetText(bm.currBow.weaponClass.ToString());
        if (showCover)
        {
            weaponPanelCover.color = Color.Lerp(Color.clear, Color.white, 0.1f);
            SetWeaponAnimtor(selectedWeapon);
        }
        if (!showCover)
        {
            weaponPanelCover.color = Color.clear;
        }

        maxHealthR.localScale = Vector3.Lerp(maxHealthR.localScale, FindLerpVal(player.maxHitpoint, maxHealthNum), 0.1f);


        maxStaminaR.localScale = Vector3.Lerp(maxStaminaR.localScale, FindLerpVal(player.maxStamina, maxStaminaNum), 0.1f);
        maxDashR.localScale = Vector3.Lerp(maxDashR.localScale, FindLerpVal(player.dashDistance, maxDashNum), 0.1f);
        maxStaminaRegenR.localScale = Vector3.Lerp(maxStaminaRegenR.localScale, FindLerpVal(player.staminaRegenNum, maxRegenNum), 0.1f);


        shop_animator.SetBool("bannerPresent", headerPopupObj.activeSelf);

        if (blessingsObj.activeSelf)
        {
            if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE)
            {
                blessingConfirmObj.SetActive(false);
            } // put here
            headerPopupObj.SetActive(true);
            if (selectedBlessing == 4)
            {
                HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
                unlockedPrompt.title = "FOUND BLESSING";
                unlockedPrompt.text = "Choose your wish";
                unlockedPrompt.type = HeaderPopup.PromptType.ORB;
                unlockedPrompt.textColor = Color.white;

                headerPopup.title.color = bannerDefaultColorTitle;
                if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE) blessingConfirmObj.SetActive(false);
                headerPopup.PlayPrompt(unlockedPrompt);

            }
            else if (selectedBlessing == 2)
            {
                HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
                unlockedPrompt.title = PerkManager.instance.currBlessings[2].blessingName;
                unlockedPrompt.text = PerkManager.instance.currBlessings[2].blessingDescription;
                unlockedPrompt.type = HeaderPopup.PromptType.SPECIAL;
                headerPopup.icon.sprite = PerkManager.instance.currBlessings[2].blessingIcon;
                headerPopup.title.color = PerkManager.instance.currBlessings[2].lightBlessingColor;
                unlockedPrompt.textColor = Color.white;
                if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE) blessingConfirmObj.SetActive(true);

                headerPopup.PlayPrompt(unlockedPrompt);
            }

            else if (selectedBlessing == 1)
            {
                HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
                unlockedPrompt.title = PerkManager.instance.currBlessings[1].blessingName;
                unlockedPrompt.text = PerkManager.instance.currBlessings[1].blessingDescription;
                unlockedPrompt.type = HeaderPopup.PromptType.SPECIAL;
                headerPopup.icon.sprite = PerkManager.instance.currBlessings[1].blessingIcon;
                headerPopup.title.color = PerkManager.instance.currBlessings[1].lightBlessingColor;
                unlockedPrompt.textColor = Color.white;
                if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE) blessingConfirmObj.SetActive(true);
                headerPopup.PlayPrompt(unlockedPrompt);
            }
            else if (selectedBlessing == 0)
            {
                HeaderPopup.Prompt unlockedPrompt = new HeaderPopup.Prompt();
                unlockedPrompt.title = PerkManager.instance.currBlessings[0].blessingName;
                unlockedPrompt.text = PerkManager.instance.currBlessings[0].blessingDescription;
                unlockedPrompt.type = HeaderPopup.PromptType.SPECIAL;
                headerPopup.icon.sprite = PerkManager.instance.currBlessings[0].blessingIcon;
                headerPopup.title.color = PerkManager.instance.currBlessings[0].lightBlessingColor;
                unlockedPrompt.textColor = Color.white;
                if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE) blessingConfirmObj.SetActive(true);

                headerPopup.PlayPrompt(unlockedPrompt);
            }
            //  
        }

    }
    // Character Selection
    #region
    public void OnArrowClick(bool right)
    {
        if (right)
        {


            currentCharacterSelection++;

            // If we went too far away
            if (currentCharacterSelection == gameManager.playerSprites.Count)
                currentCharacterSelection = 0;

            OnSelectionChanged();

        }
        else
        {


            currentCharacterSelection--;

            //If we went too far away
            if (currentCharacterSelection < 0)
                currentCharacterSelection = gameManager.playerSprites.Count - 1;

            OnSelectionChanged();
        }
    }

    private void OnSelectionChanged()
    {
        //characterSelectionSprite.sprite = gameManager.playerSprites [currentCharacterSelection];

    }



    // Weapon Upgrade
    #endregion

    // Weapon Tier Up
    #region 
    public void OnWeaponUpgradeClick()
    {
        choiceOneImage.sprite = bm.LeftOption().bowImage;
        choiceOneName.text = bm.LeftOption().bowName;
        choiceTwoImage.sprite = bm.RightOption().bowImage;
        choiceTwoName.text = bm.RightOption().bowName;
        
        hubLayout.CloseShopPrompt();
        
        if (timeSinceTiered == Time.unscaledTime || !canClickWeaponUpgrade)
        {
            Debug.Log("Slow down");
            return;
        }

        if (BowManager.instance.TryUpgradeBow())
        {
            // SUSPECT
            timesUpgradedWeapon += 1;
            gameManager.VibrateTriple();
            weaponChoice.SetTrigger("show");
            //hoveringOverLeftOption = false;
            //hoveringOverRightOption = false;
            selectedWeapon = 3;

            showCover = true;


            lightBowTextColor = bm.currBow.lightTextColor;
            darkBowTextColor = bm.currBow.darkTextColor;

            bowName.text = bm.currBow.bowName;
            bowDescription.text = bm.currBow.description;
            bowDescription.color = darkBowTextColor;

            bowHeaderChargeUp.color = darkBowTextColor;
            bowHeaderClass.color = darkBowTextColor;
            chargeUpDescription.color = lightBowTextColor;
            classType.color = lightBowTextColor;

            currWeaponSprite.sprite = bm.currBow.bowImage;
            // currWeaponDamage.text = " DAMAGE: " + bm.currBow.arrowDamage.ToString();
            // currWeaponCritDamage.text = " CRIT DAMAGE: " + (bm.currBow.arrowDamage * bm.currBow.critMultiplier).ToString();
            // currWeaponDashDamage.text = " DASH DAMAGE: " + bm.currBow.arrowDamage.ToString();
            // currWeaponRpm.text = " RPM: " + (60 / bm.currBow.cooldown).ToString();
            // if (maxBowAchieved)
            // {
            // 		weaponFee.text = "MAX";
            // } else
            // {
            // 	weaponFee.text = bm.bowPrices[bm.tierIndex].ToString();
            // }


        } else gameManager.JoystickRumble();

    }

    public void OnLeftChoiceClick()
    {
        gameManager.JoystickRumble();
      //  timeSinceTiered = Time.unscaledTime;
        if (selectedWeapon != 0)

        {
            selectedWeapon = 0;
            dialogue_animator.SetBool ("IsOpen", true);
            // hoveringOverLeftOption = true;
            // hoveringOverRightOption = false;
        }
        else
        {
            if (Time.unscaledTime - timeSinceTiered > 0.5f && canClickWeaponUpgrade)
            {
                bm.UpgradeBow(0);
                timeSinceTiered = Time.unscaledTime;
                canClickWeaponUpgrade = false;

                choiceOne.transform.SetAsLastSibling();

                lightBowTextColor = bm.currBow.lightTextColor;
                darkBowTextColor = bm.currBow.darkTextColor;

                bowName.text = bm.currBow.bowName;
                bowDescription.text = bm.currBow.description;
                bowDescription.color = darkBowTextColor;

                bowHeaderChargeUp.color = darkBowTextColor;
                bowHeaderClass.color = darkBowTextColor;
                chargeUpDescription.color = lightBowTextColor;
                classType.color = lightBowTextColor;

                currWeaponSprite.sprite = bm.currBow.bowImage;

                dialogue_animator.SetBool("IsOpen", false);
                weaponChoice.SetTrigger("leftSelected");
                showCover = false;
            }



        }


    }

    public void OnLeftChoiceHover()
    {

        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
        {
            // Debug.Log("0");
            if (selectedWeapon != 0)
            {
                selectedWeapon = 0;

            }
        }
        //     if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE) hoveringOverLeftOption = true;
    }

    public void WeaponChoiceExit()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP) selectedWeapon = 3;

    }

    // public void OnLeftChoiceExit()
    // {
    //     selectedWeapon = 3;

    //  //   if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE) hoveringOverLeftOption = false;
    // }

    public void OnRightChoiceClick()
    {
        //timeSinceTiered = Time.unscaledTime;
        // if (selectedWeapon != 1) selectedWeapon = 1;
        gameManager.JoystickRumble();
        if (selectedWeapon != 1)
        {
            
            dialogue_animator.SetBool ("IsOpen", true);
            selectedWeapon = 1;
            // hoveringOverLeftOption = true;
            // hoveringOverRightOption = false;
        }
        else
        {
            if (Time.unscaledTime - timeSinceTiered > 0.5f && canClickWeaponUpgrade)
            {
                bm.UpgradeBow(1);
                timeSinceTiered = Time.unscaledTime;
                choiceTwo.transform.SetAsLastSibling();
                canClickWeaponUpgrade = false;

                lightBowTextColor = bm.currBow.lightTextColor;
                darkBowTextColor = bm.currBow.darkTextColor;

                bowName.text = bm.currBow.bowName;
                bowDescription.text = bm.currBow.description;
                bowDescription.color = darkBowTextColor;

                bowHeaderChargeUp.color = darkBowTextColor;
                bowHeaderClass.color = darkBowTextColor;
                chargeUpDescription.color = lightBowTextColor;
                classType.color = lightBowTextColor;
                currWeaponSprite.sprite = bm.currBow.bowImage;

                dialogue_animator.SetBool("IsOpen", false);
                weaponChoice.SetTrigger("rightSelected");
                showCover = false;
            }


        }

    }

    public void OnRightChoiceHover()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
        {
            // Debug.Log("0");
            if (selectedWeapon != 1)
            {
                selectedWeapon = 1;

            }
        }


    }

    // public void OnRightChoiceExit()
    // {
    //     selectedWeapon = 3;
    //    // if (controlsHandler.currPlatform != ControlsHandler.CurrPlatform.MOBILE) hoveringOverLeftOption = false;
    // }

    public void OnStaminaUpgradeClick()
    {

        
        if (player.numUpgradedStamina >= maximumStaminaUpgrade)
        {
            staminaGold.SetActive(true);
            maxStamina.text = player.maxStamina.ToString();
            newStamina.text = "MAX";
            staminaFee.text = "X";
        }
        else
        {
            if (gameManager.TryIncreaseStamina())
            {
                maxStamina.text = player.maxStamina.ToString();
                newStamina.text = (player.maxStamina + player.UpgradeStaminaNum()).ToString();
                staminaFee.text = player.UpgradeStaminaCost().ToString();
                if (player.numUpgradedStamina >= maximumStaminaUpgrade)
                {
                    staminaGold.SetActive(true);
                    maxStamina.text = player.maxStamina.ToString();
                    newStamina.text = "MAX";
                    staminaFee.text = "X";
                }
            }
        }
        gameManager.JoystickRumble();
        PlayerSkillToUpgrade();

    }
    #endregion

    // Bow Aspect Upgrades
    #region
    public void OnArrowDamageClick()
    {

        gameManager.JoystickRumble();
        if (bm.TryUpgradeAspect(0))
        {
           
            bm.UpgradeAspect(0);
        }
        if (bm.upgrades[0].maxUpgraded)
        {
            arrDamageGold.SetActive(true);
            arrowDamagefee.text = "MAX";
            arrowDamagefee.fontSize = 11f;
        }
        else arrowDamagefee.text = bm.upgrades[0].upgradeCosts[bm.upgrades[0].numUpgraded].ToString();

    }

    public void OnDashDamageClick()
    {
        gameManager.JoystickRumble();
        if (bm.TryUpgradeAspect(1))
        {
            bm.UpgradeAspect(1);
        }
        if (bm.upgrades[1].maxUpgraded)
        {
            dashDamageGold.SetActive(true);
            dashDamageFee.fontSize = 11f;
            dashDamageFee.text = "MAX";
        }
        else dashDamageFee.text = bm.upgrades[1].upgradeCosts[bm.upgrades[1].numUpgraded].ToString();

    }

    public void OnChargeDamageClick()
    {
        gameManager.JoystickRumble();
        if (bm.TryUpgradeAspect(2))
        {
            bm.UpgradeAspect(2);
        }
        if (bm.upgrades[2].maxUpgraded)
        {
            chargeDamageGold.SetActive(true);
            chargeDamageFee.text = "MAX";
            chargeDamageFee.fontSize = 11f;
        }
        else chargeDamageFee.text = bm.upgrades[2].upgradeCosts[bm.upgrades[2].numUpgraded].ToString();

    }

    public void OnCritMultClick()
    {
        gameManager.JoystickRumble();
        if (bm.TryUpgradeAspect(3))
        {
            bm.UpgradeAspect(3);
        }

        //critMultFee.text = bm.upgrades[3].upgradeCosts[bm.upgrades[3].numUpgraded].ToString();
        if (bm.upgrades[3].maxUpgraded)
        {
            critDamageGold.SetActive(true);
            critMultFee.fontSize = 11f;
            critMultFee.text = "MAX";

        }
        else critMultFee.text = bm.upgrades[3].upgradeCosts[bm.upgrades[3].numUpgraded].ToString();
    }

    public void OnChargeCooldownClick()
    {
        gameManager.JoystickRumble();
        if (bm.TryUpgradeAspect(4))
        {
            bm.UpgradeAspect(4);
        }

        if (bm.upgrades[4].maxUpgraded)
        {
            chargeCooldownGold.SetActive(true);
            chargeCooldownFee.fontSize = 11f;
            chargeCooldownFee.text = "MAX";
        }
        else chargeCooldownFee.text = bm.upgrades[4].upgradeCosts[bm.upgrades[4].numUpgraded].ToString();
    }

    #endregion

    public void OnHealthUpgradeClick()
    {
        
        if (player.numUpgradedHealth >= maximumHealthUpgrade)
        {
            healthGold.SetActive(true);
            maxHealth.text = player.maxHitpoint.ToString();
            newHealth.text = "MAX";
            healthFee.text = "X";

            healthGold.SetActive(true);
           
        }
        else
        {
            if (gameManager.TryIncreaseHealth())
            {
                newHealth.text = (player.maxHitpoint + player.UpgradeHealthNum()).ToString();
                healthFee.text = player.UpgradeHealthCost().ToString();
                maxHealth.text = gameManager.player.GetMaxHealth().ToString();
                if (player.numUpgradedHealth >= maximumHealthUpgrade)
                {
                    healthGold.SetActive(true);
                    maxHealth.text = player.maxHitpoint.ToString();
                    newHealth.text = "MAX";
                    healthFee.text = "X";
                }
            }
        }
        gameManager.JoystickRumble();
        PlayerSkillToUpgrade();

    }

    public void OnDashUpgradeClick()
    {
       
        if (player.numUpgradedDash >= maximumDashUpgrade)
        {
            dashPowrGold.SetActive(true);
            maxDash.text = player.dashNum.ToString();
            newDash.text = "MAX";
            dashFee.text = "X";
        }
        else
        {
            if (gameManager.TryIncreaseDash())
            {

                newDash.text = (player.numUpgradedDash + 1).ToString();
                dashFee.text = player.UpgradeDashCost().ToString();
                maxDash.text = player.dashNum.ToString();
                if (player.numUpgradedDash >= maximumDashUpgrade)
                {
                    dashPowrGold.SetActive(true);
                    maxDash.text = player.dashNum.ToString();
                    newDash.text = "MAX";
                    dashFee.text = "X";
                }
            }

        }
        gameManager.JoystickRumble();
        PlayerSkillToUpgrade();
    }


    public void OnStaminaRegenUpgradeClick()
    {
        if (player.numUpgradedStaminaRegen >= maximumStaminaRegenUpgrade)
        {

           
            regenGold.SetActive(true);
            maxStaminaRegen.text = player.staminaRegenNum.ToString();
            newStaminaRegen.text = "MAX";
            staminaRegenFee.text = "X";
        }
        else
        {
            if (gameManager.TryIncreaseStaminaRegen())
            {
                newStaminaRegen.text = (player.staminaRegenNum + player.UpgradeStaminaRegenNum()).ToString();
                staminaRegenFee.text = player.UpgradeStaminaRegenCost().ToString();
                maxStaminaRegen.text = player.staminaRegenNum.ToString();
                if (player.numUpgradedStaminaRegen >= maximumStaminaRegenUpgrade)
                {
                    maxStaminaRegen.text = player.staminaRegenNum.ToString();
                    regenGold.SetActive(true);
                    newStaminaRegen.text = "MAX";
                    staminaRegenFee.text = "X";
                }
            }

        }
        gameManager.JoystickRumble();
        
        PlayerSkillToUpgrade();
    }




    public void OnOrbUpgradeClick()
    {

        // if (orb.orbLevel >= orb.orbUpgrades.Length - 1)
        // {
        //     orbLevel.SetText((orb.orbLevel + 1).ToString());
        //     orbHealth.SetText(orbhb.maxHitpoint.ToString());
        //     turretDamage.SetText(gameManager.turretDamage.ToString());
        //     orbUpgradeCost.SetText("MAX  ");
        // }
        // else
        // {
        
        hubLayout.CloseShopPrompt();
        if (orb.TryLevelUp())
        {
            gameManager.VibrateTriple();
            orb.OrbLevelUp();

        }
        else
        {
            gameManager.JoystickRumble();
            soundManager.Play("upgradeDeclined", 0);
        }
        // }


        FormatAllOrbUpgradeTexts();


        if (orb.orbLevel == 1)
        {

            orbImage.sprite = orbLev1;
        }
        if (orb.orbLevel == 2)
        {

            orbImage.sprite = orbLev2;
        }
        if (orb.orbLevel == 3)
        {

            orbImage.sprite = orbLev3;
        }
        if (orb.orbLevel == 4)
        {

            orbImage.sprite = orbLev4;
        }



        // if (orb.orbLevel <= 4)
        // {
        //     orbLevel.SetText((orb.orbLevel + 1).ToString());
        //     orbHealth.SetText(orbhb.maxHitpoint.ToString());
        //     turretDamage.SetText(gameManager.turretDamage.ToString());
        //     orbUpgradeCost.SetText(orb.UpgradeCost().ToString());
        // }

    }



    // Button Stuff
    #region
    public void MenuOFF()
    {
        shopIsActive = false;
    }
    public void MenuTriggered()
    {
        
        if (BLESSINGS_SHOWING) return;
        if (!shopHasBeenOpened) shopHasBeenOpened = true;

        soundManager.Play("toggle", 0);

        if (shopIsActive)
        {
            shopIsActive = false;
        

            shopVolume.SetActive(false);
            pauseMenu.ShopMenuResume();
            
            dialogue_animator.SetBool("IsOpen", false);
            
            
            shop_animator.SetTrigger("hide");

            if (hubLayout.turretSlotTaken && controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
            {
                hubLayout.purchaseTurretButton.SetActive(true);
            }


        }
        else
        {
            
            shopIsActive = true;

            pauseMenu.ShopMenuPause();
            shopVolume.SetActive(true);

            if (showCover)
            {
                dialogue_animator.SetBool ("IsOpen", true);
            }

            shop_animator.SetTrigger("show");
            StartCoroutine(WaitThenFixScrollView());
            
            if (hubLayout.turretSlotTaken && controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
            {
                hubLayout.purchaseTurretButton.SetActive(false);
            }
            
            

        }
    }

    IEnumerator WaitThenFixScrollView()
    {
        float timeElapsed = 0.0f;
        while (timeElapsed > 0.01f)
        {
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        scrollView.GetComponent<ScrollRect>().content.GetComponent<RectTransform>().offsetMin = new Vector2(0, -1);
        scrollView.GetComponent<ScrollRect>().content.GetComponent<RectTransform>().offsetMax = new Vector2(0, 170f);
        scrollView.GetComponent<ScrollRect>().content.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -74f);
        // scrollView.GetComponent<ScrollRect>().content.GetComponent<RectTransform>().offsetMax = new Vector2(1, 1);
    }

    public void OnClick()
    {
        MenuTriggered();
    }

    public void BackgroundExitClick()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP) MenuTriggered();

    }

    public void OnOver()
    {
        hoveringOverButton = true;

        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Select);
    }

    public void OnExit()
    {
        hoveringOverButton = false;

        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
    }
    #endregion

    // Tabs Stuff
    #region
    public void TriggerWeaponMenu()
    {
        if (!showCover)
        {
            weaponTabButtons.SetActive(true);
            playerTabButtons.SetActive(false);
            orbTabButtonsButtons.SetActive(false);

            statsHeaderUniversal.SetActive(false);
            weaponHeaderUniversal.SetActive(false);
            orbHeaderUniversal.SetActive(false);

            orbPanel.SetActive(false);

            soundManager.Play("toggle", 0);
            weaponsPanel.transform.SetAsLastSibling();
        }


    }

    public void TriggerStatsMenu()
    {
        if (!showCover)
        {
            playerTabButtons.SetActive(true);
            weaponTabButtons.SetActive(false);
            orbTabButtonsButtons.SetActive(false);

            statsHeaderUniversal.SetActive(true);
            weaponHeaderUniversal.SetActive(true);
            orbHeaderUniversal.SetActive(true);

            orbPanel.SetActive(false);
            soundManager.Play("toggle", 0);
            statsPanel.transform.SetAsLastSibling();
        }

    }



    public void TriggerOrbMenu()
    {
        if (!showCover)
        {
            orbPanel.SetActive(true);

            orbTabButtonsButtons.SetActive(true);
            playerTabButtons.SetActive(false);
            weaponTabButtons.SetActive(false);

            statsHeaderUniversal.SetActive(true);
            weaponHeaderUniversal.SetActive(true);
            orbHeaderUniversal.SetActive(true);

            soundManager.Play("toggle", 0);
            orbPanel.transform.SetAsLastSibling();
        }

    }



    #endregion


    Vector3 FindLerpVal(float currNum, float maxNum)
    {

        float diff = currNum / maxNum;
        if (diff >= 1) diff = 1;
        Vector3 lerpedVal = new Vector3(diff, 1, 1);

        return lerpedVal;
    }



    public void AddPerkToPerksDisplayInPlayerMenu(Sprite perkIcon)
    {
        if (player.perkList.Count == 1)
        {
            perkSlotOne.GetComponent<Image>().sprite = perkIcon;
            perkSlotOne.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (player.perkList.Count == 2)
        {
            perkSlotTwo.GetComponent<Image>().sprite = perkIcon;
            perkSlotTwo.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (player.perkList.Count == 3)
        {
            perkSlotThree.GetComponent<Image>().sprite = perkIcon;
            perkSlotThree.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (player.perkList.Count == 4)
        {
            perkSlotFour.GetComponent<Image>().sprite = perkIcon;
            perkSlotFour.transform.GetChild(0).gameObject.SetActive(false);
        }

    }



    public void OnBlessingPickUp(Transform blessingThatGranted)
    {
        // Known Issue: when press resume game resumes this stays 
        PerkManager.instance.GenerateOptions();

        pauseMenu.ShopMenuPause();
        shopVolume.SetActive(true);


        // currBlessings = PerkManager.instance.GenerateOptions();
        // blessingIconOne.sprite = currBlessings[0].blessingIcon;
        // blessingIconTwo.sprite = currBlessings[1].blessingIcon;
        // blessingIconThree.sprite = currBlessings[2].blessingIcon;

        //currBlessings = 

        blessingIconOne.sprite = PerkManager.instance.currBlessings[0].blessingIcon;
        blessingIconTwo.sprite = PerkManager.instance.currBlessings[1].blessingIcon;
        blessingIconThree.sprite = PerkManager.instance.currBlessings[2].blessingIcon;

        blessingsObj.SetActive(true);



        //    Destroy(blessingThatGranted.gameObject);

    }

    public void BlessingOneHover()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
        {
            // Debug.Log("1");
            if (selectedBlessing != 0)
            {
                StartCoroutine(CooldownFromSelecting());
                selectedBlessing = 0;

            }
        }
    }

    public void BlessingExit()
    {
        // Debug.Log("E");
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP) selectedBlessing = 4;


    }




    public void BlessingOneSelect()
    {
        if (selectedBlessing != 0)
        {
            //    blessingChoiceChanged = Time.time;
            selectedBlessing = 0;
            StartCoroutine(CooldownFromSelecting());

        }
        else if (canActivateBlessing)
        {
            PerkManager.instance.ActivateBlessing(PerkManager.instance.currBlessings[0]);
            BlessingChosen();
        }


    }

    public void BlessingTwoHover()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
        {
            // Debug.Log("2");
            if (selectedBlessing != 1)
            {
                //blessingChoiceChanged = Time.time;
                StartCoroutine(CooldownFromSelecting());
                selectedBlessing = 1;


            }
        }
    }


    public void BlessingTwoSelect()
    {
        if (selectedBlessing != 1)
        {
            StartCoroutine(CooldownFromSelecting());
            selectedBlessing = 1;


        }
        else if (canActivateBlessing)
        {
            PerkManager.instance.ActivateBlessing(PerkManager.instance.currBlessings[1]);
            BlessingChosen();
        }

    }

    public void BlessingThreeHover()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.DESKTOP)
        {
            if (selectedBlessing != 2)
            {
                StartCoroutine(CooldownFromSelecting());
                selectedBlessing = 2;

            }
        }
    }

    public void BlessingThreeSelect()
    {
        if (selectedBlessing != 2)
        {
            selectedBlessing = 2;
            StartCoroutine(CooldownFromSelecting());

        }
        else if (canActivateBlessing)
        {
            PerkManager.instance.ActivateBlessing(PerkManager.instance.currBlessings[2]);
            BlessingChosen();
        }

    }


    public void SmartConfirmBlessing()
    {
        if (selectedBlessing == 0) BlessingOneSelect();
        else if (selectedBlessing == 1) BlessingTwoSelect();
        else if (selectedBlessing == 2) BlessingThreeSelect();
    }

    void BlessingChosen()
    {
        //HeaderPopup.instance.title.color = PerkManager.instance.currBlessings[2].lightBlessingColor;
        pauseMenu.ShopMenuResume();
        shopVolume.SetActive(false);
        blessingsObj.SetActive(false);
        soundManager.Play("upgradeStat", 0);
        StartCoroutine(headerPopup.HidePromptUnscaledTime());



        selectedBlessing = 4;

    }

    void SetWeaponAnimtor(int weaponNum)
    {
        if (weaponNum == 3)
        {
            weaponChoice.SetBool("leftHover", false);
            weaponChoice.SetBool("rightHover", false);
            dialogueManager.EndDialogue();


        }
        else if (weaponNum == 1)
        {
            weaponChoice.SetBool("leftHover", false);
            weaponChoice.SetBool("rightHover", true);
            choiceTwoDialogueTrigger.TriggerDialogue();
        }
        else if (weaponNum == 0)
        {
            weaponChoice.SetBool("leftHover", true);
            weaponChoice.SetBool("rightHover", false);
            choiceOneDialogueTrigger.TriggerDialogue();
        }

    }
    

    void SetBlessingAnimator(int blessingNum)
    {
        if (blessingNum == 4) // No Hover
        {
            blessingAnimator.SetBool("hoverLeft", false);
            blessingAnimator.SetBool("hoverMiddle", false);
            blessingAnimator.SetBool("hoverRight", false);
        }
        else if (blessingNum == 0) // Left Hover
        {
            blessingAnimator.SetBool("hoverLeft", true);
            blessingAnimator.SetBool("hoverMiddle", false);
            blessingAnimator.SetBool("hoverRight", false);
        }
        else if (blessingNum == 1) // Middle Hover
        {
            blessingAnimator.SetBool("hoverLeft", false);
            blessingAnimator.SetBool("hoverMiddle", true);
            blessingAnimator.SetBool("hoverRight", false);
        }
        else if (blessingNum == 2) // Right Hover
        {
            blessingAnimator.SetBool("hoverLeft", false);
            blessingAnimator.SetBool("hoverMiddle", false);
            blessingAnimator.SetBool("hoverRight", true);
        }
        else
        {
            Debug.LogWarning("(CharacterMenu Blessing Animator) Calling an index that doesn't exist!");
        }

    }

    public void AddBlessingToOrbPage(Sprite icon, int index)
    {
        blessingImages[index].sprite = icon;
        blessingImages[index].color = Color.white;
        availableSlots[index].SetActive(false);

    }

    public void BlessingAtAltar(bool isThere)
    {
        if (isThere)
        {
            blessingataltarT.SetActive(true);
            protectatallcostsT.SetActive(false);
        }
        else
        {
            blessingataltarT.SetActive(false);
            protectatallcostsT.SetActive(true);
        }
    }

    IEnumerator CooldownFromSelecting()
    {

        float timeElapsed = 0f;
        canActivateBlessing = false;
        while (timeElapsed < 0.1f)
        {
            timeElapsed += 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }
        canActivateBlessing = true;

    }


    void PlayerSkillToUpgrade()
    {
        
        
        hubLayout.CloseShopPrompt();

        List<float> playerSkills = new List<float>();
        playerSkills.Add((float)player.numUpgradedHealth / (float)maximumHealthUpgrade);
        playerSkills.Add((float)player.numUpgradedStamina / (float)maximumStaminaUpgrade);
        playerSkills.Add((float)player.numUpgradedDash/ (float)maximumDashUpgrade);
        playerSkills.Add((float)player.numUpgradedStaminaRegen/ (float)maximumStaminaRegenUpgrade);

        float smallestRatio = 1;
        indexOfSmallest = 5;
        int numEqual = 0;

        for (int a = 0; a < playerSkills.Count; a++)
        {
           
            if (playerSkills[a] == smallestRatio) numEqual += 1;
            if (playerSkills[a] < smallestRatio)
            {
                
                smallestRatio = playerSkills[a];
                indexOfSmallest = a;
            }
        }

        if (indexOfSmallest == 0)
        {

            playerSkillSpecifcText.SetText("HEALTH");
            playerSkillSpecifcText.color = healthUpgradeColor;
        } else if (indexOfSmallest == 1)
        {
            playerSkillSpecifcText.SetText("STAMINA");
            playerSkillSpecifcText.color = staminaUpgradeColor;
        } else if (indexOfSmallest == 2)
        {
            playerSkillSpecifcText.SetText("DASH");
            playerSkillSpecifcText.color = dashPlayerUpgradeColor;
        } else if (indexOfSmallest == 3)
        {
            playerSkillSpecifcText.SetText("STAMINA REGEN");
            playerSkillSpecifcText.color = regenUpgradeColor;
        } else if (smallestRatio == 1)
        {
            playerSkillSpecifcText.SetText("MAX");
            playerSkillSpecifcText.color = Color.white;
        }



    }
    
}



