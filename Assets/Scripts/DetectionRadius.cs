﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionRadius : MonoBehaviour
{
    public bool playerInRadius = false;
    PerkStatue perkStatue;

    void Start()
    {
        perkStatue = GetComponentInParent<PerkStatue>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.name == "Grounding")
        {
            playerInRadius = true;
            
            if (!perkStatue.Activated)
            {
                perkStatue.HaloLight(true);
                perkStatue.CostImage(true);
            }

        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.name == "Grounding")
        {
            playerInRadius = false;
            
            if (!perkStatue.Activated)
            {
                perkStatue.HaloLight(false);
                perkStatue.CostImage(false);
            }
        }
    }
}
