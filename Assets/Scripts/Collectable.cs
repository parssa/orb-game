﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : Collidable {

	// Logic
	protected bool collected;
	//protected bool attached;
	//private Transform spiketransform;

	protected override void OnCollide(Collider2D coll)
	{
		if ((coll.name == "Player") || (coll.name == "LeechSpike"))
			OnCollect ();
		// if (coll.name == "LeechSpike")
		// {
		// ///	spiketransform = coll.transform;
		// 	//attached = true;	
		// } 
	}

	protected virtual void OnCollect()
	{
		collected = true;
	}

	// protected override void Update()
	// {
	// 	base.Update();
	// 	if (attached) transform.position = spiketransform.position;
	// }
}
