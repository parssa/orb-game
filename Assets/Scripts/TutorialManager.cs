﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    // Global References
    public static TutorialManager instance;
    private HeaderPopup headerPopup;
    private GameManager gameManager;
    private Player player;
    private Weapon weapon;
    private CharacterMenu characterMenu;
    
    public static bool TutorialInProgress = false;
    public static bool TutorialCompleted = false; // <- Change This One
    public static bool TipsDone = false;

    public GameObject skipTutorialTex;
    
    public int num_tips_done = 0;   // <- And this One (4 is done) 
    
    private ControlsHandler controlsHandler;

    void Awake() { instance = this; }

    public bool hasCompletedTurretTutorial = false;
    public bool hasCompletedShopTutorial = false;

    public bool hasCompletedChargeUpTutorial = false;

    public bool hasCompletedGuardianTutorial = false;
    
    public bool hasCompletedShrineTutorial = false;

    public enum TipsSpecify
    {
        Turret,
        Shop,
        ChargeUp,
        Guardian,
        Shrine,
    }
    // public bool hasPurchasedTurret = false;
    // public bool TURRET_ARROWS_SHOWING = false;

    public enum TurretTutorialState
    {
        NOT_COMPLETE,
        SHOWING,
        HIDDEN,
        COMPLETE
    }

    public TurretTutorialState turretTutorial;

    public enum ShopTutorialState
    {
        NOT_COMPLETE,
        SHOWING,
        HIDDEN,
        COMPLETE
    }
    public ShopTutorialState shopTutorial;
    
    //public DialogueManager dialogueManager;

    private WaveSpawner waveSpawner;

    // Tasks
    [System.Serializable]
    public class Task
    {
        public string taskName;
        public Dialogue taskDialogue;
        public int numThingsToDo;
        //public KeyCode[] keysToPress;

        // public Dictionary<KeyCode, bool> keysToPressDict = new Dictionary<KeyCode, bool>();


    }
    public float timeTimedPromptShown = 0.0f;

    // public Task[] tasks;
    public Task upgradeTip;

    [HideInInspector] public Task currTask;
    private int currTaskIndex = 0;
    public int currTaskCompletion;

    // private bool w_pressed = false;
    // private bool a_pressed = false;
    // private bool s_pressed = false;
    // private bool d_pressed = false;

    private float timeSincePromptAppear = 0f;
    private bool shotRegardingTut = false;
    private bool chargeRegardingTut = false;
    private bool dashRegardingTut = false;

    private bool menuRegardingTut = false;


    // private bool tipIsOpen = false;

    // Start is called before the first frame update
    void Start()
    {
        //BeginTutorial();
        //if (PlayerPrefs.GetInt("HighScore") <= 0) BeginTutorial();
        //TutorialCompleted = false;
        controlsHandler = ControlsHandler.instance;
        gameManager = GameManager.instance;
        headerPopup = HeaderPopup.instance;
        player = Player.instance;
        characterMenu = CharacterMenu.instance;
        waveSpawner = WaveSpawner.instance;
        weapon = Weapon.instance;


        if (PlayerPrefs.GetInt("NEW_BUILD__", 0) != 1)
        {
            // PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("finishedTutorial", 0);
            PlayerPrefs.SetInt("finishedTips", 0);
            PlayerPrefs.SetInt("shopTip", 0);
            PlayerPrefs.SetInt("chargeTip", 0);
            PlayerPrefs.SetInt("guardianTip", 0);
            PlayerPrefs.SetInt("turretTip", 0);
            PlayerPrefs.SetInt("shrineTip", 0);
            
        }
        

        if (PlayerPrefs.GetInt("finishedTutorial", 0) != 1)
        {
           
            PlayerPrefs.SetInt("finishedTutorial", 0);
            PlayerPrefs.SetInt("finishedTips", 0);
            
            PlayerPrefs.SetInt("shopTip", 0);
            PlayerPrefs.SetInt("chargeTip", 0);
            PlayerPrefs.SetInt("guardianTip", 0);
            PlayerPrefs.SetInt("turretTip", 0);
            PlayerPrefs.SetInt("shrineTip", 0);
        }

        if (PlayerPrefs.GetInt("finishedTutorial") == 1) TutorialCompleted = true;
        if (PlayerPrefs.GetInt("finishedTips") == 1) TipsDone = true;
        if (TipsDone)
        {
            PlayerPrefs.SetInt("shopTip", 1);
            PlayerPrefs.SetInt("chargeTip", 1);
            PlayerPrefs.SetInt("guardianTip", 1);
            PlayerPrefs.SetInt("turretTip", 1);
            PlayerPrefs.SetInt("shrineTip", 1);
        }
        else
        {
            if (PlayerPrefs.GetInt("shopTip") == 1)
            {
                hasCompletedShopTutorial = true;
                shopTutorial = ShopTutorialState.COMPLETE;
            }
            if (PlayerPrefs.GetInt("chargeTip") == 1) hasCompletedChargeUpTutorial = true;
            if (PlayerPrefs.GetInt("guardianTip") == 1) hasCompletedGuardianTutorial = true;
            if (PlayerPrefs.GetInt("turretTip") == 1)
            {
                hasCompletedTurretTutorial = true;
                hasCompletedShrineTutorial = true;
                turretTutorial = TurretTutorialState.COMPLETE;
            }
        
            if (PlayerPrefs.GetInt("shrineTip") == 1) hasCompletedShrineTutorial = true;    
        }
        
        
        
        
        
        
        
        
        
            
        if (!TutorialCompleted) BeginTutorial();
        if (num_tips_done == 4) TipsDone = true;
    }

    public void BeginTutorial()
    {

        TutorialInProgress = true;
        TutorialCompleted = false;
   
        if (!Application.isMobilePlatform) skipTutorialTex.SetActive(true);
        shotRegardingTut = false;
        chargeRegardingTut = false;
        dashRegardingTut = false;

        menuRegardingTut = false;

        StartTask();
    }


    void StartTask()
    {
        currTaskCompletion = 0;
    }

    void TurretTutorialSwitcher()
    {
        switch(turretTutorial)
        {
            case TurretTutorialState.COMPLETE:
                hasCompletedTurretTutorial = true;
                gameManager.TurretsShowTutorialArrow(false);
                if (headerPopup.HeaderIsOpen) headerPopup.DisablePrompt();
                break;

            case TurretTutorialState.HIDDEN:
                
                gameManager.TurretsShowTutorialArrow(false);
                if (headerPopup.HeaderIsOpen) headerPopup.DisablePrompt();
                break;

            case TurretTutorialState.SHOWING:
                
                headerPopup.transform.gameObject.SetActive(true);
                headerPopup.PlayTurretPrompt();
                gameManager.TurretsShowTutorialArrow(true);
                break;
            
            
            
            default:
                break;
        }
    }

    void Update()
    {

        
        if (gameManager.GetCurrSouls() >= 20 && (turretTutorial == TurretTutorialState.NOT_COMPLETE || turretTutorial == TurretTutorialState.HIDDEN))
        {
            if (CanPlayTurret)
            {
                turretTutorial = TurretTutorialState.SHOWING;
                TurretTutorialSwitcher();
            }
        } 

        if (turretTutorial != TurretTutorialState.COMPLETE && gameManager.numTurretsActivated > 0)
        {
            turretTutorial = TurretTutorialState.COMPLETE;
            FinishedTip(TipsSpecify.Turret);
            TurretTutorialSwitcher();
        }

        if (turretTutorial == TurretTutorialState.SHOWING && gameManager.GetCurrSouls() < 20)
        {
            turretTutorial = TurretTutorialState.HIDDEN;
            TurretTutorialSwitcher();
        }


        if (shopTutorial != ShopTutorialState.COMPLETE)
        {
            if (characterMenu.shopHasBeenOpened)
            {
                //if (TutorialCompleted && !characterMenu.BLESSINGS_SHOWING && !characterMenu.BLESSING_BANNER_SHOULD_VISIBLE) headerPopup.DisablePrompt();
                
                headerPopup.FinishedShopTip();
                FinishedTip(TipsSpecify.Shop);
                shopTutorial = ShopTutorialState.COMPLETE;
                
            } else if (gameManager.GetCurrSouls() > 5)
            {
                shopTutorial = ShopTutorialState.SHOWING;
                headerPopup.transform.gameObject.SetActive(true);
                headerPopup.PlayShopPrompt();
            } else if (shopTutorial == ShopTutorialState.SHOWING && gameManager.GetCurrSouls() < 5)
            {
                shopTutorial = ShopTutorialState.HIDDEN;
                headerPopup.DisablePrompt();
            }
        }





        if (TutorialInProgress)
        {

            if (currTaskIndex == 0) MovementTask();
            else if (currTaskIndex == 1) TaskThree();
            else if (currTaskIndex == 2) TaskTwo();
            else if (currTaskIndex == 3) TaskSeven();


            if (Input.GetKeyDown(KeyCode.Space))
            {
                currTaskIndex = 8;
                NextTask();
            }

        }
    }

    private bool startedEndTaskCorout = false;
    void MovementTask()
    {
       
        if (controlsHandler.GetInputVector().x > 0) currTaskCompletion += 1;
        if (controlsHandler.GetInputVector().x < 0) currTaskCompletion += 1;
        if (controlsHandler.GetInputVector().y > 0) currTaskCompletion += 1;
        if (controlsHandler.GetInputVector().y < 0) currTaskCompletion += 1;

        if (currTaskCompletion >= 3 && !startedEndTaskCorout) StartCoroutine(ShortDelayBeforeTaskSwitch());
    }

    IEnumerator ShortDelayBeforeTaskSwitch()
    {
        float timeElapsed = 0f;
        startedEndTaskCorout = true;
        while (timeElapsed <= 0.4f)
        {
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        Debug.Log("Delay Finished!");
        NextTask();
    }

    void TaskTwo()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        {
            controlsHandler.HighlightDashButton();
        }
        if (Time.time - player.lastDashTime < 0.1f) NextTask();
        timeTimedPromptShown = Time.time;
    }

    void TaskThree()
    {
        if (controlsHandler.currPlatform == ControlsHandler.CurrPlatform.MOBILE)
        {
            controlsHandler.HighlightShootButton();
        }
        if (controlsHandler.PlayerShootingInput() && !startedEndTaskCorout)
            StartCoroutine(ShortDelayBeforeTaskSwitch());
        timeTimedPromptShown = Time.time;
    }

    void TaskFour()
    {
        if (Time.time - player.hasChargeShotBefore < 0.1f) NextTask();
        timeTimedPromptShown = Time.time;

    }

    void TaskFive()
    {
        if (characterMenu.shopIsActive) NextTask();
        timeTimedPromptShown = Time.time;
    }

    void TaskSix()
    {
        if (Time.time - timeTimedPromptShown > 3f) NextTask();
        if (Input.GetKeyDown(KeyCode.Mouse0) || (Input.GetKeyDown(KeyCode.E))) NextTask();
    }

    void TaskSeven()
    {
        if (Time.time - timeTimedPromptShown > 3f) NextTask();
        if (Time.time - timeTimedPromptShown > 1f)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) || (Input.GetKeyDown(KeyCode.E))) NextTask();
        }

    }
    void TaskEight()
    {
        if (Time.time - timeTimedPromptShown > 2f) NextTask();
        if (Time.time - timeTimedPromptShown > 1f)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) || (Input.GetKeyDown(KeyCode.E))) NextTask();
        }

    }



    void CombatTask()
    {
        if (weapon.lastShot && !shotRegardingTut)
        {
            shotRegardingTut = true;
            currTaskCompletion += 1;
            // dialogueManager.DisplayNextSentence();
        }
        if (weapon.lastChargeShot && !chargeRegardingTut)
        {
            chargeRegardingTut = true;
            currTaskCompletion += 1;
            // dialogueManager.DisplayNextSentence();

        }

        if (currTaskCompletion == 2)
        {
            if (timeSincePromptAppear > 2.5f)
            {
                // dialogueManager.DisplayNextSentence();
                currTaskCompletion += 1;
            }
            else timeSincePromptAppear += Time.deltaTime;
        }
        if (currTaskCompletion == 3 && player.isDashing && !dashRegardingTut)
        {
            dashRegardingTut = true;
            currTaskCompletion += 1;
            timeSincePromptAppear = 0f;
        }

        if (currTaskCompletion >= 4) NextTask();
    }

    void MenuTask()
    {
        if (Input.GetKeyDown(KeyCode.E) && !menuRegardingTut)
        {
            menuRegardingTut = true;
            currTaskCompletion += 1;
            // dialogueManager.DisplayNextSentence();
        }
        if (currTaskCompletion == 1)
        {
            if (timeSincePromptAppear > 3f)
            {
                //  dialogueManager.DisplayNextSentence();
                currTaskCompletion += 1;
                timeSincePromptAppear = 0f;
            }
            else timeSincePromptAppear += Time.deltaTime;
        }
        if (currTaskCompletion == 2)
        {
            if (timeSincePromptAppear > 3f)
            {
                //dialogueManager.DisplayNextSentence();
                currTaskCompletion += 1;
                timeSincePromptAppear = 0f;
            }
            else timeSincePromptAppear += Time.deltaTime;
        }
        if (currTaskCompletion >= 3) NextTask();

    }

    public bool ConditionsMetForShrineTutorial =>
        (shopTutorial == ShopTutorialState.COMPLETE && hasCompletedChargeUpTutorial);

    private bool CanPlayTurret => (ConditionsMetForShrineTutorial &&
                                   hasCompletedShrineTutorial);

    
    void Goodluck()
    {
        if (timeSincePromptAppear > 4f)
        {
            NextTask();
            currTaskCompletion += 1;
            timeSincePromptAppear = 0f;
        }
        else timeSincePromptAppear += Time.deltaTime;
    }

    public void NextTask()
    {
        //if (tipIsOpen) CompleteTutorial();
        
        if (HeaderPopup.instance.gameObject.activeSelf) headerPopup.NextPrompt();
        if (currTaskIndex >= 4) timeTimedPromptShown = Time.time;
        currTaskIndex += 1;

        startedEndTaskCorout = false;

        if (currTaskIndex >= 4) CompleteTutorial();
        StartTask();
    }
    void CompleteTutorial()
    {
        if (!Application.isMobilePlatform) skipTutorialTex.SetActive(false);
        headerPopup.DisablePrompt();
        TutorialInProgress = false;
        TutorialCompleted = true;
        PlayerPrefs.SetInt("finishedTutorial",1);
        //   dialogueManager.EndDialogue();
    }

    public void FinishedTip(TipsSpecify whichTip)
    {
        headerPopup.CompletedTipPrompt(whichTip);
        
        switch (whichTip)
        {
            case TipsSpecify.Shop:

                if (!hasCompletedShopTutorial)
                {
                    hasCompletedShopTutorial= true;
                    PlayerPrefs.SetInt("shopTip", 1);
                    
                    num_tips_done += 1;
                }
                break;
            case TipsSpecify.Turret:
                if (!hasCompletedTurretTutorial)
                {
                    hasCompletedTurretTutorial= true;
                   
                    PlayerPrefs.SetInt("turretTip", 1);
                    num_tips_done += 1;
                }
                
                break;
            case TipsSpecify.Guardian:
                if (!hasCompletedGuardianTutorial)
                {
                    
                    hasCompletedGuardianTutorial = true;
                    
                    PlayerPrefs.SetInt("guardianTip", 1);
                   
                    num_tips_done += 1;
                }
                break;
            case TipsSpecify.ChargeUp:
                if (!hasCompletedChargeUpTutorial)
                {
                    
                    PlayerPrefs.SetInt("chargeTip", 1);
                    
                    hasCompletedChargeUpTutorial = true;
                    num_tips_done += 1;
                }
                break;
            
            case TipsSpecify.Shrine:
                if (!hasCompletedShrineTutorial)
                {
                    PlayerPrefs.SetInt("shrineTip", 1);
                    hasCompletedShrineTutorial = true;
                    num_tips_done += 1;
                }
                break;
            
        }





        if (num_tips_done >= 5)
        {
            TipsDone = true;
            PlayerPrefs.SetInt("finishedTips", 1);
        }
        
    }
}
