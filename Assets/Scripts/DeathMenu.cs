﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class DeathMenu : MonoBehaviour
{
    public GameObject deathMenu;
    public TextMeshProUGUI gameOverTex;
    public TextMeshProUGUI wayOfDeath;
    public TextMeshProUGUI currScore;
    public TextMeshProUGUI highScore;
    public TextMeshProUGUI wavesLasted;

    
    public Image backgroundPanelImage;
    public Color backgroundPanelStartingColor;
    public Color backgroundPanelFinalColor;

    void Start()
    {
        deathMenu.SetActive(false);
        backgroundPanelImage.color = backgroundPanelStartingColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameOver)
        {
            ActivateDeathMenu();   
            
        }
    }
    void ActivateDeathMenu()
    {
        deathMenu.SetActive(true);
        PlayerPrefs.SetInt("TimesDied", 1);

        if (PlayerPrefs.GetInt("PlayerOrOrb") == 1)
        {
            wayOfDeath.SetText("(The Orb Died)");
        }
        else if (PlayerPrefs.GetInt("PlayerOrOrb") == 0)
        {
            wayOfDeath.SetText("(You Died)");
        }
       
        if (PlayerPrefs.GetInt("CurrentScore") >= 1000)
        {
            currScore.text = "00" + PlayerPrefs.GetInt("CurrentScore").ToString();
        }
        else if (PlayerPrefs.GetInt("CurrentScore") >= 100)
        {
            currScore.text = "000" + PlayerPrefs.GetInt("CurrentScore").ToString();
        }
        else if (PlayerPrefs.GetInt("CurrentScore") >= 10)
        {
            currScore.text = "0000" + PlayerPrefs.GetInt("CurrentScore").ToString();
        }
        else
        {
            currScore.text = "00000" + PlayerPrefs.GetInt("CurrentScore").ToString();
        }
        
        if (PlayerPrefs.GetInt("HighScore") >= 1000)
        {
            highScore.text = "00" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 100)
        {
            highScore.text = "000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else if (PlayerPrefs.GetInt("HighScore") >= 10)
        {
            highScore.text = "0000" +PlayerPrefs.GetInt("HighScore").ToString();
        }
        else
        {
            highScore.text = "00000" + PlayerPrefs.GetInt("HighScore").ToString();
        }
        
        
        if (PlayerPrefs.GetInt("HighScore") == PlayerPrefs.GetInt("CurrentScore") && PlayerPrefs.GetInt("CurrentScore") > 0) gameOverTex.SetText("NEW HIGHSCORE!!");
        else gameOverTex.SetText("GAME OVER!");

       
        wavesLasted.text = PlayerPrefs.GetInt("WaveOnDeath").ToString();
        if (PlayerPrefs.GetInt("bestWave", 1) < PlayerPrefs.GetInt("WaveOnDeath"))
        {
            PlayerPrefs.SetInt("bestWave", PlayerPrefs.GetInt("WaveOnDeath"));
        }
        
        
    }

}
