﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.Audio;
using UnityEngine.Experimental.Rendering.Universal;
using Pathfinding;



public class Enemy : Mover, IPooledObject
{

    [HideInInspector] public CameraMotor cameraShake;
    private EnemyManager.EnemyBlueprint blueprint;
    public EnemyManager.EnemyEnum thisEnemyIs;
   
    
    [Header("Pathfinding Values")]
    private float nextWaypointDistance = 1f;
    Path path;

    // bool pathFindingCoroutInProgress = false;

    int currentWaypoint = 0;
    public bool reachedEndOfPath = false;
    public bool forceFollowing = false;

    public bool reallyFarAway = false;
    //bool pathIsNull = false;

    Seeker seeker;
    float lastStartPathChangeTime = 0.0f;


    [Header("Combat Values")]
    public bool targeted = false;
    public bool isDead = false;

    public float blueprintSpeed;
    //private float pushRecoverySpeedButPlayerBuff;

    // Ranged Values
    [Header("Ranged Values")]

    private float shotTime = 0.0f;
    private float shotInterval = 2f;

    private float timebtwShots; // Set in Start
    private float startTimebtwShots = 2f;
    public GameObject projectile;
    private float stoppingDistance = 5f;
    private float retreatDistance = 2.5f;


    // Slugger Values
    private bool isEnraged = false;

    [Header("Turbo King Values")]
    // Turbo King Values  
    public bool startedChargingUp = false;
    public bool fellOutOfRange = false;
    public GameObject dashAnimObj;
    public float timeSinceLastDash;
    private const float DELAY_DASH = 1.5f;
    public bool isDashing = false;
    public float startChargingUpDistance = 5;
    public float stopChargingUpDistance = 15f;

    [Header("Red Skull Values")]
    public Light2D redskullLight;
    public GameObject redSkullLightObj;

    [Header("Enraged Mage Values")]
    public Animator highMageAC;
    private int numShotsR = 0;
    [HideInInspector] public float triggerLength = 1;
    [HideInInspector] public float chaseLength = 5;
    [HideInInspector] public Transform playerTransform;
    private Transform orbTransform;
    private Color orbTargettingColor;

    private int targetChooser;
    private Transform target;
    public bool orbTargetter;
    //private CameraMotor cameraShake;

    // Hitbox
    public ContactFilter2D filter;
    private BoxCollider2D hitbox;
    [HideInInspector] public Collider2D[] hits = new Collider2D[10];

    public int damage;
    public int critMultiplier;
    public int pushForce;

    // Flash On Hit
    private SpriteRenderer enemySprite;

    public SpriteRenderer alertSymbol;
    public Light2D alertSymbolLight;


    [HideInInspector] public int indexInTargettingArray = 0;
    [HideInInspector] public Transform turretThatTargettedIt;

    [Header("Death Values")]
    // Experience

    private float actual_immune_time;
    public int xpValue = 1;

    // Points
    public int pointsValue = 5;
    public int chanceOfDropping;

    private AudioSource audioSource;
    private AudioClip deathClip;
    private AudioClip clip;
    private SoundManager soundManager;

    //public GameObject anyEnemyCorpse;
    private GameObject enemyCorpse;

    private ObjectPooler objectPooler;

    private float lastParticleSpawnTime = 0f;
    private const float PARTICLE_SPAWN_COOLDOWN = 0.1f;

    private bool firingStateActive = false;

    protected override void Start()
    {

        base.Start();
        objectPooler = ObjectPooler.instance;

        enemyCorpse = Resources.Load("AnyEnemyCorpse") as GameObject;
        seeker = GetComponent<Seeker>();

        cameraShake = CameraMotor.instance;
        soundManager = SoundManager.instance;
        
        deathClip = soundManager.GetSounds("enemy_death_2").clip;

        enemySprite = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();

        hitbox = GetComponent<BoxCollider2D>();

        // Skull Orb Targetting
        targetChooser = Random.Range(0, 10);
        orbTargettingColor = new Color32(100, 183, 100, 255);

        playerTransform = GameManager.instance.player.transform;
        orbTransform = GameManager.instance.orbTransform;

        // Turbo King
        timeSinceLastDash = 0f;
        dashDistance = 1f;
        dashIncr = 0.3f;

        clip = audioSource.clip;


        if (thisEnemyIs == EnemyManager.EnemyEnum.SKULL_REG && targetChooser < 2)
        {
            enemySprite.color = orbTargettingColor;
            thisEnemyIs = EnemyManager.EnemyEnum.SKULL_ORB_TARGET;
            orbTargetter = true;
            target = orbTransform;
        }
        else
        {
            orbTargetter = false;
            target = playerTransform;
        }

        alertSymbol.transform.gameObject.SetActive(false);

        // Setting Blueprint Values
        blueprint = EnemyManager.instance.FetchValues(thisEnemyIs);

        maxHitpoint = blueprint.maxHitpoint;
        hitpoint = maxHitpoint;
        regularSpeed = blueprint.regularSpeed;
        damage = blueprint.damage;
        pushForce = blueprint.pushForce;

        if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED)
        {
            startTimebtwShots = 1.5f;
            shotInterval = 1.5f;
        }
        timebtwShots = startTimebtwShots;

        regularSpeed *= 2;
        alertSymbol.color = Color.clear;
        alertSymbolLight.intensity = 0f;

        actual_immune_time = immuneTime;
        UpdatePath();

    }

    public void OnObjectSpawn()
    {
        hitpoint = maxHitpoint;
        isDead = false;
        isEnraged = false;
        isGettingKnockedBack = false;
        isWalking = true;

        StartCoroutine(InitalImmunity());


        shotTime = Time.time;

        targetChooser = Random.Range(0, 10);
        orbTargettingColor = new Color32(100, 183, 100, 255);

        playerTransform = GameManager.instance.player.transform;
        orbTransform = GameManager.instance.orbTransform;

        // Turbo King
        timeSinceLastDash = 0f;
        dashDistance = 1f;
        dashIncr = 0.3f;

        //clip = audioSource.clip;
        //InvokeRepeating("UpdatePath", 0f, .5f);

        if (thisEnemyIs == EnemyManager.EnemyEnum.SKULL_REG && orbTargetter)
        {
            //enemySprite.color = orbTargettingColor;
            GetComponent<SpriteRenderer>().color = orbTargettingColor;
            thisEnemyIs = EnemyManager.EnemyEnum.SKULL_ORB_TARGET;
            orbTargetter = true;
            target = orbTransform;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            orbTargetter = false;
            target = playerTransform;
        }

        alertSymbol.transform.gameObject.SetActive(false);

        blueprint = EnemyManager.instance.FetchValues(thisEnemyIs);


        maxHitpoint = blueprint.maxHitpoint;


        regularSpeed = blueprint.regularSpeed;
        blueprintSpeed = Random.Range(blueprint.regularSpeed + 0.6f, blueprint.regularSpeed + 1f);
        damage = blueprint.damage;
        pushForce = blueprint.pushForce;
        transform.localScale = blueprint.enemyScale;

        if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED)
        {
            startTimebtwShots = 1.5f;
            shotInterval = 1.5f;
        }
        timebtwShots = startTimebtwShots;

        regularSpeed *= 2;
        alertSymbol.color = Color.clear;
        alertSymbolLight.intensity = 0f;
        // matDefault = enemySprite.material;

        playerSpeed = regularSpeed;


        if (thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER) pushRecSpeed = 0.1f;

    }

    IEnumerator InitalImmunity()
    {
        float timeElapsed = 0f;
        float _immuneReal = immuneTime;
        // Debug.Log(_immuneReal);
        immuneTime = 1f;
        while (timeElapsed <= 0.1f)
        {
            if (timeElapsed > 0.1f)
            {
                immuneTime = _immuneReal;
                yield break;
            }
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        immuneTime = _immuneReal;

    }

    protected override void RecieveDamage(Damage dmg)
    {
        


        if (hitpoint < maxHitpoint && thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER)
        {
            //if (!isEnraged) hitpoint = maxHitpoint;
            Enraged();

        }

        if (Time.time - lastParticleSpawnTime > PARTICLE_SPAWN_COOLDOWN)
        {

            objectPooler.SpawnFromPool("hitP", transform.position, transform.rotation);
            lastParticleSpawnTime = Time.time;
            anim.SetTrigger("gotHit");
        }

        base.RecieveDamage(dmg);



    }

    // public void KillEnemy()
    // {
    //     if (thisEnemyIs != EnemyManager.EnemyEnum.BOSS || thisEnemyIs != EnemyManager.EnemyEnum.GUARDIAN)
    //     {
    //         Death();
    //     }
    // }

    private void Update()
    {
        // if (DevTesting.KILL_ALL) Death();
        // Check for overlaps
        if (hitpoint <= 0 && !isDead)
        {
            Debug.Log("Hey, you should be dead");
            Death();
            return;
        }

        if (thisEnemyIs == EnemyManager.EnemyEnum.SKULL_RED)
        {
            float val = (playerTransform.position - transform.position).normalized.x;
            if (val >= 0)
            {
                redSkullLightObj.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else redSkullLightObj.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        boxCollider.OverlapCollider(filter, hits);

        if (isGettingKnockedBack) reachedEndOfPath = true;

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            OnCollide(hits[i]);
            // Cleans Array
            hits[i] = null;
        }

        if (hitpoint < maxHitpoint && (thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER || thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED)) Enraged();
        if (Input.GetKeyDown(KeyCode.N) && DevTesting.DEVMODEACTIVE) Death();
        
        
        
        // Movement

        Vector2 testingMovement = (playerTransform.position - transform.position).normalized;

        RaycastHit2D hitColliderX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(testingMovement.x, 0), testingMovement.x * playerSpeed, LayerMask.GetMask("WorldBorder"));
        RaycastHit2D hitColliderY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, testingMovement.y), testingMovement.y * playerSpeed, LayerMask.GetMask("WorldBorder"));


        if (hitColliderX.collider != null || hitColliderY.collider != null)
        {
            //UpdatePath();
            // Debug.Log("Collider hit");
            if (Time.time - lastStartPathChangeTime > 0.3f) UpdatePath();

        }


        if (orbTargetter) testingMovement = (orbTransform.position - transform.position).normalized;
        else
        {
            float actualDistanceFromPlayer = Vector2.Distance(transform.position, playerTransform.position);
            if (actualDistanceFromPlayer >= 11)
            {
                reallyFarAway = actualDistanceFromPlayer >= 13;
                
             //   Debug.Log(actualDistanceFromPlayer);
                forceFollowing = true;
                firingStateActive = false;
                RaycastHit2D blocking = Physics2D.Raycast(transform.position, testingMovement, actualDistanceFromPlayer, LayerMask.GetMask("WorldBorder"));
                if (blocking.collider == null && hitColliderX.collider == null && hitColliderY.collider == null)
                {

                    reachedEndOfPath = true;

                }
                else
                {
                    // Debug.Log("Need to force follow");
                    reachedEndOfPath = false;
                    if (Time.time - lastStartPathChangeTime > 0.7f) UpdatePath();
                }

            }
            else
            {
                forceFollowing = false;
                reallyFarAway = false;
            }
        }
        if (thisEnemyIs == EnemyManager.EnemyEnum.TURBO_KING) TurboKingMovement();
        else if (!reachedEndOfPath)
        {
            startedChargingUp = false;
            if (path == null)
            {

                reachedEndOfPath = true;
                // Debug.Log("REOP TRUE null");
                UpdatePath();
                return;
            }
            if (currentWaypoint >= path.vectorPath.Count)
            {
                reachedEndOfPath = true;
                //   Debug.Log("REOP TRUE waypoint");
                // UpdateMotor((target.position - transform.position).normalized);
                UpdatePath();
                return;



            }
            Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - (Vector2)transform.position).normalized;
            UpdateMotor(direction);

            float distance = Vector2.Distance(transform.position, path.vectorPath[currentWaypoint]);



            if (distance < nextWaypointDistance)
            {
                currentWaypoint++;
            }

        }
        else
        {

            float distFromTarget = (playerTransform.position - transform.position).magnitude;
            if (!isWalking && distFromTarget > 0.3f)
            {
                //Debug.Log("Stuck! and Far Away");
                reachedEndOfPath = false;

            }

            if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_REG || thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED) RangedEnemyMovement();
            else if (thisEnemyIs == EnemyManager.EnemyEnum.SKULL_RED) RedSkullMovement();
            else if (thisEnemyIs != EnemyManager.EnemyEnum.GUARDIAN) UpdateMotor((target.position - transform.position).normalized);

        }
    }

    void UpdatePath()
    {
        // Debug.Log("INVOKING");
        if (seeker.IsDone())
        {
            if (orbTargetter)
            {
                lastStartPathChangeTime = Time.time;
                seeker.StartPath(transform.position, orbTransform.position, OnPathComplete);
            }
            else
            {
                lastStartPathChangeTime = Time.time;
                seeker.StartPath(transform.position, playerTransform.position, OnPathComplete);
            }
        }

    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }

        Vector2 testingMovement = (playerTransform.position - transform.position).normalized;
        RaycastHit2D hitColliderX = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(testingMovement.x, 0), testingMovement.x * playerSpeed, LayerMask.GetMask("WorldBorder"));
        RaycastHit2D hitColliderY = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, testingMovement.y), testingMovement.y * playerSpeed, LayerMask.GetMask("WorldBorder"));

        if (hitColliderX.collider == null && hitColliderY.collider == null)
        {
            reachedEndOfPath = true;

        }

    }

    void Enraged()
    {
        alertSymbol.transform.gameObject.SetActive(true);
        if (!isEnraged)
        {
            isEnraged = true;
            if (thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER)
            {


                anim.speed = 3f;
                regularSpeed = 3.4f;
                alertSymbol.color = new Color32(250, 194, 90, 255);
                alertSymbolLight.intensity = 0.88f;
            }
            if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED)
            {
                highMageAC.SetBool("isEnraged", true);


                startTimebtwShots = 0.1f;



                alertSymbol.color = new Color32(250, 194, 90, 255);
                alertSymbolLight.intensity = 0.88f;
            }
        }


    }

    protected virtual void OnCollide(Collider2D coll)
    {


        if (coll.CompareTag("OrbHitBox"))
        {
            if (thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER || thisEnemyIs == EnemyManager.EnemyEnum.TURBO_KING || thisEnemyIs == EnemyManager.EnemyEnum.RANGED_REG) return;
            else
            {
                Damage dmg = new Damage
                {
                    damageAmount = Random.Range(damage, damage * critMultiplier),
                    origin = transform.position,
                    pushForce = pushForce
                };

                Vector2 newPositionAfterPush = new Vector2(transform.position.x + orbTransform.position.x, transform.position.y + orbTransform.position.y);
                transform.position = Vector2.MoveTowards(transform.position, newPositionAfterPush, 0.2f);

                StartCoroutine(cameraShake.Shake(0.1f, 1f));
                coll.SendMessage("RecieveDamage", dmg);


            }
        }

        if (coll.CompareTag("Player"))
        {

            reachedEndOfPath = true;
            Damage dmg = new Damage
            {
                damageAmount = Random.Range(damage, damage * critMultiplier),
                origin = transform.position,
                pushForce = pushForce
            };

            Vector2 dir = new Vector2(transform.position.x - playerTransform.position.x, transform.position.y - playerTransform.position.y).normalized * pushForce;
            Vector2 newPositionAfterPush = new Vector2(transform.position.x + dir.x, transform.position.y + dir.y);
            transform.position = Vector2.MoveTowards(transform.position, newPositionAfterPush, 0.4f);



            StartCoroutine(cameraShake.Shake(0.1f, 1f));
            coll.SendMessage("RecieveDamage", dmg);

        }

        if (coll.CompareTag("Structure"))
        {

            // Create a new damage object, before sending it to the player
            int damageToDeal = Random.Range(damage, damage * critMultiplier + 1);
            Damage dmg = new Damage
            {
                damageAmount = Random.Range(damage, damage * critMultiplier),
                origin = transform.position,
                pushForce = pushForce,
                isCrit = damageToDeal >= damage * critMultiplier
            };
            
            coll.SendMessage("RecieveDamage", dmg);
        }

        if (coll.CompareTag("Fighter"))
        {
            if (Time.time - lastTimeDetected <= INTERVAL_DETECTION)
            {
                numEnemiesTouching += 1;
                if (numEnemiesTouching >= 3) inCrowd = true;
            }
        }

    }

    private int numEnemiesTouching = 0;
    private const float INTERVAL_DETECTION = 0.5f;
    private float lastTimeDetected = 0f;
    private bool inCrowd = false;

    public bool InCrowd
    {
        get
        {
            if (inCrowd) numEnemiesTouching = 0;
            return inCrowd;
        }
        set => inCrowd = value;
    }

    void PlayDeathSound()
    {
        Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 10 - GameManager.instance.mastVol * 10);
        AudioSource.PlayClipAtPoint(deathClip, soundPos);
    }


    protected override void Death()
    {

        // CancelInvoke();
        //PlayDeathSound();

        isDead = true;

        immuneTime = 0.1f;

        if (thisEnemyIs == EnemyManager.EnemyEnum.SKULL_RED)
        {
            objectPooler.SpawnFromPool("explosionRed", transform.position, transform.rotation);
            SoundManager.instance.Play("explode1", 0);
        }
        else
        {
            PowerupManager.instance.DropLoot(transform.position);
        }

        SpawnShrineSoul();


        objectPooler.SpawnFromPool("anyDeathP", transform.position, transform.rotation);



        GameObject thisCorpse = objectPooler.SpawnFromPool("corpse", transform.position, transform.rotation);
        thisCorpse.GetComponent<Corpse>().corpseSprite = enemySprite.sprite;
        thisCorpse.GetComponent<Corpse>().lastPushDir = lastPushDirection.normalized;
        thisCorpse.GetComponent<Corpse>().startScale = blueprint.enemyScale;
        thisCorpse.GetComponent<Corpse>().thisCorpseIs = thisEnemyIs;


        if (thisEnemyIs != EnemyManager.EnemyEnum.BOSS || thisEnemyIs != EnemyManager.EnemyEnum.GUARDIAN) WaveSpawner.instance.enemies_defeated_during_wave += 1;
        
        GameManager.instance.GrantPoints(pointsValue);
        DamagePopup.Create(transform.position, pointsValue, true, DamagePopup.POINTS);


        GameObject thisSoul = objectPooler.SpawnFromPool("soul", transform.position, transform.rotation);
        thisSoul.GetComponent<FlyToPlayer>().shootOutDir = lastPushDirection.normalized;
        thisSoul.GetComponent<FlyToPlayer>().cameFromShrine = false;
        //
        transform.localScale = blueprint.enemyScale;
        gameObject.SetActive(false);

        base.Death();

    }


    void SpawnShrineSoul()
    {
        if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_ENRAGED || thisEnemyIs == EnemyManager.EnemyEnum.TURBO_KING || thisEnemyIs == EnemyManager.EnemyEnum.SLUGGER)
        {
            objectPooler.SpawnFromPool("shrineSoul", transform.position, transform.rotation);
        }


    }

    #region Movement Patterns
    void RedSkullMovement()
    {
        UpdateMotor((playerTransform.position - transform.position).normalized);
        //CollisionCleaner();


        if (hitpoint < maxHitpoint)
        {
            playerSpeed = 1f;
            redskullLight.intensity = Mathf.Lerp(2.84f, 10f, Mathf.PingPong(Time.time, 0.5f));
        }
    }

    void RangedEnemyMovement()
    {
        if (isGettingKnockedBack || isRecovering)
        {
            UpdateMotor((transform.position - playerTransform.position).normalized);
        }
        else if ((Vector2.Distance(transform.position, playerTransform.position) > stoppingDistance) ||
                (Vector2.Distance(transform.position, playerTransform.position) < -stoppingDistance))
        {

            UpdateMotor((playerTransform.position - transform.position).normalized);

        }
        else if ((Vector2.Distance(transform.position, playerTransform.position) < stoppingDistance && Vector2.Distance(transform.position, playerTransform.position) > retreatDistance) ||
                  (Vector2.Distance(transform.position, playerTransform.position) > -stoppingDistance && Vector2.Distance(transform.position, playerTransform.position) < -retreatDistance))
        {

            UpdateMotor(Vector2.zero);


        }
        else if (Vector2.Distance(transform.position, playerTransform.position) <= retreatDistance || Vector2.Distance(transform.position, playerTransform.position) > -retreatDistance)
        {
            UpdateMotor((transform.position - playerTransform.position).normalized);
            //transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, -playerSpeed * Time.deltaTime);		
        }


        if (Vector2.Distance(transform.position, playerTransform.position) <= stoppingDistance + 2)
        {
            // StartFiring();
            if (!firingStateActive)
            {
               StartCoroutine(ShootingCorout());
            }
        }

        if (firingStateActive)
        {
            if (Vector2.Distance(transform.position, playerTransform.position) > (stoppingDistance + 3))
            {
                Debug.Log("Too far from player going to stop shooting");
                firingStateActive = false;
                StopCoroutine(ShootingCorout());    
            }
        }

    }

    public IEnumerator ShootingCorout()
    {
        firingStateActive = true;
        while (firingStateActive)
        {
            StartFiring();
            yield return null;
        }
    }

    void StartFiring()
    {
        if (Time.time - shotTime >= shotInterval)
        {
            // shoot player

            var position = playerTransform.position;
            Vector3 relativePos = new Vector3(position.x, position.y, 0) - transform.position;
            Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * relativePos;
            Quaternion movementRotation = Quaternion.LookRotation(new Vector3(0, 0, 1), rotatedVectorToTarget);

            Transform firepoint = transform.GetChild(3);

            GameObject enemyProj = objectPooler.SpawnFromPool("enemyFireball", firepoint.position, movementRotation);

            enemyProj.transform.localScale = new Vector3(3, 3, 1);
            pushDirection = (transform.transform.position - gameObject.transform.GetChild(3).position).normalized * 5;
            
            if (GameManager.instance.mastVol > -80f)
            {
                Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
                
                AudioClip shootClip = soundManager.GetSounds("enemyShot1").clip;
                

                var shootSound = PlayClipAt(shootClip, soundPos);
                shootSound.pitch = Random.Range(0.65f, 1f);
                shootSound.volume = 0.1f;
                shootSound.reverbZoneMix = 0.078819f;
                shootSound.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
                // AudioSource.PlayClipAtPoint(shootClip, soundPos);
            } else Debug.Log("Game has no vol rn");
            
            
            if (thisEnemyIs == EnemyManager.EnemyEnum.RANGED_REG)
            {
                numShotsR += 1;
                if (numShotsR < 4)
                {
                    startTimebtwShots = 1f;
                    shotInterval = 1f;
                }
                else
                {
                    startTimebtwShots = 2f;
                    shotInterval = 2f;
                    numShotsR = 0;
                }
            }

            if (isEnraged)
            {
                enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.ENRAGED;
                
                numShotsR += 1;
                if (numShotsR < 3)
                {
                    shotInterval = 0.1f;
                } else 
                {
                    shotInterval = 0.5f;
                    numShotsR = 0;
                }

            } else enemyProj.GetComponent<EnemyFireball>().fireballType = EnemyFireball.FireballType.REG;

            shotTime = Time.time;
            //  timebtwShots = startTimebtwShots;

        }
        // else
        // {
        //     timebtwShots -= Time.deltaTime;
        // }
    }


    void TurboKingMovement()
    {
        dashDistance = 4f;
        alertSymbol.transform.gameObject.SetActive(startedChargingUp);

        if (isGettingKnockedBack || isRecovering)
        {
            // Debug.Log("A");
            UpdateMotor((transform.position - playerTransform.position).normalized);
        }
        else if (startedChargingUp)
        {
            // Debug.Log("B");
            UpdateMotor((playerTransform.position - transform.position).normalized);
            //UpdateMotor(Vector2.zero);
        }
        else if ((Vector2.Distance(transform.position, playerTransform.position) > startChargingUpDistance) ||
                (Vector2.Distance(transform.position, playerTransform.position) < -startChargingUpDistance))
        {
            // Debug.Log("C");
            regularSpeed = 1f;
            UpdateMotor((playerTransform.position - transform.position).normalized);

        }
        else if ((Vector2.Distance(transform.position, playerTransform.position) < startChargingUpDistance && Vector2.Distance(transform.position, playerTransform.position) > stopChargingUpDistance) ||
                  (Vector2.Distance(transform.position, playerTransform.position) > -startChargingUpDistance && Vector2.Distance(transform.position, playerTransform.position) < -stopChargingUpDistance))
        {

            // Debug.Log("D");
            if (!startedChargingUp && !isDashing && (Time.time - timeSinceLastDash >= DELAY_DASH)) StartCoroutine(ChargeUpIntoDash());
            //UpdateMotor(Vector2.zero);
            UpdateMotor((playerTransform.position - transform.position).normalized);
            fellOutOfRange = false;

        }
        else if (Vector2.Distance(transform.position, playerTransform.position) <= stopChargingUpDistance || Vector2.Distance(transform.position, playerTransform.position) > -stopChargingUpDistance)
        {
            // Debug.Log("E");

            fellOutOfRange = true;
            UpdateMotor((playerTransform.position - transform.position).normalized);
            //transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, -playerSpeed * Time.deltaTime);


        }
        //else if (startedChargingUp) UpdateMotor(Vector2.zero);

    }

    IEnumerator ChargeUpIntoDash()
    {
        startedChargingUp = true;
        float timeElapsed = 0.0f;
        alertSymbolLight.intensity = 0.88f;
        alertSymbol.color = new Color32(250, 110, 0, 255);


        bool playedTheThing = false;
        while (timeElapsed < 1f)
        {
            if (alertSymbolLight.intensity >= 1.72f) alertSymbolLight.intensity = 1.5f;
            alertSymbolLight.intensity += 0.1f;
            timeElapsed += Time.deltaTime;

            if (timeElapsed > 0.2f && !playedTheThing)
            {
                Vector3 soundPos = new Vector3(transform.position.x, transform.position.y, 5 - GameManager.instance.mastVol * 10);
                
                AudioClip shootClip = soundManager.GetSounds("turboKingLockedOn").clip;
        
                var shootSound = PlayClipAt(shootClip, soundPos);
                shootSound.pitch = Random.Range(0.97f, 1.29f);
                shootSound.volume = 0.08f;
                shootSound.reverbZoneMix = 0.078819f;
                shootSound.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
                
                playedTheThing = true;
            }
            
            
            yield return null;

        }
        
        
        


        float angleOfDash = Mathf.Atan2(playerTransform.position.y - transform.position.y, playerTransform.position.x - transform.position.x) * 180 / Mathf.PI;
        Quaternion dashDirToEuler = Quaternion.Euler(0, 0, angleOfDash);
        Vector2 dashDirection = dashDirToEuler * Vector2.right;

        alertSymbolLight.intensity = 0f;
        alertSymbol.color = Color.clear;

        startedChargingUp = false;

        if (!fellOutOfRange) StartCoroutine(DashMode(dashDirection));


       

    }

    IEnumerator DashMode(Vector2 dashDirection)
    {
        float timePassed = 0;
        CreateDashShadow(0.3f, dashDirection.x);
        Vector2 startingPos = transform.position;
        isDashing = true;
        timeSinceLastDash = Time.time;

        //Debug.Log("Dashing");

        float normalPlayerSpeed = regularSpeed;
        regularSpeed = normalPlayerSpeed;
        
        
        
        
        
        while (Vector2.Distance(startingPos, transform.position) < dashDistance && !PauseMenu.GameIsPaused)
        {

            if (Vector2.Distance(startingPos, transform.position) >= dashDistance)
            {

                isDashing = false;
                yield break;
            }

            if (PauseMenu.GameIsPaused)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            if (isDashing == false)
            {
                // immuneTime = normalImmuneTime;
                isDashing = false;
                regularSpeed = normalPlayerSpeed;
                yield break;
            }

            playerSpeed = 1f;
            //lastImmune = Time.time;

            //CreateDashShadow(0.3f, dashDirection.x);
            CreateDashShadow(0.3f, dashDirection.x);
            CreateDashShadow(0.3f, dashDirection.x);
            HandleDash(dashDirection);
            dashIncr += 0.1f;

            timePassed += Time.deltaTime;

            yield return null;
        }
        dashIncr = 0.3f;
        CreateDashShadow(0.3f, dashDirection.x);
        //CreateDashShadow(0.3f, dashDirection.x);
        //lastImmune = Time.time;
        //StartCoroutine(ResidualImmunity());
        isDashing = false;
        regularSpeed = normalPlayerSpeed;
        playerSpeed = regularSpeed;


        void CreateDashShadow(float lifeSpan, float dashDir)
        {
            if (dashDir < 0)
            {

                Quaternion flippedTransformrotation = Quaternion.Euler(0, 180, 0);
                GameObject currDash = Instantiate(dashAnimObj, transform.position, flippedTransformrotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);


                Destroy(currDash, lifeSpan);
            }
            else
            {

                GameObject currDash = Instantiate(dashAnimObj, transform.position, transform.rotation);

                //currDash.GetComponent<SpriteRenderer>().sprite = playerSprite.sprite;
                currDash.GetComponentInChildren<Light2D>().color = new Color32(255, 144, 0, 255);
                currDash.GetComponentInChildren<Light2D>().intensity = Mathf.Lerp(1f, 0f, lifeSpan);
                Destroy(currDash, lifeSpan);
            }

        }
    }
    #endregion
    
    public AudioSource PlayClipAt(AudioClip clip, Vector3 pos){
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = pos; // set its position
        
        
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.spatialBlend = 0.5f;
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }
}

