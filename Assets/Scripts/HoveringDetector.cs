﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoveringDetector : MonoBehaviour
{

    public enum ParentType
    {
        TURRET,
        PERK_STATUE
    }

    public ParentType parentType;

    private Weapon weapon;
    private Turret turret;
    private PerkStatue perkStatue;
    private CursorManager cursorManager;

    private void Start()
    {
        if (parentType == ParentType.TURRET) turret = GetComponentInParent<Turret>();
        else if (parentType == ParentType.PERK_STATUE) perkStatue = GetComponentInParent<PerkStatue>();
        cursorManager = CursorManager.Instance;
        
        weapon = Weapon.instance;
    }

    public void OnMouseExit()
    {
        // transform.parent.GetComponent<Turret>().mouseIsHovering = false;
        
        if (parentType == ParentType.TURRET) turret.OnMouseExit();
        else if (parentType == ParentType.PERK_STATUE) perkStatue.OnMouseExit();
        
        weapon.hoveringOver = false;
        cursorManager.SetActiveCursorType(CursorManager.CursorType.Arrow);
        
    }

    public void OnMouseOver()
    {
        
        
        if (parentType == ParentType.TURRET) turret.mouseIsHovering = true;
        else if (parentType == ParentType.PERK_STATUE) perkStatue.mouseIsHovering = true;

        weapon.hoveringOver = true;
        cursorManager.SetActiveCursorType(CursorManager.CursorType.Select);
    }


    // public void OnMouseEnter()
    // {
    //     Debug.Log("Clicked!");
    // }
}
