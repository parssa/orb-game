﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlessingPickup : PowerUp
{
    protected override void OnCollect()
    {
       
        if (!collected)
        {

            collected = true;
            SoundManager.instance.Play("Powerup", 0);


            CharacterMenu.instance.BlessingAtAltar(false);
            PowerupManager.instance.blessingPickedUp = true;

            CharacterMenu.instance.OnBlessingPickUp(transform);
            Destroy(gameObject);
        }

    }
}
