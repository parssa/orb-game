﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberDeterminer : MonoBehaviour
{
    public int numberInQuestion = 1;

    public void DetermineEndPrefix()
    {
        string numberAsString = numberInQuestion.ToString();
        string[] allCharacters = new string[numberAsString.Length];

        for (int i = 0; i < numberAsString.Length; i++)
        {
            allCharacters[i] = System.Convert.ToString(numberAsString[i]);
        }

        string lastNum = allCharacters[numberAsString.Length - 1];
        
        Debug.Log(lastNum);
    }
    

}
