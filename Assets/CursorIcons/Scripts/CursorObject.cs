﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorObject : MonoBehaviour {
#pragma warning disable 0649
    [SerializeField] private CursorManager.CursorType cursorType;
#pragma warning restore 0649
    private void OnMouseEnter() {
        CursorManager.Instance.SetActiveCursorType(cursorType);
    }

    private void OnMouseExit() {
        CursorManager.Instance.SetActiveCursorType(CursorManager.CursorType.Arrow);
    }

}
