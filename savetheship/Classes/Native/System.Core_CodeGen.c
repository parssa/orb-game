﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000A TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000C System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000E System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000010 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000011 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000012 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000013 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000015 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000016 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000017 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001A System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000021 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000022 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000023 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000024 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000025 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000027 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000002A System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002C System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000002D System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000002E System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000002F System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000030 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000031 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000032 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000033 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000034 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000037 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000038 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000003B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003E System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000003F System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000040 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000041 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000042 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000043 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000044 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000045 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000046 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[70] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[70] = 
{
	0,
	19,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[13] = 
{
	{ 0x02000004, { 24, 4 } },
	{ 0x02000005, { 28, 9 } },
	{ 0x02000006, { 37, 7 } },
	{ 0x02000007, { 44, 10 } },
	{ 0x02000008, { 54, 1 } },
	{ 0x02000009, { 55, 34 } },
	{ 0x0200000B, { 89, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
	{ 0x06000008, { 22, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[91] = 
{
	{ (Il2CppRGCTXDataType)2, 13922 },
	{ (Il2CppRGCTXDataType)3, 10161 },
	{ (Il2CppRGCTXDataType)2, 13923 },
	{ (Il2CppRGCTXDataType)2, 13924 },
	{ (Il2CppRGCTXDataType)3, 10162 },
	{ (Il2CppRGCTXDataType)2, 13925 },
	{ (Il2CppRGCTXDataType)2, 13926 },
	{ (Il2CppRGCTXDataType)3, 10163 },
	{ (Il2CppRGCTXDataType)2, 13927 },
	{ (Il2CppRGCTXDataType)3, 10164 },
	{ (Il2CppRGCTXDataType)2, 13928 },
	{ (Il2CppRGCTXDataType)3, 10165 },
	{ (Il2CppRGCTXDataType)3, 10166 },
	{ (Il2CppRGCTXDataType)2, 10734 },
	{ (Il2CppRGCTXDataType)3, 10167 },
	{ (Il2CppRGCTXDataType)2, 10736 },
	{ (Il2CppRGCTXDataType)2, 13929 },
	{ (Il2CppRGCTXDataType)3, 10168 },
	{ (Il2CppRGCTXDataType)2, 10739 },
	{ (Il2CppRGCTXDataType)2, 10741 },
	{ (Il2CppRGCTXDataType)2, 13930 },
	{ (Il2CppRGCTXDataType)3, 10169 },
	{ (Il2CppRGCTXDataType)2, 13931 },
	{ (Il2CppRGCTXDataType)2, 10744 },
	{ (Il2CppRGCTXDataType)3, 10170 },
	{ (Il2CppRGCTXDataType)3, 10171 },
	{ (Il2CppRGCTXDataType)2, 10748 },
	{ (Il2CppRGCTXDataType)3, 10172 },
	{ (Il2CppRGCTXDataType)3, 10173 },
	{ (Il2CppRGCTXDataType)2, 10757 },
	{ (Il2CppRGCTXDataType)2, 13932 },
	{ (Il2CppRGCTXDataType)3, 10174 },
	{ (Il2CppRGCTXDataType)3, 10175 },
	{ (Il2CppRGCTXDataType)2, 10759 },
	{ (Il2CppRGCTXDataType)2, 13844 },
	{ (Il2CppRGCTXDataType)3, 10176 },
	{ (Il2CppRGCTXDataType)3, 10177 },
	{ (Il2CppRGCTXDataType)3, 10178 },
	{ (Il2CppRGCTXDataType)2, 10766 },
	{ (Il2CppRGCTXDataType)2, 13933 },
	{ (Il2CppRGCTXDataType)3, 10179 },
	{ (Il2CppRGCTXDataType)3, 10180 },
	{ (Il2CppRGCTXDataType)3, 9806 },
	{ (Il2CppRGCTXDataType)3, 10181 },
	{ (Il2CppRGCTXDataType)3, 10182 },
	{ (Il2CppRGCTXDataType)2, 10775 },
	{ (Il2CppRGCTXDataType)2, 13934 },
	{ (Il2CppRGCTXDataType)3, 10183 },
	{ (Il2CppRGCTXDataType)3, 10184 },
	{ (Il2CppRGCTXDataType)3, 10185 },
	{ (Il2CppRGCTXDataType)3, 10186 },
	{ (Il2CppRGCTXDataType)3, 10187 },
	{ (Il2CppRGCTXDataType)3, 9812 },
	{ (Il2CppRGCTXDataType)3, 10188 },
	{ (Il2CppRGCTXDataType)3, 10189 },
	{ (Il2CppRGCTXDataType)3, 10190 },
	{ (Il2CppRGCTXDataType)2, 13935 },
	{ (Il2CppRGCTXDataType)3, 10191 },
	{ (Il2CppRGCTXDataType)3, 10192 },
	{ (Il2CppRGCTXDataType)2, 10792 },
	{ (Il2CppRGCTXDataType)3, 10193 },
	{ (Il2CppRGCTXDataType)2, 10792 },
	{ (Il2CppRGCTXDataType)3, 10194 },
	{ (Il2CppRGCTXDataType)2, 10809 },
	{ (Il2CppRGCTXDataType)3, 10195 },
	{ (Il2CppRGCTXDataType)3, 10196 },
	{ (Il2CppRGCTXDataType)3, 10197 },
	{ (Il2CppRGCTXDataType)2, 13936 },
	{ (Il2CppRGCTXDataType)3, 10198 },
	{ (Il2CppRGCTXDataType)3, 10199 },
	{ (Il2CppRGCTXDataType)3, 10200 },
	{ (Il2CppRGCTXDataType)2, 10789 },
	{ (Il2CppRGCTXDataType)3, 10201 },
	{ (Il2CppRGCTXDataType)3, 10202 },
	{ (Il2CppRGCTXDataType)2, 10794 },
	{ (Il2CppRGCTXDataType)3, 10203 },
	{ (Il2CppRGCTXDataType)1, 13937 },
	{ (Il2CppRGCTXDataType)2, 10793 },
	{ (Il2CppRGCTXDataType)3, 10204 },
	{ (Il2CppRGCTXDataType)1, 10793 },
	{ (Il2CppRGCTXDataType)1, 10789 },
	{ (Il2CppRGCTXDataType)2, 13936 },
	{ (Il2CppRGCTXDataType)2, 10793 },
	{ (Il2CppRGCTXDataType)2, 10791 },
	{ (Il2CppRGCTXDataType)2, 10795 },
	{ (Il2CppRGCTXDataType)3, 10205 },
	{ (Il2CppRGCTXDataType)3, 10206 },
	{ (Il2CppRGCTXDataType)3, 10207 },
	{ (Il2CppRGCTXDataType)2, 10790 },
	{ (Il2CppRGCTXDataType)3, 10208 },
	{ (Il2CppRGCTXDataType)2, 10805 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	70,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	13,
	s_rgctxIndices,
	91,
	s_rgctxValues,
	NULL,
};
