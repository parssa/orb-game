﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m01D558819D864B0B36B12AECB92B7A9FE76D63B3 ();
// 0x00000002 System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m49F04D0D45C4CBE5F904177CF3AB4A60249AD854 ();
// 0x00000003 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_mA0A6156A4DF33D8F0AD6FD106A7A38F33A0F2BBD ();
// 0x00000004 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m67CF795BA1CEA14B6B2AE3E71191E5E980EB03F1 ();
// 0x00000005 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_mC53461B29C19CDB0FF5E75A9D257B9CF0B1E0CCE ();
// 0x00000006 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m35C5092C9B62F6BB55D45FF5ECEA2C7F742F1EDB ();
// 0x00000007 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m27F40165A6AE141735CD9680F90F53599D2CFFC2 ();
// 0x00000008 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_mE68D37B29683A04E3232D4A023093BD65336413C ();
// 0x00000009 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_mD2CEADF3C7AD02BA60F7990F1A39BC67C6D8819B ();
// 0x0000000A System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_m2326D40EF66E0A5E2B34F9CF02A53C05CCAFDED0 ();
// 0x0000000B UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_mF64961ED5359C8E31E79CAA306019CB66DF50F3E ();
// 0x0000000C System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_m3D14494BB31EF5C716DE54CBF4ED30284AEE855F ();
// 0x0000000D System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m93B28B83AB3DC010C509C16A76BE34896C27B647 ();
// 0x0000000E System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_m56376929F8539F977418D4ACB3A468758515DA85 ();
// 0x0000000F System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_mF8F4688B5A32363F2EF7DE0A0FF90B7D65F11C8B ();
// 0x00000010 AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_m41669BF41810BA976B1A230E1FB3ADCDC1F4C758 ();
// 0x00000011 System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m3194AF6B83B35084063737EBA7D8C10C652241F8 ();
// 0x00000012 System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m8BFFB04A6377302BCD1E3106930231C756150211 ();
// 0x00000013 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_mACA46808CD8386CABE024E9F55A407F0C8A138F0 ();
// 0x00000014 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_m3B972003ED2D6D62DC433D6429AD58AEDCEE5957 ();
// 0x00000015 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_mA985E49A53EBD417CF06948C805588612C0395C1 ();
// 0x00000016 System.Void Joystick::Start()
extern void Joystick_Start_mA4B921AF2FEC7B830AC4F0D5E49AF8934ECAD201 ();
// 0x00000017 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m408D43BE6A49862DFD49B5198E0B61B85A162703 ();
// 0x00000018 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m5C8DBE5110822CAF8A7DEDDE6AC240D2C1BDD7C4 ();
// 0x00000019 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_mA2FF5AE57290471865DC02DC2ED3BDA3FDBA2506 ();
// 0x0000001A System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mDE5D2FB4C4FB309B92816E806756B01F61FF26D5 ();
// 0x0000001B System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mCEF2520CAC111659A65B73C418F5E6FF1CFF05C2 ();
// 0x0000001C System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m771F7519F51F02DAADA7DE0A562F82710FA721BC ();
// 0x0000001D UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mE4C429E76D0FA78FD1567EB1361AF68141706201 ();
// 0x0000001E System.Void Joystick::.ctor()
extern void Joystick__ctor_m0CEC3DFCF02C69B8020F600539EB02297E26D1CE ();
// 0x0000001F System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m475CED919EFA09D7DBC1815BE0B92B55E5E3DD92 ();
// 0x00000020 System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m41DF6070CB77AD666C343C6FCD04F2D013FF7171 ();
// 0x00000021 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_m28FAB53F804FC3AEAA7DB62E75DA0BE645C1B494 ();
// 0x00000022 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_mF71D95263661D939DBE2E5C17C73A599E60778B9 ();
// 0x00000023 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_mC38FD6A7ADB9CDAB0BF04235DA5110A37B10E771 ();
// 0x00000024 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_mB6316E3D97C617061C01DC49DD829C0C4E91BB27 ();
// 0x00000025 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_mD87E098A06147EABDFDB7F80162B114A799F9D4A ();
// 0x00000026 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_mC91231670E4D4850A5BFB905300F155AA2DE60FD ();
// 0x00000027 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_m0F34B432F4B1DF7D2A84264E9C5E8553A4E7F0DE ();
// 0x00000028 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_m2DB74D40D3D0D670E92442E24851FF5995CCE764 ();
// 0x00000029 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m31F698F0F60ECAAB34D55DF8B1FF2E533B818DEA ();
// 0x0000002A System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_mFE6173BF4B5A37A9E365D8322334FF90AA0DE960 ();
// 0x0000002B System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m7680F75A6FE65378FA1882A60F1D2D7C10E1585F ();
// 0x0000002C System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_m70CF65250847566A28B2A418C3EE55004987552F ();
// 0x0000002D System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_m222D977BBDA4E253D4EAF1E55925FAD333EA7CAE ();
// 0x0000002E System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m5BB1A8FE9E2EBC394C4AC8B4E495AB653F61E8E2 ();
// 0x0000002F System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m066F12E7818D1CD22977EAEA9FE4AEC0DD179FAC ();
// 0x00000030 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m42B201EDAB1B3A2F2ED747FA7A4773E2654DA061 ();
// 0x00000031 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_m83CA8AFC5C395DE4C684F2C478D1DE36CAD0BF19 ();
// 0x00000032 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m8A6171BB962558D6833C6CF80E3E0092DF768D84 ();
// 0x00000033 System.Void BulletHitBox::OnCollide(UnityEngine.Collider2D)
extern void BulletHitBox_OnCollide_m37AC2D33E13FBE128AF553981B78212A2A767B1F ();
// 0x00000034 System.Void BulletHitBox::.ctor()
extern void BulletHitBox__ctor_mBA7F92C7CCE6BC1F395DCF4ACDA898A32A48537F ();
// 0x00000035 System.Void CameraMotor::Start()
extern void CameraMotor_Start_m3258841392D3CF9B40D7F0DA3DEDE68A8C6077D8 ();
// 0x00000036 System.Void CameraMotor::FixedUpdate()
extern void CameraMotor_FixedUpdate_mEF7BCCE0A7FB9A4F7DE606E923D489155696C769 ();
// 0x00000037 System.Void CameraMotor::.ctor()
extern void CameraMotor__ctor_m843866BA48EA00F9981D28FF177DF3A759F0048C ();
// 0x00000038 System.Void CharacterMenu::OnArrowClick(System.Boolean)
extern void CharacterMenu_OnArrowClick_m3F87EC0A2AEFAC40F170046C8881105E753B3254 ();
// 0x00000039 System.Void CharacterMenu::OnSelectionChanged()
extern void CharacterMenu_OnSelectionChanged_mE081D76BA2296BA132D351313FEDD45C22A18027 ();
// 0x0000003A System.Void CharacterMenu::OnUpgradeClick()
extern void CharacterMenu_OnUpgradeClick_m0C70C4E976EE0920CED4E4CE712B55C4C17176D3 ();
// 0x0000003B System.Void CharacterMenu::UpdateMenu()
extern void CharacterMenu_UpdateMenu_m23781C4F3E9D79F398D2D6A6064D09EF3D1B8169 ();
// 0x0000003C System.Void CharacterMenu::.ctor()
extern void CharacterMenu__ctor_mD5D531D3D69C4061518FD592049226073CEDDF6B ();
// 0x0000003D System.Void Chest::OnCollect()
extern void Chest_OnCollect_mEF9768A2248BC50907C6DDBBA0D9C2342D20FF44 ();
// 0x0000003E System.Void Chest::.ctor()
extern void Chest__ctor_m069CAEED0F93514435BCE8C7480D717F310B4059 ();
// 0x0000003F System.Void Collectable::OnCollide(UnityEngine.Collider2D)
extern void Collectable_OnCollide_mC90C5F191A8245C2CE4BDD5C1619E9817422A2F6 ();
// 0x00000040 System.Void Collectable::OnCollect()
extern void Collectable_OnCollect_m5E4F460C4F6C1045146F1FE729B1B7D6F21A4CFA ();
// 0x00000041 System.Void Collectable::.ctor()
extern void Collectable__ctor_m2BB5693EB0E8D466151D66585FB889275D811358 ();
// 0x00000042 System.Void Collidable::Start()
extern void Collidable_Start_mD001CE3713D169F4134043736D17E71821DF2CDE ();
// 0x00000043 System.Void Collidable::Update()
extern void Collidable_Update_m30ABDF4BDD9390FE2AEDCDC83E425BD2AF8E2D6A ();
// 0x00000044 System.Void Collidable::OnCollide(UnityEngine.Collider2D)
extern void Collidable_OnCollide_m62434B1D5A60D1C09A0AAB035462B659E391088F ();
// 0x00000045 System.Void Collidable::.ctor()
extern void Collidable__ctor_mAA510EF82CB4064380817F8B03009F74472D7B90 ();
// 0x00000046 System.Void Dialogue::.ctor()
extern void Dialogue__ctor_m856B327F2A54A6A86CF90DEEF88870C9FC881241 ();
// 0x00000047 System.Void DialogueManager::Start()
extern void DialogueManager_Start_m7EDB14805755A865CDA18DCA5D17C6C7BA270713 ();
// 0x00000048 System.Void DialogueManager::StartDialogue(Dialogue)
extern void DialogueManager_StartDialogue_m99EE68BE5A3A049461A6A4190C0461E893BB7075 ();
// 0x00000049 System.Void DialogueManager::DisplayNextSentence()
extern void DialogueManager_DisplayNextSentence_m6FCDD28C6F86E1AAC070F46F3B5871B246BE13D2 ();
// 0x0000004A System.Collections.IEnumerator DialogueManager::TypeSentence(System.String)
extern void DialogueManager_TypeSentence_m8ECD19F2B45EEE3C47C54F56C8F40D3DBD08335B ();
// 0x0000004B System.Void DialogueManager::EndDialogue()
extern void DialogueManager_EndDialogue_mC5C6283A156EB84424ED7C629712565382DB446B ();
// 0x0000004C System.Void DialogueManager::.ctor()
extern void DialogueManager__ctor_m2C71B2A08B60CB2BFFB7B4C692971BFEA2325A8E ();
// 0x0000004D System.Void DialogueTrigger::TriggerDialogue()
extern void DialogueTrigger_TriggerDialogue_m92673EC66968C97ACFD097E76245ABD0549CC164 ();
// 0x0000004E System.Void DialogueTrigger::.ctor()
extern void DialogueTrigger__ctor_m39858F576ABCC8A1E3C3E2CBEC9888BF8558E278 ();
// 0x0000004F System.Void Enemy::Start()
extern void Enemy_Start_m0681B66D4522F045EB7A33A21467994960D1E435 ();
// 0x00000050 System.Void Enemy::FixedUpdate()
extern void Enemy_FixedUpdate_m5A50917D707E3D8645185CDF5FFFDCB7072CFD7B ();
// 0x00000051 System.Void Enemy::Death()
extern void Enemy_Death_m745F149CA720993031A7F220C978589AF22E1965 ();
// 0x00000052 System.Void Enemy::.ctor()
extern void Enemy__ctor_mCD4E016A02FE662E339AA011EBA74D77B09556C5 ();
// 0x00000053 System.Void EnemyHitBox::OnCollide(UnityEngine.Collider2D)
extern void EnemyHitBox_OnCollide_m57553D18BABC3C2769E9769B8EDC15BBAC60FA46 ();
// 0x00000054 System.Void EnemyHitBox::.ctor()
extern void EnemyHitBox__ctor_mEA6697D631EF9CA12777A55B7733DC22824B4AF1 ();
// 0x00000055 System.Void Fighter::RecieveDamage(Damage)
extern void Fighter_RecieveDamage_m41F787AB2921FB8721E5E90B9AB3B0749F4C58BD ();
// 0x00000056 System.Void Fighter::Death()
extern void Fighter_Death_mE7052447B327AA30876C7F1CA48FA2E0250BF1FA ();
// 0x00000057 System.Void Fighter::.ctor()
extern void Fighter__ctor_m8E31846BBBF94BC141B8A31346CF8BCCA523805C ();
// 0x00000058 System.Void FloatingText::Show()
extern void FloatingText_Show_mD5CF015E39C9745570571109EAF17E49990E4BFB ();
// 0x00000059 System.Void FloatingText::Hide()
extern void FloatingText_Hide_mB2F9251D9CD4533A7B840C773BA9F417999AAE4D ();
// 0x0000005A System.Void FloatingText::UpdateFloatingText()
extern void FloatingText_UpdateFloatingText_mC8B83CDF384FD6F087B10BE08664A0403200A95F ();
// 0x0000005B System.Void FloatingText::.ctor()
extern void FloatingText__ctor_mA778924C195D60B3E7728DB97D13DBDAF9FDB9CD ();
// 0x0000005C System.Void FloatingTextManager::Update()
extern void FloatingTextManager_Update_m1C5CEC7361D9EB6DAC6EA6C079740DE28F4DD7BE ();
// 0x0000005D System.Void FloatingTextManager::Show(System.String,System.Int32,UnityEngine.Color,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void FloatingTextManager_Show_m03CE85455D1E398D25AC8A6F42D1E712883F7880 ();
// 0x0000005E FloatingText FloatingTextManager::GetFloatingText()
extern void FloatingTextManager_GetFloatingText_m9E667DF571AE2875F6AD450658C5CE03320C77D4 ();
// 0x0000005F System.Void FloatingTextManager::.ctor()
extern void FloatingTextManager__ctor_mB293DA169F0708AC99BFDFC28E7908E91EF72DCE ();
// 0x00000060 System.Void GameManager::Awake()
extern void GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F ();
// 0x00000061 System.Void GameManager::ShowText(System.String,System.Int32,UnityEngine.Color,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void GameManager_ShowText_m2B0E2B8982D054A04DAA8B6C956EE44A1CACBE80 ();
// 0x00000062 System.Boolean GameManager::TryUpgradeWeapon()
extern void GameManager_TryUpgradeWeapon_m013A5C1331543B56925B508A2D9626D4D2927F5F ();
// 0x00000063 System.Int32 GameManager::GetCurrentLevel()
extern void GameManager_GetCurrentLevel_mBE83994762F182C2F0FBAEF2ECC2228E6F420D4C ();
// 0x00000064 System.Int32 GameManager::GetXpToLevel(System.Int32)
extern void GameManager_GetXpToLevel_m4392D7392F65251A58153197F306E9492246CABE ();
// 0x00000065 System.Void GameManager::GrantXp(System.Int32)
extern void GameManager_GrantXp_mC3AF2870F6117170F8C3DBF65C399A699CDC8CBC ();
// 0x00000066 System.Void GameManager::OnLevelUp()
extern void GameManager_OnLevelUp_m0ECC1CC02C4722A6A9B258AAC1CBD9F8FC373266 ();
// 0x00000067 System.Void GameManager::SaveState()
extern void GameManager_SaveState_mE78E380839601809063C152D7EE0607E03686350 ();
// 0x00000068 System.Void GameManager::LoadState(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void GameManager_LoadState_mE183F7B21F3FF350BAFA298C69D3B3A220F83E2F ();
// 0x00000069 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 ();
// 0x0000006A System.Void HubLayout::Update()
extern void HubLayout_Update_m42DE760B9D23D522AD4A6B5B3C70F11C856272FC ();
// 0x0000006B System.Void HubLayout::UpdateMenu()
extern void HubLayout_UpdateMenu_m0DABA656A8F57DFE021E92B78A0ACD6D9E3CF098 ();
// 0x0000006C System.Void HubLayout::.ctor()
extern void HubLayout__ctor_m2C7A9E9152BC4E1C9A2DEAA477481EBAD87AED6F ();
// 0x0000006D System.Void Laserbeam::Update()
extern void Laserbeam_Update_mC4D61FA18D0648440DE32D88D4F28A3387D2A6B1 ();
// 0x0000006E System.Void Laserbeam::.ctor()
extern void Laserbeam__ctor_m53E8AA91C179E363E1497D87ACAEFC031B1B1107 ();
// 0x0000006F System.Void Mover::Start()
extern void Mover_Start_m8CBDB6A9FE6CB8D3BD1DA26FAB0C913559AE165C ();
// 0x00000070 System.Void Mover::UpdateMotor(UnityEngine.Vector3)
extern void Mover_UpdateMotor_m3868156EF819EAC2EAB360A4C6984FC213180DE9 ();
// 0x00000071 System.Void Mover::.ctor()
extern void Mover__ctor_mFAFEBAF042392E9011A10922FFFFCDBEED3EAA59 ();
// 0x00000072 System.Void Player::Awake()
extern void Player_Awake_mD94498D36D7F1E5FDB727209B4B6043A6F228E5C ();
// 0x00000073 System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_m910EBDB7E33512D30C25DC396E0B40702004145A ();
// 0x00000074 System.Void Player::OnLevelUp()
extern void Player_OnLevelUp_m46A63E92BDFE3F32C528953B10432E9C0503B18A ();
// 0x00000075 System.Int32 Player::GetHealth()
extern void Player_GetHealth_m8E3BBB3DB547C21E87BB07866E007AAD7A6B02B2 ();
// 0x00000076 System.Int32 Player::GetMaxHealth()
extern void Player_GetMaxHealth_mCA7FA409329D41BB3CFCA5583F6B67B6EC0FC239 ();
// 0x00000077 System.Void Player::.ctor()
extern void Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2 ();
// 0x00000078 System.Void Portal::OnCollide(UnityEngine.Collider2D)
extern void Portal_OnCollide_mCB18CAE70CD4E860220406D1A11BB5152F5338E6 ();
// 0x00000079 System.Void Portal::.ctor()
extern void Portal__ctor_mA35DDEA40AB52BC7CC02BA6BF7E530CA67AFD453 ();
// 0x0000007A System.Void Weapon::Awake()
extern void Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98 ();
// 0x0000007B System.Void Weapon::Start()
extern void Weapon_Start_m4FAB905F9926EEADEEF49D8B77218D1A14F459AC ();
// 0x0000007C System.Void Weapon::Update()
extern void Weapon_Update_mD65E9FD544FEE0974CED54FA39BB05A89FB6D99E ();
// 0x0000007D System.Void Weapon::OnCollide(UnityEngine.Collider2D)
extern void Weapon_OnCollide_m0E6F121CD57AAD84AEF231EF7ED87E916FE02C2D ();
// 0x0000007E System.Void Weapon::Shoot()
extern void Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3 ();
// 0x0000007F System.Void Weapon::Effect()
extern void Weapon_Effect_m611A8927A7724FAA7AE46E161E869208C34797C7 ();
// 0x00000080 System.Void Weapon::UpgradeWeapon()
extern void Weapon_UpgradeWeapon_mFDE24932C604C21A11ED826692A0FD109C795DBC ();
// 0x00000081 System.Void Weapon::.ctor()
extern void Weapon__ctor_mF7C215ECC1D7032E6DE76DE606A9159F840B62FB ();
// 0x00000082 System.Void DialogueManager_<TypeSentence>d__7::.ctor(System.Int32)
extern void U3CTypeSentenceU3Ed__7__ctor_m8B3725202840348DE1AB959DF828E11B77507FFB ();
// 0x00000083 System.Void DialogueManager_<TypeSentence>d__7::System.IDisposable.Dispose()
extern void U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_m77EB5CFA9A7F329AF556DD4A5559E68A8C439EB7 ();
// 0x00000084 System.Boolean DialogueManager_<TypeSentence>d__7::MoveNext()
extern void U3CTypeSentenceU3Ed__7_MoveNext_m4CBF97AA1C02704BD6E062E5808A34E6D9C40B6D ();
// 0x00000085 System.Object DialogueManager_<TypeSentence>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2094258EB47774391F9E965DB3D31A29C98604BE ();
// 0x00000086 System.Void DialogueManager_<TypeSentence>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mF31ABF39124F182133DB5C122629C3C62472EB4E ();
// 0x00000087 System.Object DialogueManager_<TypeSentence>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_m5B50A4EE795505582D5DCB3CA2E4217854BAA6D4 ();
// 0x00000088 System.Void FloatingTextManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m14C14D825BBE2A86B9D9ECFD776AD839008D27EC ();
// 0x00000089 System.Void FloatingTextManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m3C87FE8C148F0FC85BBF0FFF3428BB6B2396FD31 ();
// 0x0000008A System.Boolean FloatingTextManager_<>c::<GetFloatingText>b__5_0(FloatingText)
extern void U3CU3Ec_U3CGetFloatingTextU3Eb__5_0_mED36F2C006EABC38B39B24F4B52924C488E9B2F3 ();
static Il2CppMethodPointer s_methodPointers[138] = 
{
	JoystickPlayerExample_FixedUpdate_m01D558819D864B0B36B12AECB92B7A9FE76D63B3,
	JoystickPlayerExample__ctor_m49F04D0D45C4CBE5F904177CF3AB4A60249AD854,
	JoystickSetterExample_ModeChanged_mA0A6156A4DF33D8F0AD6FD106A7A38F33A0F2BBD,
	JoystickSetterExample_AxisChanged_m67CF795BA1CEA14B6B2AE3E71191E5E980EB03F1,
	JoystickSetterExample_SnapX_mC53461B29C19CDB0FF5E75A9D257B9CF0B1E0CCE,
	JoystickSetterExample_SnapY_m35C5092C9B62F6BB55D45FF5ECEA2C7F742F1EDB,
	JoystickSetterExample_Update_m27F40165A6AE141735CD9680F90F53599D2CFFC2,
	JoystickSetterExample__ctor_mE68D37B29683A04E3232D4A023093BD65336413C,
	Joystick_get_Horizontal_mD2CEADF3C7AD02BA60F7990F1A39BC67C6D8819B,
	Joystick_get_Vertical_m2326D40EF66E0A5E2B34F9CF02A53C05CCAFDED0,
	Joystick_get_Direction_mF64961ED5359C8E31E79CAA306019CB66DF50F3E,
	Joystick_get_HandleRange_m3D14494BB31EF5C716DE54CBF4ED30284AEE855F,
	Joystick_set_HandleRange_m93B28B83AB3DC010C509C16A76BE34896C27B647,
	Joystick_get_DeadZone_m56376929F8539F977418D4ACB3A468758515DA85,
	Joystick_set_DeadZone_mF8F4688B5A32363F2EF7DE0A0FF90B7D65F11C8B,
	Joystick_get_AxisOptions_m41669BF41810BA976B1A230E1FB3ADCDC1F4C758,
	Joystick_set_AxisOptions_m3194AF6B83B35084063737EBA7D8C10C652241F8,
	Joystick_get_SnapX_m8BFFB04A6377302BCD1E3106930231C756150211,
	Joystick_set_SnapX_mACA46808CD8386CABE024E9F55A407F0C8A138F0,
	Joystick_get_SnapY_m3B972003ED2D6D62DC433D6429AD58AEDCEE5957,
	Joystick_set_SnapY_mA985E49A53EBD417CF06948C805588612C0395C1,
	Joystick_Start_mA4B921AF2FEC7B830AC4F0D5E49AF8934ECAD201,
	Joystick_OnPointerDown_m408D43BE6A49862DFD49B5198E0B61B85A162703,
	Joystick_OnDrag_m5C8DBE5110822CAF8A7DEDDE6AC240D2C1BDD7C4,
	Joystick_HandleInput_mA2FF5AE57290471865DC02DC2ED3BDA3FDBA2506,
	Joystick_FormatInput_mDE5D2FB4C4FB309B92816E806756B01F61FF26D5,
	Joystick_SnapFloat_mCEF2520CAC111659A65B73C418F5E6FF1CFF05C2,
	Joystick_OnPointerUp_m771F7519F51F02DAADA7DE0A562F82710FA721BC,
	Joystick_ScreenPointToAnchoredPosition_mE4C429E76D0FA78FD1567EB1361AF68141706201,
	Joystick__ctor_m0CEC3DFCF02C69B8020F600539EB02297E26D1CE,
	DynamicJoystick_get_MoveThreshold_m475CED919EFA09D7DBC1815BE0B92B55E5E3DD92,
	DynamicJoystick_set_MoveThreshold_m41DF6070CB77AD666C343C6FCD04F2D013FF7171,
	DynamicJoystick_Start_m28FAB53F804FC3AEAA7DB62E75DA0BE645C1B494,
	DynamicJoystick_OnPointerDown_mF71D95263661D939DBE2E5C17C73A599E60778B9,
	DynamicJoystick_OnPointerUp_mC38FD6A7ADB9CDAB0BF04235DA5110A37B10E771,
	DynamicJoystick_HandleInput_mB6316E3D97C617061C01DC49DD829C0C4E91BB27,
	DynamicJoystick__ctor_mD87E098A06147EABDFDB7F80162B114A799F9D4A,
	FixedJoystick__ctor_mC91231670E4D4850A5BFB905300F155AA2DE60FD,
	FloatingJoystick_Start_m0F34B432F4B1DF7D2A84264E9C5E8553A4E7F0DE,
	FloatingJoystick_OnPointerDown_m2DB74D40D3D0D670E92442E24851FF5995CCE764,
	FloatingJoystick_OnPointerUp_m31F698F0F60ECAAB34D55DF8B1FF2E533B818DEA,
	FloatingJoystick__ctor_mFE6173BF4B5A37A9E365D8322334FF90AA0DE960,
	VariableJoystick_get_MoveThreshold_m7680F75A6FE65378FA1882A60F1D2D7C10E1585F,
	VariableJoystick_set_MoveThreshold_m70CF65250847566A28B2A418C3EE55004987552F,
	VariableJoystick_SetMode_m222D977BBDA4E253D4EAF1E55925FAD333EA7CAE,
	VariableJoystick_Start_m5BB1A8FE9E2EBC394C4AC8B4E495AB653F61E8E2,
	VariableJoystick_OnPointerDown_m066F12E7818D1CD22977EAEA9FE4AEC0DD179FAC,
	VariableJoystick_OnPointerUp_m42B201EDAB1B3A2F2ED747FA7A4773E2654DA061,
	VariableJoystick_HandleInput_m83CA8AFC5C395DE4C684F2C478D1DE36CAD0BF19,
	VariableJoystick__ctor_m8A6171BB962558D6833C6CF80E3E0092DF768D84,
	BulletHitBox_OnCollide_m37AC2D33E13FBE128AF553981B78212A2A767B1F,
	BulletHitBox__ctor_mBA7F92C7CCE6BC1F395DCF4ACDA898A32A48537F,
	CameraMotor_Start_m3258841392D3CF9B40D7F0DA3DEDE68A8C6077D8,
	CameraMotor_FixedUpdate_mEF7BCCE0A7FB9A4F7DE606E923D489155696C769,
	CameraMotor__ctor_m843866BA48EA00F9981D28FF177DF3A759F0048C,
	CharacterMenu_OnArrowClick_m3F87EC0A2AEFAC40F170046C8881105E753B3254,
	CharacterMenu_OnSelectionChanged_mE081D76BA2296BA132D351313FEDD45C22A18027,
	CharacterMenu_OnUpgradeClick_m0C70C4E976EE0920CED4E4CE712B55C4C17176D3,
	CharacterMenu_UpdateMenu_m23781C4F3E9D79F398D2D6A6064D09EF3D1B8169,
	CharacterMenu__ctor_mD5D531D3D69C4061518FD592049226073CEDDF6B,
	Chest_OnCollect_mEF9768A2248BC50907C6DDBBA0D9C2342D20FF44,
	Chest__ctor_m069CAEED0F93514435BCE8C7480D717F310B4059,
	Collectable_OnCollide_mC90C5F191A8245C2CE4BDD5C1619E9817422A2F6,
	Collectable_OnCollect_m5E4F460C4F6C1045146F1FE729B1B7D6F21A4CFA,
	Collectable__ctor_m2BB5693EB0E8D466151D66585FB889275D811358,
	Collidable_Start_mD001CE3713D169F4134043736D17E71821DF2CDE,
	Collidable_Update_m30ABDF4BDD9390FE2AEDCDC83E425BD2AF8E2D6A,
	Collidable_OnCollide_m62434B1D5A60D1C09A0AAB035462B659E391088F,
	Collidable__ctor_mAA510EF82CB4064380817F8B03009F74472D7B90,
	Dialogue__ctor_m856B327F2A54A6A86CF90DEEF88870C9FC881241,
	DialogueManager_Start_m7EDB14805755A865CDA18DCA5D17C6C7BA270713,
	DialogueManager_StartDialogue_m99EE68BE5A3A049461A6A4190C0461E893BB7075,
	DialogueManager_DisplayNextSentence_m6FCDD28C6F86E1AAC070F46F3B5871B246BE13D2,
	DialogueManager_TypeSentence_m8ECD19F2B45EEE3C47C54F56C8F40D3DBD08335B,
	DialogueManager_EndDialogue_mC5C6283A156EB84424ED7C629712565382DB446B,
	DialogueManager__ctor_m2C71B2A08B60CB2BFFB7B4C692971BFEA2325A8E,
	DialogueTrigger_TriggerDialogue_m92673EC66968C97ACFD097E76245ABD0549CC164,
	DialogueTrigger__ctor_m39858F576ABCC8A1E3C3E2CBEC9888BF8558E278,
	Enemy_Start_m0681B66D4522F045EB7A33A21467994960D1E435,
	Enemy_FixedUpdate_m5A50917D707E3D8645185CDF5FFFDCB7072CFD7B,
	Enemy_Death_m745F149CA720993031A7F220C978589AF22E1965,
	Enemy__ctor_mCD4E016A02FE662E339AA011EBA74D77B09556C5,
	EnemyHitBox_OnCollide_m57553D18BABC3C2769E9769B8EDC15BBAC60FA46,
	EnemyHitBox__ctor_mEA6697D631EF9CA12777A55B7733DC22824B4AF1,
	Fighter_RecieveDamage_m41F787AB2921FB8721E5E90B9AB3B0749F4C58BD,
	Fighter_Death_mE7052447B327AA30876C7F1CA48FA2E0250BF1FA,
	Fighter__ctor_m8E31846BBBF94BC141B8A31346CF8BCCA523805C,
	FloatingText_Show_mD5CF015E39C9745570571109EAF17E49990E4BFB,
	FloatingText_Hide_mB2F9251D9CD4533A7B840C773BA9F417999AAE4D,
	FloatingText_UpdateFloatingText_mC8B83CDF384FD6F087B10BE08664A0403200A95F,
	FloatingText__ctor_mA778924C195D60B3E7728DB97D13DBDAF9FDB9CD,
	FloatingTextManager_Update_m1C5CEC7361D9EB6DAC6EA6C079740DE28F4DD7BE,
	FloatingTextManager_Show_m03CE85455D1E398D25AC8A6F42D1E712883F7880,
	FloatingTextManager_GetFloatingText_m9E667DF571AE2875F6AD450658C5CE03320C77D4,
	FloatingTextManager__ctor_mB293DA169F0708AC99BFDFC28E7908E91EF72DCE,
	GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F,
	GameManager_ShowText_m2B0E2B8982D054A04DAA8B6C956EE44A1CACBE80,
	GameManager_TryUpgradeWeapon_m013A5C1331543B56925B508A2D9626D4D2927F5F,
	GameManager_GetCurrentLevel_mBE83994762F182C2F0FBAEF2ECC2228E6F420D4C,
	GameManager_GetXpToLevel_m4392D7392F65251A58153197F306E9492246CABE,
	GameManager_GrantXp_mC3AF2870F6117170F8C3DBF65C399A699CDC8CBC,
	GameManager_OnLevelUp_m0ECC1CC02C4722A6A9B258AAC1CBD9F8FC373266,
	GameManager_SaveState_mE78E380839601809063C152D7EE0607E03686350,
	GameManager_LoadState_mE183F7B21F3FF350BAFA298C69D3B3A220F83E2F,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	HubLayout_Update_m42DE760B9D23D522AD4A6B5B3C70F11C856272FC,
	HubLayout_UpdateMenu_m0DABA656A8F57DFE021E92B78A0ACD6D9E3CF098,
	HubLayout__ctor_m2C7A9E9152BC4E1C9A2DEAA477481EBAD87AED6F,
	Laserbeam_Update_mC4D61FA18D0648440DE32D88D4F28A3387D2A6B1,
	Laserbeam__ctor_m53E8AA91C179E363E1497D87ACAEFC031B1B1107,
	Mover_Start_m8CBDB6A9FE6CB8D3BD1DA26FAB0C913559AE165C,
	Mover_UpdateMotor_m3868156EF819EAC2EAB360A4C6984FC213180DE9,
	Mover__ctor_mFAFEBAF042392E9011A10922FFFFCDBEED3EAA59,
	Player_Awake_mD94498D36D7F1E5FDB727209B4B6043A6F228E5C,
	Player_FixedUpdate_m910EBDB7E33512D30C25DC396E0B40702004145A,
	Player_OnLevelUp_m46A63E92BDFE3F32C528953B10432E9C0503B18A,
	Player_GetHealth_m8E3BBB3DB547C21E87BB07866E007AAD7A6B02B2,
	Player_GetMaxHealth_mCA7FA409329D41BB3CFCA5583F6B67B6EC0FC239,
	Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2,
	Portal_OnCollide_mCB18CAE70CD4E860220406D1A11BB5152F5338E6,
	Portal__ctor_mA35DDEA40AB52BC7CC02BA6BF7E530CA67AFD453,
	Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98,
	Weapon_Start_m4FAB905F9926EEADEEF49D8B77218D1A14F459AC,
	Weapon_Update_mD65E9FD544FEE0974CED54FA39BB05A89FB6D99E,
	Weapon_OnCollide_m0E6F121CD57AAD84AEF231EF7ED87E916FE02C2D,
	Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3,
	Weapon_Effect_m611A8927A7724FAA7AE46E161E869208C34797C7,
	Weapon_UpgradeWeapon_mFDE24932C604C21A11ED826692A0FD109C795DBC,
	Weapon__ctor_mF7C215ECC1D7032E6DE76DE606A9159F840B62FB,
	U3CTypeSentenceU3Ed__7__ctor_m8B3725202840348DE1AB959DF828E11B77507FFB,
	U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_m77EB5CFA9A7F329AF556DD4A5559E68A8C439EB7,
	U3CTypeSentenceU3Ed__7_MoveNext_m4CBF97AA1C02704BD6E062E5808A34E6D9C40B6D,
	U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2094258EB47774391F9E965DB3D31A29C98604BE,
	U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mF31ABF39124F182133DB5C122629C3C62472EB4E,
	U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_m5B50A4EE795505582D5DCB3CA2E4217854BAA6D4,
	U3CU3Ec__cctor_m14C14D825BBE2A86B9D9ECFD776AD839008D27EC,
	U3CU3Ec__ctor_m3C87FE8C148F0FC85BBF0FFF3428BB6B2396FD31,
	U3CU3Ec_U3CGetFloatingTextU3Eb__5_0_mED36F2C006EABC38B39B24F4B52924C488E9B2F3,
};
static const int32_t s_InvokerIndices[138] = 
{
	13,
	13,
	9,
	9,
	44,
	44,
	13,
	13,
	656,
	656,
	1073,
	656,
	275,
	656,
	275,
	18,
	9,
	17,
	44,
	17,
	44,
	13,
	4,
	4,
	1668,
	13,
	1461,
	4,
	1440,
	13,
	656,
	275,
	13,
	4,
	4,
	1668,
	13,
	13,
	13,
	4,
	4,
	13,
	656,
	275,
	9,
	13,
	4,
	4,
	1668,
	13,
	4,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	4,
	13,
	6,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	1669,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	1670,
	14,
	13,
	13,
	1670,
	17,
	18,
	66,
	9,
	13,
	13,
	1615,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	1065,
	13,
	13,
	13,
	13,
	18,
	18,
	13,
	4,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
	13,
	32,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	138,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
